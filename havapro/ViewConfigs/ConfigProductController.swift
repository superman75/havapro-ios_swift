//
//  ConfigProductController.swift
//  havapro
//
//  Created by abdelghani on 30/05/2017.
//  Copyright © 2017 Exo. All rights reserved.
//


import UIKit
import KMPlaceholderTextView
import Alamofire
import DropDown
class ConfigProductController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    var snaps = Dictionary<String, AnyObject>()
    var productsByCategory = Dictionary<String, Array<AnyObject>>()
    var products = Dictionary<String, Dictionary<String, Any>>()
    var componentsProduct = [String : Array<Any>]()
    var categoriesProduct = Array<Dictionary<String, Any>>()
    var eCategory:Dictionary<String, Any> = Dictionary<String, Any>()
    var eProduct:Dictionary<String, Any> = Dictionary<String, Any>()
    var eComponent:Dictionary<String, Any> = Dictionary<String, Any>()

    //---|   Collection View
    @IBOutlet var productList: UICollectionView!
    @IBOutlet var categoryProduct: UIView!
        //---|   Categories
    @IBOutlet var categoriesView: UIView!
    @IBOutlet var categoriesCView: UICollectionView!
    @IBOutlet var viewProductSlide: UIView!
    @IBOutlet var productSlider: UICollectionView!

    //---|   UIButton
    @IBOutlet var btnRightProductSlider: UIButton!
    @IBOutlet var btnLeftProductSlider: UIButton!

    //---|   Vars
    var cateoryProduct:String = ""
    var indexProductSlider : Int = 0

    // Products Area
    @IBOutlet weak var productCView: UIView!
    @IBOutlet weak var vpActions: UIView!
    @IBOutlet weak var vpDropdox: UIView!
    @IBOutlet weak var bpDropbox: btnDropdown!
    @IBOutlet weak var vECategory: UIView!
    @IBOutlet weak var vEProduct: UIView!
    @IBOutlet weak var vEComponent: UIView!
    let pActions = ["Catégorie", "Produit", "Composant"]
    var eDropDown:DropDown?
    //---|   Editing Outlite
    @IBOutlet weak var eBack: UIView!
    @IBOutlet weak var eSave: UIButton!
    @IBOutlet weak var hTitle: UILabel!
        //---|   Category Fields
    @IBOutlet weak var cFile: UIImageView!
    @IBOutlet weak var cName: textFieldBorderBottom!
    @IBOutlet weak var pNote: KMPlaceholderTextView!
    @IBOutlet weak var pVCategory: UIView!
    @IBOutlet weak var pBCategory: btnDropdown!
    @IBOutlet weak var cOnline: UISwitch!
    @IBOutlet weak var cNote: KMPlaceholderTextView!
        //---|   Product Fields
    var pCDropDown:DropDown?
    var pTDropDown:DropDown?
    @IBOutlet weak var pFile: UIImageView!
    @IBOutlet weak var pName: textFieldBorderBottom!
    @IBOutlet weak var pOnline: UISwitch!
    @IBOutlet weak var pPrice: textFieldBorderBottom!
    @IBOutlet weak var pVTva: UIView!
    @IBOutlet weak var pTva: btnDropdown!
    @IBOutlet weak var pTotal: textFieldBorderBottom!
        //---|   Component Fields
    var nCDropDown:DropDown?
    var nPDropDown:DropDown?
    var nTDropDown:DropDown?
    @IBOutlet weak var nFile: UIImageView!
    @IBOutlet weak var nName: textFieldBorderBottom!
    @IBOutlet weak var nPrice: textFieldBorderBottom!
    @IBOutlet weak var nNote: KMPlaceholderTextView!
    @IBOutlet weak var nTotal: textFieldBorderBottom!
    @IBOutlet weak var nQty: textFieldBorderBottom!
    @IBOutlet weak var nBTva: btnDropdown!
    @IBOutlet weak var nVTva: UIView!
    @IBOutlet weak var nOnline: UISwitch!
    @IBOutlet weak var nBProduct: btnDropdown!
    @IBOutlet weak var nVProduct: UIView!
    @IBOutlet weak var nVCategory: UIView!
    @IBOutlet weak var nBCategory: btnDropdown!

    var views:Dictionary<String, CommunicationItemController> = Dictionary<String, CommunicationItemController>()
    //---|
    var configCtrl:ViewConfigController?
    //---|   LOAD
    override func viewDidLoad() {
        self.configCtrl = (parent as? ViewConfigController)!
        //---| INTERFACE.UPDATED
        NotificationCenter.default.addObserver(forName: BROADCAST.INTERFACE.UPDATED, object: nil, queue: nil, using: self.interface_updated)
        //---|   Init Dropdown
        self.eDropDown = DropDown()
        self.pCDropDown = DropDown()
        self.pTDropDown = DropDown()
        self.nCDropDown = DropDown()
        self.nPDropDown = DropDown()
        self.nTDropDown = DropDown()
        if String(app.uid).empty() {
            NotificationCenter.default.addObserver(forName: BROADCAST.CONNECTED, object: nil, queue: nil, using: self.connected)
        }else{
            self.connected()
        }
        // Product List
        productList.dataSource = self
        productList.delegate = self

        // Product Slider
        productSlider.dataSource = self
        productSlider.delegate = self

        // Categories DataSource
        categoriesCView.dataSource = self
        categoriesCView.delegate = self
        // self.editView(0)

        // Init Dropdown Avatar Onlin e Payment
        self.dropdownInit(self.eDropDown!, view: self.vpDropdox, datasource: self.pActions as NSArray, btnShowing: self.bpDropbox, self.addNew, false)
        //---|   TVAs
        self.dropdownInit(self.pTDropDown!, view: self.pVTva, datasource: app.tvas as NSArray, btnShowing: self.pTva, self.tvaProductChosen)
        self.dropdownInit(self.nTDropDown!, view: self.nVTva, datasource: app.tvas as NSArray, btnShowing: self.nBTva, self.tvaComponentChosen)
        //self.connected()
    }
    func connected(notification: Notification?=nil){
        // Get Products Catgoery
        DB.PCATEGORIES.observeSingleEvent(of: .value, with: { (snap) in
            if !snap.exists() { return }
            self.snaps["categories"] = snap.value as AnyObject?
            self.products_set()
        })
        // Products
        DB.PRODUCTS.observeSingleEvent(of: .value, with: { (snap) in
            if !snap.exists() { return }
            self.snaps["products"] = snap.value as AnyObject?
            self.products_set()
        })
        // Components
        self.productComponents("components/\(app.uid)", scopeData: &self.componentsProduct) { (response, snap) in
            self.componentsProduct=response
        }
    }
    //---|   products
    func products_set(){
        self.categoriesProduct = Array<Dictionary<String, Any>>()
        self.products = Dictionary<String, Dictionary<String, Any>>()
        self.productsByCategory = Dictionary<String, Array<AnyObject>>()
        if self.snaps["categories"] != nil, (self.snaps["categories"]?.count)!>0, self.snaps["products"] != nil, (self.snaps["products"]?.count)!>0  {
            for(category_id, fProducts) in (self.snaps["products"] as? Dictionary<String, Dictionary<String, AnyObject>>)!{
                if self.snaps["categories"]![category_id] != nil, let category =  self.snaps["categories"]![category_id] as? Dictionary<String, AnyObject>{ //
                    let index = self.categoriesProduct.append(category)
                    //---| s  Init Product Category
                    if self.productsByCategory[category_id] == nil { self.productsByCategory[category_id] = Array<AnyObject>() }
                    for(product_id, product) in fProducts{
                        if let productDic = product as? Dictionary<String, AnyObject> {
                            let product_id_str = String(product_id)!
                            self.products[product_id_str] = (product as? Dictionary<String, AnyObject>)!
                            self.products[product_id_str]!["id"] = product_id_str// Exception
                            self.productsByCategory[String(category_id)]!.append(self.products[product_id_str] as AnyObject)
                        }

                    }
                }
            }
            for(category_id, category) in (self.snaps["categories"] as? Dictionary<String, Any>)!{
                if self.productsByCategory[category_id] == nil, let nCategory = category as? Dictionary<String, Any> {
                    self.categoriesProduct.append(nCategory)
                    self.productsByCategory[category_id] = Array<AnyObject>()
                }
            }

        }
        self.productList.reloadData()
        self.refreshSlider()
        self.categoriesCView.reloadData()
        //---| Refresh Categories Dropdown
        self.categoryDropdownInit()

    }

    // Change orintation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        // Refresh Slider
        self.centerSliderProduct(false)
    }
    func interface_updated(notification: Notification){
        if let side = notification.object as? NSDictionary {
            refreshSlider()
            return
        }
        self.centerSliderProduct()
    }
    //-----------------------------------------------#
        //--|  Global
    //-----------------------------------------------#
    // Get Product From Web And Filter data
    func productComponents(_ path:String, scopeData:inout [String : Array<Any>], completion: @escaping ([String : Array<Any>], NSDictionary) -> Void) {
        var scopeData = scopeData
        DB.COMPONENTS.observeSingleEvent(of: .value, with: { (snapshot) in
            if !snapshot.exists() { return }
            let nsdictionary:NSDictionary = (snapshot.valueInExportFormat() as? NSDictionary)!
            for (id, obj) in nsdictionary{
                if obj != nil, (obj as? Dictionary<String, AnyObject>)!.count>0{
                    let id_str = String(describing: id)
                    scopeData[id_str] = Array<Dictionary<String, AnyObject>>()
                    for (sIndex, section) in (obj as? Dictionary<String, AnyObject>)!{
                        var section_clone = (section as? Dictionary<String, AnyObject>)!
                        section_clone["id"] = ((section_clone["id"] != nil) ? String(section_clone["id"]!) : sIndex) as AnyObject
                        scopeData[id_str]!.append(section_clone)
                    }
                }
            }
            // Completed
            completion(scopeData, nsdictionary)
            // Refresh Data
            self.productList.reloadData()
            self.refreshSlider()
        })
    }
    //
    func productGetId( _ index : Int)-> String{
        //---| Get ID
        return String(self.g(self.productsByCategory, "\(self.cateoryProduct).\(index-1).id", "" as AnyObject))
    }
    //-----------------------------------------------#
        //--|  Changing
    //-----------------------------------------------#
    // Refresh Slider
    func refreshSlider(){
        self.productSlider.reloadData()
        btnRightProductSlider.isHidden = true
        btnLeftProductSlider.isHidden = true
        viewProductSlide.isHidden = true
        if self.categoriesProduct.count>0{
            centerSliderProduct()
            ///Show Product
            if self.categoriesProduct.count>1 {
                btnRightProductSlider.isHidden = false
                btnLeftProductSlider.isHidden = false
            }
            viewProductSlide.isHidden = false
        }
    }
    fileprivate func centerSliderProduct(_ reload:Bool=true){
        DispatchQueue.main.async{
            if let flowLayout = self.productSlider.collectionViewLayout as? UICollectionViewFlowLayout {
                flowLayout.itemSize = CGSize(width: self.productSlider.bounds.size.width, height: self.productSlider.bounds.size.height)
                //--| Assing Values
                self.productSlider.collectionViewLayout = flowLayout
                if reload { self.categoryChanging() }

            }
        }
    }
    // Change Category
    func categoryChanging( _ position:UICollectionViewScrollPosition = .right){
        self.productSlider.reloadData()
        self.productSlider.selectItem(at: IndexPath(row: self.indexProductSlider, section: 0), animated: true, scrollPosition: position)
        let category = self.categoriesProduct[indexProductSlider] as! NSDictionary
        if let iscategory = category["id"] as? String { self.cateoryProduct = String(iscategory) }
        self.productList.reloadData()
        self.categoriesCView.reloadData()
    }
    // Show Component
    func componentShow(_ sender: AnyObject) {
        if sender.tag > -1 { componentShowByIndex(sender.tag as Int) }
    }
    func componentShowByIndex(_ index: Int, _ toggleModel:Bool=true) {
        if let productListCell = self.productList!.cellForItem(at: IndexPath(row: index, section: 0)) as? cellProductList{
            productListCell.components.reloadData()
            if toggleModel { modalToggle(productListCell.modal) }
        }
    }
    //---|   Product Selected !Online
    func productSelected(_ index:Int, _ refreshProductList:Bool = true){
        self.eProduct = self.productsByCategory[self.cateoryProduct]![index] as! [String:AnyObject]
        self.editView(1)
    }
    //---|   Product Selected !Online
    func categorySelected(_ index:Int, _ refreshProductList:Bool = true){
        self.indexProductSlider = index
        self.categoryChanging()
        self.categoryToggle()
    }
    //---|   Toggle Categories
    func categoryToggle(_ openCategories:Bool?=nil){
        UIView.animate(withDuration: 1.0, animations: {
            let cvh = (openCategories != nil ? openCategories : self.categoriesView.isHidden)
            self.categoriesView.isHidden = !cvh!
            self.categoryProduct.isHidden = cvh!
            self.view.layoutIfNeeded()
        })
    }
    //---|   Product ONLINE
    func productOnline(_ sender: AnyObject) {
        if sender.tag > -1 {
            let tag:Int = sender.tag as Int
            let productListCell = self.productList.cellForItem(at: IndexPath(row: tag, section: 0)) as! cellProductList
            var product = (self.productsByCategory[self.cateoryProduct]?[tag] as? Dictionary<String, AnyObject>)!
            let online = !String(product["online"]!).bool
            product["online"] = online as AnyObject
            let id = String(product["id"])
            DB.PRODUCTS.child("\(self.cateoryProduct)/\(id)/online").setValue(online)
            productListCell.online.setImage(UIImage(named: [false : "offline.png", true : "online.png"][online]!), for:.normal)
            //---| Change Locale var
            self.products[id] =  product
            self.productsByCategory[self.cateoryProduct]![tag] =  product as AnyObject
        }
    }

    //---|   Product ONLINE
    func categoryOnline(_ sender: AnyObject, _ collectionView: UICollectionView) {
        if sender.tag > -1 {
            let tag:Int = sender.tag as Int
            let category = self.categoriesProduct[tag]
            let online = !String(category["online"]!).bool
            let id = String(category["id"])
            self.categoriesProduct[tag]["online"] = online
            DB.PCATEGORIES.child("\(self.cateoryProduct)/online").setValue(online)
            if collectionView==self.categoriesCView{
                let cellCategoryProduct = collectionView.cellForItem(at: IndexPath(row: tag, section: 0)) as! cellCategories
                cellCategoryProduct.online.setImage(UIImage(named: [false : "offline.png", true : "online.png"][online]!), for:.normal)
            }else{
                let cellCategoryProduct = collectionView.cellForItem(at: IndexPath(row: tag, section: 0)) as! cellCategoryProduct
                cellCategoryProduct.online.setImage(UIImage(named: [false : "offline.png", true : "online.png"][online]!), for:.normal)
            }
        }
    }
    func categoryOnlineSlider(_ sender: AnyObject) {
        self.categoryOnline(sender, self.productSlider)
        self.categoriesCView.reloadData()
    }
    func categoryOnlineList(_ sender: AnyObject) {
        self.categoryOnline(sender, self.categoriesCView)
        self.categorySelected(sender.tag)
        self.productSlider.reloadData()
    }
    func categoryEdit(_ sender: AnyObject) {
        if sender.tag > -1 {
            self.eCategory = self.categoriesProduct[sender.tag as Int]
            self.editView(0)
        }
    }
    //---|   Product ONLINE
    func componentOnline(_ sender: PButton) {
        if sender.tag > -1 {
            let tag:Int = sender.tag as Int
            let product_id = productGetId(tag)
            let iComponent = String(sender.params["component_row"]!).toInt()
            var component = (self.componentsProduct[product_id]![iComponent] as? Dictionary<String, AnyObject>)!
            let productListCell = self.productList.cellForItem(at: IndexPath(row: tag-1, section: 0)) as! cellProductList
            let componentCell = productListCell.components!.cellForRow(at: IndexPath(row: iComponent, section: 0)) as! cellComponent
            let online = !String(component["online"]!).bool
            component["online"] = online as AnyObject
            let id = String(component["id"])
            DB.COMPONENTS.child("\(product_id)/\(id)/online").setValue(online)
            componentCell.online.setImage(UIImage(named: [false : "offline.png", true : "online.png"][online]!), for:.normal)
            //---| Change Locale var
            self.componentsProduct[product_id]![iComponent] = component
        }
    }

    //-----------------------------------------------#
        //--|  Editing
    //-----------------------------------------------#
    //---|   UPLOAD FILE TO SERVER
    func upload(_ imageView:UIImageView , _ options:Dictionary<String, Any>, _ callback:@escaping ((_ file:Dictionary<String, Any>?) -> Void), _ finished: ((_ error:Bool,_ result:Any) -> Void)?=nil){
        if String(self.objEditing()["file_updated"]).bool {
            //---|
            Alamofire.upload(multipartFormData: { multipartFormData in
                //---|   File Info
                multipartFormData.append(UIImageJPEGRepresentation(imageView.image!, 0.2)!, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
                //---|   Parameters
                //---|   Default Option
                let optionsSdefault:Dictionary<String, Any> = ["uid" : app.uid]
                for (key, value) in optionsSdefault {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                for (key, value) in options {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, to: "\(app.baseUrl)action/saveFile")
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    //---|   PROGRESS
                    upload.uploadProgress(closure: { (progress) in
                        // print("Upload Progress: \(progress.fractionCompleted)")
                        // print("#----\n#--| progress --: \(progress) |--#\n")
                        imageView.alpha = CGFloat(progress.fractionCompleted)
                    })

                    upload.responseJSON { response in
                        var json = [String: Any]()
                        if let nJson = response.result.value as? [String: Any]{ json = nJson }
                        if !String(json["isSuccess"]).empty() {
                            var filedata:Dictionary<String, Any> = [
                                "file" : String(json["isSuccess"]!),
                                "thumb" : String(json["thumb"]),
                                "filename" : String(json["filename"])
                            ]
                            callback(filedata)
                            if finished != nil{ finished!(false, json) }
                        }else{
                            self.confirm({ (str) in
                                callback(nil)
                                if finished != nil{ finished!(false, json) }
                            }, "Voulez vous continuer", String(json["isError"], "Erreur d'upload"), ["Rester", "Continue"], nil,{
                                if finished != nil{ finished!(true, response.result) }
                            })
                            print("#----\n#--| response.result.failure --: \(dump(response.result)) |--#\n")
                        }

                    }

                case .failure(let encodingError):
                    // print(encodingError)
                    // print("#----\n#--| encodingError --: \(encodingError) |--#\n")
                    self.alert("Télécharger le fichier", String(encodingError))
                    if finished != nil{ finished!(true, encodingError) }
                }
            }
        }else{
            callback(nil)
            if finished != nil{ finished!(false, "empty") }
        }
    }
    //---|   REMOVE FILE
    func removeFile(_ file:Any?){
        if file != nil, let sFile = file as? String{
            Alamofire.request("\(app.baseUrl)action/removeFile", method: .post, parameters: ["value":sFile])
        }
    }
        //---|   Load List Category
    func categoryDropdownInit(){
        //---|   Category List
        let category_list = self.categoriesProduct.olist()
        //---|   TVAs
        self.dropdownInit(self.pCDropDown!, view: self.pVCategory, datasource: category_list as NSArray, btnShowing: self.pBCategory, self.categoryProductChosen)
        self.dropdownInit(self.nCDropDown!, view: self.nVCategory, datasource: category_list as NSArray, btnShowing: self.nBCategory, self.categoryComponentChosen)
    }
        //---|   dropdownChosen
    func dropdownChosen(_ btn:UIButton, _ index:Int, _ item:String){
        btn.setTitle(item, for : .normal)
        btn.tag = index
    }
        //---|   Price Changing
    func priceChaning(_ price:UITextField, _ tva:String?, _ total:UITextField, _ keepPrice:Bool=true){
        let nTva = String(tva).toFloat()
        if keepPrice {
            let nPrice = String(price.text!).toFloat()
            total.text = String((nPrice*(100/(100+nTva)))).format()
        }else{
            let nTotal = String(total.text!).toFloat()
            price.text = String((nTotal/(100/(100+nTva)))).format()
        }
    }
        //---|   ImageView Editing
    func iViewEditing() -> UIImageView {
        if !self.vECategory.isHidden {
            return self.cFile
        }else if !self.vEProduct.isHidden{
            return self.pFile
        }
        return self.nFile
    }
        //---|   Object Editing Return
    func objEditing() -> Dictionary<String, Any>{
        if !self.vECategory.isHidden {
            return self.eCategory
        }else if !self.vEProduct.isHidden{
            return self.eProduct
        }
        return self.eComponent
    }
        //---|   OPEN CAMERA
    func openCamera (){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let picker = UIImagePickerController()
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.alert("No Camera", "Sorry, this device has no camera")
        }
    }
    //---|   SELECT IMAGE
    @IBAction func selectImageClick(_ sender: AnyObject) { selectImage() }
    func selectImage() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
        //---| Image Choose
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.iViewEditing().image = info[UIImagePickerControllerOriginalImage] as! UIImage
        if !self.vECategory.isHidden {
            self.eCategory["file_updated"] = true
            // self.cFile.image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }else if !self.vEProduct.isHidden{
            self.eProduct["file_updated"] = true
            // self.pFile.image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }else{
            self.eComponent["file_updated"] = true
            // self.nFile.image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        dismiss(animated:true, completion: nil)
    }
    //---|   IMAGE CANCEL
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) { dismiss(animated: true, completion: nil) }
    //---|  DropSelect Product Add
    func addNew(index: Int, item: String){
        var Init = true
        if index == 0 {
            Init = self.addInit(&eCategory)
            if Init{
                eCategory["provider_id"] = String(app.profile["provider_id"])
            }
            // if !String(eCategory["id"]).empty() { eCategory = Dictionary<String, Any>() }else{ Init = false }
        }else if index == 1{
            Init = self.addInit(&eProduct)
            // if !String(self.eProduct["id"]).empty() { self.eProduct = Dictionary<String, Any>() }else{ Init = false }
        }else if index == 2{
            Init = self.addInit(&eComponent)
            // if !String(eComponent["id"]).empty() { eComponent = Dictionary<String, Any>() }else{ Init = false }
        }
        self.editView(index, Init)
    }
    func addInit(_ obj: inout Dictionary<String, Any>) -> Bool{
        if !String(obj["id"]).empty() {
            obj = ["online" : false]
            return true
        }
        return false
    }
    //---| Product : Close
    func editCloseWA(){
        self.eBack.isHidden = true
        self.eSave.isHidden = true
        self.vpDropdox.isHidden = false
        self.vECategory.isHidden = true
        self.vEProduct.isHidden = true
        self.vEComponent.isHidden = true
        self.vpDropdox.isHidden = false
        self.productCView.isHidden = false
        self.hTitle.text = "Gestion des produits"
        self.categoriesCView.reloadData()
        self.productSlider.reloadData()
    }
    func editClose(_ duration:TimeInterval=0.4){
        UIView.animate(withDuration:duration, animations: self.editCloseWA)
    }
    //---| Edit View
    func editView(_ index:Int=0, _ Init:Bool = true){
        self.editCloseWA()
        UIView.animate(withDuration: 1.0, animations: {
            self.vpDropdox.isHidden = true
            self.productCView.isHidden = true
            self.eBack.isHidden = false
            self.eSave.isHidden = false
            self.vpDropdox.isHidden = true
            if index == 0 {
                self.categoryInit(Init)
                self.vECategory.isHidden = false
            }else if index == 1{
                self.productInit(Init)
                self.vEProduct.isHidden = false
            }else if index == 2{
                self.componentInit(Init)
                self.vEComponent.isHidden = false
            }
        })
    }
    //--|  Category
        //---|   beforeSave
    func beforeSave(_ errors:Array<String>, _ obj: inout Dictionary<String, Any>, _ imageView:UIImageView, _ path:String,_ callback:@escaping ((_ obj: Dictionary<String, Any>)-> Void)){
        if errors.count == 0{
            if String(obj["id"]).empty() {
                obj["id"] = app.id()
                obj["havapro_added"] = app.uid
                obj["created"] = [".sv" : "timestamp"]
                obj["provider_local_id"] = app.uid
            }
            DispatchQueue.main.async {[obj] in // copies val
                var obj = obj // mutable copy of val
                self.eBack.isHidden = true
                self.eSave.isHidden = true
                self.upload(imageView, ["fpath" : "\(path)/\(String(obj["id"]!))", "value" : String(obj["file"], ""), "width" : "300", "thumb" : "150"], {(filedata) in
                    if filedata != nil{
                        obj["file"] = String(filedata!["file"])
                        obj["thumb"] = String(filedata!["thumb"])
                        obj["filename"] = String(filedata!["filename"])
                        obj["fileType"] = "image"
                    }
                    obj["updated"] = [".sv" : "timestamp"]
                    obj.removeValue(forKey: "file_updated")
                    callback(obj)
                    self.editClose()
                    //---|   Refresh sale
                    if let saleCtrl = self.configCtrl!.appController?.controllers["Sale"] as? SaleController{
                        saleCtrl.connected()
                    }
                }, { ( error, result) in //---|   Finished
                    if error {
                        self.eBack.isHidden = false
                        self.eSave.isHidden = false
                    }
                })
            }
        }else{
            let textData: NSMutableString = NSMutableString()
            textData.append("Veuillez remplir les champs suivants :\n-   \(errors.joined(separator: "\n-   "))")
            self.alert("Erreur\(errors.count>1 ? "s" : "")", textData as String)
        }
    }
        //---|   beforeDelete
    func beforeDelete(_ obj: inout Dictionary<String, Any>,_ callback:@escaping ((_ id:String)-> Void)){
        if String(obj["id"]).empty() {
            self.editClose()
            obj["id"] = Dictionary<String, Any>()

        }else{
            //--| TODO
            DispatchQueue.main.async {[obj] in // copies val
                var obj = obj // mutable copy of val
                self.confirm({ (field) in
                    callback(String(obj["id"]!))
                    self.editClose()
                    obj = Dictionary<String, Any>()
                })
            }
        }
    }
    //--|  Category
        //--|  Init
    func categoryInit(_ Init:Bool = true){
        self.hTitle.text = "Editer une categorie"
        if String(eCategory["id"]).empty() {
            self.hTitle.text = "Ajouter une categorie"
        }
        if Init {
            self.cName.text = String(eCategory["name"])
            self.cOnline.isOn = String(eCategory["online"] ?? "0").bool
            self.cNote.text = String(eCategory["note"])
            self.cFile.assign(eCategory["file"], "image_update.png")
        }
    }
        //--|  Save
    func categorySave(){
        var errors:Array<String> = []
        if String(self.cName.text).empty(){
            errors.append("Le nom")
        }
        self.beforeSave(errors, &self.eCategory, self.cFile, "categories", { (nCategory) in
            self.eCategory = nCategory
            self.eCategory["online"] = self.cOnline.isOn
            self.eCategory["name"] = String(self.cName.text)
            self.eCategory["note"] = String(self.cNote.text)
            //---|   DB
            DB.PCATEGORIES.child("\(String(self.eCategory["id"]))").setValue(self.eCategory)
            let category_id = String(self.eCategory["id"]!)
            //---|   Created
            if String(self.g(self.categoriesProduct, "\(self.indexProductSlider).id", "")) != category_id{
                self.categoriesProduct.append(self.eCategory)
                self.indexProductSlider = self.categoriesProduct.count-1
                self.productsByCategory[category_id] = Array<AnyObject>()
                self.categoryChanging()
            }else{ //---|   Updated
                self.categoriesProduct[self.indexProductSlider] = self.eCategory
            }
            //---| Refresh Categories Dropdown
            self.categoryDropdownInit()
        })
    }
        //--|  Delete
    @IBAction func categoryDelete(_ sender: AnyObject) {
        self.beforeDelete(&self.eCategory, {(category_id) in
            // Remove category
            DB.PCATEGORIES.child(category_id).removeValue()
            self.removeFile(self.eCategory["file"])
                // Remove PRODUCTS
            for (product_id, product) in self.categoriesProduct[self.indexProductSlider] as Dictionary<String, AnyObject>{
                self.removeFile(product["file"])
                DB.PRODUCTS.child("\(category_id)/\(product_id)").removeValue()
                //self.products.removeValue
                // Remove COMPONENTs
                if let nComponents = self.componentsProduct[product_id] as? Array<Dictionary<String, AnyObject>>{
                    for component in nComponents{
                        self.removeFile(component["file"])
                        DB.COMPONENTS.child("\(product_id)/\(String(component["id"]))").removeValue()
                    // Remove COMPONENTs
                    }
                }
            }
            self.categoriesProduct.remove(at: self.indexProductSlider)
            self.indexProductSlider = self.indexProductSlider-1
            if self.indexProductSlider == -1 { self.indexProductSlider = 0 }
            self.categoryChanging()
            //---| Refresh Categories Dropdown
            self.categoryDropdownInit()
        })
    }
    //--|  Product
        //--|  Init
    func productInit(_ Init:Bool = true){
        //--| Has Category
        if self.categoriesProduct.count == 0{
            self.alert("Catégorie", "Catégorie")
            self.editView(0)
        }
        self.hTitle.text = "Editer un produit"
        if String(eProduct["id"]).empty() {
            self.hTitle.text = "Ajouter un produit"
        }

        self.dropdownInit(self.eDropDown!, view: self.vpDropdox, datasource: self.pActions as NSArray, btnShowing: self.bpDropbox, self.addNew)
        if (Init) || String(self.pTva.titleLabel!.text).toFloat() == 0 {
            self.pName.text = String(eProduct["name"])
            self.pPrice.text = String(eProduct["price"])
            self.pTotal.text = String(eProduct["total"])
            self.pOnline.isOn = String(eProduct["online"] ?? "0").bool
            self.pNote.text = String(eProduct["note"])
            self.pFile.assign(eProduct["thumb"], "image_update.png")
            //--|  TVA
            self.tvaProductChosen(index: 0, item: String(eProduct["tva"], app.tvas[0]))
            //--|  Category
            let category = self.categoriesProduct.search(String(eProduct["product_category_id"]))
            self.pBCategory.setTitle(String(category["name"], "Liste Categorie"), for: .normal)
            self.pBCategory.tag = String(category["$index"], "-1").toInt(-1)
        }
    }
        //---|   Category Product
    func categoryProductChosen(index: Int, item: String){
        dropdownChosen(self.pBCategory, index, item)
    }
        //---|   Tva Product
    func tvaProductChosen(index: Int, item: String){
        self.pTva.setTitle(item, for : .normal)
        timeout({
            self.priceChaning(self.pPrice, item, self.pTotal)
        })
    }
    //--|  Price Component Changing
    @IBAction func pPriceChanging(_ sender: AnyObject) {
        self.priceChaning(self.pPrice, self.pTva.titleLabel!.text, self.pTotal)
    }
    //--|  Total Component Changing
    @IBAction func pTotalChanging(_ sender: AnyObject) {
        self.priceChaning(self.pPrice, self.pTva.titleLabel!.text, self.pTotal, false)
    }
        //--|  Save
    func productSave(){
        var errors:Array<String> = []
        if String(self.pName.text).empty(){ errors.append("Le nom") }
        if self.pBCategory.tag < 0{ errors.append("Catégorie") }
        // if (String(self.pPrice.text).empty()) || (String(self.pPrice.text).toFloat() <= 0) { errors.append("Prix") }
        // if (String(self.pTotal.text).empty()) || (String(self.pTotal.text).toFloat() <= 0){ errors.append("Total") }
        // if String(self.pTva.titleLabel!.text).toFloat() == 0{ errors.append("TVA") }
        self.beforeSave(errors, &self.eProduct, self.pFile, "products", { (nProduct) in
            self.eProduct = nProduct
            self.eProduct["online"] = self.pOnline.isOn
            self.eProduct["name"] = String(self.pName.text)
            self.eProduct["price"] = String(self.pPrice.text, "0").format().toFloat()
            self.eProduct["total"] = String(self.pTotal.text, "0").format().toFloat()
            self.eProduct["tva"] = String(self.pTva.titleLabel!.text).format().toFloat()
            self.eProduct["note"] = String(self.pNote.text)
            let product_id  = String(self.eProduct["id"])

            let category_id = String(self.categoriesProduct.olist("id")[self.pBCategory.tag])!
                //---|  Product Index
            var product_index = ""
            if !String(self.eProduct["product_category_id"]).empty(){
                product_index = String((self.productsByCategory[String(self.eProduct["product_category_id"])] as? Array<Dictionary<String, Any>>)!.search(product_id)["$index"])
            }
                //---|  Update Category
            if let old_category_id = self.eProduct["product_category_id"] as? String, old_category_id != category_id{
                DB.PRODUCTS.child("\(old_category_id)/\(product_id)").removeValue()
                self.productsByCategory[old_category_id]?.remove(at: Int(product_index)!)
                product_index = ""
            }
            self.eProduct["product_category_id"] = category_id
            self.eProduct["provider_local_id"] = app.uid
            self.eProduct["complex"] = (self.componentsProduct[product_id]?.count == 0 ? false : true)
            //---|   DB
            DB.PRODUCTS.child("\(category_id)/\(product_id)").setValue(self.eProduct)
            //---|
            self.products[product_id] = self.eProduct
                //---| Add || Moved Product
            if product_index.empty() {
                self.productsByCategory[category_id]!.append(self.eProduct as AnyObject)
            }else{ //---| Update Product
                self.productsByCategory[category_id]![Int(product_index)!] = self.eProduct as AnyObject
            }
            self.productList.reloadData()
            if self.indexProductSlider != self.pBCategory.tag {
                self.indexProductSlider = self.pBCategory.tag
                self.categoryChanging()
            }

        })
    }
        //--|  Delete
    @IBAction func productDelete(_ sender: AnyObject) {
        self.beforeDelete(&self.eProduct, {(product_id) in
            // Remove PRODUCTS
            let category_id = String(self.eProduct["product_category_id"])
            DB.PRODUCTS.child("\(category_id)/\(product_id)").removeValue()
            let product_index = String((self.productsByCategory[category_id] as? Array<Dictionary<String, Any>>)!.search(product_id)["$index"]).toInt()
            self.removeFile(String(self.products[product_id]!["file"]))
            self.products.removeValue(forKey: product_id)
            self.productsByCategory[category_id]!.remove(at: product_index)
            // Remove COMPONENTs
            if let nComponents = self.componentsProduct[product_id] as? Array<Dictionary<String, AnyObject>>{
                for component in nComponents{
                    self.removeFile(component["file"])
                    DB.COMPONENTS.child("\(product_id)/\(String(component["id"]))").removeValue()
                // Remove COMPONENTs
                }
            }
            self.productList.reloadData()
        })
    }
        //--|  Component
    func componentInit(_ Init:Bool = true){
        //--| Has Category
        if self.categoriesProduct.count == 0{
            self.alert("Catégorie", "Catégorie")
            self.editView(0)
        }else if self.products.count == 0{
            self.alert("Produits", "Produit")
            self.editView(0)
        }
        self.hTitle.text = "Editer un composent"
        if String(eComponent["id"]).empty() {
            self.hTitle.text = "Ajouter un composent"
        }
        if (Init) || String(self.nBTva.titleLabel!.text).toFloat() == 0 {
            self.nName.text = String(eComponent["name"])
            self.nPrice.text = String(eComponent["price"])
            self.nTotal.text = String(eComponent["total"])
            self.nQty.text = String(eComponent["qty"], "1")
            self.nBTva.setTitle(String(eComponent["tva"]), for: .normal)
            self.nOnline.isOn = String(eComponent["online"] ?? "0").bool
            self.nNote.text = String(eComponent["note"])
            self.nFile.assign(eComponent["thumb"], "image_update.png")
            //--|  TVA
            self.tvaComponentChosen(index: 0, item: String(eComponent["tva"], app.tvas[0]))
            self.categoryComponentChosenSet(-1, "Liste Categorie")
            let product_id = String(eComponent["product_id"])
            //--|  Product
            if let product = self.products[product_id] {
                //--|  Category
                let category_id = String(product["product_category_id"])
                let category = self.categoriesProduct.search(category_id)
                self.categoryComponentChosenSet(String(category["$index"], "-1").toInt(-1), String(category["name"], "Liste Categorie"), category_id)
                self.productComponentChosen(index: String((productsByCategory[category_id] as? Array<Dictionary<String, Any>>)!.search(product_id)["$index"], "-1").toInt(-1), item: String(product["name"], "Liste Produit"))
            }
        }
    }
        //---|   Category Product
    func categoryComponentChosen(index: Int, item: String){
        self.categoryComponentChosenSet(index, item)
    }
    func categoryComponentChosenSet(_ index: Int, _ item: String, _ category_id : String? = nil ){
        let update_category = (self.nBCategory.tag != index)
        dropdownChosen(self.nBCategory, index, item)
        if index == -1{
            self.productComponentChosen(index: -1, item: "Liste Produit")
            self.dropdownInit(self.nPDropDown!, view: self.nVProduct, datasource: [], btnShowing: self.nBProduct, self.productComponentChosen)
        }else{
            var category_id = category_id ?? String(categoriesProduct[index]["id"])
            //---|   Product List
            let product_list = (self.productsByCategory[category_id] as? Array<Dictionary<String, Any>>)!.olist()

            //---|   Product Dropdown
            self.dropdownInit(self.nPDropDown!, view: self.nVProduct, datasource: product_list as NSArray, btnShowing: self.nBProduct, self.productComponentChosen)
            if update_category{
                self.productComponentChosen(index: -1, item: "Liste Produit")
            }
            //---|   Product Dropdown
            if product_list.count == 0{
                self.confirm({ (field) in
                    self.editView(1)
                    self.eProduct = ["product_category_id" : category_id, "online" : false]
                }, "Ajouter un nouveau produit", "S'il vous plaît ajouter avant un produit pour la catégorie '\(String(self.categoriesProduct[index]["name"]))'", ["Selection une autre catégorie", "Ajouter un nouveau produit"], nil, {
                    self.nCDropDown!.show()
                })
            }
        }
    }
        //---|   Category Product
    func productComponentChosen(index: Int, item: String){
        dropdownChosen(self.nBProduct, index, item)
    }
        //---|   Tva Product
    func tvaComponentChosen(index: Int, item: String){
        self.nBTva.setTitle(item, for : .normal)
        timeout({
            self.priceChaning(self.nPrice, item, self.nTotal)
        })
    }
        //---|   Complex
    func setComplex(_ category_id:String, _ product_id:String) -> Bool{
        let product_index = String((self.productsByCategory[category_id] as? Array<Dictionary<String, Any>>)!.search(product_id)["$index"])
        let complex = (self.componentsProduct[product_id]?.count == 0 ? false : true)
        self.products[product_id]!["complex"] = complex
        if let product_index = product_index as? Int{
            self.productsByCategory[category_id]![product_index] = self.products[product_id] as AnyObject
        }
        DB.PRODUCTS.child("\(category_id)/\(product_id)/complex").setValue(complex)
        return complex
    }

        //--|  Save
    func componentSave(){
        var errors:Array<String> = []
        if String(self.nName.text).empty(){ errors.append("Le nom") }
        if self.nBCategory.tag < 0{ errors.append("Catégorie") }
        if self.nBProduct.tag < 0{ errors.append("Produit") }
        self.beforeSave(errors, &self.eComponent, self.nFile, "components", { (nComponent) in
            self.eComponent = nComponent
            self.eComponent["online"] = self.nOnline.isOn
            self.eComponent["name"] = String(self.nName.text)
            self.eComponent["price"] = String(self.nPrice.text, "0").format().toFloat()
            self.eComponent["total"] = String(self.nTotal.text, "0").format().toFloat()
            self.eComponent["qty"] = String(self.nQty.text, "0").toInt()
            self.eComponent["tva"] = String(self.nBTva.titleLabel!.text).format().toFloat()
            self.eComponent["note"] = String(self.nNote.text)
            let component_id  = String(self.eComponent["id"])
            let category_id = String(self.categoriesProduct.olist("id")[self.nBCategory.tag])!
            let product_id = (self.productsByCategory[category_id] as? Array<Dictionary<String, Any>>)!.olist("id")[self.nBProduct.tag]
            let old_product_id = String(self.eComponent["product_id"])
            let old_category_id = (self.products[old_product_id] != nil) ? String(self.products[old_product_id]!["product_category_id"]) : ""
            var component_index = ""
            if !old_product_id.empty(){
                component_index = String((self.componentsProduct[old_product_id] as? Array<Dictionary<String, Any>>)!.search(component_id)["$index"])
            }
            self.eComponent["product_id"] = product_id
            //---|   Updated Category OR Product
            if (!component_index.empty()) , ((old_product_id != product_id) || (old_category_id != category_id)){
                DB.COMPONENTS.child("\(old_product_id)/\(component_id)").removeValue()
                self.componentsProduct[old_product_id]!.remove(at : Int(component_index)!)
                component_index = ""
                self.setComplex(old_category_id, old_product_id) //---|   Complex
            }
            //---| Update Category
            if self.indexProductSlider != self.nBCategory.tag{
                self.indexProductSlider = self.nBCategory.tag
                self.categoryChanging()
            }
            //---|   Add To DB
            DB.COMPONENTS.child("\(product_id)/\(component_id)").setValue(self.eComponent)
            //---|   Move or Add
            if component_index.empty(){
                if self.componentsProduct[product_id] == nil { self.componentsProduct[product_id] = Array<Any>() }
                self.componentsProduct[product_id]!.append(self.eComponent)
            }else{ //---|   Update
                self.componentsProduct[product_id]![Int(component_index)!] = self.eComponent
            }
            self.setComplex(category_id, product_id) //---|   Complex
            self.productList.reloadData()
            let product_index = self.nBProduct.tag
            self.timeout({ self.componentShowByIndex(product_index, component_index.empty()) }, 300)
            //---|   Init Dropdown Component
            self.categoryComponentChosenSet(-1, "Liste Categorie")
        })
    }
    //--|  Price Component Changing
    @IBAction func cPriceChanging(_ sender: AnyObject) {
        self.priceChaning(self.nPrice, self.nBTva.titleLabel!.text, self.nTotal)
    }
        //--|  Total Component Changing
    @IBAction func cTotalChanging(_ sender: AnyObject) {
        self.priceChaning(self.nPrice, self.nBTva.titleLabel!.text, self.nTotal, false)
    }
    //-----------------------------------------------#
        //--|  Elements
    //-----------------------------------------------#
    // Right Arrow Clicked
    @IBAction func clickArrowRightCategoryProduct(_ sender: AnyObject) {
        indexProductSlider = indexProductSlider+1
        if indexProductSlider >= self.categoriesProduct.count { indexProductSlider = 0 }
        self.categoryChanging()
    }
    // Left Arrow Clicked
    @IBAction func clickArrowLeftCategoryProduct(_ sender: AnyObject) {
        indexProductSlider = indexProductSlider-1
        if indexProductSlider < 0 { indexProductSlider = self.categoriesProduct.count-1 }
        self.categoryChanging(.left)
    }

    @IBAction func categoryViewOpen(_ sender: AnyObject) {
        self.categoryToggle()
    }
    //---| Product : Back
    @IBAction func productBack(_ sender: AnyObject) { self.editClose() }
    //---|
    @IBAction func dropdownProductActions(_ sender: AnyObject) { eDropDown!.show() }
    //---|   Product Category
    @IBAction func ddPCategory(_ sender: AnyObject) { pCDropDown!.show() }
    //---|   Product TVA
    @IBAction func ddPTva(_ sender: AnyObject) { pTDropDown!.show() }
    //---|   Compoment Category
    @IBAction func ddNCategory(_ sender: AnyObject) { nCDropDown!.show() }
    //---|   Compoment Product
    @IBAction func ddNProduct(_ sender: AnyObject) { nPDropDown!.show() }
    //---|   Product TVA
    @IBAction func ddNTva(_ sender: AnyObject) { nTDropDown!.show() }

        //--|  Delete
    @IBAction func componentsDelete(_ sender: AnyObject) {
        self.beforeDelete(&self.eComponent, {(component_id) in
            //  Remove Component
            let product_id = String(self.eComponent["product_id"])
            DB.COMPONENTS.child("\(product_id)/\(component_id)").removeValue()
            if let components = self.componentsProduct[product_id] as? Array<Dictionary<String, Any>>{
                //---|   Component_index
                let component_index = String(components.search(component_id)["$index"]).toInt()
                self.componentsProduct[product_id]!.remove(at: component_index)
                //---|   Complex
                let category_id = String(self.products[product_id]!["product_category_id"])
                let complex = self.setComplex(category_id, product_id)
                //--| Has no Component
                if !complex{
                    self.productList.reloadData()
                }else{ //---|   Refresh Only Component List
                    let product_index = String((self.productsByCategory[category_id] as? Array<Dictionary<String, Any>>)!.search(product_id)["$index"]).toInt()
                    self.componentShowByIndex(product_index, false)
                }
            }
            //---|   Init Dropdown Component
            self.categoryComponentChosenSet(-1, "Liste Categorie")

        })
    }
    //---| Save
    @IBAction func Save(_ sender: AnyObject) {
        if !self.vECategory.isHidden {
            self.categorySave()
        }else if !self.vEProduct.isHidden{
            self.productSave()
        }else if !self.vEComponent.isHidden{
            self.componentSave()
        }
    }
    //-----------------------------------------------#
        //--|  Table View
    //-----------------------------------------------#
    // Table View Data
    func numberOfSections(in tableView: UITableView) -> Int{ return 1 }

    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        // Components
        if  (tableView.tag as? Int)!>0, self.componentsProduct.count>0{
            let product_id = productGetId(tableView.tag)
            if product_id != "", self.componentsProduct[product_id] != nil{ return (self.componentsProduct[product_id]?.count)! as Int }
        }
        return 0
    }
    // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Components
        if tableView.tag as! Int>0, let componentItem = tableView.dequeueReusableCell(withIdentifier: "cellComponent", for: indexPath) as? cellComponent{
            componentItem.isHidden = true
            if self.componentsProduct[productGetId(tableView.tag)] != nil, let component = self.componentsProduct[productGetId(tableView.tag)]![indexPath.row] as? Dictionary<String, AnyObject>{
                let id = String(component["id"]!)
                componentItem.backgroundColor = UIColor.clear
                componentItem.component.text = (component["name"] as? String)!
                //---|   Online Button
                componentItem.online.tag = tableView.tag
                componentItem.online.params["component_row"] = indexPath.row
                componentItem.online.addTarget(self, action: #selector(self.componentOnline(_:)), for: .touchUpInside)
                componentItem.online.setImage(UIImage(named: [false : "offline.png", true : "online.png"][String(component["online"] ?? "0").bool]!), for:.normal)

                componentItem.isHidden = false
            }
            return componentItem.ini()
        }
        return tableView.cellForRow(at: indexPath)!
    }

    // Cell Selected Of Menu Side
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) as?  cellComponent{ cell.selected() }
        self.eComponent = (self.componentsProduct[productGetId(tableView.tag)]![indexPath.row] as? Dictionary<String, AnyObject>)!
        self.editView(2)
    }
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.reloadData()
    }
    // Active Editing
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // if tableView == self.productConsole { return true }
        return false
    }
    // Deleting
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        // if tableView == self.productConsole, editingStyle == .delete {
        //     let productConsole = tableView.dequeueReusableCell(withIdentifier: "productConsole", for: indexPath) as! cellProductConsole
        //     var productsCommande:Dictionary<String, AnyObject> = (self.commandes[self.commandeCurrent]["products"] as? Dictionary<String, AnyObject>)!
        //     let product = (self.products[self.consoles[indexPath.row]["id"]!] as? Dictionary<String, AnyObject>)!
        //     // let product = (self.products[(Array(productsCommande.keys)[indexPath.row] as? String)!] as? Dictionary<String, Any>)!
        //     // print("Deleting-----\(indexPath)---productConsole : \(productsCommande[(product["id"] as? String)!]!) ----- product : \(product["id"])")
        //     self.consoles.remove(at: indexPath.row)
        //     productsCommande.removeValue(forKey: (product["id"] as? String)!)
        //     self.commandes[self.commandeCurrent]["products"] = productsCommande
        //     DB.COMMANDES.child("\(String(self.commandes[self.commandeCurrent]["id"]!))/products/\((product["id"] as? String)!)").removeValue()
        //     tableView.deleteRows(at: [indexPath], with: .fade)
        //     commandCalculPrice(self.commandeCurrent)
        //     self.productList.reloadData()
        // }
    }
    //-----------------------------------------------#
        //--| Product List
    //-----------------------------------------------#
    // Number Of section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if collectionView == self.productList, self.productsByCategory[self.cateoryProduct]?.isEmpty == false{ return self.productsByCategory[self.cateoryProduct]!.count }
        if collectionView == self.productSlider || collectionView == self.categoriesCView { return self.categoriesProduct.count }
        return 0
    }
    // Set Data
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // productList
        if collectionView == self.productList {
            let productListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "productList", for: indexPath) as! cellProductList
            productListCell.isHidden = true
            // Is Not Empty
            if  self.productsByCategory[self.cateoryProduct]?.isEmpty == false{
                let product = self.productsByCategory[self.cateoryProduct]?[indexPath.row] as AnyObject
                if product.object(forKey:"id") != nil{
                    productListCell.content.text = String(product["name"]!)
                    // productListCell.content.text = String((product["name"] != nil ? product["name"]! : "")!)
                    productListCell.price.text = String(product["price"], "0").format()+" €"
                    productListCell.product.assign(product["thumb"], "background.png")
                    //---|   Online Button
                    productListCell.online.tag = indexPath.row
                    productListCell.online.addTarget(self, action: #selector(self.productOnline(_:)), for: .touchUpInside)
                    productListCell.online.setImage(UIImage(named: [false : "offline.png", true : "online.png"][String(product["online"] ?? "0").bool]!), for:.normal)
                    // BTN Component
                    let  productID:String = product["id"]! as! String
                    if productListCell.btnComponent.enabled((self.componentsProduct[productID] != nil && self.componentsProduct[productID]!.count > 0)) {
                        // Action
                        productListCell.btnComponent.tag = indexPath.row
                        productListCell.btnComponent.addTarget(self, action: #selector(self.componentShow(_:)), for: .touchUpInside)
                        // Fill the List
                        productListCell.components.delegate = self
                        productListCell.components.dataSource = self
                        productListCell.components.tag = indexPath.row+1
                    }
                    // if (self.componentsProduct[productID] == nil) || (self.componentsProduct[productID]!.count == 0) {
                    //     productListCell.btnComponent.alpha = 0.1
                    //     productListCell.btnComponent.isEnabled = false
                    //     productListCell.components.tag = 0
                    // }else{
                    //     // // Btn Component
                    //     // productListCell.btnComponent.alpha = 1
                    //     // productListCell.btnComponent.isEnabled = true

                    // }
                    productListCell.modal.isHidden = true
                    productListCell.isHidden = false
                }
            }
            return productListCell
        }
        if collectionView == self.productSlider {
            let cellCategoryProduct = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCategoryProduct", for: indexPath) as! cellCategoryProduct
            cellCategoryProduct.isHidden = true
            let category = self.categoriesProduct[indexPath.row]
            cellCategoryProduct.content.text = String(category["name"])
            cellCategoryProduct.product.assign(category["file"], "background.png")
            //---|   Online Button
            cellCategoryProduct.online.tag = indexPath.row
            cellCategoryProduct.edit.tag = indexPath.row
            cellCategoryProduct.online.addTarget(self, action: #selector(self.categoryOnlineSlider(_:)), for: .touchUpInside)
            cellCategoryProduct.edit.addTarget(self, action: #selector(self.categoryEdit(_:)), for: .touchUpInside)
            cellCategoryProduct.online.setImage(UIImage(named: [false : "offline.png", true : "online.png"][String(category["online"] ?? "0").bool]!), for:.normal)
            cellCategoryProduct.isHidden = false
            return cellCategoryProduct
        }
        if collectionView == self.categoriesCView {
            let categoriesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCategories", for: indexPath) as! cellCategories
            let category = self.categoriesProduct[indexPath.row]
            categoriesCell.name.text = String(category["name"])
            categoriesCell.image.assign(category["file"], "background.png")
            //---|   Online Button
            categoriesCell.online.tag = indexPath.row
            categoriesCell.online.addTarget(self, action: #selector(self.categoryOnlineList(_:)), for: .touchUpInside)
            categoriesCell.online.setImage(UIImage(named: [false : "offline.png", true : "online.png"][String(category["online"] ?? "0").bool]!), for:.normal)

            return categoriesCell
        }
        return collectionView.cellForItem(at: indexPath)!
    }
    // Selected Item at collection View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.productList {
            productSelected(indexPath.row)
        }else if collectionView == self.categoriesCView {
            self.categorySelected(indexPath.row)
        }
    }
}
