//
//  ConfigProductController.swift
//  havapro
//
//  Created by abdelghani on 30/05/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class ConfigProductController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate  {
    var snaps = Dictionary<String, AnyObject>()
    var productsByCategory = Dictionary<String, Array<AnyObject>>()
    var products = Dictionary<String, Dictionary<String, Any>>()
    var componentsProduct = [String : Array<Any>]()
    var categoriesProduct = Array<Dictionary<String, Any>>()

    //---|   Collection View
    @IBOutlet var productList: UICollectionView!
    @IBOutlet var categoryProduct: UICollectionView!

    //---|   UIView
    @IBOutlet var categoryProductView: UIView!

    //---|   UIButton
    @IBOutlet var btnRightCategoryProduct: UIButton!
    @IBOutlet var btnLeftCategoryProduct: UIButton!

    //---|   Vars
    var cateoryProduct:String = ""
    var indexCategoryProduct : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: BROADCAST.CONNECTED, object: nil, queue: nil, using: self.connected)
        // Product List
        productList.dataSource = self
        productList.delegate = self

        // categoryProduct
        categoryProduct.dataSource = self
        categoryProduct.delegate = self
    }
    func connected(notification: Notification){
        // Get Products Catgoery
        DB.PCATEGORIES.observeSingleEvent(of: .value, with: { (snap) in
            if !snap.exists() { return }
            self.snaps["categories"] = snap.value as AnyObject?
            self.products_set()
        })
        // Products
        DB.PRODUCTS.observeSingleEvent(of: .value, with: { (snap) in
            if !snap.exists() { return }
            self.snaps["products"] = snap.value as AnyObject?
            self.products_set()
        })
        // Components
        self.productComponents("components/\(app.uid)", scopeData: &self.componentsProduct) { (response, snap) in
            self.componentsProduct=response
        }
    }
    //---|   products
    func products_set(){
        self.categoriesProduct = Array<Dictionary<String, Any>>()
        self.products = Dictionary<String, Dictionary<String, Any>>()
        if self.snaps["categories"] != nil, (self.snaps["categories"]?.count)!>0, self.snaps["products"] != nil, (self.snaps["products"]?.count)!>0  {
            for(category_id, fProducts) in (self.snaps["products"] as? Dictionary<String, Dictionary<String, AnyObject>>)!{
                let category_id_str = String(category_id)!
                if self.snaps["categories"]![category_id_str] != nil {
                    self.categoriesProduct.append((self.snaps["categories"]![category_id_str] as? Dictionary<String, AnyObject>)!)
                    //---| s  Init Product Category
                    if self.productsByCategory[category_id_str] == nil { self.productsByCategory[category_id_str] = Array<AnyObject>() }
                    for(product_id, product) in fProducts{
                        let product_id_str = String(product_id)!
                        self.products[product_id_str] = (product as? Dictionary<String, AnyObject>)!
                        self.products[product_id_str]!["id"] = product_id_str// Exception
                        self.productsByCategory[category_id_str]!.append(self.products[product_id_str] as AnyObject)
                    }
                }
            }
            self.productList.reloadData()
            self.refreshSlider()
        }
    }
    // // Number Of Rows
    func cp_numberOfRowsInSection(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // if  (tableView.tag as? Int)!>0{
            // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        if self.componentsProduct.count>0 {
            let product_id = productGetId(tableView.tag)
            if product_id != "", self.componentsProduct[product_id] != nil{ return (self.componentsProduct[product_id]?.count)! as Int }
        }
        // }
        return 0
    }
    // Cells Of Menu Side
    func cp_cellForRowAt(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Components
        // if tableView.tag as! Int>0{
        if let componentItem = tableView.dequeueReusableCell(withIdentifier: "cellComponent", for: indexPath) as? cellComponent {
            componentItem.isHidden = true
            let product_id = productGetId(tableView.tag)
            if self.componentsProduct[product_id] != nil, let component = self.componentsProduct[product_id]![indexPath.row] as? Dictionary<String, AnyObject>{
                componentItem.checked.isHidden = !String(component["online"]).bool
                componentItem.backgroundColor = UIColor.clear
                componentItem.component.text = String(component["name"])
                componentItem.isHidden = false
            }
            return componentItem
        }
        // }
        return tableView.cellForRow(at: indexPath)!
    }
    //-----------------------------------------------#
        //--| Product List
    //-----------------------------------------------#
    // Number Of section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if collectionView == self.productList, self.productsByCategory[self.cateoryProduct]?.isEmpty == false{ return self.productsByCategory[self.cateoryProduct]!.count }
        if collectionView == self.categoryProduct { return self.categoriesProduct.count }
        return 0
    }
    // Set Data
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // productList
        if collectionView == self.productList {
            let productListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "productList", for: indexPath) as! cellProductList
            productListCell.isHidden = true
            // Is Not Empty
            if  self.productsByCategory[self.cateoryProduct]?.isEmpty == false{
                if let product = self.productsByCategory[self.cateoryProduct]?[indexPath.row] as? NSDictionary{
                    productListCell.content.text = String(product["name"])
                    productListCell.price.text = String(product["price"])
                    productListCell.product.assign(product["thumb"])
                    productListCell.checked.isHidden = !String(product["online"]).bool
                    // BTN Component
                    let  productID:String = product["id"]! as! String
                    if self.componentsProduct[productID] == nil {
                        productListCell.btnComponent.alpha = 0.1
                        productListCell.btnComponent.isEnabled = false
                    }else{
                        // Btn Component
                        productListCell.btnComponent.alpha = 1
                        productListCell.btnComponent.isEnabled = true
                        // Action
                        productListCell.btnComponent.tag = indexPath.row
                        productListCell.btnComponent.addTarget(self, action: #selector(self.componentShow(_:)), for: .touchUpInside)
                        // Fill the List
                        //let printerCtrl:UIViewController = PrinterConfigController
                        // productListCell.components.delegate = (PrinterConfigController.self as? UITableViewDelegate)!
                        // productListCell.components.dataSource = (PrinterConfigController.self as? UITableViewDataSource)!
                        productListCell.components.tag = indexPath.row+1
                    }
                    productListCell.modal.isHidden = true
                    productListCell.isHidden = false
                }
            }
            return productListCell
        }
        if collectionView == self.categoryProduct {
            let cellCategoryProduct = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCategoryProduct", for: indexPath) as! cellCategoryProduct
            cellCategoryProduct.isHidden = true
            if let category = self.categoriesProduct[indexCategoryProduct] as? NSDictionary, category["id"] != nil{
                let online = String(category["online"]).bool
                self.productList.isUserInteractionEnabled = online
                self.productList.alpha = online ? 1 : 0.2
                cellCategoryProduct.checked.isHidden = !online
                cellCategoryProduct.content.text = String(category["name"])
                cellCategoryProduct.product.assign(category["file"])
                cellCategoryProduct.isHidden = false
            }
            return cellCategoryProduct
        }
        return collectionView.cellForItem(at: indexPath)!
    }
    // Selected Item at collection View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.productList {
            productSelected(indexPath.row)
        }else if collectionView == self.categoryProduct {
            categorySelected(indexPath.row)
        }
    }
    // Change orintation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        // Refresh Slider
        refreshSlider()
    }
    //-----------------------------------------------#
        //--|  Global
    //-----------------------------------------------#
    // Get Product From Web And Filter data
    func productComponents(_ path:String, scopeData:inout [String : Array<Any>], completion: @escaping ([String : Array<Any>], NSDictionary) -> Void) {
        var scopeData = scopeData
        DB.COMPONENTS.observeSingleEvent(of: .value, with: { (snapshot) in
            if !snapshot.exists() { return }
            let nsdictionary:NSDictionary = (snapshot.valueInExportFormat() as? NSDictionary)!
            for (id, obj) in nsdictionary{
                if obj != nil, (obj as? Dictionary<String, AnyObject>)!.count>0{
                    let id_str = String(describing: id)
                    scopeData[id_str] = Array<Dictionary<String, AnyObject>>()
                    for (sIndex, section) in (obj as? Dictionary<String, AnyObject>)!{
                        var section_clone = (section as? Dictionary<String, AnyObject>)!
                        section_clone["id"] = String(describing: section_clone["id"]!) as AnyObject
                        scopeData[id_str]!.append(section_clone)
                    }
                }
            }
            // Completed
            completion(scopeData, nsdictionary)
            // Refresh Data
            self.productList.reloadData()
            self.refreshSlider()
        })
    }
    //
    func productGetId( _ index : Int)-> String{
        //---| Get ID
        if self.productsByCategory[self.cateoryProduct] != nil, let product_id = (self.productsByCategory[self.cateoryProduct] as? Array<Dictionary<String, AnyObject>>)![index-1]["id"] as? String{
            return product_id
        }
        return ""
    }
    //-----------------------------------------------#
        //--|  Changing
    //-----------------------------------------------#
    // Refresh Slider
    func refreshSlider(){
        self.categoryProduct.reloadData()
        btnRightCategoryProduct.isHidden = true
        btnLeftCategoryProduct.isHidden = true
        categoryProductView.isHidden = true
        if self.categoriesProduct.count>0{
            centerSliderProduct()
            ///Show Product
            if self.categoriesProduct.count>1 {
                btnRightCategoryProduct.isHidden = false
                btnLeftCategoryProduct.isHidden = false
            }
            categoryProductView.isHidden = false
        }
    }
    //
    fileprivate func centerSliderProduct(){
        if let flowLayout = self.categoryProduct.collectionViewLayout as? UICollectionViewFlowLayout {
            DispatchQueue.main.async{
                flowLayout.itemSize = CGSize(width: self.categoryProduct.bounds.size.width, height: self.categoryProduct.bounds.size.height)
                //--| Assing Values
                self.categoryProduct.collectionViewLayout = flowLayout
                self.categoryProduct.selectItem(at: IndexPath(row: self.indexCategoryProduct, section: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.right)
                self.categoryChanging()
            }
        }
    }
    // Change Category
    func categoryChanging(){
        let category = (self.categoriesProduct[self.indexCategoryProduct] as? NSDictionary)!
        if let iscategory = category["id"] as? String {
            self.cateoryProduct = String(iscategory)
            self.categoryProduct.reloadData()
        }
        self.productList.reloadData()
    }
    // Show Component
    func componentShow(_ sender: AnyObject) {
        if sender.tag > -1 { componentShowByIndex(sender.tag as Int) }
    }
    func componentShowByIndex(_ index: Int) {
        let productListCell = self.productList!.cellForItem(at: IndexPath(row: index, section: 0)) as! cellProductList
        productListCell.components.reloadData()
        modalToggle(productListCell.modal)
    }
    //---|   Product Selected !Online
    func productSelected(_ index:Int, _ refreshProductList:Bool = true){
        let productList = self.productList!.cellForItem(at: IndexPath(row: index, section: 0)) as! cellProductList
        let online = productList.checked.isHidden
        productList.checked.isHidden = !online
        var product = self.productsByCategory[self.cateoryProduct]![index] as! [String:AnyObject]
        let id:String = String(describing: product["id"]!)
        product["online"] = online as AnyObject
        self.productsByCategory[self.cateoryProduct]![index] = product as AnyObject
        DB.PRODUCTS.child("\(self.cateoryProduct)/\(id)/online").setValue(online)
    }
    //---|   Product Selected !Online
    func categorySelected(_ index:Int, _ refreshProductList:Bool = true){
        let categoryProductCell = self.categoryProduct!.cellForItem(at: IndexPath(row: index, section: 0)) as! cellCategoryProduct
        let online = categoryProductCell.checked.isHidden
        categoryProductCell.checked.isHidden = !online
        //---| Product List
        self.productList.isUserInteractionEnabled = online
        self.productList.alpha = online ? 1 : 0.2
        //---| Set Online
        self.categoriesProduct[index]["online"] = online as AnyObject
        //---|
        DB.PCATEGORIES.child("\(self.cateoryProduct)/online").setValue(online)
    }
    //-----------------------------------------------#
        //--|  Elements
    //-----------------------------------------------#
    // Right Arrow Clicked
    @IBAction func clickArrowRightCategoryProduct(_ sender: AnyObject) {
        indexCategoryProduct = indexCategoryProduct+1
        if indexCategoryProduct >= self.categoriesProduct.count { indexCategoryProduct = 0 }
        self.categoryChanging()
        categoryProduct.selectItem(at: IndexPath(row: indexCategoryProduct, section: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.right)
    }
    // Left Arrow Clicked
    @IBAction func clickArrowLeftCategoryProduct(_ sender: AnyObject) {
        indexCategoryProduct = indexCategoryProduct-1
        if indexCategoryProduct < 0 { indexCategoryProduct = self.categoriesProduct.count-1 }
        self.categoryChanging()
        categoryProduct.selectItem(at: IndexPath(row: indexCategoryProduct, section: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.left)
    }

}
