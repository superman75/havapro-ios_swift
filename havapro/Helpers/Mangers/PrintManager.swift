//
//  PrintManager.swift
//  havapro
//
//  Created by abdelghani on 15/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//


import UIKit
import Foundation

class PrintManager: UIViewController, Epos2PtrReceiveDelegate {


    let PRINT_PAGE_AREA_HEIGHT: Int = 500
    let PRINT_PAGE_AREA_WIDTH: Int = 500
    let PRINT_FONT_A_HEIGHT: Int = 24
    let PRINT_FONT_A_WIDTH: Int = 12
    let PRINT_BARCODE_HEIGHT_POS: Int = 70
    let PRINT_BARCODE_WIDTH_POS: Int = 110

    var cView = UIViewController()
    var printer_names:[Int:String] = [0: "customer", 1: "ketchen", 2:"fenced"]
    var printer: Epos2Printer?
    var textWarning: String=""

    var valuePrinterSeries: Epos2PrinterSeries = EPOS2_TM_M10
    var valuePrinterModel: Epos2ModelLang = EPOS2_MODEL_ANK
    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.print_ini(self)
    }
    //---|   Init
    func print_ini(_ pView:UIViewController){
        self.cView = pView
    }
    //---|
    func print_alert(_ msg:String="Printing", _ show:Bool=false, _ title:String="Attenstion"){
        if show { cView.alert(title, msg) } else { print("-----------| print_alert |----------\n \(title) --> \(msg) |\n\n\n")}
    }
    public func print_status(_ name:String, _ lPrinters:[Epos2DeviceInfo]) -> [String:Any] {
        var re:[String:Any] = ["name" : "...", "status" : 0]
        if let info = self.g(app.settings, "peripheral.printer.\(name)") as? [String:Any]{
            re = ["name" : String(info["deviceName"]), "status" : 1]
            for print in lPrinters{
                if print.target == String(info["target"]) {
                    re["status"] = 2
                    break;
                }
            }
        }
        return re
    }
    func print_start(_ name:String="customer", _ alert:Bool=true ) -> Bool{
        for (tag, name) in self.printer_names { self.print_clear(name)  }
        if let info = self.g(app.settings, "peripheral.printer.\(name)") as? [String:Any], let target = info["target"] as? String {
            do{
                app.printers[name] = try Epos2Printer(printerSeries: Int32(String(info["deviceType"], "0").toInt()), lang: 0)
                app.printers[name]!.setReceiveEventDelegate(self)
                return true
            }catch _ { self.print_alert("Exception", alert) }
            self.print_clear(name) //---|   Clear
        }
        self.print_alert("Please Set Address Mac of your print", alert)

        return false
    }
    //---|   Connect
    func print_connect(_ name:String="customer", _ alert:Bool=true) -> String{
        // var retu = self.print_statusd(name, false)
        // print("-----------| print_connect |----------\n retu --> \(retu) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        // if retu != "connected" {
            if app.printers[name] != nil, let info = self.g(app.settings, "peripheral.printer.\(name)") as? [String:Any], let target = info["target"] as? String {
            // if let info = self.g(app.settings, "peripheral.printer.\(name)") as? [String:Any], let target = info["target"] as? String {
                // do{
                //     app.printers[name] = try Epos2Printer(printerSeries: Int32(String(info["deviceType"], "0").toInt()), lang: 0)
                //     app.printers[name]!.setReceiveEventDelegate(self)
                    // // print("-----------| initializePrinterObject |----------\n printer --> \(printer) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
                    // return true
                    // print("-----------| print_connect \(target) |----------\n \(name) --> \(info) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
                    // // print("-----------| print_connectPrinter |----------\n result --> \(result) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
                    if EPOS2_SUCCESS.rawValue == nil {
                        self.print_alert("EPOS2_SUCCESS.rawValue", alert)
                        return "false"
                    }
                    if app.printers[name] != nil, app.printers[name]!.connect(target, timeout:Int(EPOS2_PARAM_DEFAULT)) != EPOS2_SUCCESS.rawValue {
                        // app.printers[name]!.disconnect()
                        // if alert { MessageView.showErrorEpos(result, method:"connect") }
                        self.print_alert("connect IS OFFLINE", alert)
                        return "false"
                    }
                    if app.printers[name] != nil, app.printers[name]!.beginTransaction() != EPOS2_SUCCESS.rawValue {
                        // if alert { MessageView.showErrorEpos(result, method:"beginTransaction") }
                        app.printers[name]!.disconnect()
                        self.print_alert("beginTransaction disconnect", alert)
                        return "false"
                    }
                    if app.printers[name] == nil { return self.print_connect(name, alert) }
                    // self.print_alert("connected", alert)
                    return "connected"
                // }catch _ { self.print_alert("Exception connect", alert) }

            }
        //     else{
        //         self.print_alert("Please Set Address Mac of your print", alert)
        //         return "false"
        //     }
        // }
        // return retu
        self.print_alert("Please Set Address Mac of your print", alert)
        return "none"
    }
    //---|   Execut
    func print_execut(_ name:String="", _ alert:Bool=true) -> Bool {
        if print_connect(name, alert) == "connected" {
            let result = app.printers[name]!.sendData(Int(EPOS2_PARAM_DEFAULT))
            if result == EPOS2_SUCCESS.rawValue {
                // self.print_clear(name)
                // app.printers[name] = nil
                return true
            }
            self.print_alert("sendData EPOS2_PARAM_DEFAULT", alert)
            // self.print_clear(name)
            // app.printers[name]!.disconnect()
        }
        self.print_clear(name)
        return false
    }

    //---|
    func print_clear(_ name:String="") {
        print("-----------| print_clear |----------\n \(name) --> \(app.printers[name]) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        if app.printers[name] == nil { return }
        // app.printers[name]!.disconnect()
        try app.printers[name]!.clearCommandBuffer()
        try app.printers[name]!.setReceiveEventDelegate(nil)
        app.printers[name] = nil
    }

    //--| Get Setting
    func print_setting_get(_ key:String, _ defaul:AnyObject?=nil)-> AnyObject{
        if  let print_setting = app.profile["setting"] as? Dictionary<String, Dictionary<String, AnyObject>>, print_setting["print"] != nil, print_setting["print"]![key] != nil{
            return print_setting["print"]![key] as AnyObject
        }

        return defaul as AnyObject
    }

    //--|
    func updateButtonState(_ state: Bool) {
    }



    public func print_run(_ data:[String:Any]=[String:Any](), _ printname:String="customer", _ fun:String?=nil) -> Bool {
        self.textWarning = ""
        let fun = fun ?? printname
        if !initializePrinterObject(printname) {
            print("-----------| initializePrinterObject(printname) return false |---------- \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            return false
        }
        // else{
        //     print("False")
        //     dump(initializePrinterObject(printname))
        // }

        if !print_createReceiptData(data, fun) {
            print("-----------| print_createReceiptData(data, fun) return false |---------- \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            print_finalizePrinterObject()
            return false
        }

        if !print_printData(printname) {
            print("-----------| print_printData(printname) return false |---------- \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            print_finalizePrinterObject()
            return false
        }

        return true
    }

    public func print_runPrinterCouponSequence() -> Bool {
        self.textWarning = ""
        print("Here")
        if !initializePrinterObject() {
            return false
        }

        if !print_createCouponData() {
            print_finalizePrinterObject()
            return false
        }

        if !print_printData() {
            print_finalizePrinterObject()
            return false
        }

        return true
    }

    //---|
    func print_text_init(_ key:String, _ value:String, _ separator:String=" ", _ max:Int=48)->String{
        var back = ""
        let keycount = key.characters.count
        let valuecount = value.characters.count
        back+=key
        if keycount+valuecount<max{
            for _ in 1...(max-(keycount+valuecount)) { back+=separator }
        }else{
            back+="\n"
            if valuecount<max{for _ in 1...(max-valuecount) { back+=separator }}
        }
        return "\(back)\(value)\n"
    }
    func print_text_center(_ txt:String, _ separator:String="-", _ max:Int=48)->String{
        let count = max - txt.characters.count
        return String(repeating: separator, count: Int(count/2))+txt+String(repeating: separator, count: Int(count/2))+"\n"
    }
    func print_text_empty(_ separator:String="-", _ max:Int=48)->String{
        return String(repeating: separator, count: max)+"\n"
    }
    //---|   End Print
    func print_cut() -> Bool{
        // var result = printer!.addPageEnd()
        // if result != EPOS2_SUCCESS.rawValue {
        //     MessageView.showErrorEpos(result, method:"addPageEnd")
        //     return false
        // }
        var result = printer!.addCut(EPOS2_CUT_FEED.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addCut")
            return false
        }

        return true
    }
    //---|
    func print_about_action() -> Bool {
        //---|   Open
        var result = EPOS2_SUCCESS.rawValue
        let textData: NSMutableString = NSMutableString()
        //---|   Center
        result = printer!.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextAlign")
            return false;
        }
        //---|   Provider Name
        if let provider_name = app.profile["name"] as? String {
            result = printer!.addTextSize(2, height:2)
            result = printer!.addText("\(provider_name)\n\n\n")
            result = printer!.addTextSize(1, height:1)
        }
        //---|   Phone
        if let phone = app.profile["phone"] as? String {
            textData.append(print_text_init("Téléphone", phone))
        }

        //---|   Hello
        if let hello = self.print_setting_get("hello") as? String {
            textData.append(self.print_text_empty())
            textData.append("\(hello)\n")
        }

        //---|   Date & Hour
        textData.append(self.print_text_empty())
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let date = formatter.string(from: Date())
        formatter.dateFormat = "HH:mm"
        let hour = formatter.string(from: Date())
        textData.append(print_text_init(date, hour))
        //---|   Wifi
        if  let namewifi = self.g(app.settings, "peripheral.wifi.name") as? String, let pwdwifi = self.g(app.settings, "peripheral.wifi.pwd") as? String{
            textData.append(self.print_text_center("WIFI"))
            textData.append(print_text_init(namewifi, pwdwifi))
        }

        print("-----------| debug |----------\n debug --> ");debugPrint(textData );print(" |\n\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        result = printer!.addText(textData as String)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addText")
            return false;
        }
        return self.print_cut() //---|   Print End
    }
    func print_about(_ name:String="customer") -> Bool {
        // return self.print_run([String:Any](), name, "about")
        if self.print_start(name), let printer = app.printers[name] {
            //---|   Open
            var result = EPOS2_SUCCESS.rawValue
            let textData: NSMutableString = NSMutableString()
            //---|   Center
            result = printer.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addTextAlign")
                return false;
            }
            //---|   Provider Name
            if let provider_name = app.profile["name"] as? String {
                result = printer.addTextSize(2, height:2)
                result = printer.addText("\(provider_name)\n\n\n")
                result = printer.addTextSize(1, height:1)
            }
            //---|   Phone
            if let phone = app.profile["phone"] as? String {
                textData.append(print_text_init("Téléphone", phone))
            }

            //---|   Hello
            if let hello = self.print_setting_get("hello") as? String {
                textData.append(self.print_text_empty())
                textData.append("\(hello)\n")
            }

            //---|   Date & Hour
            textData.append(self.print_text_empty())
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let date = formatter.string(from: Date())
            formatter.dateFormat = "HH:mm"
            let hour = formatter.string(from: Date())
            textData.append(print_text_init(date, hour))
            //---|   Wifi
            if  let namewifi = self.g(app.settings, "peripheral.wifi.name") as? String, let pwdwifi = self.g(app.settings, "peripheral.wifi.pwd") as? String{
                textData.append(self.print_text_center("WIFI"))
                textData.append(print_text_init(namewifi, pwdwifi))
            }

            print("-----------| debug |----------\n debug --> ");debugPrint(textData );print(" |\n\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            result = printer.addText(textData as String)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addText")
                return false;
            }
            result = printer.addCut(EPOS2_CUT_FEED.rawValue)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addCut")
                return false
            }
            return self.print_execut(name)
        }
        return false
    }
    //---| drawer
    func print_drawer_action() -> Bool {
        //ouvrir le tirroir caisse
        var result = printer!.addPulse(EPOS2_PARAM_DEFAULT, time: EPOS2_PULSE_100.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPulse")
            return false
        }
        return true
    }
    func print_drawer(_ name:String="customer", _ alert:Bool=true) -> Bool {
        // return self.print_run([String:Any](), name, "drawen")
        if self.print_start(name) {
        // if self.print_connect(name) == "connected" {
            var result = app.printers[name]!.addPulse(EPOS2_PARAM_DEFAULT, time: EPOS2_PULSE_100.rawValue)
            if result != EPOS2_SUCCESS.rawValue {
                self.print_alert("EPOS2_PULSE_100", alert)
                // MessageView.showErrorEpos(result, method:"addPulse")
                return false
            }
            // self.print_connect(name)
            return self.print_execut(name)
            // if !print_isPrintable(app.printers[name]!.getStatus()) {
            //     self.print_alert("print_isPrintable", alert)
            //     // MessageView.show(print_makeErrorMessage(status))
            //     app.printers[name]!.disconnect()
            //     return false
            // }

            // result = app.printers[name]!.sendData(Int(EPOS2_PARAM_DEFAULT))
            // if result != EPOS2_SUCCESS.rawValue {
            //     self.print_alert("EPOS2_PARAM_DEFAULT", alert)
            //     printer!.disconnect()
            //     return false
            // }

            // return true
        }
        return false
    }
    //---|
    func print_transaction(_ data:[String:Any]) -> String {
        //---|   Rendu
        let text: NSMutableString = NSMutableString()
        if let transaction = data["transaction"] as? Dictionary<String, Any>{
            let rendered = String(transaction["rendered"], "0").toDouble()
            text.append(self.print_text_init("Payé", "\(String(String(transaction["total"], "0").toDouble()+rendered).format()) €"))
            for action in (transaction["actions"] as? Array<Dictionary<String, AnyObject>>)!{
                text.append(self.print_text_init(" - \(app.paymenttype[String(action["type"])]!)", "\(String(action["paid"], String(action["price"]))) €"))
            }
            if rendered > 0 { text.append(self.print_text_init("Rendu", "\(String(rendered).format()) €")) }
        }
        return text as String
    }
    //---|    Products Commande Or Bill
    func print_products(_ data:[String:Any]) -> String {
        let productsText: NSMutableString = NSMutableString()
        if let products = data["products"] as? [String:[String:Any]]{
            for (_, product) in products{
                productsText.append(self.print_text_init("\(String(product["qty"], "1").toInt()) x \(String(product["name"]))", String(product["price"], "0").format()+" €"))
                //---|   Has Component
                if let components = data["components_all"] as? [[String:Any]]{
                    for component in components{
                        productsText.append(self.print_text_init("    \(String(component["qty"], "1").toInt()) x \(String(component["name"]))", String(component["price"], "0").format()+" €"))
                    }
                }
            }
        }
        return productsText as String
    }
    public func print_createReceiptData(_ data:[String:Any], _ type:String="") -> Bool {
        if type == "customer" {
            return self.print_customer(data)
        } else if type == "ketchen" {
            return self.print_ketchen(data)
        } else if type == "fenced" {
            return self.print_fenced(data)
        } else if type == "drawen" {
            return self.print_drawer_action()
        } else if type == "about" {
            return self.print_about_action()
        }
        return true
    }
    //---|   Customer
    func print_customer(_ data:[String:Any], _ name:String="customer") -> Bool {
        if self.print_start(name), let printer = app.printers[name] {
            let barcodeWidth = 2
            let barcodeHeight = 100

            var debug:String = ""

            var result = EPOS2_SUCCESS.rawValue
            let textData: NSMutableString = NSMutableString()

            //---|   Center
            result = printer.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addTextAlign")
                return false;
            }


            //---|   Ticket N
            if  let ticket_id = self.g(data, "index") as? String{
                result = printer.addTextSize(2, height:2)
                result = printer.addText("\(ticket_id)\n")
                result = printer.addTextSize(1, height:1)
                debug.append("\(ticket_id)\n")
                textData.append(self.print_text_empty())
            }
            //---|   Address && Phone
            if let customer_info =  data["customer_info"] as? [String:Any]{
                //---|   Has
                if  let ordertype = data["type"] as? String, ordertype != "on_shop" , let ordertype_str = app.ordertype[ordertype] as? String{
                    textData.append("\(ordertype_str)\n")
                    textData.append(self.print_text_empty())
                }
                textData.append(self.print_text_init("NOM", "TEL", "-"))
                let name = String(customer_info["phone"])
                let phone = String(customer_info["phone"])
                textData.append(print_text_init(name, phone))
                //---|   Address
                if let delivery_address = customer_info["delivery_address"] as? String{
                    textData.append(self.print_text_empty(" "))
                    textData.append(delivery_address)
                }
            }

            debug.append(textData as String)

            //---|   SetClean
            result = printer.addText(textData as String)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addText")
                return false;
            }
            textData.setString("")

            //---|   Wifi
            if  let namewifi = self.g(app.settings, "peripheral.wifi.name") as? String, let pwdwifi = self.g(app.settings, "peripheral.wifi.pwd") as? String{
                textData.append(self.print_text_center("WIFI"))
                textData.append(print_text_init(namewifi, pwdwifi))
            }


            //---|   Hello
            if let hello = self.print_setting_get("hello") as? String {
                textData.append(self.print_text_empty())
                textData.append("\(hello)\n")
            }

            //---|   Date & Hour
            textData.append(self.print_text_empty())
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let date = formatter.string(from: Date())
            formatter.dateFormat = "HH:mm"
            let hour = formatter.string(from: Date())
            textData.append(print_text_init(date, hour))
            debug.append(textData as String)
            //---|   SetClean
            result = printer.addText(textData as String)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addText")
                return false;
            }
            textData.setString("")

            //---|   Products List
            let productsText = self.print_products(data as! [String : Any])
            if !String(productsText).empty() {
                textData.append(self.print_text_empty())
                textData.append(print_text_init("Produit", "Prix"))
                textData.append("\n\n")
                textData.append(productsText)
                debug.append(productsText)

                result = printer.addText(textData as String!)
                if result != EPOS2_SUCCESS.rawValue {
                    MessageView.showErrorEpos(result, method:"addText")
                    return false;
                }
                textData.setString("")
            }


            //---|   Total
            let total = self.g(data, "total")
            if total != nil{
                result = printer.addTextSize(2, height:2)
                if result != EPOS2_SUCCESS.rawValue {
                    MessageView.showErrorEpos(result, method:"addTextSize")
                    return false
                }
                debug.append(print_text_init("TOTAL", String(total)))
                result = printer.addText(print_text_init("TOTAL", String(total)))
                if result != EPOS2_SUCCESS.rawValue {
                    MessageView.showErrorEpos(result, method:"addText")
                    return false;
                }

                result = printer.addTextSize(1, height:1)
                if result != EPOS2_SUCCESS.rawValue {
                    MessageView.showErrorEpos(result, method:"addTextSize")
                    return false;
                }
            }


            result = printer.addFeedLine(1)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addFeedLine")
                return false;
            }

            //---|   End : Back Monay
            let endText = self.print_transaction(data as! [String : Any])
            if !String(endText).empty() {
                 debug.append(endText)
                result = printer.addText(endText)
                if result != EPOS2_SUCCESS.rawValue {
                    MessageView.showErrorEpos(result, method:"addText")
                    return false;
                }
            }

            if let phone = app.profile["phone"] {
                textData.append(self.print_text_empty())
                textData.append("\(phone)\n")

                debug.append(textData as String)

                result = printer.addText(textData as String)
                if result != EPOS2_SUCCESS.rawValue {
                    MessageView.showErrorEpos(result, method:"addText")
                    return false;
                }
                textData.setString("")
            }

            //imprimer le contenu du ticket dans la console
            print("-----------| debug |----------\n debug --> \(debug) \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")

            if (print_setting_get("barcode", false as AnyObject?) as? Bool)!{
                result = printer.addFeedLine(2)
                if result != EPOS2_SUCCESS.rawValue {
                    MessageView.showErrorEpos(result, method:"addFeedLine")
                    return false
                }
                result = printer.addBarcode("01209457",
                    type:EPOS2_BARCODE_CODE39.rawValue,
                    hri:EPOS2_HRI_BELOW.rawValue,
                    font:EPOS2_FONT_A.rawValue,
                    width:barcodeWidth,
                    height:barcodeHeight)
                if result != EPOS2_SUCCESS.rawValue {
                    MessageView.showErrorEpos(result, method:"addBarcode")
                    return false
                }
            }

            //ouvrir le tirroir caisse
            result = printer.addPulse(EPOS2_PARAM_DEFAULT, time: EPOS2_PULSE_100.rawValue)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addPulse")
                return false
            }
            result = printer.addCut(EPOS2_CUT_FEED.rawValue)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addCut")
                return false
            }
            return self.print_execut(name)
        }
        return false
    }
    //---|    Ketchen
    func print_ketchen(_ data:[String:Any], _ name:String="ketchen") -> Bool {
        // return print_drawer(name)
        if self.print_start(name), let printer = app.printers[name] {
            let barcodeWidth = 2
            let barcodeHeight = 100


            var result = EPOS2_SUCCESS.rawValue
            let textData: NSMutableString = NSMutableString()

            //---|   Center
            result = printer.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addTextAlign")
                return false;
            }
            //---|   Ticket N
            if  let ticket_id = self.g(data, "index") as? String{
                result = printer.addTextSize(2, height:2)
                result = printer.addText("\(ticket_id)\n")
                result = printer.addTextSize(1, height:1)
            }
            //---|   Commande Type
            if  let ordertype = self.g(data, "type") as? String, let ordertype_str = app.ordertype[ordertype] as? String{
                textData.append("\(ordertype_str)\n")
            }
            //---|   Products List
            if let productsText = data["productsText"] as? String{
                textData.append(self.print_text_empty())
                textData.append(print_text_init("Produit", "Qte"))
                textData.append("\n\n")
                textData.append(productsText)
            }
            //---|   Assign To Print
            result = printer.addText(textData as String!)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addText")
                return false;
            }
            print("-----------| textData |----------\n textData --> ");debugPrint(textData);print(" |\n\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            result = printer.addCut(EPOS2_CUT_FEED.rawValue)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addCut")
                return false
            }
            return self.print_execut(name)
        }
        return false
    }
    //---|   Fenced
    func print_fenced(_ data:[String:Any]) -> Bool {
        // return self.print_cut() //---|   Print End
        return false
    }
    public func print_createCouponData() -> Bool {
        let barcodeWidth = 2
        let barcodeHeight = 64

        var result = EPOS2_SUCCESS.rawValue

        if printer == nil {
            return false
        }

        let coffeeData = UIImage(named: "coffee.png")
        let wmarkData = UIImage(named: "wmark.png")

        if coffeeData == nil || wmarkData == nil {
            return false
        }

        result = printer!.addPageBegin()
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPageBegin")
            return false
        }

        result = printer!.addPageArea(0, y:0, width:PRINT_PAGE_AREA_WIDTH, height:PRINT_PAGE_AREA_HEIGHT)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPageArea")
            return false
        }

        result = printer!.addPageDirection(EPOS2_DIRECTION_TOP_TO_BOTTOM.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPageDirection")
            return false
        }

        result = printer!.addPagePosition(0, y:Int(coffeeData!.size.height))
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPagePosition")
            return false
        }

        result = printer!.add(coffeeData, x:0, y:0,
            width:Int(coffeeData!.size.width),
            height:Int(coffeeData!.size.height),
            color:EPOS2_PARAM_DEFAULT,
            mode:EPOS2_PARAM_DEFAULT,
            halftone:EPOS2_PARAM_DEFAULT,
            brightness:3,
            compress:EPOS2_PARAM_DEFAULT)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addImage")
            return false
        }

        result = printer!.addPagePosition(0, y:Int(wmarkData!.size.height))
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPagePosition")
            return false
        }

        result = printer!.add(wmarkData, x:0, y:0,
            width:Int(wmarkData!.size.width),
            height:Int(wmarkData!.size.height),
            color:EPOS2_PARAM_DEFAULT,
            mode:EPOS2_PARAM_DEFAULT,
            halftone:EPOS2_PARAM_DEFAULT,
            brightness:Double(EPOS2_PARAM_DEFAULT),
            compress:EPOS2_PARAM_DEFAULT)

        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addImage")
            return false
        }

        result = printer!.addPagePosition(PRINT_FONT_A_WIDTH * 4, y:(PRINT_PAGE_AREA_HEIGHT / 2) - (PRINT_FONT_A_HEIGHT * 2))
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPagePosition")
            return false
        }

        result = printer!.addTextSize(3, height:3)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextSize")
            return false
        }

        result = printer!.addTextStyle(EPOS2_PARAM_DEFAULT, ul:EPOS2_PARAM_DEFAULT, em:EPOS2_TRUE, color:EPOS2_PARAM_DEFAULT)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextStyle")
            return false
        }

        result = printer!.addTextSmooth(EPOS2_TRUE)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextSmooth")
            return false
        }

        result = printer!.addText("FREE Coffee\n")
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addText")
            return false
        }

        result = printer!.addPagePosition((PRINT_PAGE_AREA_WIDTH / barcodeWidth) - PRINT_BARCODE_WIDTH_POS, y:Int(coffeeData!.size.height) + PRINT_BARCODE_HEIGHT_POS)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPagePosition")
            return false
        }

        result = printer!.addBarcode("01234567890", type:EPOS2_BARCODE_UPC_A.rawValue, hri:EPOS2_PARAM_DEFAULT, font: EPOS2_PARAM_DEFAULT, width:barcodeWidth, height:barcodeHeight)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addBarcode")
            return false
        }
        return self.print_cut()
    }

    public func print_printData(_ printname:String="") -> Bool {
        var status: Epos2PrinterStatusInfo?

        if printer == nil {
            return false
        }

        if !print_connectPrinter(printname) {
            print("-----------| print_connectPrinter(printname) \n")
            return false
        }

        status = printer!.getStatus()
        print("-----------| print_printData |----------\n status --> \(status) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        print_dispPrinterWarnings(status)

        if !print_isPrintable(status) {
            print("-----------| !print_isPrintable(status) ")
            MessageView.show(print_makeErrorMessage(status))
            printer!.disconnect()
            return false
        }
        // print("------| printer ------------")
        // dump(printer)

        let result = printer!.sendData(Int(EPOS2_PARAM_DEFAULT))
        if result != EPOS2_SUCCESS.rawValue {
            print("-----------| !printer!.sendData(Int(EPOS2_PARAM_DEFAULT)) ")
            MessageView.showErrorEpos(result, method:"sendData")
            printer!.disconnect()
            return false
        }

        return true
    }

    // public func initializePrinterObject(_ type:String="") -> Bool {

    //     do{
    //         printer = try Epos2Printer(printerSeries: (self.print_setting_get("serie", 0 as AnyObject) as? Int32)!, lang: (self.print_setting_get("lang", 0 as AnyObject) as? Int32)!)
    //         // print("-----------| printer |----------\n printer --> \(printer) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
    //     }catch _ {
    //         return false
    //     }
    //     // printer = Epos2Printer(printerSeries: (self.print_setting_get("serie", 0 as AnyObject) as? Int32)!, lang: (self.print_setting_get("lang", 0 as AnyObject) as? Int32)!)

    //     if printer == nil {
    //         return false
    //     }
    //     printer!.setReceiveEventDelegate(self)
    //     dump(printer)
    //     return true
    // }
    public func initializePrinterObject(_ type:String="") -> Bool {
        if let info = self.g(app.settings, "peripheral.printer.\(type)") as? [String:Any] {
            // print("-----------| initializePrinterObject |----------\n info --> \(info) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            do{
                printer = try Epos2Printer(printerSeries: Int32(String(info["deviceType"], "0").toInt()), lang: 0)
                printer!.setReceiveEventDelegate(self)
                // print("-----------| initializePrinterObject |----------\n printer --> \(printer) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
                return true
            }catch _ { print("-----------| initializePrinterObject |----------\n err --> | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")}
        }
        print("-----------| initializePrinterObject |----------\n No Data --> \(type) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        return false
    }

    public func print_finalizePrinterObject(_ printname:String="") {
        if printer == nil {
            return
        }

        printer!.clearCommandBuffer()
        printer!.setReceiveEventDelegate(nil)
        printer = nil
    }

    public func print_connectPrinter(_ printname:String="", _ alert:Bool=true) -> Bool {
        // if let target = self.print_setting_get("id") as? String {
        if let info = self.g(app.settings, "peripheral.printer.\(printname)") as? [String:Any], let target = info["target"] as? String {
            var result: Int32 = EPOS2_SUCCESS.rawValue
            // print("-----------| print_connectPrinter |----------\n result --> \(result) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            if result == nil { return false }
            result = printer!.connect(target, timeout:Int(EPOS2_PARAM_DEFAULT))
            // result = printer!.connect(target, timeout:Int(EPOS2_PARAM_DEFAULT))
            if result != EPOS2_SUCCESS.rawValue {
                if alert { MessageView.showErrorEpos(result, method:"connect") }
                print("-----------|  connect : \(result) |---------")
                return false
            }

            result = printer!.beginTransaction()
            if result != EPOS2_SUCCESS.rawValue {
                if alert { MessageView.showErrorEpos(result, method:"beginTransaction") }
                print("-----------|  beginTransaction : \(result) |---------")
                printer!.disconnect()
                return false
            }
        }else{
            if alert { self.alert("Attenstion", "Please Set Address Mac of your print") }
            print("-----------|  Please Set Address Mac of your print |---------")
            return false
        }
        return true
    }


    public func print_disconnectPrinter() {
        var result: Int32 = EPOS2_SUCCESS.rawValue

        if printer == nil {
            return
        }

        result = printer!.endTransaction()
        if result != EPOS2_SUCCESS.rawValue {
            DispatchQueue.main.async(execute: {
                MessageView.showErrorEpos(result, method:"endTransaction")
            })
        }

        result = printer!.disconnect()
        if result != EPOS2_SUCCESS.rawValue {
            DispatchQueue.main.async(execute: {
                MessageView.showErrorEpos(result, method:"disconnect")
            })
        }

        print_finalizePrinterObject()
    }
    public func print_isPrintable(_ status: Epos2PrinterStatusInfo?) -> Bool {
        if status == nil {
            return false
        }

        if status!.connection == EPOS2_FALSE {
            return false
        }
        else if status!.online == EPOS2_FALSE {
            return false
        }
        else {
            // print available
        }
        return true
    }

    open func onPtrReceive(_ printerObj: Epos2Printer!, code: Int32, status: Epos2PrinterStatusInfo!, printJobId: String!) {
        if code != 0 { MessageView.showResult(code, errMessage: print_makeErrorMessage(status)) }

        print_dispPrinterWarnings(status)
        updateButtonState(true)

        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            self.print_disconnectPrinter()
        })
    }

    public func print_dispPrinterWarnings(_ status: Epos2PrinterStatusInfo?) {
        if status == nil {
            return
        }

        if status!.paper == EPOS2_PAPER_NEAR_END.rawValue {
            self.alert("Attenstion", NSLocalizedString("warn_receipt_near_end", comment:""))
        }

        if status!.batteryLevel == EPOS2_BATTERY_LEVEL_1.rawValue {
            self.alert("Attenstion", NSLocalizedString("warn_battery_near_end", comment:""))
        }
    }

    public func print_makeErrorMessage(_ status: Epos2PrinterStatusInfo?) -> String {
        let errMsg = NSMutableString()
        if status == nil {
            return ""
        }

        if status!.online == EPOS2_FALSE {
            errMsg.append(NSLocalizedString("err_offline", comment:""))
        }
        if status!.connection == EPOS2_FALSE {
            errMsg.append(NSLocalizedString("err_no_response", comment:""))
        }
        if status!.coverOpen == EPOS2_TRUE {
            errMsg.append(NSLocalizedString("err_cover_open", comment:""))
        }
        if status!.paper == EPOS2_PAPER_EMPTY.rawValue {
            errMsg.append(NSLocalizedString("err_receipt_end", comment:""))
        }
        if status!.paperFeed == EPOS2_TRUE || status!.panelSwitch == EPOS2_SWITCH_ON.rawValue {
            errMsg.append(NSLocalizedString("err_paper_feed", comment:""))
        }
        if status!.errorStatus == EPOS2_MECHANICAL_ERR.rawValue || status!.errorStatus == EPOS2_AUTOCUTTER_ERR.rawValue {
            errMsg.append(NSLocalizedString("err_autocutter", comment:""))
            errMsg.append(NSLocalizedString("err_need_recover", comment:""))
        }
        if status!.errorStatus == EPOS2_UNRECOVER_ERR.rawValue {
            errMsg.append(NSLocalizedString("err_unrecover", comment:""))
        }

        if status!.errorStatus == EPOS2_AUTORECOVER_ERR.rawValue {
            if status!.autoRecoverError == EPOS2_HEAD_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_head", comment:""))
            }
            if status!.autoRecoverError == EPOS2_MOTOR_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_motor", comment:""))
            }
            if status!.autoRecoverError == EPOS2_BATTERY_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_battery", comment:""))
            }
            if status!.autoRecoverError == EPOS2_WRONG_PAPER.rawValue {
                errMsg.append(NSLocalizedString("err_wrong_paper", comment:""))
            }
        }
        if status!.batteryLevel == EPOS2_BATTERY_LEVEL_0.rawValue {
            errMsg.append(NSLocalizedString("err_battery_real_end", comment:""))
        }

        return errMsg as String
    }

}
