//
//  SettingManager.swift
//  havapro
//
//  Created by HavaPRO on 27/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit
public struct SettingManager {
    static var def:[String:[String:Any]] = [
        "peripheral" : ["printer" : [String:Any](), "wifi" : [String:Any]()]
    ]
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   Init
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    static func ini(){
        DB.SETTINGS.observe(.value, with: { (snap) in
            app.settings = SettingManager.def
            if snap.exists() {
                app.settings.merge((snap.value as? [String : [String : Any]])!)
            }
            // print("-----------| app.settings |----------\n app.settings --> ");debugPrint(app.settings);print(" | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            NotificationCenter.default.post(name: BROADCAST.SETTING.UPDATED, object: nil)
        })
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   Set
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    static func update(_ section:String, _ subsection:String, _ key:String, _ value:Any){
        if app.settings[section] == nil { app.settings[section] = [subsection : [String:Any]()] }
        if app.settings[section]![subsection] == nil { app.settings[section]![subsection] = [String:Any]() }
        var setting = (app.settings[section]![subsection]! as? [String:Any])!
        setting[key] = value
        app.settings[section]![subsection]! = setting
        DB.SETTINGS.child("\(section)/\(subsection)/\(key)").setValue(value)
    }
}
