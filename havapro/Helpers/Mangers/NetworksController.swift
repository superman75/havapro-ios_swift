////
//  NetworksController.swift
//  havapro
//
//  Created by abdelghani on 23/11/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
// import SwiftInstagram
import FBSDKLoginKit
import GoogleSignIn
import GoogleAPIClientForREST
//import SafariService
import SimpleAuth
import Alamofire

class NetworksController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    //---|   VARS
    private let service = GTLRYouTubeService()
    public let Instagram_api_url = "https://api.instagram.com/v1/"
    /*-----------------------------------------------*\
       #--| Facebook
    \*-----------------------------------------------*/
        //---|   Login
    func fb_login(_ callback:((_ result:[String:[String:Any]]) -> Void)?=nil, _ scopes:[String]=["manage_pages", "publish_actions"], _ needed:String?=nil){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        let needed:String = (needed == nil ? scopes[0] : needed!)
        // fbLoginManager.logIn(withReadPermissions: scopes, from: self) { (result, error) in
        fbLoginManager.logIn(withPublishPermissions: scopes, from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains(needed)) {
                        // print("#----\n#--| FBSDKAccessToken.current() --: \(FBSDKAccessToken.current().tokenString!) |--#\n")
                        //---|   get Profile
                        self.fb_request({(re) in
                            if let result = re["result"] as? Dictionary<String, Any> { self.f_save(re, callback) }

                        }, "me", ["fields": "id, name, accounts, groups"])
                        fbLoginManager.logOut()
                    }
                }
            }else{ /*print("#----\n#--| Facebook Error Login --: \(["error" : error, "result" : result]) |--#\n")*/}
        }
    }
    //---|   FACEBOOK : Save In DB
    func f_save(_ re:Dictionary<String, Any>, _ callback:((_ result:[String:[String:Any]]) -> Void)?=nil){
        let profile_id = String(self.g(re, "result.id"))
        var f_networks = [String:[String:Any]]()
        f_networks[profile_id] = [
            "access_token" : String(re["token"]!),
            "name" : String(self.g(re, "result.name")),
            "id" : profile_id,
            "type" : "profile",
            "picture" : "https://graph.facebook.com/\(profile_id)/picture?type=large"
        ]
        //---|   Save Facebook in DB
        DB.NETWORKS.child("facebook/\(profile_id)").setValue(f_networks[profile_id])
        //---|   Pages
        for page in (self.g(re, "result.accounts.data")  as? Array<Dictionary<String, Any>>)!{
            let page_id = String(page["id"])
            f_networks[page_id] = page
            f_networks[page_id]!["type"] = "page"
            f_networks[page_id]!["picture"] = "https://graph.facebook.com/\(page_id)/picture?type=large"
            //---|   Save Facebook in DB
            DB.NETWORKS.child("facebook/\(page_id)").setValue(f_networks[page_id])
        }
        if callback != nil { callback!(f_networks) }
    }
        //---|   REQUEST
    func fb_request(_ callback:@escaping ((_ result:Dictionary<String, Any>) -> Void), _ path:String="me", _ params:Dictionary<AnyHashable, Any>, _ token:String?=nil){
        let token = (token != nil ? token : (FBSDKAccessToken.current() != nil ? FBSDKAccessToken.current().tokenString! : token))
        FBSDKGraphRequest(graphPath: path, parameters: params, tokenString: token, version: nil, httpMethod: nil).start { (connection, result, error) in
            callback(["error" : error, "result" : result, "connection" : connection, "token" : token])
            if error != nil{
                // print("#----\n#--| Facebook Error Request --: \(["error" : error, "result" : result, "connection" : connection, "token" : token]) |--#\n")
                // self.fb_login()
            }
        }
    }
    /*-----------------------------------------------*\
        #--| Google
    \*-----------------------------------------------*/
    func g_login(_ scopes:[String]?=nil, _ silently:Bool=false){
        //---|   Configure Google Sign-in. GIDSignIn.sharedInstance().hasAuthInKeychain()
        let scopes:[String] = (scopes != nil ? scopes! : ["plus.login", "plus.me", "youtube", "youtubepartner", "youtubepartner-channel-audit", "youtube.readonly", "youtube.force-ssl", "youtube.upload", "plus.business.manage", "plus.stream.write", "plus.stream.read", "plus.circles.write", "plus.circles.read"])
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        //--| offline force
        for scope in scopes{ GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/\(scope)") }
        if silently {
            GIDSignIn.sharedInstance().signInSilently()
        }else{
            GIDSignIn.sharedInstance().signIn()
        }

    }
    //---| Logged Google & Youtube
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            //---|   PROFILE
            let profile_id = user.userID!
            var g_networks = [String:[String:Any]]()
            var y_networks = [String:[String:Any]]()
            //---|   Google Profile
            g_networks[profile_id] = [
                "id" : user.userID!,
                "name" : user.profile.name,
                "access_token" : user.authentication.accessToken,
                "refresh_token" : user.authentication.refreshToken,
                "id_token" : user.authentication.idToken,
                "picture" : user.profile.imageURL(withDimension: 60).absoluteString
            ]
            g_networks[profile_id]!["type"] = "profile"
            DB.NETWORKS.child("google/\(profile_id)").setValue(g_networks[profile_id])
            //---|   Has Youtube In Permission
            if signIn.scopes.contains(where: { String($0).matches("youtube").count > 0 }) {
                //---|   Youtube Profile
                return self.y_channels({(re) in
                    if  let y_result = re["channel"] as? Dictionary<String, Any>{
                        y_networks[profile_id] = g_networks[profile_id]!
                        y_networks[profile_id]!["name"] = y_result["name"] as! String
                        y_networks[profile_id]!["channelId"] = y_result["id"] as! String
                        y_networks[profile_id]!["picture"] = y_result["picture"] as! String
                        DB.NETWORKS.child("youtube/\(profile_id)").setValue(y_networks[profile_id])
                    }
                    self.g_login_callback(["youtube" : y_networks, "google" : g_networks])
                }, user.authentication.accessToken as! String)
            }
            self.g_login_callback(["youtube" : y_networks, "google" : g_networks])
        } else {
            // print("----\n---\n--\nGIDSignIn error\n-----");debugPrint(error);print("--\n---\n----\n")
            if error!.localizedDescription == "Invalid Credentials" { self.g_login(nil, true) }
        }
    }
    //---|   Login Callback
    func g_login_callback(_ re_google_youtube:[String:[String:[String:Any]]]) { }
    //---|   Channels Youtube ID
    func y_channels( _ callback: @escaping ((_ result:[String:Any]) -> Void),_ access_token:String){
        let query = GTLRYouTubeQuery_ChannelsList.query(withPart: "snippet,contentDetails,statistics")
        query.mine = true
        //---|   Add Token
        query.additionalURLQueryParameters = ["access_token" : access_token]
        //---|  Execute
        service.executeQuery(query) { (ticket, response, error) in
            var data:[String:Any] = ["error" : error]
            if let r = response as? GTLRYouTube_ChannelListResponse, let aItems = r.items, !aItems.isEmpty {
                let items = aItems[0]
                data = [
                    "result" : items.json!,
                    "statistics" : items.statistics!.json!,
                    "channel" : ["id" : items.identifier!, "picture" : self.g(items.snippet!.thumbnails!.json, "default.url"), "name" : items.snippet!.title]
                ]
            }
            callback(data)
            //---|   Show Error
            if error != nil {
                //---|   Refresh Token
                if error!.localizedDescription == "Invalid Credentials" { self.g_login(nil, true) }
                // print("----\n---\n- y_channels Error : \n-----");debugPrint(error!.localizedDescription);print("--\n---\n----\n")
                // print("----\n---\n- y_channels Ticket : \n-----");debugPrint(ticket);print("--\n---\n----\n")
            }
        }
    }
    // //---|
    // func y_statics_callback(_ account: Dictionary<String, Any>, _ callback:@escaping ((_ result:Dictionary<String, Any>) -> Void)){
    //     //---|   Refresh Token Token
    //     self.g_login(nil, true)
    //     // let query = GTLRYouTubeQuery_SubscriptionsList.query(withPart: "snippet,contentDetails")
    //     // query.channelId = String(account["channelId"])
    //     let query = GTLRYouTubeQuery_ChannelsList.query(withPart: "snippet,contentDetails,statistics")
    //     query.mine = true
    //     //query.identifier = String(account["channelId"])

    //     //---|   Add Token
    //     query.additionalURLQueryParameters = ["access_token" : String(account["access_token"])]
    //     //---|  Execute
    //     service.executeQuery(query) { (ticket, response, error) in
    //         if let r = response as? GTLRYouTube_ChannelListResponse, let items = r.items, !items.isEmpty{
    //             print("-----\n#-----\nitems[0].statistics!.json\n-----");debugPrint(items[0].statistics!.json);print("-----\n#-----\n")
    //             print("-----\n#-----\r.json\n-----");debugPrint(r.json);print("-----\n#-----\n")
    //             return callback(["result" : items[0].statistics!.json])
    //         }
    //         print("#----\n#--| GTLRYouTubeQuery_SubscriptionsList error --: \(error) |--#\n")
    //         print("#----\n#--| GTLRYouTubeQuery_SubscriptionsList ticket --: \(ticket) |--#\n")
    //         return callback(["error" : error])
    //     }
    // }
    /*-----------------------------------------------*\
        #--| Instagram https://github.com/calebd/SimpleAuth/issues/118
    \*-----------------------------------------------*/
    func i_login(_ callback:((_ result:[String:[String:Any]]) -> Void)?=nil, _ scopes:[String]=["basic", "comments", "likes", "relationships"]){
        //--| Init
        let i_client_id = Bundle.main.object(forInfoDictionaryKey: "InstagramClientId")
        SimpleAuth.configuration()["instagram"] = ["client_id": i_client_id!, SimpleAuthRedirectURIKey: "http://localhost:5000/callback"]
        SimpleAuth.authorize("instagram", options: ["scope" : scopes]) { (result, error) in
            if error == nil { self.i_save(result as! Dictionary<String, Any>, callback) }
            else{ print("#----\n#--| error --: \(error) |--#\n") }
        }
    }
    //---|   INSTAGRAM : Save In DB
    func i_save(_ re:Dictionary<String, Any>, _ callback:((_ result:[String:[String:Any]]) -> Void)?=nil){
        let profile_id = re["uid"] as! String
        let user_info = (re["user_info"] as? Dictionary<String, String>)!
        let i_network = [
            "access_token" : (re["credentials"] as? Dictionary<String, Any>)!["token"],
            "name" : user_info["username"],
            "id" : profile_id,
            "type" : "profile",
            "picture" : user_info["image"]
        ]
        //---|   Save Instagram in DB
        DB.NETWORKS.child("instagram/\(profile_id)").setValue(i_network)
        if callback != nil { callback!([profile_id : i_network]) }
    }
    //--|
    func i_request(_ callback:@escaping ((_ result:Dictionary<String, Any>) -> Void), _ path:String="/users/id", _ params:Parameters, _ errorback: ((_ result:Dictionary<String, Any>?) -> Void)?=nil){
        Alamofire.request("\(Instagram_api_url)\(path)", parameters: params).responseJSON { (response) in
            callback(["error" : response.error, "result" : self.g(response.result.value, "data", [String:Any]()) as! [String:Any]])
        }
    }
}
