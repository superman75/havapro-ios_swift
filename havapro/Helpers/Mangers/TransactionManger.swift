//
//  TransactionManager.swift
//  havapro
//
//  Created by abdelghani on 10/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class TransactionManger : AppViewController{
    var keep_data = false
    //---|
    func transaction_get(_ start:Int64, _ end:Int64, _ callback : @escaping ((_ re:[String:[String:Any]]) -> ()) ){
        DB.TRANSACTIONS.queryOrdered(byChild: "created").queryStarting(atValue: start).queryEnding(atValue: end).observe(.value, with: {
            (snap) in
            // print("-----------| start |----------\n start --> \(Date(String(start), "timestamp").string()) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            // print("-----------| end |----------\n end --> \(Date(String(end), "timestamp").string()) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            // print("-----------| snap.value |----------\n snap.value --> \(snap.value) \n")
            if snap.exists() {
                callback((snap.value as? [String:[String:Any]])!)
                return
            }
            callback([String:[String:Any]]())
        })
    }
    //---|   Total
    func transaction_total(_ re:[String:[String:Any]], _ path:String="total") -> Double{
        var total=0.0
        for (id, item) in re { total += String(self.g(item, path), "0").toDouble() }
        return total
    }
    //---|
    func transaction_by(_ start:Int64, _ end:Int64, _ callback : @escaping ((_ re:[String:[String:[String:Any]]]) -> ()) ){
        self.transaction_get(start, end, { (re) in
            var mTrs:[String:[String:[String:Any]]] = [String:[String:[String:Any]]]()
            var totals:[String:[String:Any]] = [String:[String:Any]]()
            for (id, item) in re{
                //---|   Init Date
                let date = Date(String(item["created"]!), "timestamp").string("dd/MM/yyyy")
                    //---|
                if mTrs[date] == nil {
                    mTrs[date] = [String:[String:Any]]()
                    totals[date] = [ "date" : date, "cb" : 0.0, "cash": 0.0, "ticket": 0.0, "tva": 0.0, "total": 0.0 ]
                }
                mTrs[date]![id] = item
                totals[date]!["tva"] = String(totals[date]!["tva"]).toDouble()+String(item["tva"], "0").toDouble()
                totals[date]!["total"] = String(totals[date]!["total"]).toDouble()+String(item["total"], "0").toDouble()
                if let actions = item["actions"] as? [[String:Any]]{
                    for action in actions{
                        let type = String(action["type"]!).lowercased()
                        totals[date]![type] = String(totals[date]![type], "0").toDouble()+String(action["price"]!).toDouble()
                    }
                }
            }
            callback(["totals" : totals, "transactions" : mTrs])
        })
    }
}
