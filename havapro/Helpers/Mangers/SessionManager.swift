//
//  SessionManager.swift
//  havapro
//
//  Created by abdelghani on 10/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class SessionManager: NSObject {
    //---|   Init Session
    func session_init(){
        DB.SESSION.queryOrderedByKey().queryLimited(toLast: 1).observe(.value, with: { (snap) in
            app.profile["session_starting"] = nil //---|   timestamp Int64
            if let session = (snap.value as? [String : [String : Any]])?.current(), String(session["start"]).bool {
                self.session_started(session["time"])
            }
            NotificationCenter.default.post(name: BROADCAST.SESSION_LOADED, object: nil)
            //---|   DB Set In Provider Started value
            DB.LPROVIDER.child("session_starting").setValue(app.profile["session_starting"])
        })
    }
    //---|   Start && End
    func session_set(_ state:String="start"){
        DB.SESSION.childByAutoId().setValue([state : true, "time" : [".sv" : "timestamp"]])
        if state == "start" { self.session_started() }
    }
    //---|   Session Started
    func session_started(_ time:Any?=nil){
        app.profile["session_starting"] = String((time ?? Date().timestamp)).toInt64()
        NotificationCenter.default.post(name: BROADCAST.SESSION_STARTED, object: nil)
    }
}
