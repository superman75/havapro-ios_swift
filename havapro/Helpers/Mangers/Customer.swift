//
//  Customer.swift
//  havapro
//
//  Created by abdelghani on 25/08/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import Foundation
import SearchTextField
open class SearchTextFieldItemCustom:SearchTextFieldItem {
    public var options: [String:Any]?

    public init(title: String, subtitle: String?, options:[String:Any]?) {
        super.init(title: title, subtitle: subtitle)
        self.options = options
    }
}
protocol CustomerDelegate{

}
class Customer:NSObject, UITextFieldDelegate{
    //---|   VARS
    public var customers:Dictionary<String, Dictionary<String, AnyObject>> = Dictionary<String, Dictionary<String, AnyObject>>()
    public var eCustomer:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    var aIndex:Dictionary<String, Array<String>> = Dictionary<String, Array<String>>()

    //---|   Outlet
    // var avatar: UIImageView! = UIImageView()
    var firstName: SearchTextField! = SearchTextField()
    var lastName: SearchTextField! = SearchTextField()
    var mobile: SearchTextField! = SearchTextField()
    var phone: SearchTextField! = SearchTextField()
    var email: SearchTextField! = SearchTextField()
    var website: UITextField! = UITextField()
    var address: UITextField! = UITextField()
        //---|    Address
    var addr_number: UITextField! = UITextField()
    var addr_street: UITextField! = UITextField()
    var addr_zip: UITextField! = UITextField()
    var addr_city: UITextField! = UITextField()
    var addr_interphone: UITextField! = UITextField()
    var addr_digicode: UITextField! = UITextField()
    var addr_building: UITextField! = UITextField()
    var addr_floor: UITextField! = UITextField()
    var addr_door: UITextField! = UITextField()
    var publik: UISwitch! = UISwitch()
    var parentCtrl = UIViewController()
   //---|
    func clean(){
        self.eCustomer = Dictionary<String, AnyObject>()
        self.firstName.text = ""
        self.lastName.text = ""
        self.mobile.text = ""
        self.phone.text = ""
        self.email.text = ""
        self.website.text = ""
        self.address.text = ""
            //---|   Address
        self.addr_number.text = ""
        self.addr_street.text = ""
        self.addr_zip.text = ""
        self.addr_city.text = ""
        self.addr_interphone.text = ""
        self.addr_digicode.text = ""
        self.addr_building.text = ""
        self.addr_floor.text = ""
        self.addr_door.text = ""
        publik.isOn = false
        self.hide_result()
    }
   //---|
    func setup(_ field:Dictionary<String, AnyObject>, _ parentCtrl:UIViewController=UIViewController()){
        //---| View
        self.parentCtrl = parentCtrl
        //---|   NOTIFICATION
        NotificationCenter.default.addObserver(forName: BROADCAST.CONNECTED, object: nil, queue: nil, using: self.connected)
        if let fn = field["first_name"] as? SearchTextField { self.firstName = fn }
        if let ln = field["last_name"]  as? SearchTextField { self.lastName = ln }
        if let mo = field["mobile"]  as? SearchTextField { self.mobile = mo }
        if let ph = field["phone"]  as? SearchTextField { self.phone = ph }
        if let em = field["email"]  as? SearchTextField { self.email = em }
        if let ws = field["website"] as? UITextField { self.website = ws }
        if let addres = field["address"] as? UITextField { self.address = addres }
        //---|   Address
        if let addrnumber = field["addr_number"] as? UITextField { self.addr_number = addrnumber }
        if let addrstreet = field["addr_street"] as? UITextField { self.addr_street = addrstreet }
        if let addrzip = field["addr_zip"] as? UITextField { self.addr_zip = addrzip }
        if let addrcity = field["addr_city"] as? UITextField { self.addr_city = addrcity }
        if let addrinterphone = field["addr_interphone"] as? UITextField { self.addr_interphone = addrinterphone }
        if let addrdigicode = field["addr_digicode"] as? UITextField { self.addr_digicode = addrdigicode }
        if let addrbuilding = field["addr_building"] as? UITextField { self.addr_building = addrbuilding }
        if let addrfloor = field["addr_floor"] as? UITextField { self.addr_floor = addrfloor }
        if let addrdoor = field["addr_door"] as? UITextField { self.addr_door = addrdoor }
        if let pl = field["publik"] as? UISwitch { self.publik = pl }
        self.darkTheme(firstName)
        self.darkTheme(lastName)
        self.darkTheme(mobile)
        self.darkTheme(phone)
        self.darkTheme(email)
    }
    func assignField(_ field:SearchTextField, _ title:String, _ subtitle:String, _ selected:Bool=true){
        field.filterItems(self.list(title, subtitle))
        // field.stopLoadingIndicator()
        field.itemSelectionHandler = { item, itemPosition in
            // Just in case you need the item position
            let item = item[itemPosition] as! SearchTextFieldItemCustom
            self.selected(String(item.options!["id"]))
        }
    }
    //---|
    func connected(notification: Notification){
        //---|   Customer get
        self.loaded()
    }
    func loaded(_ callback : (() -> Void)? = nil){
        //---|   Customer get
        DB.CUSTOMERS.observe(.value, with: { (snap) in
            self.customers = Dictionary<String, Dictionary<String, AnyObject>>()
            if snap.exists() {
                for (id, customer) in (snap.value as? Dictionary<String, Dictionary<String, AnyObject>>)!{
                    if customer["uid"] != nil && (String(customer["uid"]) == app.uid || String(customer["public"]).bool) {
                        self.customers[id] = customer
                    }
                }
            }
            //---|   Callback
            if callback != nil { callback!() }
            //---|
            self.assignField(self.firstName, "first_name", "last_name")
            self.assignField(self.lastName, "last_name", "first_name")
            self.assignField(self.mobile, "mobile", "name")
            self.assignField(self.phone, "phone", "name")
            self.assignField(self.email, "email", "name")
            //---|   Loaded
            NotificationCenter.default.post(name: BROADCAST.CUSTOMERS_LOADED, object: nil)
        })
    }
    func saveInit(){
        //---|
        let first_name = String(self.firstName.text)
        let last_name = String(self.lastName.text)
        let mobile = String(self.mobile.text)
        let phone = String(self.phone.text)
        let email = String(self.email.text)
        //---|   is New
        if self.eCustomer["id"] != nil && String(self.eCustomer["first_name"]) != first_name && String(self.eCustomer["last_name"]) != last_name && String(self.eCustomer["mobile"]) != mobile && String(self.eCustomer["phone"]) != phone && String(self.eCustomer["email"]) != email {
            self.eCustomer = Dictionary<String, AnyObject>()
        }
        self.eCustomer["name"] = "\(first_name) \(last_name)" as AnyObject
        self.eCustomer["first_name"] = first_name as AnyObject
        self.eCustomer["last_name"] = last_name as AnyObject
        self.eCustomer["mobile"] = mobile as AnyObject
        self.eCustomer["phone"] = phone as AnyObject
        self.eCustomer["email"] = email as AnyObject
        self.eCustomer["website"] = String(self.website.text) as AnyObject
            //---|   Address
        self.eCustomer["address"] = String(self.address.text) as AnyObject
        self.eCustomer["addr_number"] = String(self.addr_number.text!) as AnyObject
        self.eCustomer["addr_street"] = String(self.addr_street.text!) as AnyObject
        self.eCustomer["addr_zip"] = String(self.addr_zip.text!) as AnyObject
        self.eCustomer["addr_city"] = String(self.addr_city.text!) as AnyObject
        self.eCustomer["addr_interphone"] = String(self.addr_interphone.text!) as AnyObject
        self.eCustomer["addr_digicode"] = String(self.addr_digicode.text!) as AnyObject
        self.eCustomer["addr_building"] = String(self.addr_building.text!) as AnyObject
        self.eCustomer["addr_floor"] = String(self.addr_floor.text!) as AnyObject
        self.eCustomer["addr_door"] = String(self.addr_door.text!) as AnyObject
        self.eCustomer["public"] = self.publik.isOn as AnyObject
        self.eCustomer["delivery_address"] = "\(String(self.eCustomer["addr_number"]!)) \(String(self.eCustomer["addr_street"]!)) \(String(self.eCustomer["addr_zip"]!)) \(String(self.eCustomer["addr_city"]!)) \(String(self.eCustomer["addr_interphone"]!)) \(String(self.eCustomer["addr_digicode"]!)) \(String(self.eCustomer["addr_building"]!)) \(String(self.eCustomer["addr_floor"]!)) \(String(self.eCustomer["addr_door"]!))" as AnyObject
        self.eCustomer["delivery_address"] = String(self.eCustomer["delivery_address"]).trim() as AnyObject //---|   Replace Space
        self.eCustomer["address"] = String(self.eCustomer["address"]).trim() as AnyObject //---|   Replace Space
    }
    //---|
    func save(_ saved : ((_ id:String) -> Void)? = nil){
        self.saveInit()
        var id:String = ""
        if !String(self.eCustomer["name"]).empty() {
            if self.eCustomer["id"] == nil { self.eCustomer["id"] = app.id() as AnyObject }
            //---|   is Copy or Not Yet Set Author
            if self.eCustomer["uid"] == nil || String(self.eCustomer["uid"]!) != app.uid { self.eCustomer["uid"] = app.uid as AnyObject }
            self.eCustomer.removeValue(forKey: "draft")
            id = String(self.eCustomer["id"])
            self.customers[id] = self.eCustomer
            DB.CUSTOMERS.child(id).setValue(self.eCustomer)
            //---|   ADDED
            NotificationCenter.default.post(name: BROADCAST.CUSTOMER_NAME_CHANGED, object: self.eCustomer)
        }
        // self.clean() //---|   Clean
        if saved != nil{ saved!(id) }
        self.hide_result()
    }
    func list(_ title:String, _ subtitle:String) -> Array<SearchTextFieldItem>{
        var arr = Array<SearchTextFieldItem>()
        self.aIndex[title] = Array<String>()
        for(id , customer) in self.customers{
            aIndex[title]!.append(id)
            arr.append(SearchTextFieldItemCustom(title: String(customer[title]), subtitle: String(customer[subtitle]), options: ["id" : id]))
        }
        return arr
    }
    func selected(_ id:String){
        eCustomer = [String:AnyObject]() //---| TODO
        if self.customers[id] != nil{
            eCustomer = self.customers[id]!
            self.firstName.text = String(eCustomer["first_name"])
            self.lastName.text = String(eCustomer["last_name"])
            self.mobile.text = String(eCustomer["mobile"])
            self.phone.text = String(eCustomer["phone"])
            self.email.text = String(eCustomer["email"])
            self.website.text = String(eCustomer["website"])
            self.address.text = String(eCustomer["address"])
            //---|   Address
            self.addr_number.text = String(eCustomer["addr_number"])
            self.addr_street.text = String(eCustomer["addr_street"])
            self.addr_zip.text = String(eCustomer["addr_zip"])
            self.addr_city.text = String(eCustomer["addr_city"])
            self.addr_interphone.text = String(eCustomer["addr_interphone"])
            self.addr_digicode.text = String(eCustomer["addr_digicode"])
            self.addr_building.text = String(eCustomer["addr_building"])
            self.addr_floor.text = String(eCustomer["addr_floor"])
            self.addr_door.text = String(eCustomer["addr_door"])
            self.publik.isOn = String(eCustomer["public"]).bool
        }
        NotificationCenter.default.post(name: BROADCAST.CUSTOMER_NAME_CHANGED, object: eCustomer)
        self.hide_result()

    }
    //---|   Design
    func darkTheme(_ field: SearchTextField){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x:0.0, y:field.frame.height - 1, width:field.frame.width, height:1.0)
        bottomLine.backgroundColor = UIColor("#eeeeee").cgColor
        field.borderStyle = UITextBorderStyle.none
        field.layer.addSublayer(bottomLine)
        // field.theme.fontColor = Color.active
        // field.theme.bgColor = UIColor.white
        // field.theme.borderColor = Color.active
        // field.theme.separatorColor =  Color.active
        // field.theme.cellHeight = 40
        // field.maxResultsListHeight = 800
        // field.startVisible = false
        // field.theme = SearchTextFieldTheme.darkTheme()
        // field.highlightAttributes = [NSBackgroundColorAttributeName: UIColor("#023d60", alpha: 0.2)]

    }
    func hide_result(){
        print("-----------| hide_result() |----------\n")
        DispatchQueue.main.async{
            for tfield in [self.firstName, self.lastName, self.mobile, self.phone, self.email]{
                tfield!.textFieldDidEndEditing()
                tfield!.stopLoadingIndicator()
                tfield!.hideResultsList()
            }
        }
        parentCtrl.view.endEditing(true)
    }
    //---|
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("-----------| textField |----------\n textField --> \(textField) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        // Do not add a line break
        if textField == mobile {
            firstName.resignFirstResponder()
        }else if textField == firstName {
            lastName.resignFirstResponder()
        }
        return true
    }
}
