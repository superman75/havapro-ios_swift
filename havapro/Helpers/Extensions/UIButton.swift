//
//  UIButton.swift
//  havapro
//
//  Created by HavaPRO on 31/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

extension UIButton {

    func enabled(_ enable:Bool=true, _ alpha:CGFloat=0.4) -> Bool{
        self.isEnabled = enable
        self.alpha = (enable ? 1.0 : alpha)
        return enable
    }
    //---|
    func loading(_ delay:Int=800){
        self.enabled(false)
        UIViewController().timeout({ self.enabled(true) }, delay)
    }

}
