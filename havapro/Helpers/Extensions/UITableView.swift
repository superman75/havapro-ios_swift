//
//  UIViewController.swift
//  havapro
//
//  Created by Exo on 29/10/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
extension UITableView {
    //---|   Binding
    func binding(_ vc: UIViewController, _ reload:Bool=true) -> UITableView{
        self.dataSource = vc as? UITableViewDataSource
        self.delegate = (vc as! UITableViewDelegate)
        if reload { self.reloadData() }
        return self
    }
    //---|   DeSelect indexPath
    func deSelectRow(_ indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.cellForRow(at: indexPath) { return cell.deSelectRow() }
        return UITableViewCell()
    }
    //---| Select Row
    func selectRow(_ vc: UITableViewDelegate, _ row:Int=0, _ active:Bool=true, _ scrollPosition:UITableViewScrollPosition = .top) -> UITableView{
        let indexPath = IndexPath(row: row, section: 0)
        if row >= 0, let cell = self.cellForRow(at: indexPath){
            self.selectRow(at: indexPath, animated: true, scrollPosition: scrollPosition)
            if active { vc.tableView!(self, didSelectRowAt: indexPath) }
        }
        return self
    }
}
extension UITableViewCell {
    //---|   DeSelect
    func deSelectRow() -> UITableViewCell{
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        return self
    }
}
