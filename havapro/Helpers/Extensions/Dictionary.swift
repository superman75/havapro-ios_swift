//
//  Dictionary.swift
//  havapro
//
//  Created by abdelghani on 27/05/2017.
//  Copyright © 2017 Exo. All rights reserved.
//https://github.com/pNre/ExSwift
//

import Foundation
extension Dictionary {
    func sortedKeys(_ isOrderedBefore:(Key,Key) -> Bool) -> [Key] {
        return Array(self.keys).sorted(by: isOrderedBefore)
    }
    //---|   Key
    func key(_ index:Int, _ sort:Bool=true)-> String {
        //---|   With A Sort
        if  self.count > index, index >= 0{
            if sort { return String(((self as NSDictionary).allKeys as! [String]).sorted(by: <)[index]) }
            return String(Array(self.keys)[index])
        }
        return ""
    }
    //---|   Search
    func search(_ search:String,  _ field:String="id",_ opts:[String:Any]=[String:Any]()) -> [String:Any]{
        var index = 0
        let count = self.count
        for (id, item) in (self as? [String:[String:Any]])!{
            if String(item[field]) == search {
                var nItem = item
                if String(opts["$index"]).bool { nItem["$index"] = index }
                return nItem
                break
            }
            index += 1
        }
        return [String:Any]()
    }
    //---| INDEX
    func index(_ search:String,  _ field:String="id") -> Int{
        return (self.olist(field).index(of: search) ?? -1)
    }
    //---|   Dictionary From Index
    func dic(_ index:Int, _ sort:Bool=true)-> Dictionary<String, Any> {
        if let dic = (self as NSDictionary)[self.key(index, sort)] { return dic as! Dictionary<String, Any> }
        return NSDictionary() as! Dictionary<String, Any>
    }
    //--| List One Item
    func olist(_ field:String="name") -> Array<String>{
        var olist:Array<String> = []
        for (_, item) in self{
            if let found = (item as? Dictionary<String, Any>)![field] as? String, !String(found).empty(){ olist.append(found) }
        }
        return olist
    }
    func current() -> [String : Any]{
        for(id, item) in (self as? [String : [String : Any]])!{
            return item
            break
        }
        return [String : Any]()
    }
    /// Merges the dictionary with dictionaries passed. The latter dictionaries will override
    /// values of the keys that are already set
    ///
    /// - parameter dictionaries: A comma seperated list of dictionaries
    mutating func merge<K, V>(_ dictionaries: Dictionary<K, V>...) -> Dictionary<K, V>{
        for dict in dictionaries {
            for (key, value) in dict {
                self.updateValue(value as! Value, forKey: key as! Key)
            }
        }
        return self as! Dictionary<K, V>
    }
}
