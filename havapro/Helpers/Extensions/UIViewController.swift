//
//  UIViewController.swift
//  havapro
//
//  Created by Exo on 29/10/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
import Firebase
import DropDown
import PMAlertController
import Spring
// import PopupController
extension UIViewController {
    //---|   Showed ViewController
    func showed(){}
    //---|   Profile
    func profile() -> NSDictionary{ return app.profile as NSDictionary }
    //---|   Get Information
    func connection(_ callback : ((_ autoconnect:Bool) -> Void)? = nil){
        // print("---| User --------- : \(FIRAuth.auth()?.currentUser?.uid)")
        // print("self----------\(self.classForCoder)")
        // print("NSStringFromClass(type(of: self))----------\(NSStringFromClass(type(of: self)))")
        if FIRAuth.auth()?.currentUser?.uid != nil {
            if app.profile["id"] == nil {
                //---|   Get Information
                //DB.PROVIDER_LOCALS.observe(.value, with: { (snap) in
                DB.LIPROVIDER.queryOrdered(byChild: "uid").queryEqual(toValue: String((FIRAuth.auth()?.currentUser?.uid)!)).observe(.value, with: { (snap) in
                    if !snap.exists() {
                        if String(self.classForCoder) != "loginController"{ return self.logout(500) }
                        self.alert("Erreur", "Please Contact CEO your account has suspended.")
                        return
                    }
                    //---|   Set Data locally
                    app.profile = (snap.value as? [String:Any])!.current()
                    let autoconnect = String(app.uid).empty()
                    //---|   Set UID
                    app.uid = String(app.profile["id"]) //"-Ki9glEPbvr36e9p0R1p"//
                    self.redirect()
                    //---|   Callback
                    if callback != nil { callback!(autoconnect) }
                    // //---|   Callback
                    // for (id, provider_local) in (snap.value as? Dictionary<String, Dictionary<String, AnyObject>>)!{
                    //     app.uid = String(id)! //"-Ki9glEPbvr36e9p0R1p"//
                    //     print("---| app.uid --------- : \(app.uid)")
                    //     //---|   Set Data locally
                    //     app.profile = (provider_local as? Dictionary<String, AnyObject>)!
                    //     // app.logo.assign(app.profile["logo"])
                    //     //---|
                    //     self.redirect()
                    //     //---|   Callback
                    //     if callback != nil { self.timeout(callback!, 500) }
                    //     break
                    // }
                })
            //---|   Callback
            }else {
                self.redirect()
                //if callback != nil{ self.timeout(callback!, 500) }
                if callback != nil { callback!(false) }
            }

        }//---|   Is not in login form | Go to login form with messege ""
        else if String(self.classForCoder) != "loginController"{
            return self.logout(500)
        }
    }
    //---|   TODO : Connecting
    func connecting(_ callback : (() -> Void)){ if FIRAuth.auth()?.currentUser?.uid != nil, app.uid != nil { callback() } }
    // dropDown
    func dropdownInit(_ dropDown : DropDown, view : UIView, datasource : NSArray, btnShowing : UIButton,_ completion: @escaping (_ index: Int, _ item: String) -> Void, _ center_left:Bool=true){
        // The view to which the drop down will appear on
        dropDown.anchorView = view // UIView or UIBarButtonIte
        dropDown.direction = .bottom
        //dropDown.width = btnShowing.frame.width
        dropDown.frame = CGRect(x: btnShowing.frame.origin.x, y: dropDown.frame.origin.y, width: btnShowing.frame.width, height: dropDown.frame.height)
        // Left Text Button
        if center_left {
            btnShowing.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            btnShowing.titleEdgeInsets.left = 12
        }
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = datasource as! [String]
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            // print("Selected item: \(item) at index: \(index)")
            completion(index, item)
        }
    }
    // redirect
    func redirect(){
        if String(describing: self.classForCoder) == "loginController" {
            //if (app.profile["caisse"] != nil) || (app.profile["reservation"] != nil) {
                self.include("AppController")
            // }else{
            //     self.alert("Erreur", "Please, You need to Contact CEO you don't have any module active.")
            // }

        }
    }
    // Logout
    func logout(_ time:Int=0){
        print("-----\n#-----\nString(describing: self.classForCoder)\n-----\n\(String(describing: self.classForCoder))\n-----\n#-----\n")
        self.timeout({
            try! FIRAuth.auth()!.signOut()
            app.profile = Dictionary<String, AnyObject>()
            app.uid = ""
            self.include("loginController", nil, "Main")
        }, time)
    }
    // Alert
    // func alertWithTitle(_ title: String!, message: String, ViewController: UIViewController, _ toFocus:UITextField = UITextField()) {
    //     app.alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    //     //let alert = app.init(alert: UIAlertController(title: title, message: message, preferredStyle: .alert))
    //     let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: {_ in
    //         toFocus.becomeFirstResponder()
    //     });
    //     app.alert.addAction(action)
    //     ViewController.present(app.alert, animated: true, completion:nil)
    // }
    func timeout(_ closure:(() -> ())? = nil, _ delay:Int=200){
        if closure != nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay), execute: closure!)
        }
    }
    // Confirm
    func confirm( _ callback: @escaping (_ str:String) -> Void, _ title:String="Supprission", _ msg:String="Voulez-vous vraiment supprimer", _ btns:
        Array<String>=["Fermer", "Supprimer"], _ fieldText:Dictionary<String, Any>?=nil, _ cancelback : (() -> Void)? = nil){
        let alertVC = PMAlertController(title: title, description: msg, image: nil, style: .alert)
        //---|   Text Field
        if fieldText != nil, !fieldText!.isEmpty {
            alertVC.addTextField { (textField) in
                if let placeholder = fieldText!["placeholder"] as? String{ textField?.placeholder = placeholder }
                if let keyboardType = fieldText!["keyboardType"] as? UIKeyboardType{ textField!.keyboardType = keyboardType }
            }
        }
        alertVC.addAction(PMAlertAction(title: btns[0], style: .cancel, action: { () -> Void in
            self.timeout(cancelback)
        }))

        if btns.count>1 {
            alertVC.addAction(PMAlertAction(title: btns[1], style: .default, action: { () -> Void in
                self.timeout({ () -> Void in
                    callback((alertVC.textFields.count>0  ? (alertVC.textFields[0].text ?? "") : ""))
                })
            }))
        }
        self.present(alertVC, animated: true, completion: nil)
    }
    //---| Alert
    func alert(_ title:String="Information", _ msg:String="Les données ont été enregistrées avec succès", _ close:String="Ok", _ cancelback:(() -> Void)? = nil){
        self.confirm( { (_) -> Void in }, title, msg, [close], nil, cancelback)
    }
    // Loading
    func loading(_ message : String = "Please wait...") -> UIAlertController{
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)

        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x:10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        return alert
    }
    // LoadOff
    func loadoff(_ callback : @escaping ()-> Void, alert: UIAlertController = UIAlertController()){
        alert.dismiss(animated: false, completion: callback)
    }
    // Toggle Modal
    func modalToggle(_ modal: UIView){
        if modal.isHidden == true{
            UIView.animate(withDuration: 0.2, animations: {
                modal.alpha = 1
                // self.modalBg.alpha = 0.4
                }, completion: { (value: Bool) in
                    modal.isHidden = false
                    // self.modalBg.isHidden = false
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                modal.alpha = 0
                // self.modalBg.alpha = 0
                }, completion: { (value: Bool) in
                    modal.isHidden = true
                    // self.modalBg.isHidden = true
            })
        }
    }

    // clean from null and return NSArray
    func nsarray(_ obj : Any) -> NSArray{
        var back = [AnyObject]()
        if obj is NSArray {
            for (index, value) in (obj as! NSArray).enumerated() {
                if !(value is NSNull){ back.append(value as AnyObject) }
            }
        }else{
            for (index, value) in obj as! NSDictionary {
                if !(value is NSNull){ back.append(value as AnyObject) }
            }
        }
        return back as! NSArray

    }
    // GET
    func g(_ obj:Any, _ path:String, _ def:Any?=nil)->Any{
        // if obj != nil {
            var def = def
            var node = path.components(separatedBy: ".")
            if  node.count>0{
                let item = node.remove(at: 0)
                let newpath = node.joined(separator: ".")

                if let subobj = obj as? NSDictionary, subobj[item] != nil{
                    if node.count==0{ def = subobj[item]! }
                    else{ def = g(subobj[item]!, newpath, def) }
                }else if let subobj = obj as? NSArray{
                    let int = String(item, "-1").toInt()
                    if int >= 0, subobj.count > int {
                        if node.count == 0 { def = subobj[int] }
                        else{ def = g(subobj[int], newpath, def) }
                    }
                }
            }
        // }
        return def
    }
    // MARK : Including Controller View In View Container
    func includeView(_ viewControllerIncluding:UIViewController, _ containerView:UIView, _ identifier:String?=nil){
        if let includeVC = storyboard?.instantiateViewController(withIdentifier:  String(identifier ?? type(of: viewControllerIncluding)) ){
            self.addChildViewController(includeVC)
            includeVC.view.frame = CGRect(x:0, y:0, width:containerView.frame.size.width, height:containerView.frame.size.height);
            containerView.addSubview((includeVC.view)!)
            includeVC.didMove(toParentViewController: self)
        }
    }
    // MARK : deSelectRow
    // func deSelectRow(_ selectedCell:UITableViewCell){
    //     selectedCell.backgroundColor = UIColor.clear
    //     selectedCell.selectionStyle = .none
    // }
    // func deSelectRow(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell? {
    //     if let selectedCell = tableView.cellForRow(at: indexPath) {
    //         self.deSelectRow(selectedCell)
    //         return selectedCell
    //     }
    //     return nil
    // }
    // MARK : Including Controller View In View Container
    func include(_ vc:UIViewController, _ container:UIView, _ options:[String:Any]?=nil) -> UIViewController{
        let options = options ?? [String:Any]()
        if let container = container as? SpringView {
            container.duration = 0.2
            container.animation = String(options["animation_out"], "fadeOut")
            container.animate()
        }
        self.addChildViewController(vc)
        vc.view.frame = CGRect(x:0, y:0, width: container.frame.size.width, height: container.frame.size.height);
        container.addSubview((vc.view)!)
        vc.didMove(toParentViewController: self)
        if let container = container as? SpringView {
            container.duration = 1
            // container.delay = 0.5
            container.animation = String(options["animation_in"], "fadeIn")
            container.animate()
        }
        vc.showed()
        return vc
    }
    //---|   Include By Name Class && storyboard
    func include(_ identifier:String, _ container:UIView?=nil, _ storyBoard:String?=nil, _ options:[String:Any]?=nil) -> UIViewController{
        let options = options ?? [String:Any]()
        let object = self.g(options, "object", [String:Any]()) as! [String:Any]
        let opts = self.g(options, "options", [String:Any]()) as! [String:Any]
        if let includeVC = UIStoryboard(name: (storyBoard ?? identifier.replace("Controller", "")), bundle: nil).instantiateViewController(withIdentifier: identifier) as? UIViewController {
            //---|   TODO Connected
            //---|   Set Data To Var In ControllerView
            includeVC.setValuesForKeys(object)
            //---|   Include In Container
            if container != nil { return self.include(includeVC, container!, opts) }
            //---|   Include In The Same UIViewController
            self.present(includeVC, animated: true, completion: nil)
            return includeVC
        }else{ print("include ---\(identifier)--- Error") }
        return UIViewController()
    }
    //---| Include With ByRef
    func include(_ identifier:String, _ container:UIView?=nil, _ storyBoard:String?=nil, _ options:[String:Any]?=nil, _ viewCtrl: inout Any?) -> UIViewController{
        if viewCtrl != nil, container != nil { return self.include((viewCtrl as! UIViewController), container!, (self.g(options, "options", [String:Any]()) as! [String:Any])) }
        return include(identifier, container, storyBoard, options)
    }
    //---|
    // func nib(_ name:String?=nil) -> Any{
    //     // return Bundle.main.loadNibNamed((name ?? "\(self)".characters.split{$0 == "."}.map(String.init).last!), owner:self, options: nil)?.first
    //     // return UINib(nibName: (name ?? "\(self)".characters.split{$0 == "."}.map(String.init).last!), bundle: nil).instantiateWithOwner(self, options: nil).first as! self
    // }
    // //---|   Popup
    func popup(
        _ vc:UIViewController,
        _ callback : ((_ data:[String: Any], _ popup:PopupController)-> Void)? = nil,
        _ showed : (( _ popup:PopupController)-> Void)? = nil,
        _ container:UIViewController?=nil,
        _ customize:[PopupCustomOption]? = [
            .layout(.center),
            .animation(.fadeIn),
            .backgroundStyle(.blackFilter(alpha: 0.8)),
            .dismissWhenTaps(true),
            .scrollable(true)
        ]
    ) -> PopupController {
        let popup = PopupController
        .create((container != nil ? container! : (self as? AppViewController)!.appController!))
        .customize(customize!)
        if showed != nil { popup.didShowHandler(showed!) }
        //
        if let popContainer = vc as? PopupViewController{
            popup.didCloseHandler { popup in
                if callback != nil { callback!(popContainer.popData, popup) }
            }
            popContainer.popup = popup
            popContainer.popShowing()
        }
        // Show
        popup.show(vc)
        if let popContainer = vc as? PopupViewController{ popContainer.popShowed() }
        return popup
    }
}
