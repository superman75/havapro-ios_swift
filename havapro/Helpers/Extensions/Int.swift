//
//  Int.swift
//  havapro
//
//  Created by abdelghani on 14/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

import Foundation

extension Int{
    // Convert To Boolean
    var bool:Bool{ return (self > 0) }
}
