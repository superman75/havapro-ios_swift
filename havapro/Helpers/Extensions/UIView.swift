//
//  UIView.swift
//  havapro
//
//  Created by Exo on 24/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//
import UIKit
import Spring
extension UIView {

    func dropShadow() {
        self.layer.cornerRadius = 2
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 2)

        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 0.4
        self.backgroundColor = UIColor.white
        self.layer.shadowPath = shadowPath.cgPath

    }
    func resize(_ pos:Any, _ val:String){
        //self.frame = CGRect(x: <#T##CGFloat#>, y: <#T##CGFloat#>, width: <#T##CGFloat#>, height: <#T##CGFloat#>)
    }
    /**
     Get Set Height

     - parameter height: CGFloat
     by DaRk-_-D0G
     */
    var height:CGFloat { get { return self.frame.size.height } set { self.frame.size.height = newValue } }
    var width:CGFloat { get { return self.frame.size.width } set { self.frame.size.width = newValue } }
    func animateStart(_ key:String="rotationAnimation", _ animate:String="transform.rotation.z", _ options:Dictionary<String, Any>?=nil) {
        if options == nil { var options = Dictionary<String, Any>() }
        let animation = CABasicAnimation(keyPath: animate)
        animation.fromValue = 0
        animation.toValue = NSNumber(value: Double.pi)
        animation.duration = 0.3
        animation.isCumulative = true
        animation.repeatCount = FLT_MAX
        self.layer.add(animation, forKey: key)
    }

    func animateRemove(_ key:String="rotationAnimation") {
        self.layer.removeAnimation(forKey: key)
    }
    //---| Load Xib File
    func xib(_ name:String?=nil) -> UIView {
        return UINib(nibName: (name ?? "\(self)".characters.split{$0 == "."}.map(String.init).last!), bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
    //---|   Include
    func include(_ vi : UIView, _ animation:[String:Any]=[:]) -> UIView {
        if let vi = vi as? SpringView {
            vi.duration = 0.2
            vi.animation = String(animation["out"], "fadeOut")
            vi.animate()
        }
        vi.frame = bounds
        vi.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        vi.translatesAutoresizingMaskIntoConstraints = true
        self.addSubview(vi)
        return self
        // return self.fit(vi)
    }
    //---|
    func fit(_ vi : UIView) -> UIView{
        vi.translatesAutoresizingMaskIntoConstraints = false
        let margins = self.layoutMarginsGuide
        vi.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        vi.heightAnchor.constraint(equalTo: margins.heightAnchor).isActive = true
        vi.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        vi.widthAnchor.constraint(equalTo: margins.widthAnchor).isActive = true
//        let top = NSLayoutConstraint(item: vi, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
//        let bottom = NSLayoutConstraint(item: vi, attribute: .bottom, relatedBy: .equal, toItem: vi, attribute: .bottom, multiplier: 1.0, constant: 0.0)
//        let leading = NSLayoutConstraint(item: vi, attribute: .leading, relatedBy: .equal, toItem: vi, attribute: .leading, multiplier: 1.0, constant: 0.0)
//        let trailing = NSLayoutConstraint(item: vi, attribute: .trailing, relatedBy: .equal, toItem: vi, attribute: .trailing, multiplier: 1.0, constant: 0.0)
//        //self.needsUpdateConstraints()
//        self.addConstraints([top, bottom, leading, trailing])
        return self
    }
}
