//
//  UIColor.swift
//  havapro
//
//  Created by Exo on 17/09/2016.
//  Copyright © 2016 Exo. All rights reserved.
//
import UIKit
import Foundation
extension UIColor {

    //---|   INIT
    convenience init(_ hex:String, alpha:Float = 1.0) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
        }

        if ((cString.count) != 6) {
            self.init("#eeeeee", alpha:alpha)
        }else{
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)

            self.init(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(alpha)
            )
        }
    }
    //---| Lighter && Darker
    func adjust(_ percentage:CGFloat=30.0) -> UIColor {
        let percentage = -1 * percentage
        var r:CGFloat=0, g:CGFloat=0, b:CGFloat=0, a:CGFloat=0;
        if(self.getRed(&r, green: &g, blue: &b, alpha: &a)){
            return UIColor(red: min(r + percentage/100, 1.0),
                           green: min(g + percentage/100, 1.0),
                           blue: min(b + percentage/100, 1.0),
                           alpha: a)
        }
        return self
    }
}
