//
//  UICollectionView.swift
//  havapro
//
//  Created by Exo on 29/10/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
extension UICollectionView {
    //---|   Binding
    func binding(_ vc: UIViewController) -> UICollectionView{
        self.dataSource = vc as? UICollectionViewDataSource
        self.delegate = (vc as! UICollectionViewDelegate)
        self.reloadData()
        return self
    }
    // //---|   DeSelect indexPath
    // func deSelectRow(_ indexPath: IndexPath) -> UICollectionViewCell {
    //     if let selectedCell = self.cellForRow(at: indexPath) { return selectedCell.deSelectRow() }
    //     return UICollectionViewCell()
    // }
    // //---| Select Row
    // func selectRow(_ vc: UICollectionViewDelegate, _ row:Int=0) -> UICollectionView{
    //     let indexPath = IndexPath(row: row, section: 0);
    //     self.selectRow(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.none)
    //     vc.tableView!(self, didSelectRowAt: indexPath)
    //     return self
    // }
}
// extension UICollectionViewCell {
//     //---|   DeSelect
//     func deSelectRow() -> UICollectionViewCell{
//         self.backgroundColor = UIColor.clear
//         self.selectionStyle = .none
//         return self
//     }
// }
