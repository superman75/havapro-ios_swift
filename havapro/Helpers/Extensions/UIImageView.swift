//
//  UIImageView.swift
//  havapro
//
//  Created by Exo on 27/11/2016.
//  Copyright © 2016 Exo. All rights reserved.
//
import UIKit
extension UIImageView {
    func downloadedFrom(_ url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url ) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
        }.resume()
    }
    func downloadedFrom( link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url, contentMode: mode)
    }
    func assign(_ file: Any?,_ def:String?=nil) {
        self.image = nil
        self.image = !String(def).empty() ? UIImage(named : def!) : nil
        if file != nil, file is String {
            let file = String(file!)
            self.downloadedFrom(link:(file.matches("^http(s)?:").count > 0 ? "" : app.baseUrl)+file)
        }
    }

}
