//
//  Array.swift
//  havapro
//
//  Created by abdelghani on 31/10/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import Foundation
extension Sequence where Iterator.Element == [String: Any]{
    func search(_ search:String,  _ field:String="id",_ opts:[String:Any]=["$index" : true]) -> Dictionary<String, Any>{
        for (index, item) in self.enumerated(){
            if String(item[field]) == search {
                var nItem = item
                if String(opts["$index"]).bool { nItem["$index"] = index }
                return nItem
            }
        }
        return Dictionary<String, Any>()
    }
    //--| List One Item
    func olist(_ field:String="name") -> Array<String>{
        var olist:Array<String> = []
        for (_, item) in self.enumerated(){
            if let found = item[field] as? String, !String(found).empty(){ olist.append(found) }
        }
        return olist
    }
}
