//
//  Date.swift
//  havapro
//
//  Created by Exo on 21/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import Foundation

extension Date {
    //---|   Init Date By Format
    init(_ byString:String,_ format:String="yyyy-MM-dd HH:mm") {
        var d = Date()
        if format.lowercased() == "timestamp"{
            d = Date(timeIntervalSince1970: TimeInterval(byString.toInt64())/1000)
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = format
            d = dateFormatter.date(from: byString)!
        }
        self.init(timeInterval:0, since:d)
    }
    //---|   Convert Date To Timestamp
    var timestamp: String { return String(Int64(self.timeIntervalSince1970 * 1000)) }
    //---|   Convert To String
    func string(_ format:String="dd/MM/yyyy HH:mm")-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    //---|   Add To Date
    func add(_ value:Int,_ type:Calendar.Component = .day)-> Date {
        return Calendar.current.date(byAdding: type, value: value, to: self)!
    }
    //---|   date diff
    func diff(_ start: Date,_ type:Calendar.Component = .day) -> Int {
        return Calendar.current.dateComponents([type], from: start, to: self).value(for: type) ?? 0
    }

    //---|   Set new value
    func set(_ value: Int,_ type:Calendar.Component = .day) -> Date {
        let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        var components = gregorian.components([.year, .month, .day, .hour, .minute, .second], from: self)
        // Change the time to 9:30:00 in your locale
        components.setValue(value, for: type)
        return gregorian.date(from: components)!
    }
}

