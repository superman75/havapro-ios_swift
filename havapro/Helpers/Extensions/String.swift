//
//  String.swift
//  havapro
//
//  Created by Exo on 21/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import Foundation

extension String{
    init(_ any:Any?, _ def:String=""){ self = String(describing: (any ?? def)!) }
    // Convert To Boolean
    var bool:Bool{ return ["true", "1"].contains(self.lowercased().trim()) }
    //---|   Trim
    func trim(_ character:CharacterSet = .whitespacesAndNewlines) -> String { return self.trimmingCharacters(in: character) }
    // Empty
    func empty() -> Bool{ return ((self == nil || self.isEmpty || self.trim() == "") ? true : false) }
    //---|   Format
    func format(_ format:String="%.2f") -> String{ return String(NSString(format:NSString(string: format), self.toDouble())) }
    // Convert To Int
    func toInt64(_ def:Int64=0) -> Int64{
        //---|   For IPad
        if let str_int64 = self as? Int64 { return Int64(str_int64) }
        let int64 = self.matches("(-)?[0-9]+")
        if int64.count > 0 { return Int64(int64[0])! }
        return def
    }
    func toInt(_ def:Int64=0) -> Int{ return Int(self.toInt64(def)) }
    // Convert To Double
    func toDouble(_ def:Double=0.0) -> Double{
        let double = self.matches("(-)?[0-9.]+")
        if double.count > 0 { return Double(double[0])! }
        return def
    }
    // Convert To Float
    func toFloat(_ def:Double=0.0) -> Float{ return Float(self.toDouble(def)) }
    //---|   Matches
    func matches(_ regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    //---|   Font
    func font(_ attrs:[String:Any]=[NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 20)!]) -> String{
        return String(NSMutableAttributedString(string: self, attributes:attrs))
    }
    //---|   SOURCE : https://gist.github.com/albertbori/0faf7de867d96eb83591
    // var length: Int { get { return countElements(self) } }

    // func contains(s: String) -> Bool { return self.rangeOfString(s) ? true : false }

    func replace(_ target: String, _ withString: String) -> String {
         return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }

    // subscript (i: Int) -> Character {
    //     get {
    //         let index = advance(startIndex, i)
    //         return self[index]
    //     }
    // }

    // subscript (r: Range<Int>) -> String {
    //     get {
    //         let startIndex = advance(self.startIndex, r.startIndex)
    //         let endIndex = advance(self.startIndex, r.endIndex - 1)
    //         return self[Range(start: startIndex, end: endIndex)]
    //     }
    // }

    // func subString(startIndex: Int, length: Int) -> String {
    //     var start = advance(self.startIndex, startIndex)
    //     var end = advance(self.startIndex, startIndex + length)
    //     return self.substringWithRange(Range<String.Index>(start: start, end: end))
    // }

    // func indexOf(target: String) -> Int {
    //     var range = self.rangeOfString(target)
    //     if let range = range {
    //         return distance(self.startIndex, range.startIndex)
    //     } else {
    //         return -1
    //     }
    // }

    // func indexOf(target: String, startIndex: Int) -> Int {
    //     var startRange = advance(self.startIndex, startIndex)

    //     var range = self.rangeOfString(target, options: NSStringCompareOptions.LiteralSearch, range: Range<String.Index>(start: startRange, end: self.endIndex))

    //     if let range = range {
    //         return distance(self.startIndex, range.startIndex)
    //     } else {
    //         return -1
    //     }
    // }

    // func lastIndexOf(target: String) -> Int {
    //     var index = -1
    //     var stepIndex = self.indexOf(target)
    //     while stepIndex > -1 {
    //         index = stepIndex
    //         if stepIndex + target.length < self.length {
    //             stepIndex = indexOf(target, startIndex: stepIndex + target.length)
    //         } else {
    //             stepIndex = -1
    //         }
    //     }
    //     return index
    // }

    // func isMatch(regex: String, options: NSRegularExpressionOptions) -> Bool {
    //     var error: NSError?
    //     var exp = NSRegularExpression(pattern: regex, options: options, error: &error)

    //     if let error = error {
    //         println(error.description)
    //     }
    //     var matchCount = exp.numberOfMatchesInString(self, options: nil, range: NSMakeRange(0, self.length))
    //     return matchCount > 0
    // }

    // func getMatches(regex: String, options: NSRegularExpressionOptions) -> [NSTextCheckingResult] {
    //     var error: NSError?
    //     var exp = NSRegularExpression(pattern: regex, options: options, error: &error)

    //     if let error = error {
    //         println(error.description)
    //     }
    //     var matches = exp.matchesInString(self, options: nil, range: NSMakeRange(0, self.length))
    //     return matches as [NSTextCheckingResult]
    // }

    // private var vowels: [String] { get { return ["a", "e", "i", "o", "u"] } }

    // private var consonants: [String] { get { return ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z"]  } }

    // func pluralize(count: Int) -> String {
    //     if count == 1 {
    //         return self
    //     } else {
    //         var lastChar = self.subString(self.length - 1, length: 1)
    //         var secondToLastChar = self.subString(self.length - 2, length: 1)
    //         var prefix = "", suffix = ""

    //         if lastChar.lowercaseString == "y" && vowels.filter({x in x == secondToLastChar}).count == 0 {
    //             prefix = self[0...self.length - 1]
    //             suffix = "ies"
    //         } else if lastChar.lowercaseString == "s" || (lastChar.lowercaseString == "o" && consonants.filter({x in x == secondToLastChar}).count > 0) {
    //             prefix = self[0...self.length]
    //             suffix = "es"
    //         } else {
    //             prefix = self[0...self.length]
    //             suffix = "s"
    //         }

    //         return prefix + (lastChar != lastChar.uppercaseString ? suffix : suffix.uppercaseString)
    //     }
    // }
}
//---|   Idea : https://stackoverflow.com/questions/27989094/how-to-unwrap-an-optional-value-from-any-type/32780793#32780793
// protocol OptionalProtocol {
//     func isSome() -> Bool
//     func unwrap() -> Any
// }

// extension Optional : OptionalProtocol {
//     func isSome() -> Bool {
//         switch self {
//             case .none: return false
//             case .some: return true
//         }
//     }

//     func unwrap() -> Any {
//         switch self {
//             // If a nil is unwrapped it will crash!
//             case .none: preconditionFailure("nill unwrap")
//             case .some(let unwrapped): return unwrapped
//         }
//     }
//     func unOptional() -> Any {
//         if let n = self {
//             let mirror = Mirror(reflecting: n)
//             guard mirror.displayStyle == .optional, let first = mirror.children.first else {
//                 return n
//             }
//             return first.value.unOptional()
//         }
//         return "def"
//     }
// }
