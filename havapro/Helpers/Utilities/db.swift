//
//  db.swift
//  havapro
//
//  Created by abdelghani on 27/08/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import Foundation
import Firebase
public struct DB {
    static var START:String = "" // "dev/" or ""
    static var COMMON:String = "\(DB.START)" // common/v0-0-1/ | common beetweb havapro
    static var HAVAPRO:String = "\(DB.START)havapro/"
    //---|   PROVIDER_LOCALS INIT
    static var LIPROVIDER:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)provider_locals") }
    //---|   PROVIDER_LOCALS
    static var LPROVIDER:FIRDatabaseReference { return DB.LIPROVIDER.child(app.uid) }
    //---|   Commande
    static var COMMANDES:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)commandes/\(app.uid)") }
    //---|   settings
    static var SETTINGS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)settings/\(app.uid)") }
    //---|   Commande Archive
    static var ACOMMANDES:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)archives/commandes/\(app.uid)") }
    //---|   AGENDA
    static var AGENDA:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)agenda/\(app.uid)") }
    //---|   Commande Archive
    static var AAGENDA:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)archives/agenda/\(app.uid)") }
    //---|   CATEGORIES _products
    static var PCATEGORIES:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)categories_products/\(app.uid)") }
    //---|   Products
    static var PRODUCTS:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)products/\(app.uid)") }
    //---|   components
    static var COMPONENTS:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)components/\(app.uid)") }
    //---|   Orders
    static var ORDERS:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)provider_orders/\(app.uid)") }
    //---|  Order Items
    static var ORDER_ITEMS:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)provider_order_items/\(app.uid)") }
    //---|  Order Item Details
    static var ORDER_ITEM_DETAILS:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)provider_order_item_details/\(app.uid)") }
    //---|   Ssers
    static var USERS:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)users") }
    //---|   Bills
    static var BILLS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)bills/\(app.uid)") }
    //---|   Bill Archive
    static var ABILLS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)archives/bills/\(app.uid)") }
    //---|   Customers
    static var CUSTOMERS:FIRDatabaseReference { return app.fbase.child("\(DB.COMMON)customers") }
    //---|   Statisticals
    static var STATISTICALS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)statisticals/\(app.uid)") }
    //---|   Transactions
    static var TRANSACTIONS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)transactions/\(app.uid)") }
    //---|   CONTACTS
    static var CONTACTS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)contacts/\(app.uid)") }
    //---|   Accountings
    //---|   EXECUTED ACCOUNTINGS
    static var EACCOUNTINGS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)accountings/executeds/communications") }
    //---|   PROCESSING ACCOUNTINGS
    static var PACCOUNTINGS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)accountings/processings/communications") }
    //---|   COMMUNICATIONS
    static var COMMUNICATIONS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)communications") }
    //---|   SUPPORTS
    static var SUPPORTS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)supports") }
    //---|   NETWORKS
    static var NETWORKS:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)networks/\(app.uid)") }
    //---|   SESSION
    static var SESSION:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)sessions/\(app.uid)") }
    //---|   Bill Archive
    static var ARCHIVES:FIRDatabaseReference { return app.fbase.child("\(DB.HAVAPRO)archives/\(app.uid)") }

}

