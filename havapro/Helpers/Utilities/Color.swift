//
//  Color.swift
//  havapro
//
//  Created by abdelghani on 13/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import Foundation

public struct Color {
    //---|   COLORS
    static var active:UIColor{ return UIColor("#023d60") }
    static var active_b:UIColor{ return UIColor("#013351") }
    static var clean:UIColor{ return UIColor("#ECF0F1") }
    static var def:UIColor{ return UIColor("#eeeeee") }
    static var dark:UIColor{ return UIColor("#181303") }
    static var gray:UIColor{ return UIColor("#60646D") }//#C4C4C4
    static var blue:UIColor{ return UIColor("#2F79C3") }
    static var yellow:UIColor{ return UIColor("#F9BF3B") }
    static var green:UIColor{ return UIColor("#71BA51") }
    static var red:UIColor{ return UIColor("#CF352B") }
    static var orange:UIColor{ return UIColor("#EE7546") }
}