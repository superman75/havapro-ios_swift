//
//  Fun.swift
//  havapro
//
//  Created by abdelghani on 03/05/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
open class Fun{
    open func g(_ obj:Any, _ path:String, _ def:AnyObject?=nil)->AnyObject{
        var def = def
        var node = path.components(separatedBy: ".")
        
        if  node.count>0{
            let item = node.remove(at: 0)
            let newpath = node.joined(separator: ".")
            if let subobj = obj as? NSDictionary, subobj[item] != nil{
                
                if node.count==0{ def = subobj[item]! as AnyObject }
                else{ def = g(subobj[item]!, newpath, def) }
            }else if let subobj = obj as? NSArray{
                let int = Int((item as? String)!)!
                if subobj[int] != nil{
                    if node.count==0{ def = subobj[int] as AnyObject }
                    else{ def = g(subobj[int], newpath, def) }
                }
            }
        }
        return def as AnyObject
    }
}
