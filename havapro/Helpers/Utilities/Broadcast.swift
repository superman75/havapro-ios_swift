//
//  Broadcast.swift
//  havapro
//
//  Created by abdelghani on 28/08/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import Foundation
public struct BROADCAST {
    //---|   Global
        //---|   Connection
    static var CONNECTED = Notification.Name(rawValue:"CONNECTED")
        //---|   Session
    static var SESSION_LOADED = Notification.Name(rawValue:"SESSION_LOADED")
    static var SESSION_STARTED = Notification.Name(rawValue:"SESSION_STARTED")
    //---|   Customer
        //---|   loaded
    static var CUSTOMERS_LOADED = Notification.Name(rawValue:"CUSTOMESRLOADED")
        //---|   Name changed
    static var CUSTOMER_NAME_CHANGED = Notification.Name(rawValue:"customerNameChanged")
    //---|   PROVIDER
    static var PROVIDER_UPDATED = Notification.Name(rawValue:"PROVIDER_UPDATED")
    //---|   PRODUCTS
    // static var PROVIDER_UPDATED = Notification.Name(rawValue:"PROVIDER_UPDATED")
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   Interface
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    struct INTERFACE {
        //---|   Connection
        static var UPDATED = Notification.Name(rawValue:"UPDATED")
    }
    struct SETTING {
        //---|   Connection
        static var UPDATED = Notification.Name(rawValue:"UPDATED")
    }

}
