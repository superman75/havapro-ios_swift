//
//  PopupBehavior.swift
//  havapro
//
//  Created by abdelghani on 15/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit
import Spring

class PopupViewController: UIViewController, PopupContentViewController {
    //
    var popSize:CGSize = CGSize(width: 300,height: 300)
    var popup = PopupController()
    var popData:[String: Any] = [String: Any]()

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func popShowing(){}
    func popShowed(){}
    // Change orintation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        UIView.animate(withDuration: 1, animations: {
            self.view.alpha = 0
        })
        coordinator.animate(alongsideTransition: { (_) in
            self.popResize()
        }) { (_) in
            self.popResize()
            UIView.animate(withDuration: 2, animations: {
                self.view.alpha = 1
            })
        }
    }
    //---|
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize { return self.popSize }
    //---| Resize PopUp
    func popResize(_ sSize: CGSize?=nil){
        if sSize != nil{ self.popSize = sSize! }
        self.popup.viewDidLayoutSubviews()
    }

}
