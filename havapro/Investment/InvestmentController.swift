//
//  InvestmentController.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class InvestmentController: NetworksController, UITableViewDataSource, UITableViewDelegate {
    //---|   VARS
    var names:Array<String> = ["facebook", "google", "instagram", "youtube"]
    var networks:Dictionary<String, Dictionary<String, Dictionary<String, Any>>> = Dictionary<String, Dictionary<String, Dictionary<String, Any>>>()
    var design:Dictionary<String, Dictionary<String, Any>> = Dictionary<String, Dictionary<String, Any>>()
    // Outlet
        //---|   Facebook
    @IBOutlet weak var f_vHead: UIView!
    @IBOutlet weak var f_iHead: UIImageView!
    @IBOutlet weak var f_vCount: UIView!
    @IBOutlet weak var f_lCount: UILabel!
    @IBOutlet weak var f_vLoader: UIView!
    @IBOutlet weak var f_vStatics: UIView!
    @IBOutlet weak var f_lLike: UILabel!
    @IBOutlet weak var f_lShare: UILabel!
    @IBOutlet weak var f_lComment: UILabel!
    @IBOutlet weak var f_vPages: UIView!
    @IBOutlet weak var f_tPages: UITableView!
        //---|   Instgram
    @IBOutlet weak var i_vHead: UIView!
    @IBOutlet weak var i_iHead: UIImageView!
    @IBOutlet weak var i_vCount: UIView!
    @IBOutlet weak var i_lCount: UILabel!
    @IBOutlet weak var i_vLoader: UIView!
    @IBOutlet weak var i_vStatics: UIView!
    @IBOutlet weak var i_lFollow: UILabel!
    @IBOutlet weak var i_lShare: UILabel!
    @IBOutlet weak var i_lComment: UILabel!
    @IBOutlet weak var i_vPages: UIView!
    @IBOutlet weak var i_tPages: UITableView!
        //---|   Youtube
    @IBOutlet weak var y_vHead: UIView!
    @IBOutlet weak var y_iHead: UIImageView!
    @IBOutlet weak var y_vCount: UIView!
    @IBOutlet weak var y_lCount: UILabel!
    @IBOutlet weak var y_vLoader: UIView!
    @IBOutlet weak var y_vStatics: UIView!
    @IBOutlet weak var y_lLike: UILabel!
    @IBOutlet weak var y_lShare: UILabel!
    @IBOutlet weak var y_lFollow: UILabel!
    @IBOutlet weak var y_lView: UILabel!
    @IBOutlet weak var y_vPages: UIView!
    @IBOutlet weak var y_tPages: UITableView!
        //---|   Google
    @IBOutlet weak var g_vHead: UIView!
    @IBOutlet weak var g_iHead: UIImageView!
    @IBOutlet weak var g_vCount: UIView!
    @IBOutlet weak var g_lCount: UILabel!
    @IBOutlet weak var g_vLoader: UIView!
    @IBOutlet weak var g_vStatics: UIView!
    @IBOutlet weak var g_lLike: UILabel!
    @IBOutlet weak var g_lShare: UILabel!
    @IBOutlet weak var g_lComment: UILabel!
    @IBOutlet weak var g_vPages: UIView!
    @IBOutlet weak var g_tPages: UITableView!
    //---|
    var staticsCtrl:StatisticalController?=nil
    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        //---| StatisticalController
        self.staticsCtrl = (self.parent as? StatisticalController)!
        //---|   NOTIFICATION
        NotificationCenter.default.addObserver(forName: BROADCAST.CONNECTED, object: nil, queue: nil, using: self.connected)
        self.designLoad()
        for name in names{ self.animationStart(name) }
        // Sourcing Table View
        self.dataSourceing(f_tPages)
        self.dataSourceing(g_tPages)
        self.dataSourceing(i_tPages)
        self.dataSourceing(y_tPages)
    }
    //---|  dataSource
    func dataSourceing(_ tableView:UITableView){
        tableView.dataSource = self
        tableView.delegate = self

    }
    func designLoad(){
        self.design = [
            "facebook" : ["v_head" : self.f_vHead, "i_head" : self.f_iHead, "v_count" : self.f_vCount, "l_count" : self.f_lCount, "v_loader" : self.f_vLoader, "v_statics" : self.f_vStatics, "v_pages" : self.f_vPages, "t_pages" : self.f_tPages ],
            "google" : ["v_head" : self.g_vHead, "i_head" : self.g_iHead, "v_count" : self.g_vCount, "l_count" : self.g_lCount, "v_loader" : self.g_vLoader, "v_statics" : self.g_vStatics, "v_pages" : self.g_vPages, "t_pages" : self.g_tPages ],
            "instagram" : ["v_head" : self.i_vHead, "i_head" : self.i_iHead, "v_count" : self.i_vCount, "l_count" : self.i_lCount, "v_loader" : self.i_vLoader, "v_statics" : self.i_vStatics, "v_pages" : self.i_vPages, "t_pages" : self.i_tPages ],
            "youtube" : ["v_head" : self.y_vHead, "i_head" : self.y_iHead, "v_count" : self.y_vCount, "l_count" : self.y_lCount, "v_loader" : self.y_vLoader, "v_statics" : self.y_vStatics, "v_pages" : self.y_vPages, "t_pages" : self.y_tPages ]
        ]
    }
    //---| Connected
    func connected(notification: Notification?=nil){
        // print("#----\n#--| connected --:")
        // DB.PACCOUNTINGS.queryOrdered(byChild: "uid").queryEqual(toValue: app.uid).observe(.value, with: { (snap) in
        DB.PACCOUNTINGS.observe(.value, with: { (snap) in
            //---|   Show Form
            self.networks = Dictionary<String, Dictionary<String, Dictionary<String, Any>>>()
            if snap.exists() {
                for (network_id, network) in (snap.value as? Dictionary<String, Dictionary<String, Any>>)!{
                    let network_name = String(network["name"])
                    if self.networks[network_name] == nil { self.networks[network_name] = [String:[String:Any]]() }
                    self.networks[network_name]![network_id] = network
                    self.accountGet(network, network_name)
                }
            }
            for name in self.names{ self.animationStop(name) }
        })
    }
    //---|   Get The Account
    func accountGet(_ network:[String:Any], _ network_name:String){
        DB.NETWORKS.child("\(network_name)/\(String(self.g(network, "account", "")))").observe(.value, with: { (snap) in
            let network_id = network["id"] as! String
            if snap.exists(), self.networks[network_name] != nil, self.networks[network_name]![network_id] != nil {
                self.networks[network_name]![network_id]!["account"] = snap.value
            }
            //---|   Reload List
            (self.design[network_name]!["t_pages"]! as! UITableView).reloadData()
            let selectedIndex:Int = (self.design[network_name
                ]!["i_head"]! as! UIImageView).tag ?? 0
            //---|   Select Account if is first
            if self.networks[network_name]!.key(selectedIndex) == network_id {//, (self.design[network_name]!["network_name"]! as! UIView).isHidden {
                // print("#----\n#--| accountGet --: \(network_name)  --: \(network_id) |--#\n")
                self.accountSelected(network_name, self.networks[network_name]![network_id]!)
            }
        })
    }
    //---|  Animation Stop
    func animationStop(_ name:String){
        UIView.animate(withDuration: 1.0, animations: {
            (self.design[name]!["t_pages"]! as! UITableView).reloadData()
            let vLoader:UIView = (self.design[name]!["v_loader"]! as! UIView)
            vLoader.animateRemove("\(name)_rotation")
            let v_count = self.design[name]!["v_count"]! as! UIView
            v_count.isHidden = true
            if self.networks[name] != nil, self.networks[name]!.count>0 {
                //---|   Show Count Page
                if self.networks[name]!.count>1 {
                    (self.design[name]!["l_count"]! as! UILabel).text = String(self.networks[name]!.count)
                    v_count.isHidden = false
                }
            }
        })
    }
    //---|   Animation Start
    func animationStart(_ name:String){
        // UIView.animate(withDuration: 1.0, animations: {
            (self.design[name]!["v_head"]! as! UIView).isHidden = true
            (self.design[name]!["v_pages"]! as! UIView).isHidden = true
            let vLoader:UIView = (self.design[name]!["v_loader"]! as! UIView)
            vLoader.isHidden = false
            vLoader.animateStart("\(name)_rotation")
            (self.design[name]!["v_statics"]! as! UIView).alpha = 0.1
            self.view.layoutIfNeeded()
        // })
    }
    //---|
    func hasData(_ name:String){
        // UIView.animate(withDuration: 1.0, animations: {
            // print("#----\n#--| hasData --: \(name) |--#\n")
            (self.design[name]!["v_head"]! as! UIView).isHidden = false
            (self.design[name]!["v_loader"]! as! UIView).isHidden = true
            (self.design[name]!["v_statics"]! as! UIView).alpha = 1
            self.view.layoutIfNeeded()
        // })
    }
    func networkCount(_ name:String, _ network_name:String, _ field:String, _ result:[String:Any], _ network_id:String) -> String{
        let name_count = String(self.g(result, field)).toInt()
        var counts:[String:Any] = self.g(self.networks, "\(network_name).\(network_id).counts", [String:Any]()) as! [String : Any]
        if (counts["\(name)_start"] == nil) || ((name_count-String(counts["\(name)_start"]).toInt()) < 0) {
            counts["\(name)_start"] = name_count
            self.networks[network_name]![network_id]!["counts"] = counts
            DB.PACCOUNTINGS.child("\(network_id)/counts/\(name)_start").setValue(name_count)
        }
        return String(name_count-String(counts["\(name)_start"]).toInt())
    }
    //---| Statics Facebook
    func f_statics(_ network:Dictionary<String, Any>){
        //---|   Request Get Information
        var account = (network["account"] as? Dictionary<String, Any>)!
        fb_request({ (re) in
            self.animationStop("facebook")
            if let result = re["result"] as? Dictionary<String, Any>{
                let network_id = String(network["id"])
                //---|   Like
                self.f_lLike.text = self.networkCount("likes", "facebook", "fan_count", result, network_id)
                //---|   Show Information
                self.hasData("facebook")
            }

        }, String(account["id"]), ["fields" : "fan_count"], String(account["access_token"]))
    }
    //---| Statics Youtube
    func y_statics(_ network:Dictionary<String, Any>){
        //---|   Request Get Information
        var account = (network["account"] as? Dictionary<String, Any>)!
        self.y_channels({ (re) in
            self.animationStop("youtube")
            if  let statistics = re["statistics"] as? Dictionary<String, Any>{
                let network_id = String(network["id"])
                // print("----\n---\n--\nstatistics\n-----");debugPrint(statistics);print("--\n---\n----\n")
                self.y_lFollow.text = self.networkCount("follows", "youtube", "subscriberCount", statistics, network_id)
                self.y_lView.text = self.networkCount("views", "youtube", "viewCount", statistics, network_id)
                //---|   Show Information
                self.hasData("youtube")
            }
        }, String(account["access_token"]))
    }
    //---| Statics Google
    func g_statics(_ network:Dictionary<String, Any>){
        var account = (network["account"] as? Dictionary<String, Any>)!
        self.hasData("google")
    }
    //---| Statics Instagram
    func i_statics(_ network:Dictionary<String, Any>){
        var account = (network["account"] as? Dictionary<String, Any>)!
        i_request({ (re) in
            self.animationStop("instagram")
            if let result = re["result"] as? Dictionary<String, Any>{
                // print("----\n---\n--\nresult\n-----");debugPrint(result);print("--\n---\n----\n")
                let network_id = String(network["id"])
                //---|   Follows
                self.i_lFollow.text = self.networkCount("follows", "instagram", "counts.followed_by", result, network_id)
                //---|   Show Information
                self.hasData("instagram")
            }
        }, "users/\(String(account["id"]))", ["access_token" : String(account["access_token"])])
    }
    //-----------------------------------------------#
        //--|  Table View
    //-----------------------------------------------#
    // Table View Data
    func numberOfSections(in tableView: UITableView) -> Int{ return 1 }

    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(36, 0, 0, 0);
        // Menu Side Count Row
        if tableView == self.f_tPages { return (self.networks["facebook"] == nil ? 0 : self.networks["facebook"]!.count) }
        if tableView == self.i_tPages { return (self.networks["instagram"] == nil ? 0 : self.networks["instagram"]!.count) }
        if tableView == self.y_tPages { return (self.networks["youtube"] == nil ? 0 : self.networks["youtube"]!.count) }
        if tableView == self.g_tPages { return (self.networks["google"] == nil ? 0 : self.networks["google"]!.count) }
        return 0
    }
    //---|   Fill Page Table View
    func fill_pages(_ name:String,_ tableView: UITableView,_ indexPath: IndexPath) -> UITableViewCell{
        let cellPageNetwork = tableView.dequeueReusableCell(withIdentifier: "cellPageNetwork", for: indexPath) as! cellPageNetwork
        let request_id = (self.networks[name] ?? [String: Any]())!.key(indexPath.row)
        if let account = self.g(self.networks, "\(name).\(request_id).account") as? Dictionary<String, Any> {
            cellPageNetwork.name.text = String(account["name"])
            cellPageNetwork.logo.assign(String(account["picture"]))
        }
        return cellPageNetwork
    }
    // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // f_tPages
        if tableView == self.f_tPages{ return self.fill_pages("facebook", tableView, indexPath) }
        if tableView == self.i_tPages{ return self.fill_pages("instagram", tableView, indexPath) }
        if tableView == self.g_tPages{ return self.fill_pages("youtube", tableView, indexPath) }
        if tableView == self.y_tPages{ return self.fill_pages("google", tableView, indexPath) }
        return tableView.cellForRow(at: indexPath)!
    }
    //---|   Account Selected
    func accountSelected(_ name:String, _ network:Dictionary<String, Any>, _ index:Int=0){
        (self.design[name]!["v_pages"]! as! UIView).isHidden = true
        if let account = network["account"] as? [String:Any] {
            self.animationStart(name)
            let iHead = (self.design[name]!["i_head"]! as! UIImageView)
            iHead.assign(String(account["picture"]))
            iHead.tag = index
            //---|
            if name == "facebook" { self.f_statics(network) }
            if name == "instagram" { self.i_statics(network) }
            if name == "youtube" { self.y_statics(network) }
            if name == "google" { self.g_statics(network) }
        }
    }
    // Cell Selected Of Menu Side
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.f_tPages { self.accountSelected("facebook", self.networks["facebook"]!.dic(indexPath.row)) }
        if tableView == self.i_tPages { self.accountSelected("instagram", self.networks["instagram"]!.dic(indexPath.row)) }
        if tableView == self.y_tPages { self.accountSelected("youtube", self.networks["youtube"]!.dic(indexPath.row)) }
        if tableView == self.g_tPages { self.accountSelected("google", self.networks["google"]!.dic(indexPath.row)) }
    }
    func showList(_ name:String){
        UIView.animate(withDuration: 1.0, animations: {
            if self.networks[name]!.count > 1 {
                (self.design[name]!["t_pages"]! as! UITableView).reloadData()
                (self.design[name]!["v_pages"]! as! UIView).isHidden = false
                (self.design[name]!["v_loader"]! as! UIView).isHidden = true
            }else{
                self.accountSelected(name, self.networks[name]!.dic(0))
            }
        })
    }
    //---| List Facebook
    @IBAction func f_showList(_ sender: AnyObject) { self.showList("facebook") }
    //---| List Instagram
    @IBAction func i_showList(_ sender: AnyObject) { self.showList("instagram") }
    //---| List Youtube
    @IBAction func y_showList(_ sender: AnyObject) { self.showList("youtube") }
    //---| List Google plus
    @IBAction func g_showList(_ sender: AnyObject) { self.showList("google") }
}
