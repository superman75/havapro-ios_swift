//
//  BillController.swift
//  havapro
//
//  Created by Exo on 29/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import DropDown
import SearchTextField

class BillController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate  {

    //---|   VARS
    var cCustomer:Customer = Customer()
    var bills:Dictionary<String, Dictionary<String, AnyObject>> = Dictionary<String, Dictionary<String, AnyObject>>()
    var bill:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    var products:Array<Dictionary<String, AnyObject>> = Array<Dictionary<String, AnyObject>>()
    var totals:Array<Dictionary<String, AnyObject>> = Array<Dictionary<String, AnyObject>>()

    //---|   Outlet
    @IBOutlet weak var totalsCView: UICollectionView!
    @IBOutlet weak var productsTView: UITableView!
    @IBOutlet weak var billsTView: UITableView!
    @IBOutlet weak var productsView: UIView!
    @IBOutlet weak var logoProvider: avatar!
    @IBOutlet weak var nameProvider: UILabel!
    @IBOutlet weak var addressProvider: UILabel!
    @IBOutlet weak var tvaProvider: UILabel!
    @IBOutlet weak var siretProvider: UILabel!
    @IBOutlet weak var menuCView: UIView!
    //--
    //---|   Client
    @IBOutlet weak var cLoader: UIButton!
    @IBOutlet weak var cName: UIButton!
    @IBOutlet weak var cContainer: UIView!
    @IBOutlet weak var nameBill: textFieldBorderBottom!
    @IBOutlet weak var cFirstName: SearchTextField!
    @IBOutlet weak var cLastName: SearchTextField!
    @IBOutlet weak var cMobile: SearchTextField!
    @IBOutlet weak var cPhone: SearchTextField!
    @IBOutlet weak var cEmail: SearchTextField!
    @IBOutlet weak var cWebsite: textFieldBorderBottom!
    @IBOutlet weak var cAddress: textFieldBorderBottom!
    @IBOutlet weak var cPublic: UISwitch!
    @IBOutlet weak var cHeightConst: NSLayoutConstraint!
    //---|   Dropdown
    var tavDropDown:Dictionary<Int, DropDown> = Dictionary<Int, DropDown>()


    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // Products table View
        self.productsTView.dataSource = self
        self.productsTView.delegate = self

        // Totals
        self.totalsCView.dataSource = self
        self.totalsCView.delegate = self
        // Bills
        self.billsTView.dataSource = self
        self.billsTView.delegate = self
        // Design
        self.productsView.dropShadow()
        // Bill Init
        self.billInit()
        // Design
        self.datePicker.backgroundColor = UIColor("#ffffff", alpha: 0.8)
        //---|   Hide Customer Edit
        self.cHeightConst.constant == 0

        self.cCustomer.assign([
            "first_name" : cFirstName,
            "last_name" : cLastName,
            "mobile" : cMobile,
            "phone" : cPhone,
            "email" : cEmail,
            "website" : cWebsite,
            "address" : cAddress,
            "publik" : cPublic,
        ])
        //---|   Connection | Get Provider Local Information
        self.connection(){ Void in
            self.nameProvider.text = String(app.profile["company"])
            self.addressProvider.text = String(app.profile["address"])
            self.tvaProvider.text = String(app.profile["tva"])
            //self.siretProvider.text = (app.profile["siret"] as? String)!
            //--| Avatar
            self.logoProvider.assign(app.profile["logo"])
            //---|   get Customers List
            self.cCustomer.connected({
                self.billsTView.reloadData()
            })
            //---|   Bills Get
            db.bills.observe(.value, with: { (snap) in
                if !snap.exists() { return }
                self.bills = (snap.value as? Dictionary<String, Dictionary<String, AnyObject>>)!
                self.billsTView.reloadData()
            })
        }
    }
    func connected(){
        print("----------------------connected", app.uid)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        if tableView == self.productsTView { return self.products.count }
        if tableView == self.billsTView { return self.bills.count }
        return 0
    }
    // Item Set
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // productCell
        if tableView == self.productsTView {
            let productCell = tableView.dequeueReusableCell(withIdentifier: "cellBillProduct", for: indexPath) as! cellBillProduct
            let product = self.products[indexPath.row]

            productCell.name.tag = indexPath.row
            productCell.name.text = (product["name"] as? String)!
            productCell.name.addTarget(self, action: #selector(self.nameLeaved(_:)), for: .editingDidEnd)
            productCell.qty.tag = indexPath.row
            productCell.qty.text = String(describing: product["qty"]!)
            productCell.qty.addTarget(self, action: #selector(self.qtyChanging(_:)), for: .editingChanged)
            productCell.price.tag = indexPath.row
            productCell.price.text = String(describing: product["price"]!)
            productCell.price.addTarget(self, action: #selector(self.priceChanging(_:)), for: .editingChanged)
            productCell.tva.setTitle((product["tva"] as? String)!, for: UIControlState())
            // Action
            productCell.tva.tag = indexPath.row
            productCell.tva.addTarget(self, action: #selector(self.tvaDropdown(_:)), for: .touchUpInside)
            self.tavDropDown[indexPath.row] = DropDown();
            // Init Dropdown TVA
            self.dropdownInit(self.tavDropDown[indexPath.row]!, view: productCell.tvaView, datasource: app.tvas as NSArray, btnShowing: productCell.tva){ (index, tva) in
               productCell.tva.setTitle(tva, for: .normal)
               self.products[productCell.tva.tag]["tva"] = tva as AnyObject
               self.calculation()
            }
            return productCell
        }
        if tableView == self.billsTView {
            let billCell = tableView.dequeueReusableCell(withIdentifier: "cellBills", for: indexPath) as! cellBills
            billCell.isHidden = true
            if  let bill = self.bills[self.bills.key(indexPath.row)]{
                billCell.name.text = String(bill["name"], "Facture - \(indexPath.row)")
                print("#----\n#--| customer \(String(bill["customer"])).name --: \(String(self.g(cCustomer.customers, "\(String(bill["customer"])).name", "..." as AnyObject))) |--#\n")
                billCell.customer.text = String(self.g(cCustomer.customers, "\(String(bill["customer"])).name", "..." as AnyObject))
                billCell.total.text = "\(String(bill["total"], "0")) €"
                billCell.count.text = String(((bill["products"] as? NSArray)! ?? NSArray()).count)
                billCell.isHidden = false
            }
            return billCell
        }
        return tableView.cellForRow(at: indexPath)!
    }
    // Active Editing
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    // Editing
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    }
    //---|   Total
    // Number Of section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.totals.count
    }
    // Set Data
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //---|   Total
        let cellTotal = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTotals", for: indexPath) as! cellTotals
        let total = self.totals[indexPath.row]
        cellTotal.name.text = String(total["name"])
        cellTotal.value.text = String(total["value"]!)
        return cellTotal
    }
    // Selected Item at collection View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    func productAdding(){
        self.customerToggle(true)
        self.products.append(["tva":app.tvas[0] as AnyObject,"qty":"1" as AnyObject,"price":"" as AnyObject,"name":"" as AnyObject])
        self.productsTView.reloadData()
        // Focus
        if let productCell = self.productsTView!.cellForRow(at: IndexPath(row: self.products.count-1, section: 0)) as? cellBillProduct{
            productCell.name.becomeFirstResponder()
        }
    }
    // Add Product
    @IBAction func productAdd(_ sender: Any) {
        self.productAdding()

    }
    func priceInt(_ price:Any? = nil, _ back:Double=0)->Int{
        var back = back
        if price is String{
            back = Double((price as? String)!)!
        }else if price is Int{
            back = Double((price as? Int)!)
        }else if (price is Double) || (price is Float){
            back = Double((price as? Double)!)
        }
        return Int(back*100)
    }
    func priceStr(_ price:Int)->String{ return String(describing: (price/100)) }
    // Calucation
    func calculation(){
        var t_ttva:Float = 0
        var t_mtva:Float = 0
        var t_ht:Float = 0
        var t_ttc:Float = 0
        var tav_group = Dictionary<String, Float>()
        for product in self.products{
            // TVA
            if !String(product["price"]).empty(){
                let price = String(product["price"], "0").toFloat()
                if  price > 0{
                    // Convert To Int
                    let qty:Float = String(product["qty"], "1").toFloat(1)
                    let tva:Float =  String(product["tva"]).toFloat()
                    let ttc:Float = price*qty
                    let mtva:Float = ttc*(tva/100)
                    let ht:Float = ttc-mtva
                    // Totals
                    let tva_str = String(product["tva"]!)
                    if tav_group[tva_str] == nil{ tav_group[tva_str] = 0}
                    tav_group[tva_str] = tav_group[tva_str]!+mtva
                    t_ttc+=ttc
                    t_ht+=ht
                }
            }

        }
        self.totals = Array<Dictionary<String, AnyObject>>()

        self.totals.append(["key":"total_ht" as AnyObject, "name":"Total HT" as AnyObject, "value": String(NSString(format:"%.2f", Float(t_ht)))+" €" as AnyObject])
        for (ttva, mtva) in tav_group{
            self.totals.append(["key":"total_tva_\(ttva)" as AnyObject, "name":"Dont TVA \(ttva)%" as AnyObject, "value": NSString(format:"%.2f", Float(mtva)) as AnyObject])
        }
        self.totals.append(["key":"total_ttc" as AnyObject, "name":"Total TTC" as AnyObject, "value": String(NSString(format:"%.2f", Float(t_ttc)))+" €" as AnyObject])
        self.totalsCView.reloadData()
    }
    // TVA Dropdown
    func tvaDropdown(_ sender: AnyObject) {
        self.tavDropDown[Int(sender.tag!)]!.show()
    }
    func nameLeaved(_ sender:AnyObject){
        self.products[sender.tag]["name"] = (sender as! UITextField).text as AnyObject
    }
    func qtyChanging(_ sender:AnyObject){
        self.products[sender.tag]["qty"] = (sender as! UITextField).text as AnyObject
        self.calculation()
    }
    func priceChanging(_ sender:AnyObject){
        self.products[sender.tag]["price"] = (sender as! UITextField).text as AnyObject
        self.calculation()
    }
    //---|   init
    func billInit(){
        datePickerBtn.setTitle(self.datePicker.date.string("dd/MM/yyyy"), for: .normal)
        // self.nameCustomer.text=""
        self.products = Array<Dictionary<String, AnyObject>>()
        self.productAdding()
        self.calculation()
    }
    //---|   Click
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerBtn: UIButton!
    @IBAction func datePickerClick(_ sender: Any) {
        self.customerToggle(true)
        if datePickerBtn.currentTitle == "Done"{
            datePickerBtn.setTitle(self.datePicker.date.string("dd/MM/yyyy"), for: .normal)
            self.datePicker.isHidden = true

        }else{
            self.datePicker.isHidden = false
            datePickerBtn.setTitle("Done", for: .normal)
        }
    }
        //---|   Delete
    @IBAction func deleteBill(_ sender: Any) { self.billInit() }
        //---|   Save
    @IBAction func save(_ sender: Any) {
        cCustomer.save { (customer) in
            let id = app.id()
            self.bills[id] = [
                "id" : id as AnyObject,
                "name" : String(self.nameBill.text, "\(String(self.cFirstName.text)) \(String(self.cLastName.text))") as AnyObject,
                "customer" : customer as AnyObject,
                "date" : self.datePicker.date.timestamp as AnyObject,
                "products" : self.products as AnyObject,
                "totals" : self.totals as AnyObject
            ]
            db.bills.child(id).setValue(self.bills[id])
            self.billInit()
        }
    }
    //---|   logout
    @IBAction func logout(_ sender: AnyObject) { return self.logout() }
    //---|
    func customerToggle(_ close:Bool=false){
        UIView.animate(withDuration: 0.2, animations: {
            if !close && self.cHeightConst.constant == 0 {
                self.cHeightConst.constant = 420
                self.cLoader.isHidden = false
            }else{
                self.cHeightConst.constant = 0
                self.cLoader.isHidden = true
            }
        })
    }

    @IBAction func customerToggleClick(_ sender: AnyObject) {
        self.customerToggle()
    }
    @IBAction func cNameChanging(_ sender: AnyObject) {
        cName.setTitle(!String(nameBill.text).empty() ? String(nameBill.text) : "\(String(cFirstName.text)) \(String(cLastName.text))", for: .normal)
    }

}
