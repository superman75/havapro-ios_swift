//
//  cellCustomer.swift
//  havapro
//
//  Created by abdelghani on 21/08/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import Foundation
class cellCustomer: UITableViewCell {
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var option: UIButton!

}
