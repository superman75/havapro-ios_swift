//
//  MonthView.swift
//  TestCalendarLib
//
//  Created by EXIS on 17/06/16.
//  Copyright © 2016 EXIS. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import CalendarLib

class MonthView:  UIView, MGCMonthPlannerViewDelegate
{

    func monthPlannerView(_ view: MGCMonthPlannerView!, attributedStringForDayHeaderAt date: Date!) -> NSAttributedString! {
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "d";
        let dayStr = dateFormatter.string(from: date)

        var str = dayStr;

        if (Int(dayStr) == 1) {
            dateFormatter.dateFormat = "MMM d";
            str = dateFormatter.string(from: date)
        }

        let font = UIFont.systemFont(ofSize: 15)
        let attrStr = NSMutableAttributedString(string: str, attributes: [ NSFontAttributeName: font ])

        // if ([self.calendar mgc_isDate:date sameDayAsDate:[NSDate date]]) {
        if date == Date()
        {
            let boldFont = UIFont.boldSystemFont(ofSize: 15)

            let mark = MGCCircleMark()
            mark.yOffset = boldFont.descender - mark.margin;

            //let theRangeText = str.rangeOfString(dayStr)
            let start = str.characters.distance(from: str.startIndex, to: dayStr.startIndex)
            let length = dayStr.characters.distance(from: dayStr.startIndex, to: dayStr.endIndex)
            let range = NSMakeRange(start, length)
            attrStr.addAttributes([NSFontAttributeName: boldFont, NSForegroundColorAttributeName: UIColor.white, MGCCircleMarkAttributeName: mark], range: range)

            attrStr.processCircleMarks(in: NSMakeRange(0, attrStr.length))
        }

        let para = NSMutableParagraphStyle()
        para.alignment = NSTextAlignment.right;
        para.tailIndent = -6;

        attrStr.addAttributes([NSParagraphStyleAttributeName: para], range: NSMakeRange(0, attrStr.length))
        return attrStr
    }

    func monthPlannerView(_ view: MGCMonthPlannerView!, didSelectDayCellAt date: Date!) {

    }

    func monthPlannerView(_ view: MGCMonthPlannerView!, didSelectEventAt index: UInt, date: Date!) {

    }

    func monthPlannerView(_ view: MGCMonthPlannerView!, willStartMovingEventAt index: UInt, date: Date!) {

    }

    func monthPlannerView(_ view: MGCMonthPlannerView!, didDeselectEventAt index: UInt, date: Date!) {

    }
    func monthPlannerView(_ view: MGCMonthPlannerView!, shouldDeselectEventAt index: UInt, date: Date!) -> Bool {
        return true
    }

    func monthPlannerView(_ view: MGCMonthPlannerView!, shouldSelectEventAt index: UInt, date: Date!) -> Bool {
        return true
    }

    func monthPlannerView(_ view: MGCMonthPlannerView!, didShowCell cell: MGCEventView!, forNewEventAt date: Date!) {

    }

    func monthPlannerView(_ view: MGCMonthPlannerView!, didMoveEventAt index: UInt, date dateOld: Date!, to dayNew: Date!) {

    }

    func monthPlannerViewDidScroll(_ view: MGCMonthPlannerView!) {
        /*
         super.monthPlannerViewDidScroll(view)
         var date: NSDate = self.monthPlannerView.dayAtPoint(self.monthPlannerView.center)
         if date && self.delegate.respondsToSelector("calendarViewController:didShowDate:") {
         self.delegate.calendarViewController(self, didShowDate: date)
         }
         */

    }


}
