//
//  DayViewController.swift
//  havapro
//
//  Created by Exo on 21/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import CalendarLib


class DayViewController: MGCDayPlannerEAViewController, CalendarViewControllerNavigation{
    internal var visibleCalendars: NSSet?

    var myDelegate:WeekViewControllerDelegate?
    var centerDate :Date? {get{ return self.dayPlannerView.date(at: self.dayPlannerView.center, rounded:false).add(-3)}}

    var dateFormatter: DateFormatter?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dayPlannerView.backgroundColor = UIColor.clear
        self.dayPlannerView.backgroundView = UIView()
        self.dayPlannerView.backgroundView.backgroundColor = UIColor.white
        self.dayPlannerView.dateFormat = "MMM"
        self.dayPlannerView.dayHeaderHeight = 60
        //self.dayPlannerView.setac
        // self.dayPlannerView.canCreateEvents = false
        // self.dayPlannerView.canMoveEvents = false


    }

    // override func dayPlannerView(_ view: MGCDayPlannerView!, canCreateNewEventOf type: MGCEventType, at date: Date!) -> Bool {
    //     return false
    // }

    override func dayPlannerView(_ view: MGCDayPlannerView!, didScroll scrollType: MGCDayPlannerScrollType)
    {
        let date = view.date(at: view.center, rounded:true)
        if (date != nil && (self as? CalendarViewController) != nil)
       // if (date != nil && self.delegate.respondsToSelector("calendarViewController:didShowDate:") )
        {
            (self.delegate! as! CalendarViewControllerDelegate).calendarViewController(self, didShowDate: date!)
        }
    }

    // override func dayPlannerView(_ view: MGCDayPlannerView!, canMoveEventOf type: MGCEventType, at index: UInt, date: Date!, to targetType: MGCEventType, date targetDate: Date!) -> Bool {
    //    // let comps = view.calendar!.components(NSCalendarUnit.Dayday, fromDate:targetDate)
    //    // return (comps.day != 1 && comps.day != 7)
    //     return false
    // }

    override func dayPlannerView(_ view: MGCDayPlannerView!, attributedStringForDayHeaderAt date: Date!) -> NSAttributedString! {
        // let dateFormatter = DateFormatter()

        // dateFormatter.dateFormat = "eee d";
        // let dayStr = dateFormatter.string(from: date)


        // let font = UIFont.systemFont(ofSize: 15)
        // let attrStr = NSMutableAttributedString(string: dayStr, attributes: [ NSFontAttributeName: font ])

        // let now = Date()
        // if (view.calendar! as Calendar).isDateInToday(date)

        // {
        //     //.mgc_isDate(date, sameDayAs: now)
        //     let boldFont = UIFont.boldSystemFont(ofSize: 15)

        //     let mark = MGCCircleMark()
        //     mark.yOffset = boldFont.descender - mark.margin;

        //     let indexof = dayStr.characters.index(of: " ")
        //     // let circleStart = String.CharacterView.index(indexof)
        //     // //.index(indexof!, offsetBy: 1)
        //     // let start = dayStr.characters.distance(from: dayStr.startIndex, to: circleStart)
        //     // let length = String.CharacterView.distance(from: circleStart, to: dayStr.endIndex)
        //     // let range = NSMakeRange(start, length)
        //     // attrStr.addAttributes([NSFontAttributeName: boldFont, NSForegroundColorAttributeName: UIColor.white, MGCCircleMarkAttributeName: mark], range: range)

        //     //attrStr.processCircleMarks(in: NSMakeRange(0, attrStr.length))

        //     var dayStringStart: Int = (dayStr as NSString).range(of: " ").location + 1
        //     attrStr.addAttributes([NSFontAttributeName: boldFont, NSForegroundColorAttributeName: UIColor.white, MGCCircleMarkAttributeName: mark], range: NSRange(location: dayStringStart, length: dayStr.characters.count - dayStringStart))
        //     attrStr.processCircleMarks(in: NSRange(location: 0, length: attrStr.length))
        // }

        // let para = NSMutableParagraphStyle()
        // para.alignment = NSTextAlignment.center
        // attrStr.addAttributes([NSParagraphStyleAttributeName: para], range: NSMakeRange(0, attrStr.length))
        // return attrStr
        var dateFormatter: DateFormatter? = nil
        if dateFormatter == nil {
            dateFormatter = DateFormatter()
            dateFormatter?.dateFormat = "eee d"
        }
        var dayStr: String? = dateFormatter?.string(from: date)
        var font = UIFont.systemFont(ofSize: CGFloat(15))
        var attrStr = NSMutableAttributedString(string: dayStr!, attributes: [NSFontAttributeName: font])
        //if calendar.mgc_isDate(date, sameDayAsDate: Date()) {
        if (view.calendar! as Calendar).isDateInToday(date){
            var boldFont = UIFont.boldSystemFont(ofSize: CGFloat(15))
            var mark = MGCCircleMark()
            mark.yOffset = boldFont.descender - mark.margin
            var dayStringStart: Int = (dayStr as? NSString)!.range(of: " ").location + 1
            attrStr.addAttributes([NSFontAttributeName: boldFont, NSForegroundColorAttributeName: UIColor.white, MGCCircleMarkAttributeName: mark], range: NSRange(location: dayStringStart, length: (dayStr?.characters.count ?? 0) - dayStringStart))
            attrStr.processCircleMarks(in: NSRange(location: 0, length: attrStr.length))
        }
        var para = NSMutableParagraphStyle()
        para.alignment = .center
        attrStr.addAttribute(NSParagraphStyleAttributeName, value: para, range: NSRange(location: 0, length: attrStr.length))
        return attrStr

    }

    func moveToNextPageAnimated(_ animated: Bool) {
       var date:Date? = nil
        self.dayPlannerView.pageForward(animated: animated, date: nil)
    }

    func moveToPreviousPageAnimated(_ animated: Bool) {
        var date:Date? = nil
        self.dayPlannerView.pageBackwards(animated: animated, date: nil)
    }

    func moveToDate(_ date: Date, animated: Bool) {

        if (self.dayPlannerView!.dateRange == nil || self.dayPlannerView!.dateRange.contains(date))
        {
            self.dayPlannerView!.scroll(to: date, options: MGCDayPlannerScrollType.dateTime, animated:animated)
        }
    }

}
