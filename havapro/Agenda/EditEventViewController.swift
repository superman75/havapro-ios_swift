//
//  EditEventViewController.swift
//  TestCalendarLib
//
//  Created by eanton on 30/06/16.
//  Copyright © 2016 eanton. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

// class EditEventViewController: PopupViewController, UITextViewDelegate {
//     //---|   Var
//     var agendaCtrl:AgendaController?
//     var event:EAEvent?
// }





protocol EventChangedDelegate {
    func eventDismissed(_ sendor:EditEventViewController, onEventChanged event:EAEvent,_ action:String)
}
class EditEventViewController: UITableViewController, UITextViewDelegate {
    var delegate: EventChangedDelegate?
    var agendaCtrl: AgendaCtrl?
    // Outlet
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var link: UITextField!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!


    @IBOutlet weak var saveBtn: UIBarButtonItem!

    // Vars
    var event:EAEvent?
    var toggle:Dictionary<Int, Bool> = [0:false, 2:false]
    var dissmised = false
    //Load
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func showed(){
        toggleDatePicker(0)
        toggleDatePicker(2)
        notes.delegate = self
        if event != nil{
            self.titleTF.text = String(event!.title)
            self.locationTF.text = String(event!.location)
            self.link.text = String(event!.link)
            self.notes.text = String(event!.notes)
            self.startDatePicker.date = event!.startDate!
            self.endDatePicker.date = event!.endDate!
            self.startDateChanging()
            self.endDateChanging()
        }
        titleTF.becomeFirstResponder()
    }
    //---|   dismiss
    func close(){
        self.dissmised = true
        self.dismiss(animated: true)
    }
    // Cancel
    @IBAction func cancelClick(_ sender: Any) {
        if delegate != nil{ delegate!.eventDismissed(self, onEventChanged: self.event!, "cancel") }
        self.close()
    }
    // Save
    @IBAction func saveClick(_ sender: Any) {
        self.event!.title = self.titleTF.text
        self.event!.location = self.locationTF.text
        self.event!.link = self.link.text
        self.event!.notes = self.notes.text
        self.event!.startDate = self.startDatePicker.date
        self.event!.endDate = self.endDatePicker.date
        if delegate != nil{ delegate!.eventDismissed(self, onEventChanged: self.event!, "save") }
        self.close()
    }
    override func viewWillDisappear(_ animated: Bool){
        if delegate != nil, !self.dissmised{ delegate!.eventDismissed(self, onEventChanged: self.event!, "dissmis") }
    }
    //---|   TableView
        //---|   Select
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            toggleDatePicker([0:2, 2:0][indexPath.row]!, true) //---|   Close
            toggleDatePicker(indexPath.row)
        }else if indexPath.section == 3{
            //---|   Validated Event From Havatar
            if indexPath.row == 0{ self.event?.validated = true }
            //---|   Delete Event
            else{
                if delegate != nil{ delegate!.eventDismissed(self, onEventChanged: self.event!, "delete") }
                self.close()
            }
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
        //---|   Height Cell
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 1 && self.toggle[indexPath.row-1] != nil && self.toggle[indexPath.row-1]!) || (indexPath.section == 3 && event != nil && ( (indexPath.row == 0 && (String(event!.havapro).bool || String(event!.validated).bool)) || (indexPath.row == 1 && event!.adding == "true") ) )  {
            return 0
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }

    //---| DatePicker Toggle
    private func toggleDatePicker(_ row:Int, _ value:Bool?=nil) {
        self.toggle[row] = value != nil ? value : !self.toggle[row]!
        // Force table to update its contents
        tableView.beginUpdates()
        tableView.endUpdates()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //---|   Start Date Changing
    func startDateChanging(){
        self.startDateLabel.text = startDatePicker.date.string()
        self.endDatePicker.minimumDate = startDatePicker.date
    }
    @IBAction func startDateChanged(_ sender: Any) {
        self.startDateChanging()
        self.endDateChanging()
    }
    //---|   End Date Changing
    func endDateChanging(){
        self.endDateLabel.text = endDatePicker.date.string((endDatePicker.date.diff(self.startDatePicker.date, .day)>0 ? "dd/MM/yyyy HH:mm" : "HH:mm"))
        self.startDatePicker.maximumDate = self.endDatePicker.date
    }
    @IBAction func endDateChanged(_ sender: Any) {
        self.endDateChanging()
    }
    @IBAction func titleChanging(_ sender: AnyObject) {
        self.saveBtn.isEnabled = !self.titleTF.text!.isEmpty
    }
    @IBAction func pickerInit(_ sender: AnyObject) {
        toggleDatePicker(0, true)
        toggleDatePicker(2, true)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        toggleDatePicker(0, true)
        toggleDatePicker(2, true)
    }

}
