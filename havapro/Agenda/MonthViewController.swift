//  ViewController.swift
//  TestCalendarLib
//
//  Created by eanton on 13/06/16.
//  Copyright © 2016 eanton. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import CalendarLib


class MonthViewController:  MGCMonthPlannerEAViewController, CalendarViewControllerNavigation
{
    var delegate:CalendarViewControllerDelegate?
    internal var visibleCalendars: NSSet?
    var centerDate :Date?
        {
        get{
            let visibleRange = self.monthPlannerView.visibleDays
            if visibleRange != nil
            {
                let dayCount = (self.calendar! as NSCalendar).components(.day, from:visibleRange!.start, to:visibleRange!.end, options: []).day
                var comp = DateComponents()
                comp.day = dayCount! / 2;
                let centerDate = (self.calendar! as NSCalendar).date(byAdding: comp, to:visibleRange!.start, options:[])

                let startOfWeekDateComponents = (calendar! as NSCalendar).components([.yearForWeekOfYear, .weekOfYear ], from: centerDate!)
                return self.calendar!.date(from: startOfWeekDateComponents)?.add(-7)
            }
            return nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()


    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func monthPlannerViewDidScroll(_ view: MGCMonthPlannerView!) {

        super.monthPlannerViewDidScroll(view)
        let date: Date? = self.monthPlannerView.day(at: self.monthPlannerView.center)
        if (date != nil && (self as? CalendarViewController) != nil)
        {
              self.delegate!.calendarViewController(self, didShowDate: date!)
        }
    }
    override func monthPlannerView(_ view: MGCMonthPlannerView!, attributedStringForDayHeaderAt date: Date!) -> NSAttributedString!
    {
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "d";
        let dayStr = dateFormatter.string(from: date)

        var str = dayStr;

        if (Int(dayStr) == 1) {
            dateFormatter.dateFormat = "MMM d";
            str = dateFormatter.string(from: date)
        }

        let font = UIFont.systemFont(ofSize: 15)
        let attrStr = NSMutableAttributedString(string: str, attributes: [ NSFontAttributeName: font ])

        let now = Date()
        if (self.calendar! as NSCalendar).mgc_isDate(date, sameDayAs: now)
        {
            let boldFont = UIFont.boldSystemFont(ofSize: 15)

            let mark = MGCCircleMark()
            mark.yOffset = boldFont.descender - mark.margin;

            //let theRangeText = str.rangeOfString(dayStr)
            let start = str.characters.distance(from: str.startIndex, to: dayStr.startIndex)
            let length = dayStr.characters.distance(from: dayStr.startIndex, to: dayStr.endIndex)
            let range = NSMakeRange(start, length)
            attrStr.addAttributes([NSFontAttributeName: boldFont, NSForegroundColorAttributeName: UIColor.white, MGCCircleMarkAttributeName: mark], range: range)

            attrStr.processCircleMarks(in: NSMakeRange(0, attrStr.length))
        }

        let para = NSMutableParagraphStyle()
        para.alignment = NSTextAlignment.right;
        para.tailIndent = -6;

        attrStr.addAttributes([NSParagraphStyleAttributeName: para], range: NSMakeRange(0, attrStr.length))
        return attrStr
    }

    func moveToNextPageAnimated(_ animated: Bool) {
        let date = (self.calendar! as NSCalendar).mgc_nextStartOfMonth(for: self.monthPlannerView.visibleDays.start)
        self.moveToDate(date!, animated:animated)
    }

    func moveToPreviousPageAnimated(_ animated: Bool) {
        var date = (self.calendar! as NSCalendar).mgc_startOfMonth(for: self.monthPlannerView.visibleDays.start)
        if self.monthPlannerView.visibleDays.start == date  {
            var comps = DateComponents()
            comps.month = -1;
            date = (self.calendar! as NSCalendar).date(byAdding: comps, to:date!, options:[])
        }
        self.moveToDate(date!, animated:animated)
    }

    func moveToDate(_ date: Date, animated: Bool) {

         if (self.monthPlannerView!.dateRange == nil || self.monthPlannerView!.dateRange.contains(date))
        {
            self.monthPlannerView!.scroll(to: date, alignment:MGCMonthPlannerScrollAlignment.weekRow ,animated:animated)
        }
    }



}

