//
//  MGCDayPlannerEAViewController.swift
//  TestCalendarLib
//
//  Created by eanton on 28/06/16.
//  Copyright © 2016 eanton. All rights reserved.
//

import Foundation
import UIKit
import EventKit
import EventKitUI
import CalendarLib


@objc protocol MGCDayPlannerEAViewControllerDelegate {
    @objc optional func dayPlannerEAEViewController(_ vc: MGCDayPlannerEAViewController, willPresentEventViewController eventViewController: EditEventViewController)

    @objc optional func dayPlannerEAViewController(_ vc: MGCDayPlannerEAViewController, navigationControllerForPresentingEventViewController eventViewController: EditEventViewController) -> UINavigationController
}

enum EventType : Int {
    case timedEventType = 1
    case allDayEventType = 2
}


class EAEvent : NSObject{
    var id:String?
    var title:String?
    var location:String?
    var link:String?
    var notes:String?
    var havapro:Bool=false
    var validated:Bool=false
    var startDate:Date?
    var endDate:Date?
    var type:EventType = EventType.timedEventType
    var adding:String = "false"
    var allDay:Bool?
    var isAllDay:Bool=false
    //---|   Extract a dictionary
    class func extract(_ dict:Dictionary<String, AnyObject>) ->EAEvent{
        var event = EAEvent()
        for (id, value) in dict{
            if  ["startDate", "endDate"].contains(id) { event.setValue(Date(String(value), "timestamp"), forKey: id) }
            else if  ["allDay", "isAllDay", "havapro", "validated"].contains(id){ event.setValue(String(value).bool, forKey: id) }
            else if(event.responds(to: Selector(id))){ event.setValue(value, forKey: id) }
        }
        return event
    }
    //---|   Init
    class func Init(_ date:Date, _ i:Int=1)->EAEvent{
        let id = app.id()
        app.agenda[id] = [
            "id" : id as AnyObject,
            "startDate" : date.timestamp as AnyObject,
            "endDate" : date.addingTimeInterval(3600).timestamp as AnyObject,
            "adding" : "true" as AnyObject,
            "havapro" : true as AnyObject
        ]
        return EAEvent.extract(app.agenda[id]!)
    }
    //---|   Changed
    class func changed(_ event:EAEvent, _ action:String="cancel"){
        if (event.adding == "true" && (event.title == nil || event.title!.trimmingCharacters(in: .whitespaces) == "")) || action == "delete"{ app.agenda.removeValue(forKey: event.id!) }
        if app.agenda[event.id!] != nil, action == "save"{
            app.agenda[event.id!]!["adding"] = nil
            app.agenda[event.id!]!["title"] = String(event.title) as AnyObject
            app.agenda[event.id!]!["location"] = String(event.location) as AnyObject
            app.agenda[event.id!]!["link"] = String(event.link) as AnyObject
            app.agenda[event.id!]!["notes"] = String(event.notes) as AnyObject
            app.agenda[event.id!]!["havapro"] = String(event.havapro) as AnyObject
            app.agenda[event.id!]!["validated"] = String(event.validated) as AnyObject
            app.agenda[event.id!]!["startDate"] = Int64(event.startDate!.timestamp) as AnyObject
            app.agenda[event.id!]!["endDate"] = Int64(event.endDate!.timestamp) as AnyObject
            DB.AGENDA.child(event.id!).setValue(app.agenda[event.id!])
        }else if action == "delete" {
            DB.AGENDA.child(event.id!).removeValue()
        }
    }
}

class MGCDayPlannerEAViewController : MGCDayPlannerViewController, EventChangedDelegate, UIPopoverPresentationControllerDelegate{
    var calendar: Calendar?
        {
        didSet {
           self.dayPlannerView.calendar = self.calendar
        }
    }
    // var centerDate :Date? {get{ return self.dayPlannerView.date(at: self.dayPlannerView.center, rounded:false)}}

    var delegate: MGCDayPlannerEAViewControllerDelegate?

    var  bgQueue: DispatchQueue?
    var daysToLoad:[Date] = []
    var eventsCache: [Date: [AnyObject]]?
    var createdEventType:Int = 0
    var createdEventDate: Date?
    var eventController: EditEventViewController?
    var eventNavigation: UINavigationController?

    // var data : [EAEvent]?
    var ddate:Date?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventsCache = [Date:[AnyObject]]()//NSCache()
       // self.eventsCache.countLimit = cacheSize
        self.bgQueue = DispatchQueue(label: "MGCDayPlannerEAViewController.bgQueue", attributes: [])
        self.reloadEvents()
        self.dayPlannerView.register(MGCStandardEventView.self, forEventViewWithReuseIdentifier: "EventCellReuseIdentifier")
        self.dayPlannerView.dateFormat = "eee\nd \nMMM"
        //---|
        self.pop_ini()
    }
    //---|   Init pop
    func pop_ini(){
        self.eventController = UIStoryboard.init(name: "Agenda", bundle: nil).instantiateViewController(withIdentifier: "EditEventViewController") as! EditEventViewController
        self.eventController!.delegate = self
        eventNavigation = UINavigationController(rootViewController: self.eventController!)
        eventNavigation!.modalPresentationStyle = UIModalPresentationStyle.popover
    }
    //---|   Show Pop
    // func pop_show(_ event:EAEvent, _ type:MGCEventType, _ index:UInt,_ date: Date){
    func pop_show(_ date: Date, _ type:MGCEventType, _ index:UInt?=nil){
        //---|   Init
        let popover = eventNavigation!.popoverPresentationController!
        var event = EAEvent()
        var index = index
        //---|   Update Event
        if index != nil {
            event = self.eventOfType(type, atIndex: index!, date: date)
        } //---|   New Event
        else{
            event = EAEvent.Init(date, 12)
            index = UInt(self.eventOfIndex(type, date: event.startDate!, endDate: event.endDate!)!)
        }
        self.eventController?.event = event
        self.eventController?.showed()
        popover.delegate = self
        popover.sourceView = self.view
        popover.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        self.present(eventNavigation!, animated: false, completion: nil)
        DispatchQueue.main.async{
            if let view: MGCEventView = self.dayPlannerView.eventView(of: type, at: index!, date: date){
                popover.permittedArrowDirections = [.left, .right]
                popover.sourceRect = self.dayPlannerView.bounds.intersection(self.dayPlannerView.convert(view.bounds, from: view))
            }else{
                popover.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            }
        }
        self.reloadEvents()
    }

    func reloadEvents() {
        for date in self.daysToLoad {
            self.dayPlannerView.setActivityIndicatorVisible(false, for: date)
        }

        self.daysToLoad.removeAll()
        self.eventsCache!.removeAll()
        self.fetchEventsInDateRange(self.dayPlannerView.visibleDays)
        self.dayPlannerView.reloadAllEvents()
    }

    func fetchEventsInDateRange(_ range:MGCDateRange)
    {
        range.start = self.calendar?.startOfDay(for: range.start)// self.calendar!.mgc_startOfDayForDate(range.start)
        range.end = self.calendar?.startOfDay(for: range.end)//.mgc_nextStartOfDayForDate(range.end)
        range.enumerateDays(with: self.calendar, using: {(date, stop) in
            let dayEnd: Date = (self.calendar! as NSCalendar).mgc_nextStartOfDay(for: date)
            let events: [AnyObject] = self.fetchEventsFrom(date!, endDate: dayEnd, calendars: nil)
            self.eventsCache?[date!] = events
        })
    }

    func fetchEventsFrom(_ startDate: Date, endDate: Date, calendars: [AnyObject]?) -> [AnyObject] {
        var filtered = [AnyObject]()
        for (id, agenda) in app.agenda
        {
            let dato:EAEvent = EAEvent.extract(agenda)
            if dato.startDate != nil,  dato.endDate != nil, (dato.startDate!.compare(startDate) == ComparisonResult.orderedDescending || dato.startDate!.compare(startDate) == ComparisonResult.orderedSame) && (dato.endDate!.compare(endDate) == ComparisonResult.orderedAscending || dato.endDate!.compare(endDate) == ComparisonResult.orderedSame)
            {
                filtered.append(dato)
            }
        }
        return filtered
    }

    func eventsForDay(_ date: Date) -> [AnyObject] {
        let dayStart: Date = (self.calendar?.startOfDay(for: date))!
        var eventsTemp = [AnyObject]()
        //let temp = self.eventsCache?.objectForKey(dayStart)  as? [AnyObject]
        let temp = self.eventsCache?[dayStart]
        if temp == nil || temp?.count == 0 {
            let dayEnd: Date = (self.calendar! as NSCalendar).mgc_nextStartOfDay(for: dayStart)
            eventsTemp = self.fetchEventsFrom(dayStart, endDate: dayEnd, calendars: nil)
            self.eventsCache?[dayStart] = eventsTemp
        } else { eventsTemp = temp! }

        return eventsTemp
    }

    func eventsOfType(_ type: EventType?, forDay date: Date) -> [AnyObject] {
        let events: [AnyObject] = self.eventsForDay(date)
        let filteredEvents: [AnyObject] = NSMutableArray() as [AnyObject]

        return events

    }

    func eventOfType(_ type: MGCEventType, atIndex index: UInt, date: Date) -> EAEvent
    {
        var events: [AnyObject]? = nil
        if type == MGCEventType.allDayEventType {
            events = self.eventsOfType(EventType.allDayEventType, forDay: date)
        }
        else if type == MGCEventType.timedEventType {
            events = self.eventsOfType(EventType.timedEventType, forDay: date)
        }
        return events![Int(index)] as! EAEvent
    }
    func eventOfIndex(_ type: MGCEventType, date: Date, endDate:Date)->Int?{
        var events: [AnyObject]? = nil
        // let startDate = Calendar.current.startOfDay(for: date)
        if type == MGCEventType.allDayEventType {
            events = self.eventsOfType(EventType.allDayEventType, forDay: date)
        }
        else if type == MGCEventType.timedEventType {
            events = self.eventsOfType(EventType.timedEventType, forDay: date)
        }
        for (index, event) in (events?.enumerated())!{
            if let enddate = (event as! EAEvent).endDate, enddate == endDate{ return index }
        }
        return 0
    }




    override func dayPlannerView(_ weekView: MGCDayPlannerView, numberOfEventsOf type: MGCEventType, at date: Date) -> Int {
        var count: Int = 0

        count = self.eventsOfType(nil, forDay: date).count

        return count
    }

    override func dayPlannerView(_ view: MGCDayPlannerView, viewForEventOf type: MGCEventType, at index: UInt, date: Date) -> MGCEventView {
        let ev: EAEvent = self.eventOfType(type, atIndex: index, date: date)
        let evCell: MGCStandardEventView = (view.dequeueReusableView(withIdentifier: "EventCellReuseIdentifier", forEventOf: type, at: index, date: date) as! MGCStandardEventView)
        evCell.font = UIFont.systemFont(ofSize: 11)
        evCell.title = ev.title
        evCell.subtitle = ev.location
        evCell.tintColor = UIColor.black
        evCell.backgroundColor = Color.active
        evCell.color = UIColor.orange//UIColor(CGColor: ev.calendar.CGColor)
        if !String(ev.adding).bool && (String(ev.havapro).bool || String(ev.validated).bool) { evCell.color = UIColor.green }
        evCell.style = [MGCStandardEventViewStyle.border,MGCStandardEventViewStyle.plain,MGCStandardEventViewStyle.subtitle]
        return evCell
    }
    //
    override func dayPlannerView(_ view: MGCDayPlannerView!, viewForNewEventOf type: MGCEventType, at date: Date!) -> MGCEventView! {
        let evCell = MGCStandardEventView()
        evCell.title = NSLocalizedString("Nouvel évènement", comment: "")
        evCell.color = UIColor.black
        return evCell
    }

   override func dayPlannerView(_ view: MGCDayPlannerView, dateRangeForEventOf type: MGCEventType, at index: UInt, date: Date) -> MGCDateRange {
        let ev: EAEvent = self.eventOfType(type, atIndex: index, date: date)
        var end: Date = ev.endDate!
        if ev.type == EventType.allDayEventType {
            end = (self.calendar! as NSCalendar).mgc_nextStartOfDay(for: end)
        }
        return MGCDateRange(start: ev.startDate, end: end)
    }


    // Add New Event
    override func dayPlannerView(_ view: MGCDayPlannerView!, createNewEventOf type: MGCEventType, at date: Date!) {
        self.pop_show(date, type)
        self.dayPlannerView.endInteraction()
    }


    //MGCDayPlannerViewDelegate

    override func dayPlannerView(_ view: MGCDayPlannerView!, didSelectEventOf type: MGCEventType, at index: UInt, date: Date!) {
        self.pop_show(date, type, index)
    }
    func eventDismissed(_ sendor:EditEventViewController, onEventChanged event:EAEvent, _ action:String) {
        EAEvent.changed(event, action)
        self.reloadEvents()
    }


}
