//
//  MGCMonthPlannerEAViewController.swift
//  TestCalendarLib
//
//  Created by eanton on 28/06/16.
//  Copyright © 2016 eanton. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import CalendarLib

class MGCMonthPlannerEAViewController : MGCMonthPlannerViewController, EventChangedDelegate, UIPopoverPresentationControllerDelegate
{
    var calendar: Calendar?
        {
        didSet {
            self.dateFormatter.calendar = self.calendar
            self.monthPlannerView.calendar = self.calendar
        }
    }

    var  bgQueue: DispatchQueue?
    var datesForMonthsToLoad:[Date] = []
    var cachedMonths: [Date: AnyObject]?
    var dateFormatter: DateFormatter = DateFormatter()
    var visibleMonths: MGCDateRange?
    var eventController: EditEventViewController?
    var eventNavigation: UINavigationController?

    // var data : [EAEvent]?

    var agendaCtrl: AgendaCtrl = AgendaCtrl()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cachedMonths = [Date: [EAEvent]]() as [Date : AnyObject]?
        self.bgQueue = DispatchQueue(label: "MGCMonthPlannerEAViewController.bgQueue", attributes: []);

        self.dateFormatter = DateFormatter()
        self.dateFormatter.dateStyle = DateFormatter.Style.none
        self.dateFormatter.timeStyle = DateFormatter.Style.short
        self.dateFormatter.calendar = self.calendar

        self.monthPlannerView.calendar = self.calendar

        self.reloadEvents()

        self.monthPlannerView.register(MGCStandardEventView.self, forEventCellReuseIdentifier: "EventCellReuseIdentifier")
        //---|
        self.pop_ini()
    }
    //---|   Init pop
    func pop_ini(){
        self.eventController = UIStoryboard.init(name: "Agenda", bundle: nil).instantiateViewController(withIdentifier: "EditEventViewController") as! EditEventViewController
        self.eventController!.delegate = self
        eventNavigation = UINavigationController(rootViewController: self.eventController!)
        eventNavigation!.modalPresentationStyle = UIModalPresentationStyle.popover
    }
    //---|   Show Pop
    func pop_show(_ date: Date, _ view: MGCEventView!, _ index:UInt?=nil){
        //---|   Init
        let popover = eventNavigation!.popoverPresentationController!
        var event = EAEvent()
        var index = index
        //---|   Update Event
        if index != nil {
            event = self.eventAtIndex(index!, date: date)!
        } //---|   New Event
        else{
            event = EAEvent.Init(date, 12)
        }
        self.eventController?.event = event
        self.eventController?.showed()
        popover.delegate = self
        popover.sourceView = self.view
        popover.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popover.permittedArrowDirections = [.left, .right]
        self.present(eventNavigation!, animated: false, completion: nil)
        popover.sourceRect = self.monthPlannerView.bounds.intersection(self.monthPlannerView.convert(view.bounds, from: view))
        self.reloadEvents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadEventsIfNeeded()
    }

    func reloadEvents() {
        self.cachedMonths?.removeAll()
        self.loadEventsIfNeeded()
    }


    func eventsAtDate(_ date:Date) -> [EAEvent]? {
        let firstOfMonth : Date = (self.calendar! as NSCalendar).mgc_startOfMonth(for: date)
        let days = self.cachedMonths![firstOfMonth]

        if days != nil{
            let events = days![date]
            return events as? [EAEvent]
        }

        return nil

    }

    func eventAtIndex(_ index:UInt, date:Date) -> EAEvent? {
        let events = self.eventsAtDate(date)
        return events == nil ? nil : events![Int(index)]
    }

    func visibleMonthsRange() -> MGCDateRange? {
        var visibleMonthsRange: MGCDateRange? = nil
        let visibleDaysRange: MGCDateRange? = self.monthPlannerView.visibleDays
        if visibleDaysRange != nil {
            let start: Date = (self.calendar! as NSCalendar).mgc_startOfMonth(for: visibleDaysRange!.start)
            let end: Date = (self.calendar! as NSCalendar).mgc_nextStartOfMonth(for: visibleDaysRange!.end)
            visibleMonthsRange = MGCDateRange(start: start, end: end)
        }
        return visibleMonthsRange
    }

    func fetchEventsFrom(_ startDate: Date, endDate: Date, calendars: [AnyObject]?) -> [EAEvent] {
        var filtered = [EAEvent]()
        for (id, agenda) in app.agenda {
            let dato:EAEvent = EAEvent.extract(agenda)
            if dato.startDate != nil,  dato.endDate != nil, (dato.startDate!.compare(startDate) == ComparisonResult.orderedDescending || dato.startDate!.compare(startDate) == ComparisonResult.orderedSame) && (dato.endDate!.compare(endDate) == ComparisonResult.orderedAscending || dato.endDate!.compare(endDate) == ComparisonResult.orderedSame)
            {
                filtered.append(dato)
            }
        }

        let results : [EAEvent] = filtered.sorted(by: { $0.startDate!.compare($1.startDate! as Date) == .orderedAscending })
        return results
    }

    func allEventsInDateRange(_ range: MGCDateRange) -> [Date : [EAEvent]] {
        let allEvents: [EAEvent] = self.fetchEventsFrom(range.start, endDate: range.end, calendars: nil)
        let numDaysInRange: Int = range.components(.day, for: self.calendar).day!

        var eventsPerDay: [Date : [EAEvent]] = [Date : [EAEvent]](minimumCapacity: numDaysInRange)

        for ev: EAEvent in allEvents {
            let start: Date = (self.calendar! as NSCalendar).mgc_startOfDay(for: ev.startDate as Date!)
            //calendar.mgc_startOfDay(for: ev.startDate)
            let eventRange: MGCDateRange = MGCDateRange(start: start, end: ev.endDate as Date!)
            eventRange.intersect(range)
            eventRange.enumerateDays(with: self.calendar, using: { (date, stop) in
                if eventsPerDay[date!] != nil { eventsPerDay[date!]!.append(ev) }
                else { eventsPerDay[date!] = [ev] }
            })
        }


        return eventsPerDay
    }


    func addMonthToLoadingQueue(_ monthStart: Date) {
        self.datesForMonthsToLoad.append(monthStart)
        let end = (self.calendar! as NSCalendar).mgc_nextStartOfMonth(for: monthStart)!
        let range = MGCDateRange(start: monthStart, end: end)

        let dic = self.allEventsInDateRange(range!)
        self.cachedMonths![monthStart] = dic as AnyObject?

        self.monthPlannerView.reloadEvents(in: range)

    }

    func loadEventsIfNeeded() {
        self.datesForMonthsToLoad.removeAll()
        let visibleRange: MGCDateRange? = self.visibleMonthsRange()
        if visibleRange != nil{
            let months: Int = visibleRange!.components(.month, for: self.calendar).month!
            for i in 0 ..< months {
                var dc: DateComponents = DateComponents()
                dc.month = i
                let date: Date = (self.calendar! as NSCalendar).date(byAdding: dc, to: visibleRange!.start, options: [])!
                if self.cachedMonths![date] == nil
                {
                    self.addMonthToLoadingQueue(date)
                }
            }
        }
    }



    override func monthPlannerView(_ view: MGCMonthPlannerView!, numberOfEventsAt date: Date!) -> Int {
        var count = 0
        if self.eventsAtDate(date) != nil
        {
            count = (self.eventsAtDate(date)?.count)!
        }
        return count
    }

    override func monthPlannerView(_ view: MGCMonthPlannerView!, dateRangeForEventAt index: UInt, date: Date!) -> MGCDateRange! {
        let events = self.eventsAtDate(date)
        let event :EAEvent? = events![Int(index)]
        var range : MGCDateRange? = nil
        if event != nil{
            range = MGCDateRange(start: event!.startDate as Date!, end: event!.endDate as Date!)
        }

        return range
    }

    override func monthPlannerView(_ view: MGCMonthPlannerView!, cellForEventAt index: UInt, date: Date!) -> MGCEventView! {
        var events: [EAEvent] = self.eventsAtDate(date)!
        let event: EAEvent? = events[Int(index)]
        var evCell: MGCStandardEventView? = nil
        // if event != nil {
            evCell = (view.dequeueReusableCell(withIdentifier: "EventCellReuseIdentifier", forEventAt: index, date: date) as! MGCStandardEventView)
            evCell!.title = event!.title
            evCell!.subtitle = event!.location
            evCell!.detail = self.dateFormatter.string(from: event!.startDate! as Date)
            evCell!.font = UIFont.systemFont(ofSize: 11)
            evCell!.color = UIColor.orange
            if !String(event?.adding).bool && (String(event?.havapro).bool || String(event?.validated).bool) { evCell?.color = UIColor.green }
            let start: NSDate = self.calendar!.startOfDay(for: event!.startDate!) as NSDate
               // .startOfDayForDate(event!.startDate!)
            let end: NSDate = self.calendar!.startOfDay(for: event!.endDate!) as NSDate
            //let end: NSDate = self.calendar!.mgc_nextStartOfDayForDate(event!.endDate)
            let range: MGCDateRange = MGCDateRange(start: (start as? Date)!, end: (end as? Date)!)
            var numDays: Int = range.components(.day, for: self.calendar).day!
            evCell!.style = [MGCStandardEventViewStyle.detail,MGCStandardEventViewStyle.dot]
            //evCell!.style = (event!.isAllDay || numDays > 1 ? MGCStandardEventViewStyle.plain : MGCStandardEventViewStyle.default | MGCStandardEventViewStyle.dot)
            //evCell!.style |= event!.isAllDay ?? MGCStandardEventViewStyle.detail
        // }
        return evCell!
    }
    override func monthPlannerViewDidScroll(_ view: MGCMonthPlannerView!) {
        let visibleMonthsTemp = self.visibleMonthsRange()

        if !visibleMonthsTemp!.isEqual(to: self.visibleMonths)
        {
            self.visibleMonths = visibleMonthsTemp
            self.loadEventsIfNeeded()
        }
    }
    override func monthPlannerView(_ view: MGCMonthPlannerView!, didSelectEventAt index: UInt, date: Date!) {
        let cell = view.cellForEvent(at: index, date: date)
        if cell != nil{ self.pop_show(date, cell, index) }
    }
    override func monthPlannerView(_ view: MGCMonthPlannerView!, cellForNewEventAt date: Date!) -> MGCEventView! {
        //let defaultCalendar: EKCalendar? = eventStore.defaultCalendarForNewEvents
        let evCell = MGCStandardEventView()
        evCell.title = NSLocalizedString("Nouvel évènement", comment: "")
        evCell.color = UIColor.black
        return evCell
    }
    // Add New Event
    override func monthPlannerView(_ view: MGCMonthPlannerView!, didShowCell cell: MGCEventView!, forNewEventAt date: Date!) {
        self.pop_show(date, cell)
        self.monthPlannerView.endInteraction()
    }
    override func monthPlannerView(_ view: MGCMonthPlannerView!, didDeselectEventAt index: UInt, date: Date!) {}
    override func monthPlannerView(_ view: MGCMonthPlannerView!, didSelectDayCellAt date: Date!) {}
    func eventDismissed(_ sendor:EditEventViewController, onEventChanged event:EAEvent, _ action:String) {
        EAEvent.changed(event, action)
        self.reloadEvents()
    }
}
