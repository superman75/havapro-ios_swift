//
//  CalendarProtocols.swift
//  TestCalendarLib
//
//  Created by eanton on 21/06/16.
//  Copyright © 2016 eanton. All rights reserved.
//
import UIKit
import CalendarLib

protocol CalendarViewControllerNavigation
{
    var centerDate: Date? {get}
    //var visibleCalendars:Set<AnyHashable>? {get}
    var visibleCalendars: NSSet? { get set }
    


    func moveToDate(_ date: Date, animated: Bool)
    func moveToNextPageAnimated(_ animated: Bool)
    func moveToPreviousPageAnimated(_ animated: Bool)

}

typealias CalendarViewController = NSObjectProtocol & CalendarViewControllerNavigation

protocol CalendarViewControllerDelegate
{
    func calendarViewController(_ controller: CalendarViewController, didShowDate date: Date)
    func calendarViewController(_ controller: CalendarViewController, didSelectEvent event: AnyObject)
}
