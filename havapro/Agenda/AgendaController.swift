//
//  AgendaController.swift
//  havapro
//
//  Created by Exo on 20/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

enum CalendarViewType : Int {
    case calendarViewWeekType = 0
    case calendarViewMonthType = 1
    case calendarViewYearType = 2
    case calendarViewDayType = 3
}

//class CustomEventView: MGCStandardEventView {


//}
class AgendaController: AppViewController, CalendarViewControllerDelegate, EKCalendarChooserDelegate, YearViewControllerDelegate, WeekViewControllerDelegate {

    var calendarViewController: CalendarViewController?
    var monthViewController: MonthViewController?
    var weekViewController: WeekViewController?
    var yearViewController: YearViewController?
    var dayViewController: DayViewController?
    var firstTimeAppears:Bool?
    var calendar:Calendar?
    var dateFormatter:DateFormatter?
    // var datasource:[EAEvent]?
    var calendarChooser:EKCalendarChooser?
    //---|
    var eEventCtrl: EditEventViewController?
    var eventPop: PopupController?
    //---|
    @IBOutlet weak var calendar_switch: UISegmentedControl!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var current_date: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        var calendar = Calendar(identifier: Calendar.Identifier.gregorian)

        let firstWeekday = UserDefaults.standard.object(forKey: "firstDay") as? Int
        if firstWeekday != nil{
            if (firstWeekday != 0) {
                self.calendar!.firstWeekday = firstWeekday!
            }
        }
        else { calendar.firstWeekday = 1 }


        dateFormatter = DateFormatter()
        dateFormatter!.calendar = calendar
        self.calendar = calendar

        let controller:CalendarViewController = self.controllerForViewType(.calendarViewWeekType)

        self.addChildViewController(controller as! UIViewController)
        self.container.addSubview((controller as! UIViewController).view)
        (controller as! UIViewController).view.frame = self.container.bounds
        (controller as! UIViewController).didMove(toParentViewController: self)

        self.calendarViewController = controller
        self.firstTimeAppears = true

    }
    //---|   Connection | Get Provider Local Information
    override func connected(notification: Notification?=nil){
        //---|   get Agenda
            DB.AGENDA.observe(.value, with: { (snap) in
                if !snap.exists() { return }
                app.agenda = (snap.value as? Dictionary<String, Dictionary<String, AnyObject>>)!
                self.weekViewController?.reloadEvents()
                self.dayViewController?.reloadEvents()
                self.monthViewController?.reloadEvents()
            })
            //---|   Removed
            DB.AGENDA.observe(.childRemoved, with: { (snap) in
                if let agenda = snap.value as? Dictionary<String, AnyObject>{
                    app.agenda.removeValue(forKey: (agenda["id"] as? String)!)
                    self.weekViewController?.reloadEvents()
                    self.dayViewController?.reloadEvents()
                    self.monthViewController?.reloadEvents()
                }
            })
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.firstTimeAppears != nil {
            let now = Date()
            let currentDateComponents = (calendar! as NSCalendar).components([.yearForWeekOfYear, .weekOfYear ], from: now)
            let startOfWeek = calendar!.date(from: currentDateComponents)
            if startOfWeek != nil{ self.calendarViewController!.moveToDate(startOfWeek!, animated:false) }
        }
    }
    func controllerForViewType(_ type:CalendarViewType) ->  CalendarViewController
    {

        switch type
        {
            case .calendarViewWeekType:
                return getWeekViewController()
            case .calendarViewMonthType:
                return getMonthViewController()
            case .calendarViewYearType:
                return getYearViewController()
            case .calendarViewDayType:
                return getDayViewController()
        }
    }

    func getYearViewController()-> YearViewController
    {
        if self.yearViewController == nil {
            self.yearViewController = YearViewController()
            //self.yearViewController!.data = datasource
            self.yearViewController!.calendar = self.calendar
            self.yearViewController!.delegate = self
        }
        return self.yearViewController!
    }

    func getMonthViewController()-> MonthViewController
    {
        if self.monthViewController == nil {
            self.monthViewController = MonthViewController()
            // self.monthViewController!.data = self.datasource
            self.monthViewController!.calendar = self.calendar
            self.monthViewController!.delegate = self
        }
        else
        {
            // self.monthViewController!.data = self.datasource
            self.monthViewController!.reloadEvents()
        }
        return self.monthViewController!
    }

    func getWeekViewController()-> WeekViewController
    {
        if self.weekViewController == nil {
            self.weekViewController = WeekViewController()
            // self.weekViewController?.data = self.datasource
            (self.weekViewController! as MGCDayPlannerEAViewController).calendar = self.calendar
            self.weekViewController!.delegate = self
        }
        return self.weekViewController!
    }
    func getDayViewController()-> DayViewController
    {
        if self.dayViewController == nil {
            self.dayViewController = DayViewController()
            self.dayViewController!.calendar = calendar
            //self.dayViewController!.showsWeekHeaderView = true
            self.dayViewController!.dayPlannerView.dateFormat = "ee"
            self.dayViewController!.delegate = self
            //self.dayViewController!.dayPlannerView.eventCoveringType = MGCDayPlannerCoveringType.complex
            //self.dayViewController!.dayPlannerView.type


        }
        return self.dayViewController!
    }


    func calendarViewController(_ controller: CalendarViewController, didSelectEvent event: AnyObject)
    {

    }

    func calendarViewController(_ controller: CalendarViewController, didShowDate date: Date)
    {
        if controller.isKind(of: YearViewController.self)
        {
            self.dateFormatter?.dateFormat = "yyyy"
        }
        else
        {
           self.dateFormatter?.dateFormat = "MMMM yyyy"
        }

        let str = self.dateFormatter!.string(from: date)
        self.current_date.text = str
        //self.current_date.sizeToFit()
    }

    @IBAction func switchControllers(_ sender: UISegmentedControl) {
        let date = self.calendarViewController!.centerDate
        let controller = self.controllerForViewType(CalendarViewType(rawValue: sender.selectedSegmentIndex)!)
        self.moveToNewController(controller, atDate:date! as Date)
       self.calendarViewController?.moveToDate(Date(), animated: true)
    }

    @IBAction func showToday(_ sender: Any) { self.calendarViewController?.moveToDate(Date(), animated: true) }

    @IBAction func previousPage(_ sender: AnyObject) { self.calendarViewController!.moveToPreviousPageAnimated(true) }

    @IBAction func nextPage(_ sender: AnyObject) { self.calendarViewController!.moveToNextPageAnimated(true) }
    @IBAction func showCalendars(_ sender: Any) {
        if (self.calendarViewController?.responds(to: Selector("visibleCalendars")))! {
            self.calendarChooser = EKCalendarChooser(selectionStyle: .multiple, displayStyle: .allCalendars, eventStore: EKEventStore.init())
            self.calendarChooser?.delegate = self
            self.calendarChooser?.showsDoneButton = true
            self.calendarChooser?.selectedCalendars = (self.calendarViewController!.visibleCalendars as? Set<EKCalendar>)!
        }
        if (self.calendarChooser != nil) {
            var nc = UINavigationController(rootViewController: self.calendarChooser!)
            self.calendarChooser?.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.calendarChooserStartEdit))
            nc.modalPresentationStyle = .popover
            showDetailViewController(nc, sender: self)
            var popController: UIPopoverPresentationController? = nc.popoverPresentationController
            popController?.barButtonItem = (sender as? UIBarButtonItem)
        }

    }
    @IBAction func showSettings(_ sender: Any) {
        if (calendarViewController is WeekViewController) {
            // return self.performSegue(withIdentifier: "dayPlannerSettingsSegue", sender:self)
            performSegue(withIdentifier: "dayPlannerSettingsSegue", sender: nil)
        }
        else if (calendarViewController is MonthViewController) {
            // return self.performSegue(withIdentifier: "monthPlannerSettingsSegue", sender:self)
            performSegue(withIdentifier: "monthPlannerSettingsSegue", sender: nil)
        }

    }

    func dismissSettings(_ sender: UIBarButtonItem) { dismiss(animated: true, completion: { _ in }) }

    func calendarChooserStartEdit() {
        calendarChooser?.isEditing = true
        calendarChooser?.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.calendarChooserEndEdit))
    }

    func calendarChooserEndEdit() {
        calendarChooser?.isEditing = false
        calendarChooser?.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.calendarChooserStartEdit))
    }

    func moveToNewController(_ newController:CalendarViewController, atDate:Date)
    {
        let currentController = self.calendarViewController as! UIViewController
        let nextController = newController as! UIViewController
        currentController.willMove(toParentViewController: nil)
        self.addChildViewController(nextController)

        self.transition(from: currentController, to: nextController, duration: 0.5, options: .transitionFlipFromLeft, animations: {() -> Void in
            nextController.view.frame = self.container.bounds
            nextController.view.isHidden = true
            }, completion: {(finished: Bool) -> Void in
                currentController.removeFromParentViewController()
                nextController.didMove(toParentViewController: self)
                self.calendarViewController = newController
                newController.moveToDate(atDate, animated: false)
                nextController.view.isHidden = false
        })

    }

    func yearViewController(_ controller: YearViewController, didSelectMonthAtDate date: Date) {
        let controllerNew:CalendarViewController = self.controllerForViewType(.calendarViewMonthType)

        self.moveToNewController(controllerNew, atDate: date)
        self.calendar_switch.selectedSegmentIndex = 1
    }
}
