//
//  YearViewController.swift
//  TestCalendarLib
//
//  Created by eanton on 23/06/16.
//  Copyright © 2016 eanton. All rights reserved.
//

import Foundation
import CalendarLib


protocol YearViewControllerDelegate: CalendarViewControllerDelegate
{
    func yearViewController(_ controller:YearViewController, didSelectMonthAtDate date: Date)
}

class YearViewController: UIViewController, CalendarViewControllerNavigation, MGCYearCalendarViewDelegate
{
    internal var visibleCalendars: NSSet?
    var yearCalendarView:MGCYearCalendarView?
    {
        get
        {
            return self.view as? MGCYearCalendarView
        }
    }
    var calendar:Calendar?
    var delegate:YearViewControllerDelegate?
    var lastSelectedDate:Date?
    
    var centerDate :Date?
        {
        get{
            let visibleRange = self.yearCalendarView?.visibleMonthsRange
            if visibleRange != nil
            {
                let monthCount = (self.calendar! as NSCalendar).components(.month, from:visibleRange!.start, to:visibleRange!.end, options: []).month
                var comp = DateComponents()
                comp.day = monthCount! / 2;
                let centerDate = (self.calendar! as NSCalendar).date(byAdding: comp, to:visibleRange!.start, options:[])
                
                let startOfWeekDateComponents = (calendar! as NSCalendar).components([.yearForWeekOfYear, .weekOfYear ], from: centerDate!)
                let startOfWeek = self.calendar!.date(from: startOfWeekDateComponents)                
                return startOfWeek
            }
            return nil
        }
    }
    
    var dateFormatter:DateFormatter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastSelectedDate = nil
        
        self.yearCalendarView!.delegate = self;
        self.yearCalendarView!.calendar = self.calendar
        
        dateFormatter = DateFormatter()
        dateFormatter!.calendar = self.calendar
        dateFormatter!.dateFormat = "yyyy"        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        let view: MGCYearCalendarView = MGCYearCalendarView(frame: CGRect.null)
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.view = view
    }
    
    func calendarYearViewDidScroll(_ view: MGCYearCalendarView!) {
        
        if lastSelectedDate == nil
        {
            let date = self.yearCalendarView?.dateForMonth(at: self.yearCalendarView!.center)
            if date != nil{
                self.delegate?.calendarViewController(self, didShowDate: date!)
            }
        }
        else
        {
            self.delegate?.calendarViewController(self, didShowDate: lastSelectedDate!)
            
        }
        
        
    }
    
    func calendarYearView(_ view: MGCYearCalendarView!, didSelectMonthAt date: Date!) {
        self.delegate?.yearViewController(self, didSelectMonthAtDate: date)
    }
    
    func moveToDate(_ date: Date, animated: Bool) {
        lastSelectedDate = date
        self.yearCalendarView?.scroll(to: date, animated: animated)
    }
    
    func moveToNextPageAnimated(_ animated: Bool) {
        var comps = DateComponents()
        comps.year = 1
        let date = (self.calendar as NSCalendar?)?.date(byAdding: comps, to: self.yearCalendarView!.visibleMonthsRange.start, options: [])
        self.moveToDate(date!, animated: animated)
    }
    
    func moveToPreviousPageAnimated(_ animated: Bool) {
        var comps = DateComponents()
        comps.year = -1
        let date = (self.calendar as NSCalendar?)?.date(byAdding: comps, to: self.yearCalendarView!.visibleMonthsRange.start, options: [])
        self.moveToDate(date!, animated: animated)
    }
}
