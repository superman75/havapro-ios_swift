//
//  Print.swift
//  havapro
//
//  Created by abdelghani on 01/05/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import Foundation
class Print: AppViewController, Epos2PtrReceiveDelegate {

    let PRINT_PAGE_AREA_HEIGHT: Int = 500
    let PRINT_PAGE_AREA_WIDTH: Int = 500
    let PRINT_FONT_A_HEIGHT: Int = 24
    let PRINT_FONT_A_WIDTH: Int = 12
    let PRINT_BARCODE_HEIGHT_POS: Int = 70
    let PRINT_BARCODE_WIDTH_POS: Int = 110



    var printer: Epos2Printer?
    var textWarning: String=""

    var valuePrinterSeries: Epos2PrinterSeries = EPOS2_TM_M10
    var valuePrinterModel: Epos2ModelLang = EPOS2_MODEL_ANK

    //--| Get Setting
    func print_setting_get(_ key:String, _ defaul:AnyObject?=nil)-> AnyObject{
        if  let print_setting = app.profile["setting"] as? Dictionary<String, Dictionary<String, AnyObject>>, print_setting["print"] != nil, print_setting["print"]![key] != nil{
            return print_setting["print"]![key] as AnyObject
        }

        return defaul as AnyObject
    }

    //--|
    func updateButtonState(_ state: Bool) {

    }


    public func print_run(_ data:NSDictionary=NSDictionary()) -> Bool {
        self.textWarning = ""

        if !initializePrinterObject() {
            return false
        }else{
            print("False")
            dump(initializePrinterObject())
        }

        if !print_createReceiptData(data) {
            print_finalizePrinterObject()
            return false
        }

        if !print_printData() {
            print_finalizePrinterObject()
            return false
        }

        return true
    }

    public func print_runPrinterCouponSequence() -> Bool {
        self.textWarning = ""
        print("Here")
        if !initializePrinterObject() {
            return false
        }

        if !print_createCouponData() {
            print_finalizePrinterObject()
            return false
        }

        if !print_printData() {
            print_finalizePrinterObject()
            return false
        }

        return true
    }

    //---|
    func print_text_init(_ key:String, _ value:String, _ max:Int=30)->String{
        let keycount = key.characters.count
        let valuecount = value.characters.count
        var back = ""
        back+=key
        if keycount+valuecount<max{
            for _ in 1...(max-(keycount+valuecount)) { back+=" " }
        }else{
            back+="\n"
            if valuecount<max{for _ in 1...(max-valuecount) { back+=" " }}
        }
        return "\(back)\(value)\n"
    }


    public func print_createReceiptData(_ data:NSDictionary=NSDictionary()) -> Bool {
        let barcodeWidth = 2
        let barcodeHeight = 100

        var result = EPOS2_SUCCESS.rawValue

        let textData: NSMutableString = NSMutableString()
        // let logoData = UIImage(named: "store.png")
        // print("---------result---------")
        // dump(result)
        // print("---------textData---------")
        // dump(textData)

        // if logoData == nil {
        //     return false
        // }
        //---|   Center
        result = printer!.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextAlign")
            return false;
        }
        // if app.logo.image != nil{
        //     result = printer!.add(app.logo.image, x: 0, y:0,
        //         width:Int(app.logo.image!.size.width),
        //         height:Int(app.logo.image!.size.height),
        //         color:EPOS2_COLOR_1.rawValue,
        //         mode:EPOS2_MODE_MONO.rawValue,
        //         halftone:EPOS2_HALFTONE_DITHER.rawValue,
        //         brightness:Double(EPOS2_PARAM_DEFAULT),
        //         compress:EPOS2_COMPRESS_AUTO.rawValue)

        //     if result != EPOS2_SUCCESS.rawValue {
        //         MessageView.showErrorEpos(result, method:"addImage")
        //         return false
        //     }
        // }


        //---|   Ticket N
        if  let ticket_id = self.g(data, "id") as? String{
            result = printer!.addText("\n------------------------------\n")
            result = printer!.addTextSize(2, height:2)
            result = printer!.addText("\(ticket_id)\n")
            result = printer!.addTextSize(1, height:1)

        }
        //---|   Type Commande
        if  let ordertype = self.g(data, "type") as? String, app.ordertype[ordertype] != nil{
            textData.append("\(app.ordertype[ordertype]!)\n")
            var phonename = false
            if ordertype                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      == "deliver" || ordertype == "take_away" {
                var name = "N°"
                var phone = "+"
                if  let phonec = self.g(data, "phone") as? String{ phone = phonec }
                if  let namec = self.g(data, "name") as? String{ name = namec }
                if (name != "N°") ||  (phone != "+"){
                    phonename = true
                    textData.append("NOM------------------------TEL\n")
                    textData.append(print_text_init(name, phone))
                }
            }
            //---|   Address
            if ordertype == "deliver" {
                if let address = self.g(data, "state") as? String{
                    textData.append("------------------------------\n")
                    textData.append("\(address)\n")
                }else{
                    var address:Array<String> = Array<String>()
                    if let add =  self.g(data, "addr_number" ) as? String{ address.append(add) }
                    if let add =  self.g(data, "addr_street" ) as? String{ address.append(add) }
                    if let add =  self.g(data, "addr_city" ) as? String{ address.append(add) }
                    if let add =  self.g(data, "addr_interphone" ) as? String{ address.append(add) }
                    if let add =  self.g(data, "addr_digicode" ) as? String{ address.append(add) }
                    if let add =  self.g(data, "addr_building" ) as? String{ address.append(add) }
                    if let add =  self.g(data, "addr_floor" ) as? String{ address.append(add) }
                    if let add =  self.g(data, "addr_door" ) as? String{ address.append(add) }
                    if address.count>0 {
                        textData.append("------------------------------\n")
                        textData.append("\(address.joined(separator: " "))\n")
                    }
                }
            }
        }
        //---|   SetClean
        result = printer!.addText(textData as String)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addText")
            return false;
        }
        textData.setString("")

        //---|   Wifi
        if  let namewifi = self.g(app.profile, "setting.print.namewifi") as? String, let pwdwifi = self.g(app.profile, "setting.print.pwdwifi") as? String{
            textData.append("-------------WIFI-------------\n")
            textData.append(print_text_init(namewifi, pwdwifi))
        }


        //---|   Hello
        if let hello = self.print_setting_get("hello") as? String {
            textData.append("------------------------------\n")
            textData.append("\(hello)\n")
        }

        //---|   Date & Hour
        textData.append("------------------------------\n")
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let date = formatter.string(from: Date())
        formatter.dateFormat = "HH:mm"
        let hour = formatter.string(from: Date())
        textData.append(print_text_init(date, hour))

        //---|   Products
        textData.append("------------------------------\n")
        textData.append(print_text_init("Produit", "Prix"))
        textData.append("\n\n")

        //---|   SetClean
        result = printer!.addText(textData as String)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addText")
            return false;
        }
        textData.setString("")

        //---|   Products List
        if let productsText = data["productsText"] as? String{
            result = printer!.addText(productsText)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addText")
                return false;
            }
        }


        //---|   Total
        let total = self.g(data, "total")
        if total != nil{
            result = printer!.addTextSize(2, height:2)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addTextSize")
                return false
            }
            result = printer!.addText(print_text_init("TOTAL", String(describing: total), 15))
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addText")
                return false;
            }
            result = printer!.addTextSize(1, height:1)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addTextSize")
                return false;
            }
        }


        result = printer!.addFeedLine(1)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addFeedLine")
            return false;
        }

        //---|   End : Back Monay
        if let endText = data["endText"] as? String{
            result = printer!.addText(endText)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addText")
                return false;
            }
        }

        if let phone = app.profile["phone"] {
            textData.append("------------------------------\n")
            textData.append("\(phone)\n")
            result = printer!.addText(textData as String)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addText")
                return false;
            }
            textData.setString("")
        }
        // result = printer!.addText(textData as String)
        // if result != EPOS2_SUCCESS.rawValue {
        //     MessageView.showErrorEpos(result, method:"addText")
        //     return false
        // }
        // textData.setString("")

        // // Section 4 : Advertisement
        // textData.append("Purchased item total number\n")
        // textData.append("Sign Up and Save !\n")
        // textData.append("With Preferred Saving Card\n")
        // result = printer!.addText(textData as String)
        // if result != EPOS2_SUCCESS.rawValue {
        //     MessageView.showErrorEpos(result, method:"addText")
        //     return false;
        // }
        // textData.setString("")
        //---|
        if (print_setting_get("barcode", false as AnyObject?) as? Bool)!{
            result = printer!.addFeedLine(2)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addFeedLine")
                return false
            }
            result = printer!.addBarcode("01209457",
                type:EPOS2_BARCODE_CODE39.rawValue,
                hri:EPOS2_HRI_BELOW.rawValue,
                font:EPOS2_FONT_A.rawValue,
                width:barcodeWidth,
                height:barcodeHeight)
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addBarcode")
                return false
            }
        }

        result = printer!.addCut(EPOS2_CUT_FEED.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addCut")
            return false
        }

        return true
    }

    public func print_createCouponData() -> Bool {
        let barcodeWidth = 2
        let barcodeHeight = 64

        var result = EPOS2_SUCCESS.rawValue

        if printer == nil {
            return false
        }

        let coffeeData = UIImage(named: "coffee.png")
        let wmarkData = UIImage(named: "wmark.png")

        if coffeeData == nil || wmarkData == nil {
            return false
        }

        result = printer!.addPageBegin()
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPageBegin")
            return false
        }

        result = printer!.addPageArea(0, y:0, width:PRINT_PAGE_AREA_WIDTH, height:PRINT_PAGE_AREA_HEIGHT)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPageArea")
            return false
        }

        result = printer!.addPageDirection(EPOS2_DIRECTION_TOP_TO_BOTTOM.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPageDirection")
            return false
        }

        result = printer!.addPagePosition(0, y:Int(coffeeData!.size.height))
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPagePosition")
            return false
        }

        result = printer!.add(coffeeData, x:0, y:0,
            width:Int(coffeeData!.size.width),
            height:Int(coffeeData!.size.height),
            color:EPOS2_PARAM_DEFAULT,
            mode:EPOS2_PARAM_DEFAULT,
            halftone:EPOS2_PARAM_DEFAULT,
            brightness:3,
            compress:EPOS2_PARAM_DEFAULT)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addImage")
            return false
        }

        result = printer!.addPagePosition(0, y:Int(wmarkData!.size.height))
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPagePosition")
            return false
        }

        result = printer!.add(wmarkData, x:0, y:0,
            width:Int(wmarkData!.size.width),
            height:Int(wmarkData!.size.height),
            color:EPOS2_PARAM_DEFAULT,
            mode:EPOS2_PARAM_DEFAULT,
            halftone:EPOS2_PARAM_DEFAULT,
            brightness:Double(EPOS2_PARAM_DEFAULT),
            compress:EPOS2_PARAM_DEFAULT)

        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addImage")
            return false
        }

        result = printer!.addPagePosition(PRINT_FONT_A_WIDTH * 4, y:(PRINT_PAGE_AREA_HEIGHT / 2) - (PRINT_FONT_A_HEIGHT * 2))
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPagePosition")
            return false
        }

        result = printer!.addTextSize(3, height:3)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextSize")
            return false
        }

        result = printer!.addTextStyle(EPOS2_PARAM_DEFAULT, ul:EPOS2_PARAM_DEFAULT, em:EPOS2_TRUE, color:EPOS2_PARAM_DEFAULT)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextStyle")
            return false
        }

        result = printer!.addTextSmooth(EPOS2_TRUE)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextSmooth")
            return false
        }

        result = printer!.addText("FREE Coffee\n")
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addText")
            return false
        }

        result = printer!.addPagePosition((PRINT_PAGE_AREA_WIDTH / barcodeWidth) - PRINT_BARCODE_WIDTH_POS, y:Int(coffeeData!.size.height) + PRINT_BARCODE_HEIGHT_POS)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPagePosition")
            return false
        }

        result = printer!.addBarcode("01234567890", type:EPOS2_BARCODE_UPC_A.rawValue, hri:EPOS2_PARAM_DEFAULT, font: EPOS2_PARAM_DEFAULT, width:barcodeWidth, height:barcodeHeight)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addBarcode")
            return false
        }

        result = printer!.addPageEnd()
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addPageEnd")
            return false
        }

        result = printer!.addCut(EPOS2_CUT_FEED.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addCut")
            return false
        }

        return true
    }

    public func print_printData() -> Bool {
        var status: Epos2PrinterStatusInfo?

        if printer == nil {
            return false
        }

        if !print_connectPrinter() {
            return false
        }

        status = printer!.getStatus()
        print_dispPrinterWarnings(status)

        if !print_isPrintable(status) {
            MessageView.show(print_makeErrorMessage(status))
            printer!.disconnect()
            return false
        }
        // print("------| printer ------------")
        // dump(printer)

        let result = printer!.sendData(Int(EPOS2_PARAM_DEFAULT))
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"sendData")
            printer!.disconnect()
            return false
        }

        return true
    }

    public func initializePrinterObject() -> Bool {
        do{
            printer = try Epos2Printer(printerSeries: (self.print_setting_get("serie", 0 as AnyObject) as? Int32)!, lang: (self.print_setting_get("lang", 0 as AnyObject) as? Int32)!)
        }catch _ {
            return false
        }
        // printer = Epos2Printer(printerSeries: (self.print_setting_get("serie", 0 as AnyObject) as? Int32)!, lang: (self.print_setting_get("lang", 0 as AnyObject) as? Int32)!)

        if printer == nil {
            return false
        }
        printer!.setReceiveEventDelegate(self)
        dump(printer)
        return true
    }

    public func print_finalizePrinterObject() {
        if printer == nil {
            return
        }

        printer!.clearCommandBuffer()
        printer!.setReceiveEventDelegate(nil)
        printer = nil
    }

    public func print_connectPrinter() -> Bool {
        if let target = self.print_setting_get("id") as? String {
            var result: Int32 = EPOS2_SUCCESS.rawValue

            if 52 == nil { return false }
            result = printer!.connect(target, timeout:Int(EPOS2_PARAM_DEFAULT))
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"connect")
                return false
            }

            result = printer!.beginTransaction()
            if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"beginTransaction")
                printer!.disconnect()
                return false

            }
        }else{
            MessageView.show("Please Set Address Mac of your print")
            return false
        }
        return true
    }

    public func print_disconnectPrinter() {
        var result: Int32 = EPOS2_SUCCESS.rawValue

        if printer == nil {
            return
        }

        result = printer!.endTransaction()
        if result != EPOS2_SUCCESS.rawValue {
            DispatchQueue.main.async(execute: {
                MessageView.showErrorEpos(result, method:"endTransaction")
            })
        }

        result = printer!.disconnect()
        if result != EPOS2_SUCCESS.rawValue {
            DispatchQueue.main.async(execute: {
                MessageView.showErrorEpos(result, method:"disconnect")
            })
        }

        print_finalizePrinterObject()
    }
    public func print_isPrintable(_ status: Epos2PrinterStatusInfo?) -> Bool {
        if status == nil {
            return false
        }

        if status!.connection == EPOS2_FALSE {
            return false
        }
        else if status!.online == EPOS2_FALSE {
            return false
        }
        else {
            // print available
        }
        return true
    }

    open func onPtrReceive(_ printerObj: Epos2Printer!, code: Int32, status: Epos2PrinterStatusInfo!, printJobId: String!) {
        MessageView.showResult(code, errMessage: print_makeErrorMessage(status))

        print_dispPrinterWarnings(status)
        updateButtonState(true)

        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            self.print_disconnectPrinter()
        })
    }

    public func print_dispPrinterWarnings(_ status: Epos2PrinterStatusInfo?) {
        if status == nil {
            return
        }

        self.textWarning = ""

        if status!.paper == EPOS2_PAPER_NEAR_END.rawValue {
            self.textWarning = NSLocalizedString("warn_receipt_near_end", comment:"")
        }

        if status!.batteryLevel == EPOS2_BATTERY_LEVEL_1.rawValue {
            self.textWarning = NSLocalizedString("warn_battery_near_end", comment:"")
        }
    }

    public func print_makeErrorMessage(_ status: Epos2PrinterStatusInfo?) -> String {
        let errMsg = NSMutableString()
        if status == nil {
            return ""
        }

        if status!.online == EPOS2_FALSE {
            errMsg.append(NSLocalizedString("err_offline", comment:""))
        }
        if status!.connection == EPOS2_FALSE {
            errMsg.append(NSLocalizedString("err_no_response", comment:""))
        }
        if status!.coverOpen == EPOS2_TRUE {
            errMsg.append(NSLocalizedString("err_cover_open", comment:""))
        }
        if status!.paper == EPOS2_PAPER_EMPTY.rawValue {
            errMsg.append(NSLocalizedString("err_receipt_end", comment:""))
        }
        if status!.paperFeed == EPOS2_TRUE || status!.panelSwitch == EPOS2_SWITCH_ON.rawValue {
            errMsg.append(NSLocalizedString("err_paper_feed", comment:""))
        }
        if status!.errorStatus == EPOS2_MECHANICAL_ERR.rawValue || status!.errorStatus == EPOS2_AUTOCUTTER_ERR.rawValue {
            errMsg.append(NSLocalizedString("err_autocutter", comment:""))
            errMsg.append(NSLocalizedString("err_need_recover", comment:""))
        }
        if status!.errorStatus == EPOS2_UNRECOVER_ERR.rawValue {
            errMsg.append(NSLocalizedString("err_unrecover", comment:""))
        }

        if status!.errorStatus == EPOS2_AUTORECOVER_ERR.rawValue {
            if status!.autoRecoverError == EPOS2_HEAD_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_head", comment:""))
            }
            if status!.autoRecoverError == EPOS2_MOTOR_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_motor", comment:""))
            }
            if status!.autoRecoverError == EPOS2_BATTERY_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_battery", comment:""))
            }
            if status!.autoRecoverError == EPOS2_WRONG_PAPER.rawValue {
                errMsg.append(NSLocalizedString("err_wrong_paper", comment:""))
            }
        }
        if status!.batteryLevel == EPOS2_BATTERY_LEVEL_0.rawValue {
            errMsg.append(NSLocalizedString("err_battery_real_end", comment:""))
        }

        return errMsg as String
    }

}
