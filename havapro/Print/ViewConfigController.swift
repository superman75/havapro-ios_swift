

import UIKit
import DropDown

class ViewConfigController: AppViewController, UITableViewDataSource, UITableViewDelegate, Epos2DiscoveryDelegate {
    //---|   CLASS
    // let cPrint:PrintManager = PrintManager()
    @IBOutlet weak var buttonLang: UIButton!
    @IBOutlet weak var buttonPrinterSeries: UIButton!
    @IBOutlet weak var buttonReceipt: UIButton!
    @IBOutlet weak var buttonCoupon: UIButton!
    @IBOutlet weak var textWarnings: UITextView!
    @IBOutlet weak var textTarget: UITextField!
    // Outlet
    @IBOutlet weak var pwdwifiTF: textFieldBorderBottom!
    @IBOutlet weak var namewifiTF: textFieldBorderBottom!
    @IBOutlet weak var companyProvider: UILabel!
    @IBOutlet weak var nameProvider: UILabel!
    @IBOutlet weak var logoProvider: avatar!

    // Table View
    @IBOutlet weak var printerTV: UITableView!
    @IBOutlet weak var menuCView: UIView!
    @IBOutlet weak var productCView: UIView!

    // var printerSelect: CustomPickerDataSource?
    // var langList: CustomPickerDataSource?

    // var printerPicker: CustomPickerView?
    // var langPicker: CustomPickerView?
    /*-----------------------------------------------*\
       #--|  Search Print
    \*-----------------------------------------------*/
    public var printerList: [Epos2DeviceInfo] = []
    fileprivate var filterOption: Epos2FilterOption = Epos2FilterOption()
    //---|
    var printerCtrl: PrinterDefaultConfigController?
    var printerPop: PopupController?
    // var delegate: DiscoveryViewDelegate?

    //--|  Outlit
    @IBOutlet weak var printerTView: UITableView!
    @IBOutlet weak var v_printer: PrinterDefaultConfigView!

    override func viewDidLoad() {
        super.viewDidLoad()
        //---|   Print
        // self.cPrint.print_ini(self)
        //---|   Includes Products LIST
        self.includeView(ConfigProductController(), self.productCView) //---|   Includes Products LIST
        self.printerTView.binding(self)
        filterOption.deviceType = EPOS2_TYPE_PRINTER.rawValue
        // print("-----------| filterOption.deviceType |----------\n filterOption.deviceType --> ");debugPrint(filterOption.deviceType);print(" |\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
    }
    //---|   Showed
    override func showed(){
        super.showed()
        self.v_printer.ini()
        refresh_list()
    }
    override func connected(notification: Notification?=nil){
        refresh_list()
    }
    override func setting_updated(notification: Notification?=nil){
        self.pwdwifiTF.text = String(self.g(app.settings, "peripheral.wifi.pwd", ""))
        self.namewifiTF.text = String(self.g(app.settings, "peripheral.wifi.name", ""))
        refresh_list()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // let result = Epos2Discovery.start(filterOption, delegate: self)
        // if result != EPOS2_SUCCESS.rawValue {
        //     MessageView.showErrorEpos(result, method: "start")
        // }
        // print("-----------| viewDidAppear |----------\n result --> \(result) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        // printerTView.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)


        // while Epos2Discovery.stop() == EPOS2_ERR_PROCESSING.rawValue {
        //     // retry stop function
        // }

        // printerList.removeAll()
    }


    func setDoneToolbar() {
        let doneToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent

        doneToolbar.sizeToFit()
        let space = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target:self, action:nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.done, target:self, action:#selector(ViewConfigController.doneKeyboard(_:)))

        doneToolbar.setItems([space, doneButton], animated:true)
        textTarget.inputAccessoryView = doneToolbar
    }

    func doneKeyboard(_ sender: AnyObject) {
        textTarget.becomeFirstResponder()
    }


    func refresh_list(){
        //---| Connect
        let btConnection = Epos2BluetoothConnection()
        let result = Epos2Discovery.start(filterOption, delegate: self)
        if result != EPOS2_SUCCESS.rawValue {
            //ShowMsg showErrorEpos(result, method: "start")
        }
        printerTView.reloadData()
    }
    // func customPickerView(_ pickerView: CustomPickerView, didSelectItem text: String, itemValue value: Any) {
    //     if pickerView == printerPicker {
    //         self.buttonPrinterSeries.setTitle(text, for:UIControlState())
    //         self.valuePrinterSeries = value as! Epos2PrinterSeries
    //         self.providerSet("serie", self.valuePrinterSeries.rawValue as AnyObject)
    //     }
    //     if pickerView == langPicker {
    //         self.buttonLang.setTitle(text, for:UIControlState())
    //         self.valuePrinterModel = value as! Epos2ModelLang
    //         self.providerSet("lang", self.valuePrinterModel.rawValue as AnyObject)
    //     }
    // }

    func connectDevice( ) {
        // print("-----------| connectDevice |----------\n")
        Epos2Discovery.stop()
        printerList.removeAll()

        let btConnection = Epos2BluetoothConnection()
        // print("-----------| btConnection |----------\n btConnection --> ");debugPrint(btConnection);print(" |\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        let BDAddress = NSMutableString()
        // print("-----------| BDAddress |----------\n BDAddress --> ");debugPrint(BDAddress);print(" |\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        let result = btConnection?.connectDevice(BDAddress)
        // print("-----------| connectDevice result |----------\n result --> ");debugPrint(result);print(" |\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        if result == EPOS2_SUCCESS.rawValue {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else {
            Epos2Discovery.start(filterOption, delegate:self)
            printerTView.reloadData()
        }
    }
    func f_dump(_ deviceInfo: Epos2DeviceInfo!){
        print("deviceInfo.deviceType --> \(deviceInfo.deviceType) \n")
        print("deviceInfo.target --> \(deviceInfo.target) \n")
        print("deviceInfo.deviceName --> \(deviceInfo.deviceName) \n")
        print("deviceInfo.ipAddress --> \(deviceInfo.ipAddress) \n")
        print("deviceInfo.macAddress --> \(deviceInfo.macAddress) \n")
        print("deviceInfo.bdAddress --> \(deviceInfo.bdAddress) \n")
    }
    func onDiscovery(_ deviceInfo: Epos2DeviceInfo!) {
        // print("-----------| onDiscovery |----------\n deviceInfo --> ");f_dump(deviceInfo);print(" |\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        printerList.append(deviceInfo)
        printerTView.reloadData()
    }

    //---|   Set to provider
    func providerSet(_ key:String, _ value:AnyObject){
        DB.LPROVIDER.child("setting/print/\(key)").setValue(value)
        var printSetting:Dictionary<String, Dictionary<String, AnyObject>> = Dictionary<String, Dictionary<String, AnyObject>>()
        if app.profile["setting"] != nil {
            printSetting = (app.profile["setting"] as? Dictionary<String, Dictionary<String, AnyObject>>)!
        }
        if printSetting == nil { printSetting = ["print":[:]] }
        if printSetting["print"] == nil { printSetting["print"] = [:] }
        printSetting["print"]![key] = value
        app.profile["setting"] = printSetting as AnyObject
    }
    func printer_search(){
        self.connectDevice()
        //---|
        var result = EPOS2_SUCCESS.rawValue;
        while true {
            result = Epos2Discovery.stop()

            if result != EPOS2_ERR_PROCESSING.rawValue {
                if (result == EPOS2_SUCCESS.rawValue) {
                    break;
                }
                else {
                    // MessageView.showErrorEpos(result, method:"stop")
                    return;
                }
            }
        }
        print("-----------| result |----------\n result --> \(result) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")


        printerList.removeAll()
        printerTView.reloadData()

        result = Epos2Discovery.start(filterOption, delegate:self)
        print("-----------| Epos2Discovery.start |----------\n result --> \(result) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"start")
        }
        //---|   Refresh Print
        self.setting_updated()
    }
    func printer_dic(_ info:Epos2DeviceInfo) -> [String:Any]{
        return [
            "deviceType" : info.deviceType,
            "target" : info.target,
            "deviceName" : info.deviceName,
            "ipAddress" : String(info.ipAddress),
            "macAddress" : String(info.macAddress),
            "bdAddress" : String(info.bdAddress)
        ]
    }
    //---|status = printer!.getStatus()
    func printer_pop( _ info:[String:Any]=[String:Any]() ) {
        if printerPop == nil {
            printerCtrl = UIStoryboard(name: "ViewConfig", bundle: nil).instantiateViewController(withIdentifier: "PrinterDefaultConfigController") as! PrinterDefaultConfigController
            printerCtrl!.configCtrl = self
            printerPop = self.popup(printerCtrl!)
        } //---|
        else{
            printerPop!.show(printerCtrl!)
            printerCtrl!.popShowed()
        }
        printerCtrl!.info = info
    }
    //---|
    func printer_set(_ tag:Int, _ info:[String:Any]){
        let name = self.cPrint.printer_names[tag]!
        SettingManager.update("peripheral", "printer", name , info)
        // self.timeout({
        //     self.v_printer.print(tag, self.cPrint.print_status(name))
        // }, 10)
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   BUTTON ACTIONS
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    @IBAction func printer_connect_click(_ sender: AnyObject) {
        // self.v_printer.print(sender.tag, self.cPrint.print_status(self.cPrint.printer_names[sender.tag]!))
        (sender as? UIButton)!.loading()
    }
    @IBAction func didTouchUpInside(_ sender: AnyObject) {
        // self.cPrint.print_about()
        self.cPrint.print_drawer()
        (sender as? UIButton)!.loading()
    }
    @IBAction func searchPrinter(_ sender: AnyObject) {
        self.printer_search()
        (sender as? UIButton)!.loading()
    }
    @IBAction func pwdwifiBlur(_ sender: Any) { SettingManager.update("peripheral", "wifi", "pwd", pwdwifiTF.text) }
    @IBAction func namewifiBlur(_ sender: Any) { SettingManager.update("peripheral", "wifi", "name", namewifiTF.text) }
    @IBAction func targetBlur(_ sender: Any) { self.providerSet("id", textTarget.text as AnyObject) }
    //---|  Provider State (Enable || Disable)
    @IBOutlet weak var providerState: UISwitch!
    @IBAction func providerStateChanged(_ sender: Any) {
        app.profile["online"] = providerState.isOn as AnyObject
        DB.LPROVIDER.child("online").setValue(providerState.isOn)
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   TABLE VIEW
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    func numberOfSections(in tableView: UITableView) -> Int {
        if  (tableView.tag as? Int)!>0{ return 1 }
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //---|   Refresh status
        for (tag, name) in self.cPrint.printer_names { self.v_printer.print(tag, self.cPrint.print_status(name, printerList)) }
        // Components
        // if  (tableView.tag as? Int)!>0{
        //     return self.cp_numberOfRowsInSection( tableView, numberOfRowsInSection:section)
        // }
        // else{
            if section == 0 { return printerList.count }
            return 1
        // }
        // return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //---|   Components
        // if tableView.tag as! Int>0{
        //     return self.cp_cellForRowAt( tableView, cellForRowAt: indexPath)
        // } //---|   Printer
        // else{
            let identifier = "basis-cell"
            var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier)
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: identifier)
            }

            if indexPath.section == 0 {
                if indexPath.row >= 0 && indexPath.row < printerList.count {
                    cell!.textLabel?.text = String(printerList[indexPath.row].deviceName)
                    cell!.detailTextLabel?.text = String(printerList[indexPath.row].target)
                   // self.printer_pop(self.printer_dic(printerList[indexPath.row]))
                }
            }
            else {
                cell!.textLabel?.text = "Actualités..."
                cell!.detailTextLabel?.text = "..."
            }

            return cell!
        // }
        return tableView.cellForRow(at: indexPath)!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag as! Int>0{
            print("-----------| tableView.tag : \(tableView.tag) |----------")
        } else if indexPath.section == 0 {
            self.printer_pop(self.printer_dic(printerList[indexPath.row]))
        }
        else {
            print("-----------| performSelector |----------\n indexPath.row --> \(indexPath.row) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            performSelector(onMainThread: #selector(ViewConfigController.connectDevice), with:self, waitUntilDone:false)
        }
    }
}

