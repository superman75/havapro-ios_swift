//
//  ViewController.swift
//  havapro
//
//  Created by Exo on 05/09/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
import DropDown
import PMAlertController
import SearchTextField

class SaleController: Transaction, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    //---|   CLASS
    // let cPrint:PrintManager = PrintManager()
    var cCustomer:Customer = Customer()
    //---|   VARS
    var snaps = Dictionary<String, AnyObject>()
    var productsByCategory = Dictionary<String, Array<AnyObject>>()
    var products = Dictionary<String, Dictionary<String, Any>>()
    var componentsProduct = [String : Array<Any>]()
    var categoriesProduct = Array<Dictionary<String, Any>>()
    var consoles = Array<Dictionary<String, String>>()
    var cateoryProduct:String = ""
    var db_orders = Dictionary<String, Dictionary<String, Any>>()
    var db_users = Dictionary<String, Dictionary<String, Any>>()
    var commandes:[String:[String:Any]] = [String:[String:Any]]()//[[:]]//Dictionary<String, Dictionary<String, Any>>
    var commandeCurrent:String = ""
    var commandeId:Int = 1
        // Init
    //---|   Components
    // var componentsCommande:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    var consoleComponentEditing:String?=nil
    var components_commande:[String:[String:Any]] = [String:[String:Any]]()
    @IBOutlet var tv_component: UITableView!
    @IBOutlet var v_component_model: UIView!
    @IBOutlet var v_component_validate: Btn!
    //---|
    @IBOutlet var productConsole: UITableView!
    @IBOutlet var productList: UICollectionView!
        //---|   Categories
    @IBOutlet var categoriesCView: UICollectionView!
    @IBOutlet var categoriesView: UIView!
    @IBOutlet var categoryProduct: UIView!

        //---|
    @IBOutlet var viewProductSlide: UIView!
    @IBOutlet var productSlider: UICollectionView!
    @IBOutlet var btnRightProductSlider: UIButton!
    @IBOutlet var btnLeftProductSlider: UIButton!
        // For System
    var indexProductSlider : Int = 0

    // UIView
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productConsoleView: UIView!
    @IBOutlet weak var mainView: UIView!
    // Constraint

    // Labels
    @IBOutlet var orderNowBtn: UIButton!

    //---| Customer
    @IBOutlet weak var cFirstName: SearchTextField!
    @IBOutlet weak var cLastName: SearchTextField!
    @IBOutlet weak var cMobile: SearchTextField!
    @IBOutlet weak var addr_number: UITextField!
    @IBOutlet weak var addr_street: UITextField!
    @IBOutlet weak var addr_zip: UITextField!
    @IBOutlet weak var addr_city: UITextField!
    @IBOutlet weak var addr_interphone: UITextField!
    @IBOutlet weak var addr_digicode: UITextField!
    @IBOutlet weak var addr_building: UITextField!
    @IBOutlet weak var addr_floor: UITextField!
    @IBOutlet weak var addr_door: UITextField!

    //---|   Commande
    var commandeCtrl:CommandeController?
    @IBOutlet weak var commandeType: commandeType!
    @IBOutlet weak var v_Icustomer: InfoCustomerSaleView!
    @IBOutlet weak var v_footer: FooterSaleView!
    @IBOutlet weak var v_cProduct: ProductCategorySaleView!
    @IBOutlet weak var v_totals: TotalsInvoiceView!
    @IBOutlet weak var v_head: HeadSaleView!
    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil, using: self.keyboardWillShow)
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil, using: self.keyboardWillHide)
        // NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_ :)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        // NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_ :)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        // //---|   Print
        // self.cPrint.print_ini(self)
        //---|   Commande Init
        self.commandeCtrl = self.appController!.include("CommandeController", self.appController!.v_side_container, "Sale") as! CommandeController
        self.commandeCtrl!.saleCtrl = self
            //---|   CUSTOMERS_LOADED
        NotificationCenter.default.addObserver(forName: BROADCAST.CUSTOMERS_LOADED, object: nil, queue: nil, using: self.customers_loaded)
            //---|   CUSTOMERS_LOADED
        NotificationCenter.default.addObserver(forName: BROADCAST.CUSTOMER_NAME_CHANGED, object: nil, queue: nil, using: self.customers_changed)
        //---|   Open Category
        self.categoryToggle(true)
        //---|   Components
        self.tv_component.binding(self)

        // Product Console
        productConsole.binding(self)

        // Product List
        productList.binding(self)

        // Product Slider
        productSlider.binding(self)

        // Categories DataSource
        categoriesCView.binding(self)

        //---|   SetUp
        self.cCustomer.setup([
            "first_name" : cFirstName,
            "last_name" : cLastName,
            "mobile" : cMobile,
                //---|   Address
            "addr_number" : addr_number,
            "addr_street" : addr_street,
            "addr_zip" : addr_zip,
            "addr_city" : addr_city,
            "addr_interphone" : addr_interphone,
            "addr_digicode" : addr_digicode,
            "addr_building" : addr_building,
            "addr_floor" : addr_floor,
            "addr_door " : addr_door,
        ], self)
        //---|   Init Views
        self.v_ini()

    }
    override func showed(){
        self.appController?.v_side_container.tag = 68
        timeout({
            self.appController?.menu_toggle(true)
        }, 1000)
    }
    override func connected(notification: Notification?=nil){
        super.connected()
        //---|   Setting
        self.setting_updated()
        // Center Product Slider
        self.refreshSlider()
        //---|   Load Customer
        self.cCustomer.loaded()
        //---|   Transaction Init
        self.tModel = DB.COMMANDES
        // Get Products Catgoery.queryOrdered(byChild: "online").queryEqual(toValue: 1)
        DB.PCATEGORIES.observe(.value, with: { (snap) in
            if !snap.exists() { return }
            self.snaps["categories"] = snap.value as AnyObject?
            self.products_set()
        })
        // ProductsqueryOrdered(byChild: "online").queryEqual(toValue: true).
        DB.PRODUCTS.observe(.value, with: { (snap) in
            if !snap.exists() { return }
            self.snaps["products"] = snap.value as AnyObject?
            self.products_set()
        })
        // Components
        self.productComponents("components/\(app.uid)", scopeData: &self.componentsProduct) { (response, snap) in
            self.componentsProduct=response
        }
        // Commandes
        DB.COMMANDES.observeSingleEvent(of: .value, with: { (snap) in
            var commandes:[String:[String:Any]] = [String:[String:Any]]()
            self.v_ini()
            if snap.exists() {
                self.commandes = (snap.value as? [String:[String:Any]])!
                // (self.nsarray(snap.value) as? [Dictionary<String, Any>])!.sorted(by: { Int(String($0["id"] ?? "0" ))! < Int(String($1["id"] ?? "0"))! })
                // Set Commande Id
                //self.commandeId = Int((self.commandes[self.commandes.count-1]["id"] as? String)!)!+1
                self.commandeId = String(self.commandes.dic(self.commandes.count-1)["index"], "1").toInt()+1
                // [self.commandes.count-1]
                // -String(self.commandes[self.commandes.count-1]["id"] as? String).toInt()+1
            }
            self.commandeCtrl?.tv_commande.reloadData()
            if self.commandes.count > 0 { self.timeout({ self.commandeCtrl!.tv_commande.selectRow(self.commandeCtrl!, 0) }, 10) }
        })
    }
    //---|   Session Loaded
    override func session_loaded(notification: Notification?=nil){
        super.session_loaded()
        self.v_head.ini()
        //---|   Refresh Commande
        self.commandeSelected()
    }
    //---|   interface_updated
    override func interface_updated(notification: Notification? ){
        if notification != nil, let side = notification!.object as? NSDictionary {
            refreshSlider()
            return
        }
        self.centerSliderProduct()
    }
    override func setting_updated(notification: Notification?=nil){
        //---| Connecte Print
        self.v_head.ini()
    }
    //---|   Transaction Save
    override func transactionSave( _ transaction: inout Dictionary<String, Any>, _ obj:inout Dictionary<String, Any>){
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
            //---|
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   ARCHIVE
        var products_commande = self.products_commande()
        for(product_commande_id, product_commande) in products_commande{
            if let product = self.products[String(product_commande["product_id"])] as? [String:Any], products_commande[product_commande_id] != nil{
                products_commande[product_commande_id]!.merge(["name" : String(product["name"]), "price" : String(product["price"], "0").toDouble(), "tva" : String(product["tva"], "0").toDouble(), "thumb" : String(product["thumb"])])
                //---|   Has Component
                if let components_base = self.componentsProduct[String(product["id"])] as? Array<Dictionary<String, Any>> {
                    var components_commande = [String:[String:Any]]()
                    for component_base in components_base {
                        var commponentIn = (self.g(product_commande, "components.\(component_base["id"]!)", [String:Any]()) as? [String:Any])!
                        //---|   Save Qty Base
                        commponentIn["qty_base"] = String(component_base["qty"], "0").toInt()
                        components_commande[String(component_base["id"])] = commponentIn.merge(component_base, commponentIn)
                    }
                    products_commande[product_commande_id]!["components_all"] = components_commande
                }
            }
        }

        //---|   Update Commande
        self.commande(nil, ["update" : ["products" : products_commande, "view" : "sale"]])
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
            //---|   Save Transaction
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        super.transactionSave( &transaction,  &self.commandes[self.commandeCurrent]!)
        // print("#----\n#--| transactionSave --: \(transaction) --: \(self.commandes[self.commandeCurrent]) |--#\n")
        //---|   Save Customer
        cCustomer.save { (customer) in
            var commande = self.commande()
            //---|   Save Customer
            var customer_info:[String:Any]? = nil
            if String(commande["type"], "on_shop") != "on_shop", let nCustomer = self.cCustomer.customers[customer] as? [String:Any]{
                customer_info = nCustomer
                if let delivery_address = commande["delivery_address"] as? String,  !delivery_address.empty() { customer_info!["delivery_address"] = delivery_address }
            }
            commande["customer_info"] = customer_info
            commande["customer"] = customer
            //---|   Update
            self.commande(nil, ["update" : ["customer" : customer, "customer_info" : customer_info]])
            DB.ARCHIVES.child(String(commande["id"])).setValue(commande)
            //---|   PRINT
            self.cPrint.print_customer(self.commande(self.commandeCurrent))
            //---|   Refresh List
            self.commandCalculPrice()
            //---|
            self.view.endEditing(true)
            self.showed()
        }
    }
    // Change orintation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        // Refresh Slider
        self.centerSliderProduct(false)
        coordinator.animate(alongsideTransition: { (_) in
            self.centerSliderProduct(false)
        }) { (_) in
            self.commandeType.keyboard_toogle()
        }

    }
    //---|
    func v_ini(){
        self.cCustomer.clean()
        self.v_Icustomer.ini() //---|   Customer Info
        self.v_footer.ini() //---|   Footer BTNS
        self.v_cProduct.ini() //---|   CATEGORIES & PRODUCTS
        self.v_totals.ini() //---|   TOTALS
    }
    //---|   products
    func products_set(){
        self.categoriesProduct = Array<Dictionary<String, Any>>()
        self.products = Dictionary<String, Dictionary<String, Any>>()
        self.productsByCategory = Dictionary<String, Array<AnyObject>>()
        if self.snaps["categories"] != nil, (self.snaps["categories"]?.count)!>0, self.snaps["products"] != nil, (self.snaps["products"]?.count)!>0  {
            for(category_id, fProducts) in (self.snaps["products"] as? Dictionary<String, Dictionary<String, AnyObject>>)!{
                if let oCatgory = self.snaps["categories"]![category_id] as? [String : Any], String(oCatgory["online"]).bool {
                    let index = self.categoriesProduct.append(oCatgory)
                    //---| s  Init Product Category
                    if self.productsByCategory[category_id] == nil { self.productsByCategory[category_id] = Array<AnyObject>() }
                    for(product_id, product) in fProducts{
                        if let productDic = product as? Dictionary<String, AnyObject>, String(productDic["online"]).bool {
                            let product_id_str = String(product_id)!
                            self.products[product_id_str] = (product as? Dictionary<String, AnyObject>)!
                            self.products[product_id_str]!["id"] = product_id_str// Exception
                            self.productsByCategory[String(category_id)]!.append(self.products[product_id_str] as AnyObject)
                        }

                    }
                }
            }
        }
        self.productList.reloadData()
        self.productConsole.reloadData()
        self.refreshSlider()
        self.categoriesCView.reloadData()
    }
    // Get Product From Web And Filter data
    func productComponents(_ path:String, scopeData:inout [String : Array<Any>], completion: @escaping ([String : Array<Any>], NSDictionary) -> Void) {
        var scopeData = scopeData
        DB.COMPONENTS.observe(.value, with: { (snapshot) in
            if !snapshot.exists() { return }
            let nsdictionary:NSDictionary = (snapshot.valueInExportFormat() as? NSDictionary)!
            for (id, obj) in nsdictionary{
                if obj != nil, (obj as? Dictionary<String, AnyObject>)!.count>0{
                    let id_str = String(id)
                    scopeData[id_str] = Array<Dictionary<String, AnyObject>>()
                    for (sIndex, section) in (obj as? Dictionary<String, AnyObject>)!{
                        var section_clone = (section as? Dictionary<String, AnyObject>)!
                        //---|   TOSEE
                        section_clone["id"] = ((section_clone["id"] != nil) ? String(section_clone["id"]!) : sIndex) as AnyObject
                        scopeData[id_str]!.append(section_clone)
                    }
                }
            }
            // Completed
            completion(scopeData, nsdictionary)
            // Refresh Data
            self.productList.reloadData()
            self.refreshSlider()
        })
    }
    //-----------------------------------------------#
        //--|  GET && SET && REMOVE
    //-----------------------------------------------#
    func canEdit() -> Bool{
        return (!String(self.commandeCurrent).empty() && self.commandes[self.commandeCurrent] != nil && !String(self.commande()["paid"]).bool)
    }
    //---| GET && SET && REMOVE
    func commande(_ commande_id:String?=nil, _ operation:Any?=nil, _ update_db:Bool=true) -> [String:Any] {
        var commande_id = commande_id ?? self.commandeCurrent
        var commande = [String:Any]()
        //---|   Add
        if operation != nil,let operation = operation as? [String:[String:Any]],let createing = operation["add"] as? [String:Any]{
            commande = createing
            commande_id = app.id()
            commande.merge(["created" : [".sv": "timestamp"], "total" : 0.00, "id" : commande_id, "index" : self.commandeId])
            DB.COMMANDES.child(commande_id).setValue(commande)
            self.commandes[commande_id] = commande
            self.commandeId += 1
        }
        //---|   has Commande
        if !String(commande_id).empty(), let commandeIF = self.commandes[commande_id] as? [String:Any]{
            commande = commandeIF
            if operation != nil{
                //---|   UPDATE
                if let operation = operation as? [String:[String:Any]], let updateing = operation["update"] as? [String:Any] {
                    //---|
                    for (item_id, item) in updateing{
                        commande[item_id] = item
                        // commande.updateValue(item, forKey: item_id)
                        // Save In FireBase
                        DB.COMMANDES.child("\(commande_id)/\(item_id)").setValue(item)
                    }
                    self.commandes[commande_id]! = commande
                    return commande
                }
                //---|   DELETE
                if String(operation) == "delete" {
                    var index = self.commandes.index(commande_id)
                    //---|   REMOVE
                    DB.COMMANDES.child(commande_id).removeValue()
                    self.commandes.removeValue(forKey: commande_id)
                    //---|   Refresh Commande
                    self.commandeCtrl?.tv_commande.reloadData()
                    //---|   Select Commande
                    if self.commandes.count <= index { index -= 1 }
                    if index >= 0 { self.commandeCtrl?.tv_commande.selectRow(self.commandeCtrl!, index) }
                    else{ self.commandeSelected(index) }
                    commande = [String:Any]()
                }
            }
        }
        return commande
    }
    // Get && Set && Update && Remove Product In Commande
    func products_commande(_ operation:Any?=nil, _ commande_id:String?=nil) -> [String:[String:Any]]{
        var products_commande = [String:[String:Any]]()
        if let commande = self.commande(commande_id) as? [String:Any] {
            //---|   Has ProductsComponent
            if let products_commandeIF = commande["products"] as? [String:[String:Any]] { products_commande = products_commandeIF }
            //---|   Has Operation
            if let operation = operation as? [String:Any] {
                if let createing = operation["add"] as? [String:Any] {
                    let product_commande_id = app.id()
                    var product_commande = createing
                    product_commande.merge(["id" : product_commande_id, "components" : [String:Any](), "qty" : 1])
                    products_commande[product_commande_id] = product_commande
                    //---|   Add To DB && Commandes
                    self.commande(commande_id, ["update" : ["products" : products_commande]])
                }
                //---|   Id Products Commande
                var product_commande_id = ""
                if let index = operation["index"] as? Int { product_commande_id = products_commande.key(index) } //String(["id"])
                //---|   UPDATE
                if let updating = operation["update"] as? [String:Any] {
                    if let updatting_id = operation["id"] as? String  { product_commande_id = updatting_id }
                    if !String(product_commande_id).empty() {
                        for (item_id, item) in updating{
                            products_commande[product_commande_id]![item_id] = item
                            //products_commande[product_commande_id]!.updateValue(item, forKey: item_id)
                        }
                        self.commande(commande_id, ["update" : ["products" : products_commande]])
                    }
                }
                //---|   Delete
                if operation["delete"] != nil {
                    if let deletting_id = operation["delete"] as? String  { product_commande_id = deletting_id }
                    if !String(product_commande_id).empty() {
                        products_commande.removeValue(forKey: product_commande_id)
                        self.commande(commande_id, ["update" : ["products" : products_commande]])
                    }
                }
            }
        }
        return products_commande
    }
    //---| Product Commande
    func product_commande(_ index:Int, _ commande_id:String?=nil) -> [String:Any]{
        return self.products_commande(nil, commande_id).dic(index)
    }
    //---|   CUSTOMERS_LOADED
    func customers_loaded(notification: Notification){
        //---|   ReSet Customer After Loaded
        if let customer = self.commande()["customer"] as? String { cCustomer.selected(customer) }
    }
    //---|   CUSTOMERS_Changed
    func customers_changed(notification: Notification){
        self.commande(nil, ["delivery_address" : self.v_Icustomer.define(notification.object)])
    }
    //-----------------------------------------------#
        //--|  Changing
    //-----------------------------------------------#
    // Refresh Slider
    func refreshSlider(){
        self.productSlider.reloadData()
        btnRightProductSlider.isHidden = true
        btnLeftProductSlider.isHidden = true
        viewProductSlide.isHidden = true
        if self.categoriesProduct.count>0{
            centerSliderProduct()
            ///Show Product
            if self.categoriesProduct.count>1 {
                btnRightProductSlider.isHidden = false
                btnLeftProductSlider.isHidden = false
            }
            viewProductSlide.isHidden = false
        }
    }
    //
    fileprivate func centerSliderProduct(_ reload:Bool=true){
        if let flowLayout = self.productSlider.collectionViewLayout as? UICollectionViewFlowLayout {
            DispatchQueue.main.async{
                flowLayout.itemSize = CGSize(width: self.productSlider.bounds.size.width, height: self.productSlider.bounds.size.height)
                //--| Assing Values
                self.productSlider.collectionViewLayout = flowLayout
                if reload { self.categoryChanging() }
            }
        }
    }
    // Change Category
    func categoryChanging( _ position:UICollectionViewScrollPosition = .right){
        self.productSlider.selectItem(at: IndexPath(row: self.indexProductSlider, section: 0), animated: true, scrollPosition: position)
        let category = self.categoriesProduct[indexProductSlider] as! NSDictionary
        if let iscategory = category["id"] as? String { self.cateoryProduct = String(iscategory) }
        self.productList.reloadData()
        self.categoriesCView.reloadData()
        self.centerSliderProduct(false)
    }
    //-----------------------------------------------#
        //--|  Selecting
    //-----------------------------------------------#
    // Get index Category from id
    func categoryGetIndex(_ id : String)->Int{
        if String(((self.categoriesProduct[self.indexProductSlider] as? Dictionary<String, AnyObject>)!["id"])!) == (id as? String)! {
            return indexProductSlider
        }else{
            for (index, category) in self.categoriesProduct.enumerated(){
                let categoryC = (category as? Dictionary<String, AnyObject>)!
                if String(categoryC["id"]!) == String(id){
                    return index
                    break
                }
            }
        }
        return -1
    }
    // Product Selected
    func productSelected(_ index:Int, _ refreshProductList:Bool = true){
        if self.canEdit(){
            let product = self.productsByCategory[self.cateoryProduct]![index] as! [String:Any]
            let product_id:String = String(product["id"]!)
            let productList = self.productList!.cellForItem(at: IndexPath(row: index, section: 0)) as! cellProductList
            self.products_commande(["add" : ["product_id" : product_id]])
            self.commandCalculPrice()
        }
    }
    // Product Console
    func productConsoleSelected(_ index:Int){
        // Get Product From Index
        let product = (self.products[String(self.product_commande(index)["product_id"])] as? [String:Any])!
        // Selet Category
        let category_id = String(product["product_category_id"])
        let category_index = categoryGetIndex(category_id)
        if indexProductSlider != category_index{
            self.indexProductSlider = category_index
            self.categoryChanging()
            self.categoriesCView.reloadData()
        }
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   Commande SELECTED
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    func commandeSelected(_ index_id:Any?=nil, _ opts:[String:Any] = [String:Any]()) -> [String:Any]{
        //---|   Init
        self.v_ini()
        //---|   Init COmmande
        var commande = [String:Any]()
        var index = -1
        if (index_id == nil) || (index_id is Int) {
            index = (index_id != nil ? String(index_id, "-1").toInt() : self.commandes.index(self.commandeCurrent))
            commande = self.commandes.dic(index)
            print("-----------| index_id |----------\n \(index) --> \(String(commande["index"])) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        }else{
            //---| params : [String:Any]
            if let commandeIF = index_id as? [String:Any]{
                commande = commandeIF
                print("-----------| commandeIF |----------\n commandeIF --> ");debugPrint(commandeIF["index"]);print(" |\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            }
            //---| params : String
            else if let commande_id = index_id as? String{ commande = self.commande(commande_id) }
            index = self.commandes.index(String(commande["id"]))
            print("-----------| index |----------\n \(index) --> \(String(commande["index"])) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        }
        //---|   Init Current Commande
        self.commandeCurrent = ""
        //---|
        if index >= 0, self.commandes.count > index, let commande_id = commande["id"] as? String {
            //---|   Commande Current
            self.commandeCurrent =  commande_id
            let typeC = (self.g(commande, "type", "on_shop" as AnyObject) as? String)!
            // Select
            // if let cell = self.commandeCtrl!.tv_commande.cellForRow(at: IndexPath(row: index, section: 0)) as? cellCommande{
            //     cell.selected(commande)
            // }
            self.commandeCtrl!.tv_commande.reloadData()
            //---|   Select Customer
            if typeC != "on_shop" { cCustomer.selected(String(commande["customer"])) }
            // Total
            self.v_totals.ini(commande)
            //---|   Footer Init
            self.v_footer.ini(commande)
        }
        self.productConsole.reloadData()
        return commande
    }
    //-----------------------------------------------#
        //--|  Calucation
    //-----------------------------------------------#
    // Commande Calculation
    func commandCalculPrice(_ commande_id:String?=nil){
        if let commande = self.commande(commande_id) as? [String:Any], commande["id"] != nil{
            let commande_id = String(commande["id"])
            var total:Double = 0.00
            for (_, product_commande) in self.products_commande(nil, commande_id){
                //---|   Product Found
                if let product_id = product_commande["product_id"] as? String, let product = self.products[product_id] as? Dictionary<String, Any>{
                    let price = self.calculatComponent(product, product_commande, commande_id).toDouble()
                    let qty:Double = String(product_commande["qty"], "1").toDouble()
                    total+=price*qty
                }
            }
            //---|   TODO : Refresh
            self.commandeSelected(self.commande(commande_id, ["update" : ["total" : total]]))
        }
        //---|   Reload
        self.productConsole.reloadData()
    }
    func calculatComponent(_ product : Dictionary<String, Any>, _ product_commande:[String:Any], _ commande_id:String?=nil) -> String{
        var total:Double = 0
        if product != nil{
            total = String(product["price"]).toDouble()
            //---|   Change Component by default
            if let components = product_commande["components"] as? [String:[String:Any]], components.count>0, let components_base = self.componentsProduct[(product["id"] as? String)!] as? Array<Dictionary<String, Any>> {
                for (component_id, component) in components{
                    let component_base:Dictionary<String, Any> = components_base.search(component_id)
                    total = total+(String(component_base["price"], "0").toDouble()*String(component["qty"], String(component_base["qty"], "0")).toDouble())
                }
            }
        }
        return String(total)
    }
    //-----------------------------------------------#
        //--|  Click
    //-----------------------------------------------#
    func product_console_plus(_ index:Int, _ plus:Int=1){
        if index > -1 {
            let product_commande = self.product_commande(index)
            var qty = String(product_commande["qty"], "0").toInt()+plus
            if qty < 1{ qty = 1 }
            //---|   Update Qty
            self.products_commande(["id" : product_commande["id"], "update" : ["qty" : qty]])
            self.commandCalculPrice()
        }
    }
    // Plus Product Console
    func productConsolePlus(_ sender: AnyObject) { self.product_console_plus(sender.tag) }
    // Minus Product Console
    func productConsoleMinus(_ sender: AnyObject) { self.product_console_plus(sender.tag, -1) }
    //---|   Category Selected
    func categorySelected(_ index: Int){
        self.indexProductSlider = index
        self.categoryChanging()
        self.categoryToggle()
    }
    //---|   Toggle Categories
    func categoryToggle(_ openCategories:Bool?=nil){
        UIView.animate(withDuration: 1.0, animations: {
            let cvh = (openCategories != nil ? openCategories : self.categoriesView.isHidden)
            self.categoriesView.isHidden = !cvh!
            self.categoryProduct.isHidden = cvh!
            self.view.layoutIfNeeded()
        })
    }
    func print_ketchen(_ id:String){
        var printData = self.commande(id)
        let productsText: NSMutableString = NSMutableString()
        for (_, product_commande) in self.products_commande(nil, id){
            let product_id = String(product_commande["product_id"])
            let product = (self.products[product_id] as? Dictionary<String, AnyObject>)!
            productsText.append(self.cPrint.print_text_init(String(product["name"]), String(product_commande["qty"], "1") ))
            //---|   Show Composent
            if let componentArray = self.componentsProduct[product_id] as? Array<Dictionary<String, Any>> {
                for component in componentArray{
                    let commponentIn = (self.g(product_commande, "components.\(component["id"]!)", [String:Any]()) as? [String:Any])!
                    if  String(commponentIn["qty"], "1").toInt() > 0 {
                        productsText.append(self.cPrint.print_text_init("   \(String(component["name"]))", String(commponentIn["qty"], "1")))
                    }
                }
            }
        }
        printData["productsText"] = productsText as String
        self.cPrint.print_ketchen(printData)
    }

    //-----------------------------------------------#
        //--|  Elements
    //-----------------------------------------------#
        //-----------------------------------------------#
            //--|  Modal
        //-----------------------------------------------#
    // Toggle Modal Order Now
    func componentModelOpen(_ sender: AnyObject){
        if let btn = sender as? Btn {
            //---|
            self.consoleComponentEditing = String(btn.params["product"]!)
            self.tv_component.tag = String(btn.params["index"], "0").toInt()
            //---|  Init List Component Commande
            var product_commande = self.product_commande(self.tv_component.tag)
            if product_commande["components"] == nil { product_commande["components"] = [String:[String:Any]]() }
            self.components_commande = (product_commande["components"] as? [String:[String:Any]])!
            //---|   Load Component List
            self.tv_component.reloadData()
            UIView.animate(withDuration: 1.0, animations: component_model_toggle)
        }
    }
    func component_model_toggle(){
        UIView.animate(withDuration: 1.0, animations: {
            //---|
            if !self.v_component_model.isHidden { self.consoleComponentEditing = nil }
            self.v_component_model.isHidden = !self.v_component_model.isHidden
            self.v_component_validate.setTitle((self.canEdit() ? "VALIDER" : "FERMER"), for: .normal)
        })
    }
    //---|
    func component_console_plus(_ index: Int, _ plus:Int=1){
        //---|   Get Component
        let component = (self.componentsProduct[self.consoleComponentEditing!]![index] as? [String : Any])!
        let component_id = String(component["id"])
        //---|   Init Qty Get Default Qty
        if self.components_commande[component_id] == nil { self.components_commande[component_id] = ["qty" : String(component["qty"], "1").toInt() ]}
        var qty = String(self.components_commande[component_id]!["qty"], "0").toInt()+plus
        if qty < 0  { qty = 0 }
        self.components_commande[component_id]!["qty"] = qty
        //---|   Change In TableView
        let cell = self.tv_component.cellForRow(at: IndexPath(row: index, section: 0)) as? componentCell
        cell?.qty.text = String(qty)
    }
    // Plus console Console
    func componentConsolePlus(_ sender: AnyObject) { self.component_console_plus(sender.tag) }
    // Minus console Console
    func componentConsoleMinus(_ sender: AnyObject) { self.component_console_plus(sender.tag, -1) }
    //-----------------------------------------------#
        //--|  keyboard Will
    //-----------------------------------------------#
    func keyboardWillShow(notification: Notification) {
        var kHeight:CGFloat?=nil
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            kHeight = keyboardRectangle.height
        }
        self.commandeType.keyboard_toogle(false, kHeight)
    }
    func keyboardWillHide(notification: Notification) { self.commandeType.keyboard_toogle(true) }
    //-----------------------------------------------#
        //--|  ACTIONS BUTTONS
    //-----------------------------------------------#
    @IBAction func categoryViewOpen(_ sender: AnyObject) {
        self.categoryToggle()
    }
    // Right Arrow Clicked
    @IBAction func clickArrowRightProductSlider(_ sender: AnyObject) {
        indexProductSlider = indexProductSlider+1
        if indexProductSlider >= self.categoriesProduct.count { indexProductSlider = 0 }
        self.categoryChanging()
    }
    // Left Arrow Clicked
    @IBAction func clickArrowLeftProductSlider(_ sender: AnyObject) {
        indexProductSlider = indexProductSlider-1
        if indexProductSlider < 0 { indexProductSlider = self.categoriesProduct.count-1 }
        self.categoryChanging(.left)
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   Footer Actions
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    //---| PAID
    @IBAction func a_pay(_ sender: AnyObject) {
        let total:String = String(self.commandes[self.commandeCurrent]!["total"])
        self.calculationTvas(&self.commandes[self.commandeCurrent]!, self.products as NSDictionary)
        self.commandes[self.commandeCurrent]!["total"] = total
        self.transactionModal(total)
        (sender as? UIButton)!.loading()
    }
    //---| DELETE
    @IBAction func a_delete(_ sender: AnyObject) {
        if String(self.commande()["total"]).toDouble() > 0 {
            self.confirm({ (_) -> Void in
                self.commande(nil, "delete")
            })
        }else{
            self.commande(nil, "delete")
        }
    }
    //---| VALIDATE
    @IBAction func a_validate(_ sender: AnyObject) {
        let commande = self.commande(nil, ["update" : ["validate" : true]])
        //---|   Refresh List
        self.commandCalculPrice()
        self.print_ketchen(self.commandeCurrent)
        (sender as? UIButton)!.loading()
    }
    //---| ARCHIVER
    @IBAction func a_archive(_ sender: AnyObject) {
        //---|   Already Saved Archive
        self.commande(nil, "delete")
    }
    //---| PRODUCTION
    @IBAction func a_production(_ sender: AnyObject) {
        // self.alert("Impression", "Reimprimer ...")
        self.print_ketchen(self.commandeCurrent)
        (sender as? UIButton)!.loading()

    }
    //---| TICKET
    @IBAction func a_ticket(_ sender: AnyObject) {
        // self.alert("Impression", "Reimprimer ...")
        self.cPrint.print_customer(self.commande(self.commandeCurrent))
        (sender as? UIButton)!.loading()
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   Head Actions
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    //---| Start Session
    @IBAction func session_start_click(_ sender: AnyObject) {
        self.alert("SESSION", "session commencée à \(Date().string())")
        SessionManager().session_set()
        self.v_head.ini()
    }
    //---| About Print
    @IBAction func about_print_click(_ sender: AnyObject) {
        self.cPrint.print_about()
        (sender as? UIButton)!.loading()
    }
    //---| Drawer Open
    @IBAction func drawer_click(_ sender: AnyObject) {
        self.cPrint.print_drawer()
        (sender as? UIButton)!.loading()
    }
    @IBAction func componentModeltoggle(_ sender: AnyObject) { self.component_model_toggle() }
    @IBAction func componentModelValidate(_ sender: AnyObject) {
        self.component_model_toggle()
        if canEdit() {
            self.products_commande(["update" : ["components" : self.components_commande], "index" : self.tv_component.tag])
            self.commandCalculPrice()
            self.productConsole.selectRow(self, self.tv_component.tag)
        }
    }
    //---|
    @IBAction func showModalOrderNow(_ sender: AnyObject) {
        self.commandeType.toggle()
    }
    //---|   Type Commande Choose sender.tag 0 => Close, 1 => on_show, 2 => take_away, 3 => deliver
    @IBAction func commandeTypeChoose(_ sender: AnyObject) {
        if sender.tag > 0{
            let type = [1 : "on_shop", 2 : "take_away", 3 : "deliver"][sender.tag]!
            self.v_Icustomer.ini()
            //---|   Add COMMANDE
            let commande_id = String(self.commande(nil, ["add" : ["type" : type]])["id"])
            self.commandeCtrl?.tv_commande.reloadData()
            self.commandeCtrl?.tv_commande.selectRow(self.commandeCtrl!, self.commandes.count-1)
            categoryToggle(true)
            //---|   Open PopUp Customer Edit
            if sender.tag != 1 {
                //---|   Focus
                self.cMobile.becomeFirstResponder()
                //---|   Open Form Customer
                self.commandeType.fromShow((sender.tag == 3))
                return
            }
        }
        self.cCustomer.hide_result()
        self.commandeType.toggle()
    }
    //---|
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        // Do not add a line break
        // if textField == mobile {
        //     firstName.resignFirstResponder()
        // }else if textField == firstName {
        //     lastName.resignFirstResponder()
        // }
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("-----------| SaleController |----------\n textField --> \(textField) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        return true
    }
    //---|   Save Customer
    @IBAction func customer_save(_ sender: AnyObject) {
        self.cCustomer.save { (customer) in
            self.commande(nil, ["update" : ["customer" : customer, "delivery_address" : String(self.cCustomer.customers[customer]!["delivery_address"])]])
            self.cCustomer.hide_result()
        }
    }
    @IBAction func customer_pop_open(_ sender: AnyObject) { self.commandeType.fromShow((String(self.commandes[self.commandeCurrent]!["type"]) == "deliver")) }
    //-----------------------------------------------#
        //--|  Table View
    //-----------------------------------------------#
    // Table View Data
    func numberOfSections(in tableView: UITableView) -> Int{ return 1 }

    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        // Menu Side Count Row
        // Product Console Count Row
        if tableView == self.productConsole, self.products.count>0, self.commandes.count>0, !String(self.commandeCurrent).empty(){
            return self.products_commande().count
        }
        // Components
        if tableView == self.tv_component, self.consoleComponentEditing != nil, self.products.count > 0, self.componentsProduct[self.consoleComponentEditing!] != nil{
            return self.componentsProduct[self.consoleComponentEditing!]!.count
        }
        return 0
    }
    // Set Data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //---|   CanEdit
        let canEdit = self.canEdit()
        // Product Console ITEMS
        if tableView == self.productConsole {
            let cell = tableView.dequeueReusableCell(withIdentifier: "productConsole", for: indexPath) as! cellProductConsole
            let product_commande = self.product_commande(indexPath.row)
            if let product_id = product_commande["product_id"] as? String, let product = self.products[product_id] as? [String: Any] {
                cell.title.text = String(product["name"])
                cell.qty.text = String(product_commande["qty"], "1")
                cell.price.text = self.calculatComponent(product, product_commande).format()
                // Action
                cell.canEdit(canEdit)
                //---|   Actions
                //
                if cell.minusBtn.enabled(canEdit) {
                    cell.minusBtn.tag = indexPath.row
                    cell.minusBtn.addTarget(self, action: #selector(self.productConsoleMinus(_:)), for: .touchUpInside)
                }
                //
                if cell.plusBtn.enabled(canEdit) {
                    cell.plusBtn.tag = indexPath.row
                    cell.plusBtn.addTarget(self, action: #selector(self.productConsolePlus(_:)), for: .touchUpInside)
                }
                // Action
                cell.componentBtn.enabled((self.componentsProduct[product_id] != nil && self.componentsProduct[product_id]!.count > 0))
                cell.componentBtn.params = ["product" : product_id, "index" : indexPath.row]
                cell.componentBtn.addTarget(self, action: #selector(self.componentModelOpen(_:)), for: .touchUpInside)
            }
            return cell.deSelectRow()
        }
        // Components
        if tableView == self.tv_component {
            let cell = tableView.dequeueReusableCell(withIdentifier: "componentCell", for: indexPath) as! componentCell
            if self.consoleComponentEditing != nil{
                let component = (self.componentsProduct[self.consoleComponentEditing!]![indexPath.row] as? [String : Any])!
                cell.name.text = String(component["name"])
                cell.price.text = String(component["price"], "0").format()
                cell.qty.text = String(component["qty"], "1")
                //---|   Alreay Change Qty Component
                if self.components_commande[String(component["id"])] != nil {
                    cell.qty.text = String(self.components_commande[String(component["id"])]!["qty"], "0")
                }
                // Action
                cell.canEdit(canEdit)
                if cell.minus.enabled(canEdit) {
                    cell.minus.tag = indexPath.row
                    cell.minus.addTarget(self, action: #selector(self.componentConsoleMinus(_:)), for: .touchUpInside)
                }
                if cell.plus.enabled(canEdit) {
                    cell.plus.tag = indexPath.row
                    cell.plus.addTarget(self, action: #selector(self.componentConsolePlus(_:)), for: .touchUpInside)
                }
            }
            return cell.deSelectRow()
        }
        return tableView.cellForRow(at: indexPath)!
    }

    // Cell Selected Of Menu Side
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.productConsole {
            self.productConsoleSelected(indexPath.row)
        }else if let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath){
            selectedCell.contentView.backgroundColor = UIColor.clear
        }
    }
    //---|   DeSelect
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.reloadData()
    }
    // Active Editing
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return (tableView == self.productConsole && canEdit())
    }
    // Deleting
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == self.productConsole, editingStyle == .delete {
            DispatchQueue.main.async{
                let productConsole = tableView.dequeueReusableCell(withIdentifier: "productConsole", for: indexPath) as! cellProductConsole
                self.products_commande(["delete" : true, "index" : indexPath.row])
                tableView.deleteRows(at: [indexPath], with: .fade)
                self.commandCalculPrice()
                self.productList.reloadData()
            }
        }
    }
    //-----------------------------------------------#
        //--| Collection View
    //-----------------------------------------------#
    // Number Of section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if collectionView == self.productList, self.productsByCategory[self.cateoryProduct]?.isEmpty == false{ return self.productsByCategory[self.cateoryProduct]!.count }
        if collectionView == self.productSlider || collectionView == self.categoriesCView { return self.categoriesProduct.count }
        return 0
    }
    // Set Data
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // productList
        if collectionView == self.productList {
            let productListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "productList", for: indexPath) as! cellProductList
            productListCell.isHidden = true
            // Is Not Empty
            if  self.productsByCategory[self.cateoryProduct]?.isEmpty == false{
                let product = self.productsByCategory[self.cateoryProduct]?[indexPath.row] as AnyObject
                if product["id"] != nil{
                    productListCell.content.text = String((product["name"] != nil ? product["name"]! : "")!)
                    productListCell.product.assign(product["thumb"], "background.png")
                    productListCell.isHidden = false
                }
            }
            return productListCell
        }
        if collectionView == self.productSlider {
            let cellCategoryProduct = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCategoryProduct", for: indexPath) as! cellCategoryProduct
            cellCategoryProduct.isHidden = true
            let category = self.categoriesProduct[indexPath.row]
            cellCategoryProduct.content.text = (category["name"] as? String)!
            cellCategoryProduct.product.assign(category["file"], "background.png")
            cellCategoryProduct.isHidden = false
            return cellCategoryProduct
        }
        if collectionView == self.categoriesCView {
            let categoriesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCategories", for: indexPath) as! cellCategories
            let category = self.categoriesProduct[indexPath.row]
            categoriesCell.name.text = (category["name"] as? String)!
            categoriesCell.image.assign(category["file"], "background.png")
            return categoriesCell
        }
        return collectionView.cellForItem(at: indexPath)!
    }
    // Selected Item at collection View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.productList { self.productSelected(indexPath.row) }
        else if collectionView == self.categoriesCView { self.categorySelected(indexPath.row) }
    }
}
