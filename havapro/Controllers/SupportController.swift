//
//  SupportController.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class SupportController: AppViewController, UITableViewDataSource, UITableViewDelegate {
    var supports:Dictionary<String, Dictionary<String, AnyObject>> = [:]

    // Outlet
    @IBOutlet weak var supportTView: UITableView!
    @IBOutlet weak var contactsView: UIView!
    @IBOutlet weak var logoProvider: avatar!
    @IBOutlet weak var nameProvider: UILabel!
    @IBOutlet weak var companyProvider: UILabel!
    @IBOutlet weak var menuCView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Support table View
        self.supportTView.binding(self)
        // Design
        //self.contactsView.dropShadow()
        // Do any additional setup after loading the view.
    }
    //---|   Connection | Get Provider Local Information
    override func connected(notification: Notification?=nil){
        //self.companyProvider.text = (app.profile["company"] as? String)!
        //self.nameProvider.text = (app.profile["name"] as? String)!
        //--| Avatar
        //self.logoProvider.assign(app.profile["logo"])
        // Supports
        DB.SUPPORTS.queryOrdered(byChild: app.uid).queryEqual(toValue: String(app.profile["provider_id"])).observe(.value, with: { (snap) in
            if !snap.exists() { return }
            self.supports = (snap.value as? Dictionary<String, Dictionary<String, AnyObject>>)!
            self.supportTView.reloadData()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{ return self.supports.count }
    // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        // supportTView
        let supportTView = tableView.dequeueReusableCell(withIdentifier: "cellSupport", for: indexPath) as! cellSupport
        let support = self.supports[Array(self.supports.keys)[indexPath.row]]!

        supportTView.name.text = String(self.g(support, "info.name", "" as AnyObject))
        supportTView.email.text = String(self.g(support, "info.email", "" as AnyObject))
        supportTView.phone.text = String(self.g(support, "info.phone", "" as AnyObject))
        //--| Avatar
        supportTView.avatar.assign(self.g(support, "info.avatar", ""))
        return supportTView.deSelectRow()
    }


    //-----------------------------------------------#
    //--|  Navigation
    //-----------------------------------------------#
    @IBAction func productClick(_ sender: Any) {
        return self.performSegue(withIdentifier: "productSegue", sender:self)
    }
    @IBAction func agendaClick(_ sender: UIButton) {
        return self.performSegue(withIdentifier: "agendaSegue", sender:self)
    }
    @IBAction func contactClick(_ sender: Any) {
        return self.performSegue(withIdentifier: "contactSegue", sender:self)
    }
    @IBAction func communicationClick(_ sender: Any) {
        return self.performSegue(withIdentifier: "communicationSegue", sender:self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
