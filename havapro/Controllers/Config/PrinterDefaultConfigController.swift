//
//  PrinterDefaultConfigController.swift
//  havapro
//
//  Created by HavaPRO on 05/02/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class PrinterDefaultConfigController: PopupViewController {
    //---| Outlet
    var configCtrl:ViewConfigController?=nil
    var info:[String:Any] = [String:Any]()
    //---|
    @IBOutlet weak var v_printer: PrinterDefaultConfigView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func popShowing(){
        self.popResize(CGSize(width: 300,height: 200))
    }
    override func popShowed(){
        for (tag, name) in self.configCtrl!.cPrint.printer_names { self.v_printer.print(tag, self.configCtrl!.cPrint.print_status(name, self.configCtrl!.printerList)) }
    }
    //--| Actions
    @IBAction func click(_ sender: AnyObject) {
        self.configCtrl?.printer_set(sender.tag, info)
        popup.dismiss()
    }

}
