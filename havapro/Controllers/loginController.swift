//
//  loginController.swift
//  havapro
//
//  Created by Exo on 22/10/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class loginController: UIViewController {

    @IBOutlet var username: UITextField!
    @IBOutlet var pwd: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Raduis Border
        self.username.layer.cornerRadius = 0
        self.pwd.layer.cornerRadius = 0
        //---|   Check if Already Connected
        self.connection()
        self.username.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //---|   LOGIN
    @IBAction func login(_ sender: AnyObject) {
        if self.username.text == "" || self.pwd.text == ""{
            //alertWithTitle("Warning", message: "Please enter an email and password.", ViewController: self, self.pwd)
            self.alert("Attension", "Please enter an email and password.")
            self.pwd.becomeFirstResponder()
        }else{
            app.alert = loading()
            FIRAuth.auth()?.signIn(withEmail: username.text!, password: pwd.text!) { (user, error) in
                self.loadoff({
                    if error != nil{
                        //self.alertWithTitle("Erreur", message: (error?.localizedDescription)!, ViewController: self, self.username!)
                        self.alert("Attension", (error?.localizedDescription)!)
                        self.username.becomeFirstResponder()
                    }else{
                        //---| Redirect
                        self.connection()
                    }
                }, alert : app.alert)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
