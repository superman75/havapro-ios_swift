//
//  MainTransactionController.swift
//  havapro
//
//  Created by abdelghani on 10/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit
import Spring

class MainTransactionController: TransactionManger, UITableViewDataSource, UITableViewDelegate {
    //---|   VARS
    var mTransactions:[String:[String:[String:Any]]] = [String:[String:[String:Any]]]() //---|   [DATE_STRING:[TRANSACTION]]
    var tTransactions:[String:[String:Any]] = [String:[String:Any]]() //---|   [DATE_STRING:[TRANSACTION]]
    var dateCurrent:String=""
    var transaction_id:String=""
        //---|   ViewController
    var intervalTransactionCtrl:IntervalTransactionController?
    var detailTransactionCtrl:DetailTransactionController?
    //---|   CLASS
    //let cPrint:PrintManager = PrintManager()
    //---|   Outlets
    @IBOutlet weak var container: SpringView!
    @IBOutlet weak var side: SideMainTransactionView!
    @IBOutlet weak var footer: FooterMainTransactionView!
    @IBOutlet weak var head: HeadMainTransactionView!
    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
        //---|   TODO : Side View Ini
        self.side.ini()
        self.footer_refresh()
        self.head.ini()
        //---|   TableView Init
        self.side.tv_transaction.binding(self)
        //---|   AutoLoad Interval
        if self.side.saleEnable() { self.saleShow() }
        else { self.intervalShow() }

    }
    //---|   Connected
    override func connected(notification: Notification?=nil){
        // print("-----\n#-----\n MainTransactionController connected \n-----\n\(app.uid)\n-----\n#-----\n")
        self.footer_refresh()
    }
    //---|   Session Loaded
    override func session_loaded(notification: Notification?=nil){
        super.session_loaded()
        self.footer_refresh()
        self.side.saleEnable()
    }
    //---| Refresh Transaction list
    func transactionRefresh(_ date:String?=nil){
        // print("-----\n#-----\ntransactionRefresh\n-----\n\(date)\n-----\(mTransactions[self.dateCurrent]!)\n-----\n#-----\n")
        if date != nil{
            self.dateCurrent = date!
        }
        self.side.l_date.text = self.dateCurrent
        self.side.tv_transaction.reloadData()
        self.transaction_id = ""
        self.side.deSale(self.transactionDic().count.bool)
    }
    //---|   Interval Transaction Show
    func intervalShow(){
        self.side.deSale(self.transactionDic().count.bool)
        if self.intervalTransactionCtrl == nil{
            self.intervalTransactionCtrl = self.include("IntervalTransactionController", self.container, "Statistical") as? IntervalTransactionController
        }else{
            self.include(self.intervalTransactionCtrl!, self.container)
        }
    }
    //---|
    func saleShow(){
        //---|   Active Sale Button
        self.side.deSale(self.transactionDic().count.bool, false)
        //---|   Get Data
        self.trunover()
    }
    //---|   Detail Transaction Show
    func detailShow(_ id:String?=nil){
        if self.transactionDic().count.bool {
            self.transaction_id = id ?? self.transaction_id
            //---|   Select The First Item if Not select
            if String(id).empty(), String(self.transaction_id).empty() { self.side.tv_transaction.selectRow(self, 0) }
            self.footer_refresh()
            //---|   Show View Controller
            if self.detailTransactionCtrl == nil{
                self.detailTransactionCtrl = (self.include("DetailTransactionController", self.container, "Statistical") as! DetailTransactionController)
            }else{
                self.detailTransactionCtrl!.showed()
                self.include(self.detailTransactionCtrl!, self.container)
            }
        }
    }
    //---|   Trunover
    func trunover(){
        //---|   Amount Trunover
        if let session_starting = app.profile["session_starting"] as? Int64 {
            self.dateCurrent = Date(String(session_starting), "timestamp").string("dd/MM/yyyy")
            self.side.l_date.text = self.dateCurrent
            self.transaction_get(session_starting, Date().add(1, .day).timestamp.toInt64(), { (re) in
                self.tTransactions = re
                self.side.tv_transaction.reloadData()
                self.detailShow()
            })
        }
    }
    //---|
    // func transactionCount()().count->Int{
    //     // if self.side.vs_journal.isHidden { return tTransactions.count }
    //     // else if !String(self.dateCurrent).empty(), let dic = self.mTransactions[self.dateCurrent]{ return dic.count }
    //     return transactionDic().count
    // }
    //---|
    func transactionDic()->[String:[String:Any]]{
        if self.side.vs_journal.isHidden { return tTransactions }
        else if !String(self.dateCurrent).empty(), let dic = self.mTransactions[self.dateCurrent]{ return dic }
        return [String:[String:Any]]()
    }
    func footer_refresh() -> [String:Any]?{
        var order:[String:Any]? = nil
        if self.detailTransactionCtrl != nil, !String(self.detailTransactionCtrl!.order_id).empty(), let orderif = self.detailTransactionCtrl!.orders[self.detailTransactionCtrl!.order_id] as? [String:Any]{
            order = orderif
        }
        self.footer.ini((order != nil))
        return order
    }
    //-----------------------------------------------#
      //--| head Actions
    //-----------------------------------------------#
        //---|
    @IBAction func backClicked(_ sender: AnyObject) { self.appController!.controllerInclude(3, true) }
    //-----------------------------------------------#
      //--| Footer Actions
    //-----------------------------------------------#
        //---| Close Session
    @IBAction func closeClicked(_ sender: AnyObject) {
        self.confirm({ (str) in
            self.alert("Clôturer", "La session a commencé à \(Date(String(app.profile["session_starting"]), "timestamp").string()) et clôturer à \(Date().string())")
            SessionManager().session_set("end")
            self.tTransactions = [String:[String:Any]]()
            self.intervalShow()
        }, "Clôturer", "Voulez-vous vraiment clôturer la session", ["Annuler", "Clôturer"])
    }
        //---|
    @IBAction func printClicked(_ sender: AnyObject) {
        if let order = self.footer_refresh() as? [String:Any] {
            self.alert("Impression", "Reimprimer ...")
            self.cPrint.print_run(order)
        }
    }
        //---|
    @IBAction func refundClicked(_ sender: AnyObject) {
        self.alert("Impression", "Reimprimer ...")
        print("----- refundClicked Clicked ---------")
    }
    //-----------------------------------------------#
      //--| Sale && Journal Side Action
    //-----------------------------------------------#
        //---|
    @IBAction func intervalShowClick(_ sender: AnyObject) { self.intervalShow() }
        //---|
    @IBAction func detailShowClick(_ sender: AnyObject) { self.saleShow() }
    //-----------------------------------------------#
      //--| TABLE VIEW ACTIONS
    //-----------------------------------------------#
        // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{ return self.transactionDic().count }
        //---|   Set Data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTransactionCell", for: indexPath) as! MainTransactionCell
        var transaction = transactionDic().dic(indexPath.row)
        //---|   is Sale
        // if self.side.vs_journal.isHidden {
        //     transaction = tTransactions.dic(indexPath.row)
        // }else{
        //     transaction = self.mTransactions[self.dateCurrent]!.dic(indexPath.row)
        // }
        cell.total.text = String(transaction["total"]).format()+" €"
        cell.index.text = String(indexPath.row+1)
        cell.time.text = Date(String(transaction["created"]), "timestamp").string("HH:mm")
        let action = (self.g(transaction, "actions.0", [String:Any]()) as? [String:Any])!
        cell.icon.image = UIImage(named: "\(String(action["type"]).lowercased()).png")
        return cell.ini()
    }
        // Cell Selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) as? MainTransactionCell{
            cell.selected()
            self.detailShow(transactionDic().key(indexPath.row))
        }
        //---|   Refresh list in main
        // print("-----------| transactionDic() |----------\n \(indexPath.row) --> \(transactionDic()) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
    }
        // Cell Deselected
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) as? MainTransactionCell{ cell.ini() }
    }

}
