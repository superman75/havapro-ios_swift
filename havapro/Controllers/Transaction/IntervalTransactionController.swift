//
//  IntervalTransactionController.swift
//  havapro
//
//  Created by abdelghani on 10/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class IntervalTransactionController: TransactionManger, UITableViewDataSource, UITableViewDelegate {

    //---|   VARS
        //---|   ViewController
    var mainTransactionCtrl:MainTransactionController?
    var iTransactions:[String:[String:Any]] = [String:[String:Any]]() //---|   [DATE_STRING:[TOTALS]]
    //---|   Outlets
    @IBOutlet weak var b_start: BtnDate!
    @IBOutlet weak var b_end: BtnDate!
    @IBOutlet weak var tv_transaction: UITableView!
    @IBOutlet weak var v_calendar: UIView!
    @IBOutlet weak var d_calendar: DatePicker!
    @IBOutlet var v_loading: UIView!
    @IBOutlet var v_transaction: UIView!

    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
        //---|   Parent Is MainTransactionController
        self.mainTransactionCtrl = self.parent as! MainTransactionController
        //---|   Binding UITableView
        self.tv_transaction.binding(self)
        //---|   Init Calendar
        d_calendar.date = Date().set(00, .hour).set(00, .minute).set(00, .second).add(-12, .month)
        //---|   Init Start End
        self.b_start.ini(["is" : "start"]).title(d_calendar.date)
        self.b_end.ini(["is" : "end"]).title(Date())
        //---|   Include Calendar && Loader
        self.v_transaction.include(v_loading).include(v_calendar)
        //---|   Get Statistique
        self.transactionList()
    }
    //---|   Connected. IntervalTransactionCell
    override func connected(notification: Notification?=nil){
        // print("-----\n#-----\n IntervalTransactionController connected \n-----\n\(app.uid)\n-----\n#-----\n")
        //---|   Get Statistique
        self.transactionList()
    }

    //---|   Showed
    override func showed(){
        super.showed()
        //---|   Select First
        self.mainTransactionCtrl?.transactionRefresh((self.iTransactions.count > 0 ?self.iTransactions.key(0) : nil))
    }
    //---|
    func transactionList(){
        UIView.animate(withDuration: 0.2, animations: { self.v_loading.isHidden = false })
        //---|   Transaction
        var end = (self.b_end.params["date"] as! Date).timestamp.toInt64()
        //---|   Show Only Historique
        if let session_starting = app.profile["session_starting"] as? Int64, end > session_starting { end = session_starting }
        //---|
        self.transaction_by((self.b_start.params["date"] as! Date).timestamp.toInt64(), end, { (re) in
            // print("----\n---\n--\nre\n-----");debugPrint(re);print("--\n---\n----\n")
            self.mainTransactionCtrl?.mTransactions = (re["transactions"] as? [String : [String : [String : Any]]])!
            self.iTransactions = re["totals"]!
            self.tv_transaction.reloadData()
            UIView.animate(withDuration: 0.2, animations: { self.v_loading.isHidden = true })
            //---|   Select The First Row
            if self.iTransactions.count > 0 { self.tv_transaction.selectRow(self, 0) }
            //---|   Refresh in the Main
            else{ self.mainTransactionCtrl?.transactionRefresh() }

        })
    }
    //---| Calendar Toggle
    func calendarToggle(_ btn:BtnDate){
        self.d_calendar.toggle(btn, b_start, b_end, v_calendar, transactionList)
    }
    @IBAction func CalendarToggle(_ sender: AnyObject) { self.calendarToggle( sender as! BtnDate) }
    //-----------------------------------------------#
      //--|
    //-----------------------------------------------#
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{ return self.iTransactions.count }
    //---|   Set Data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        let cell = tableView.dequeueReusableCell(withIdentifier: "IntervalTransactionCell", for: indexPath) as! IntervalTransactionCell
        let transaction = self.iTransactions.dic(indexPath.row)
        cell.date.text = String(transaction["date"])
        cell.total.text = String(transaction["total"], "0").format()+" €"
        cell.tva.text = String(transaction["tva"], "0").format()+" €"
        cell.ticket.text = String(transaction["ticket"], "0").format()+" €"
        cell.cash.text = String(transaction["cash"], "0").format()+" €"
        cell.cb.text = String(transaction["cb"], "0").format()+" €"
        return cell.ini()
    }
    // Cell Selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        (tableView.cellForRow(at: indexPath as IndexPath) as! IntervalTransactionCell).selected()
        //---|   Refresh list in main
        self.mainTransactionCtrl?.transactionRefresh(self.iTransactions.key(indexPath.row))
    }
    // Cell Deselected
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        (tableView.cellForRow(at: indexPath as IndexPath) as! IntervalTransactionCell).ini()
    }

}
