//
//  DetailTransactionController.swift
//  havapro
//
//  Created by abdelghani on 10/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class DetailTransactionController: TransactionManger, UITableViewDataSource, UITableViewDelegate {

    //---|   VARS
    var oProducts:[String:[String:Any]] = [String:[String:Any]]() //---|   Order : [Order_ID : [Product_id : [Product]]]
    var orders:[String:[String:Any]] = [String:[String:Any]]() //---|   Order : [Order_ID : [Product_id : [Product]]]

    var order_id:String=""
        //---|   ViewController
    var mainTransactionCtrl:MainTransactionController?
    //---|   Outlets
    @IBOutlet weak var v_order: UIView!
    @IBOutlet weak var tv_order: UITableView!
        //---|
    @IBOutlet weak var l_ht: UILabel!
    @IBOutlet weak var l_tva: UILabel!
    @IBOutlet weak var l_ttc: UILabel!
        //---|
    @IBOutlet weak var l_time: UILabel!
    @IBOutlet weak var l_order_id: UILabel!
        //---|
    @IBOutlet weak var i_ttype: UIImageView!
    @IBOutlet weak var l_ttype: UILabel!

    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
        //---|   Parent Is MainTransactionController
        self.mainTransactionCtrl = self.parent as! MainTransactionController
        // TableView
        self.tv_order.binding(self)
    }
    //---|   Connected
    override func connected(notification: Notification?=nil){ }
    override func showed() {
        super.showed()
        //---|   Order View Hide
        // self.v_order.isHidden = true
        self.order_id = ""
        self.tv_order.reloadData()
        //---|
        var transaction = [String:Any]()
        if !String(self.mainTransactionCtrl?.transaction_id).empty(), let dic = self.mainTransactionCtrl?.transactionDic() as? [String:[String:Any]], dic[self.mainTransactionCtrl!.transaction_id] != nil {
            transaction = dic[self.mainTransactionCtrl!.transaction_id]!
            self.order(String(transaction["id"]))
        }
        //---|   Define
        self.l_ht.text = String(transaction["total_ht"], "0.00 €").format()+" €"
        self.l_tva.text = String(transaction["tva"], "0.00 €").format()+" €"
        self.l_ttc.text = String(transaction["total"], "0.00 €").format()+" €"
        self.l_time.text = Date(String(transaction["created"]), "timestamp").string("HH:mm")
        self.l_order_id.text = ["sale" : "Vente", "bill" : "Facture"][String(transaction["view"])] ?? "Vente ##"
        let type = String(self.g(transaction, "actions.0.type")).lowercased()
        self.l_ttype.text = String(app.paymenttype[type], "...")
        self.i_ttype.image = UIImage(named: "\(type).png")

    }
    func order(_ order_id:String){
        self.order_id = order_id
        if self.orders[self.order_id] != nil {
            // self.v_order.isHidden = false
            self.tv_order.reloadData()
            return
        }
        //---|   Products Order Get
        self.oProducts[self.order_id] = [String : [String : Any]]()
        //---|   TODO : Order From Archive
        DB.ARCHIVES.child("\(self.order_id)").observeSingleEvent(of: .value, with: { (snap) in
            if snap.exists() { self.orders[self.order_id] = (snap.value as? [String : Any])! }
            self.tv_order.reloadData()
            // self.v_order.isHidden = false
        })
    }
    //-----------------------------------------------#
      //--|
    //-----------------------------------------------#
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //---|
        self.mainTransactionCtrl?.footer_refresh()
        //---|
        if !String(self.order_id).empty(), self.orders[self.order_id] != nil, let products = self.orders[self.order_id]!["products"] as? [String:Any] { return products.count }
        return 0
    }
    //---|   Set Data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTransactionCell", for: indexPath) as! DetailTransactionCell
        if self.orders[self.order_id] != nil, let products = self.orders[self.order_id]!["products"] as? [String:Any], let product = products.dic(indexPath.row) as? [String:Any]{
            cell.name.text = String(product["name"])
            cell.price.text = String(product["price"]).format()+" €"
            cell.thumb.assign(product["thumb"])
        }
        return cell.ini()
    }
    // Cell Selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        (tableView.cellForRow(at: indexPath as IndexPath) as! DetailTransactionCell).selected()
    }
    // Cell Deselected
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        (tableView.cellForRow(at: indexPath as IndexPath) as! DetailTransactionCell).ini()
    }

}
