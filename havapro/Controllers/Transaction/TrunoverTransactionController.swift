//
//  TrunoverTransactionController.swift
//  havapro
//
//  Created by abdelghani on 10/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class TrunoverTransactionController: TransactionManger {
    // var
    var mainTransactionCtrl:MainTransactionController?

    @IBOutlet weak var l_amount: UILabel!
    @IBOutlet weak var l_session: UILabel!
    @IBOutlet weak var l_total: UILabel!

    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // timeout({
        //     self.appController!.include("MainTransactionController", self.appController!.container!, "Statistical")
        // }, 1000)
    }
    override func showed() {
        super.showed()
        self.trunover()
    }
    //---|   Connection | Get Provider Local Informati
    override func connected(notification: Notification?=nil){
        super.connected()
        self.trunover()
    }
    //---|   Session Loaded
    override func session_loaded(notification: Notification?=nil){
        super.session_loaded(notification: notification)
        self.trunover()
    }
    //---|
    func trunover(){
        self.l_amount.text = "00.00".format()+" €"
        self.l_total.text = "00.00".format()+" €"
        self.l_session.textColor = UIColor("#F04903")
        let midnight = Date().set(00, .hour).set(00, .minute).set(00, .second)
        // let end = Date().set(00, .hour).set(00, .minute).set(00, .second)
        // print("-----\n#-----\napp.profile[session_starting]\n-----\n\(app.profile["session_starting"])\n-----\n#-----\n")
        //---|   Total By Day
        // print("-----------| midnight |----------\n \(midnight.add(-1, .day).string()) --> \(midnight.add(1, .day).string()) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        self.transaction_get(midnight.add(-1, .day).timestamp.toInt64(), midnight.add(1, .day).timestamp.toInt64(), { (re_byday) in
            // print("-----------| re_byday |----------\n \(String(self.transaction_total(re_byday)).format()) --> \(re_byday) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            // print("-----------| session_starting re_byday |----------\n re_byday --> \(re_byday) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            self.l_total.text = String(self.transaction_total(re_byday)).format()+" €"
            //---|   Amount Trunover
            // print("-----------| session_starting |----------\n session_starting --> \(Date(String(app.profile["session_starting"]), "timestamp").string()) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            if let session_starting = app.profile["session_starting"] as? Int64 {
                // print("-----------| session_starting |----------\n Int64 --> \(session_starting) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
                self.l_session.textColor = UIColor("#249991")
                self.transaction_get(session_starting, Date().add(1, .day).timestamp.toInt64(), { (re) in
                    // print("-----------| self.transaction_total(re) |----------\n \(String(self.transaction_total(re)).format()) --> \(re) |\n \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
                    self.l_amount.text = String(self.transaction_total(re)).format()+" €"
                })
            }
        })
    }
    @IBAction func trunoverRefresh(_ sender: AnyObject) { self.trunover() }
    //---|   Open Main Transaction
    @IBAction func transactionMainOpen(_ sender: AnyObject) {
        //self.appController?.include("", (self.appController?.container)!, "Statistical", nil, &self.mainTransactionCtrl)
        self.appController!.include("MainTransactionController", (self.appController!.container)!, "Statistical")
        // print("-----\n#-----\ntransactionMainOpen\n-----\n#-----\n")
    }
}
