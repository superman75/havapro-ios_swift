//
//  BillController.swift
//  havapro
//
//  Created by Exo on 29/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import DropDown
import SearchTextField

class BillController: Transaction, UITableViewDataSource, UITableViewDelegate{
    //---|   VARS
    //let cPrint:PrintManager = PrintManager()
    var cCustomer:Customer = Customer()
    var bills:Dictionary<String, Dictionary<String, Any>> = Dictionary<String, Dictionary<String, Any>>()
    let iBill:Dictionary<String, Any> = ["products": Dictionary<String, Dictionary<String, AnyObject>>(), "total" : 0, "tvas" : Dictionary<String, AnyObject>()]
    var bill:Dictionary<String, Any> = Dictionary<String, Any>()
    var totals:Array<Dictionary<String, Any>> = Array<Dictionary<String, Any>>()
    var iSelected:Dictionary<String, String?> = ["bill" : nil]

    //---|   Outlet
    @IBOutlet weak var v_list: UIView!
    @IBOutlet weak var v_edit_main: UIView!
    @IBOutlet var v_edit: EditInvoiceView!
    @IBOutlet var v_totals: TotalsInvoiceView!
    @IBOutlet weak var v_footer: FooterInvoiceView!

    @IBOutlet weak var totalsCView: UICollectionView!
    @IBOutlet weak var productsTView: UITableView!
    @IBOutlet weak var billsTView: UITableView!
    @IBOutlet weak var productsView: UIView!
    @IBOutlet weak var logoProvider: avatar!
    @IBOutlet weak var nameProvider: UILabel!
    @IBOutlet weak var addressProvider: UILabel!
    @IBOutlet weak var tvaProvider: UILabel!
    @IBOutlet weak var siretProvider: UILabel!
    @IBOutlet weak var menuCView: UIView!
        //---|   BUTTON
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var archiveBtn: UIButton!
    @IBOutlet weak var printBtn: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerBtn: buttonBorderBottom!
    //---|   Client
    @IBOutlet weak var cLoader: UIButton!
    @IBOutlet weak var cName: UIButton!
    @IBOutlet weak var cContainer: UIView!
    @IBOutlet weak var bName: UITextField!
    @IBOutlet weak var bdate: textFieldBorderBottom!
    @IBOutlet weak var bRef: textFieldBorderBottom!
    @IBOutlet weak var cFirstName: SearchTextField!
    @IBOutlet weak var cLastName: SearchTextField!
    @IBOutlet weak var cMobile: SearchTextField!
    @IBOutlet weak var cPhone: SearchTextField!
    @IBOutlet weak var cEmail: SearchTextField!
    @IBOutlet weak var cWebsite: textFieldBorderBottom!
    @IBOutlet weak var cAddress: textFieldBorderBottom!
    @IBOutlet weak var cHeightConst: NSLayoutConstraint!
    //---|   Dropdown
    var tavDropDown:Dictionary<Int, DropDown> = Dictionary<Int, DropDown>()
    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        //---|   Include Edit View
        self.v_edit_main.include(self.v_edit.ini(v_edit_main, v_list))
        self.v_footer.ini()
        //---|   NOTIFICATION
            //---|   Connected
        NotificationCenter.default.addObserver(forName: BROADCAST.CONNECTED, object: nil, queue: nil, using: self.connected)
            //---|   Customer
        // NotificationCenter.default.addObserver(forName: BROADCAST.CUSTOMER_NAME_CHANGED, object: nil, queue: nil, using: self.notifCustomerNameChanged)
        NotificationCenter.default.addObserver(forName: BROADCAST.CUSTOMERS_LOADED, object: nil, queue: nil, using: self.customersLoaded)
        // Products table View
        self.productsTView.binding(self)
        // Totals
        // Bills
        self.billsTView.binding(self)
        // Design
        self.productsView.dropShadow()
        // Bill Init
        // self.billInit()
        // Design
        self.datePicker.backgroundColor = UIColor("#ffffff", alpha: 0.8)
        //---|   Hide Customer Edit
        //---|   SetUp
        self.cCustomer.setup([
            "first_name" : cFirstName,
            "last_name" : cLastName,
            "mobile" : cMobile,
            "phone" : cPhone,
            "email" : cEmail,
            "website" : cWebsite,
            "address" : cAddress,
        ], self)
    }
    override func showed(){
        super.showed()
        self.v_edit.show(false)
        self.v_footer.ini()
    }
    //---|   connected
    override func connected(notification: Notification?=nil){
        super.connected()
        //---|   Load Customer
        self.cCustomer.loaded()
        //---|   Bills Get
        DB.BILLS.observe(.value, with: { (snap) in
            if snap.exists() {
                self.bills = (snap.value as? Dictionary<String, Dictionary<String, Any>>)!
            }
            self.billsTView.reloadData()
        })
    }
    //---|   Customer Loaded
    func customersLoaded(notification: Notification){
        self.billsTView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //---|
    func can() -> Bool{
        if self.bill["id"] != nil && self.bills[String(self.bill["id"])] != nil{ return false }
        return true
    }
    func canEdit(){
        self.v_edit.show()
        let can = self.can()
        // UIView.animate(withDuration: 1.0, animations: {
            self.datePickerBtn.isEnabled = can
            self.bName.isEnabled = can
            self.bRef.isEnabled = can
            self.v_footer.new(can)
            // self.v_footer.save.isHidden = !can
            // self.v_footer.delete.isHidden = !can
            // self.v_footer.archive.isHidden = can
            // self.v_footer.print.isHidden = can
        // })
    }

    //---|   Total
    // Number Of section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return String(self.bill["total"], "0").toFloat(0) > 0 ? (self.bill["tvas"] as! NSArray).count+2 : 0
    }
    func productAdding(_ newItem:Bool=false){
        print("-----\n#-----\nproductAdding\n-----\n\(self.can())\n-----\n#-----\n")
        //---|   Can't Edit
        if !self.can() { self.billInit() }
        else{
            let products = self.productsAction(["tva":app.tvas[0] as AnyObject,"qty":"1" as AnyObject,"price":"" as AnyObject,"name":"" as AnyObject] as AnyObject)
            self.productsTView.reloadData()
            //---|   Scroll to Buttom
            if products.count > 1 { self.productsTView.selectRow(self, products.count-2) }
            self.v_edit.show()
            if newItem, let cell = self.productsTView!.cellForRow(at: IndexPath(row: self.productsAction().count-1, section: 0)) as? cellBillProduct{
                cell.name.becomeFirstResponder()
            }else{
                self.bName.becomeFirstResponder()
            }
        }
    }
    //---|   Open Editing
    func editing(_ index:Int ){
        self.v_footer.new(false)
        self.clean()
        self.productsTView.reloadData()
        //---|
        self.bill = self.bills.dic(index)
        cCustomer.selected(String(self.bill["customer"]))
        self.bName.text = String(self.bill["name"])
        self.bRef.text = String(self.bill["ref"])
        self.datePicker.date = Date(String(self.bill["date"]), "timestamp")
        datePickerBtn.setTitle(self.datePicker.date.string("dd/MM/yyyy"), for: .normal)
        //self.customerNameChanged()
        self.calculationTvas(&self.bill)
        self.canEdit()
    }
    func showList(_ select:Int?=nil){
        // self.billInit()
        self.billsTView.reloadData()
        self.v_footer.ini()
        //---|   Select Row
        if let select = select as? Int, self.bills.count > select, select >= 0 { self.billsTView.selectRow(self, select) }
        timeout({ self.v_edit.show(false) }, 100)
        self.cCustomer.hide_result()
    }
    //---|
    func productDel(_ index: Int) {
        DispatchQueue.main.async{
            self.productsAction(index as AnyObject)
            self.productsTView.deleteRows(at: [IndexPath(row: index, section : 0)], with: .fade)
            self.productsTView.reloadData()
        }
    }
    func productDelClicked(_ sender: AnyObject) { self.productDel(sender.tag) }

    //---|
    func priceInt(_ price:Any? = nil, _ back:Double=0)->Int{
        var back = back
        if price is String{
            back = Double((price as? String)!)!
        }else if price is Int{
            back = Double((price as? Int)!)
        }else if (price is Double) || (price is Float){
            back = Double((price as? Double)!)
        }
        return Int(back*100)
    }
    func priceStr(_ price:Int)->String{ return String(describing: (price/100)) }
    // Calucation
    override func calculationTvas(_ obj: inout Dictionary<String, Any>, _ sProducts:NSDictionary? = nil, _ callback : (() -> Void)? = nil){
        super.calculationTvas(&obj, sProducts, callback)
        var tvas = 0.0
        for tva in (obj["tvas"] as? [[String:Any]])! { tvas += String(tva["value"]).toDouble() }
        self.v_totals.l_ht.text = String(obj["total_ht"], "0").format()+" €"
        self.v_totals.l_tva.text = String(tvas).format()+" €"
        self.v_totals.l_ttc.text = String(obj["total"], "0").format()+" €"
    }
    // TVA Dropdown
    func tvaDropdown(_ sender: AnyObject) {
        self.tavDropDown[Int(sender.tag!)]!.show()
    }
    //---| IF Index IsSet => Update || Value is Int => Delete || Value is Dictionary => Add
    func productsAction(_ value:AnyObject?=nil, _ index:Int?=nil, _ key:String?="") -> Dictionary<String, Dictionary<String, Any>>{
        var products = Dictionary<String, Dictionary<String, Any>>()
        if let prs = self.bill["products"] as? Dictionary<String, Dictionary<String, Any>> {
            products = prs
            if value != nil{
                if index != nil{ products[products.key(index!)]?[key!] = value }
                else if value is Int{ products.removeValue(forKey: products.key(value as! Int)) }
                else{ products[app.id()] = value as! Dictionary<String, AnyObject> }
                self.bill["products"] = products
            }
        }
        return products
    }
    func productNameChanging(_ sender:AnyObject){
        self.productsAction((sender as! UITextField).text as AnyObject, sender.tag, "name")
    }
    func productQtyChanging(_ sender:AnyObject){
        self.productsAction((sender as! UITextField).text as AnyObject, sender.tag, "qty")
        self.calculationTvas(&self.bill)
    }
    func productPriceChanging(_ sender:AnyObject){
        self.productsAction((sender as! UITextField).text as AnyObject, sender.tag, "price")
        self.calculationTvas(&self.bill)
    }
    func clean(){
        self.cName.setTitle("Nom de la facture & Nom du client", for: .normal)
        self.bName.text = ""
        self.bRef.text = ""
        self.cCustomer.clean()
    }
    //---|   init
    func billInit(){
        self.v_totals.ini()
        self.datePicker.date = Date()
        datePickerBtn.setTitle(self.datePicker.date.string("dd/MM/yyyy"), for: .normal)
        self.clean()
        self.bill = self.iBill
        self.canEdit()
        self.productAdding()
        self.calculationTvas(&self.bill)
        self.billsTView.reloadData()
    }
    override func transactionSave(_ transaction: inout Dictionary<String, Any>, _ obj:inout Dictionary<String, Any>){
        super.transactionSave(&transaction, &self.bill)
        self.saving()
    }
    //---|   Name Change
    // func customerNameChanged(){
    //     self.cName.setTitle(!String(self.bName.text).empty() ? String(self.bName.text) : "\(String(self.cFirstName.text)) \(String(self.cLastName.text))", for: .normal)
    // }
    // func notifCustomerNameChanged(notification: Notification){ self.customerNameChanged() }

    //---|   Archived && Delete
    func deleting(_ pId:String?=nil, _ del:Bool=false){
        let id = pId ?? String(self.bill["id"])
        if !id.empty() {
            //---|   Delete
            if del { DB.ARCHIVES.child(id).removeValue() }
            DB.BILLS.child(id).removeValue()
            //---|   Remove From
            self.bills.removeValue(forKey: id)
        }
        self.showList()
    }
    // Add Product
    @IBAction func productAdd(_ sender: AnyObject) { self.productAdding(true) }
    @IBAction func BillNew(_ sender: AnyObject) { self.billInit() }
    @IBAction func datePickerClick(_ sender: Any) {
        if datePickerBtn.currentTitle == "Términer"{
            datePickerBtn.setTitle(self.datePicker.date.string("dd/MM/yyyy"), for: .normal)
            self.datePicker.isHidden = true
        }else{
            self.datePicker.isHidden = false
            datePickerBtn.setTitle("Términer", for: .normal)
        }
    }
        //---|   Delete
    @IBAction func deleteBill(_ sender: Any) { self.deleting(nil, true) }
        //---|   Save
    func saving(){
        cCustomer.save { (customer) in
            let id = String(self.bill["id"])
            //---|   Save Customer
            var customer_info:[String:Any]? = nil
            if let nCustomer = self.cCustomer.customers[customer] as? [String:Any]{ customer_info = nCustomer }
            self.bill["customer_info"] = customer_info
            self.bill["customer"] = customer
            self.bill["ref"] = String(self.bRef.text)
            self.bill["name"] = String(self.bName.text, "\(String(self.cFirstName.text)) \(String(self.cLastName.text))")
            self.bill["date"] = self.datePicker.date.timestamp
            self.bills[id] = self.bill as [String : AnyObject]
            //---|
            DB.BILLS.child(id).setValue(self.bill)
            //---|   Archive
            self.bill["view"] = "bills"
            DB.ARCHIVES.child(id).setValue(self.bill)
            //---|   Select Last ROW
            self.showList(self.bills.index(id))
            self.cPrint.print_run(self.bill)
            self.cCustomer.clean()
            self.bill = Dictionary<String, Any>()
        }
    }
    @IBAction func save(_ sender: Any) {
        if self.bill["id"] == nil { self.bill["id"] = app.id() as AnyObject }
        if self.can() {
            self.transactionModal(String(self.bill["total"]))
        }else{
            self.saving()
        }
    }
    @IBAction func cNameChanging(_ sender: AnyObject) {  }
    @IBAction func archivedClick(_ sender: AnyObject) { self.deleting() }
    //---|   Print
    @IBAction func printClick(_ sender: AnyObject) {
        self.cPrint.print_run(self.bill)
    }
    @IBAction func listShow(_ sender: AnyObject) {
        self.v_edit.show(false)
    }
    //-----------------------------------------------#
      //--|
    //-----------------------------------------------#
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        if tableView == self.productsTView { return self.productsAction().count+(self.can() ? 1 : 0) }
        if tableView == self.billsTView {
            let count = self.bills.count
            self.billsTView.isHidden = (count == 0)
            return count
        }
        return 0
    }
    // Item Set
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // cell
        if tableView == self.productsTView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellBillProduct", for: indexPath) as! cellBillProduct
            // cell.isHidden = true
            let products = self.productsAction()
            let can = self.can()
            if (products.count > indexPath.row) || (!can) {
                //---|   Init Product
                let product = self.productsAction().dic(indexPath.row) as Dictionary<String, AnyObject>
                cell.new(false)
                // cell.isHidden = false
                cell.name.tag = indexPath.row
                cell.name.text = String(product["name"], "...")
                cell.qty.tag = indexPath.row
                cell.qty.text = String(product["qty"])
                cell.price.tag = indexPath.row
                cell.price.text = String(product["price"])
                cell.tva.setTitle(String(product["tva"]), for: UIControlState())
                // Action
                cell.tva.tag = indexPath.row
                cell.b_minus.isHidden = !can
                if can {
                    cell.name.addTarget(self, action: #selector(self.productNameChanging(_:)), for: .editingDidEnd)
                    cell.qty.addTarget(self, action: #selector(self.productQtyChanging(_:)), for: .editingChanged)
                    cell.price.addTarget(self, action: #selector(self.productPriceChanging(_:)), for: .editingChanged)
                    cell.tva.addTarget(self, action: #selector(self.tvaDropdown(_:)), for: .touchUpInside)
                    // Action
                    cell.b_minus.tag = indexPath.row
                    cell.b_minus.addTarget(self, action: #selector(self.productDelClicked(_:)), for: .touchUpInside)

                    self.tavDropDown[indexPath.row] = DropDown();
                    // Init Dropdown TVA
                    self.dropdownInit(self.tavDropDown[indexPath.row]!, view: cell.tvaView, datasource: app.tvas as NSArray, btnShowing: cell.tva, { (index, tva) in
                        cell.tva.setTitle(tva, for: .normal)
                        self.productsAction(tva as AnyObject, cell.tva.tag, "tva")
                        self.calculationTvas(&self.bill)
                    })
                }
                cell.name.isEnabled = can
                cell.qty.isEnabled = can
                cell.price.isEnabled = can
                cell.tva.isEnabled = can
            }else{
                cell.new(true)
                cell.b_plus.tag = indexPath.row
                cell.b_plus.addTarget(self, action: #selector(self.productAdd(_:)), for: .touchUpInside)
            }
            return cell.deSelectRow()
        }
        if tableView == self.billsTView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellBills", for: indexPath) as! cellBills
            // cell.isHidden = true
            if  let bill = self.bills[self.bills.key(indexPath.row)]{
                cell.name.text = "Facture #\(indexPath.row+1)"
                if let name = bill["name"] as? String, !name.empty() { cell.name.text = name }
                // cell.customer.text = String(self.g(cCustomer.customers, "\(String(bill["customer"])).name", "..." as AnyObject))
                cell.total.text = String(bill["total"], "0").format()+" €"
                // cell.count.text = String(((bill["products"] as? NSDictionary)! ?? NSDictionary()).count)
                // cell.isHidden = false
            }
            return cell.ini()
        }
        return tableView.cellForRow(at: indexPath)!
    }
    // Active Deleting
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return (tableView == self.productsTView && self.can())
    }
    // Deleting
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == self.productsTView, editingStyle == .delete { self.productDel(indexPath.row) }
    }
    // Cell Selected Of Menu Side
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.billsTView, let cell = self.billsTView.cellForRow(at: indexPath as IndexPath) as? cellBills {
            //---|   Only When ListView Is Showing
            if !self.v_list.isHidden{ self.editing(indexPath.row) }
            cell.selected()
        }
    }
    //---|   DidDeselect
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == self.billsTView {
            print("-----\n#-----\nDeSelect\n#-----\n")
            (tableView.cellForRow(at: indexPath as IndexPath) as! cellBills).ini()
            // tableView.deSelectRow(indexPath)
        }
    }
}
