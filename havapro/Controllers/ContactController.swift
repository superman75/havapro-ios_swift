//
//  ContactController.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class ContactController: AppViewController, UITableViewDataSource, UITableViewDelegate  {
    var contacts:Dictionary<String, Dictionary<String, AnyObject>> = [:]

    // Outlet
    @IBOutlet weak var contactTView: UITableView!
    @IBOutlet weak var contactsView: UIView!
    @IBOutlet weak var companyProvider: UILabel!
    @IBOutlet weak var nameProvider: UILabel!
    @IBOutlet weak var logoProvider: avatar!
    @IBOutlet weak var menuCView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Contact table View
        self.contactTView.dataSource = self
        self.contactTView.delegate = self
        // Design
        self.contactsView.dropShadow()
    }
    //---|   Connection | Get Provider Local Information
    override func connected(notification: Notification?=nil){
        // Contacts
        DB.CONTACTS.observeSingleEvent(of: .value, with: { (snap) in
            if !snap.exists() { return }
            self.contacts = (snap.value as? Dictionary<String, Dictionary<String, AnyObject>>)!
            self.contactTView.reloadData()
        })
        self.companyProvider.text = (app.profile["company"] as? String)!
        self.nameProvider.text = (app.profile["name"] as? String)!
        //--| Avatar
        self.logoProvider.assign(app.profile["logo"])
    }
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{ return self.contacts.count }
    // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        // contactTView
        let contactTView = tableView.dequeueReusableCell(withIdentifier: "cellContact", for: indexPath) as! cellContact
        let support = self.contacts[Array(self.contacts.keys)[indexPath.row]]!

        contactTView.name.text = (support["name"] as? String)!
        contactTView.email.text = (support["email"] as? String)!
        contactTView.phone.text = (support["phone"] as? String)!
        //--| Avatar
        contactTView.avatar.assign(support["avatar"])
        return contactTView
    }

}
