import UIKit
import Spring
//---|   AppBehavior
protocol AppBehavior{
    func connected()
    func dismissed()
    func side_updated(_ close:Bool)
}
//---|   AppViewController
class AppViewController: UIViewController, AppBehavior{


    var appController: AppController?
    let cPrint:PrintManager = PrintManager()
    //---|   Get Parent
    func appController(_ s_parent : UIViewController) -> AppController{
        if let parentCtrl = s_parent as? AppController { return parentCtrl }
        return appController(s_parent.parent!)
    }
    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        appController = self.appController(self.parent!)
        appController?.delegate = self
        //---|
        self.cPrint.print_ini(self)
        //---| Connected
        NotificationCenter.default.addObserver(forName: BROADCAST.CONNECTED, object: nil, queue: nil, using: self.connected)
        //---|   PROVIDER_UPDATED
        NotificationCenter.default.addObserver(forName: BROADCAST.PROVIDER_UPDATED, object: nil, queue: nil, using: self.provider_updated)
        //---| SESSION_STARTED
        NotificationCenter.default.addObserver(forName: BROADCAST.SESSION_LOADED, object: nil, queue: nil, using: self.session_loaded)
        //---| INTERFACE.UPDATED
        NotificationCenter.default.addObserver(forName: BROADCAST.INTERFACE.UPDATED, object: nil, queue: nil, using: self.interface_updated)
        //---|   SETTING.UPDATED
        NotificationCenter.default.addObserver(forName: BROADCAST.SETTING.UPDATED, object: nil, queue: nil, using: self.setting_updated)
    }
    //---|   Connected
    public func connected(){}
    public func connected(notification: Notification?=nil){}
    public func provider_updated(notification: Notification?=nil){}
    public func session_loaded(notification: Notification?=nil){}
    public func setting_updated(notification: Notification?=nil){}
    func side_updated(_ close: Bool) { }
    //---|   sessionStarted
    public func sessionStarted(notification: Notification?=nil){}
    //---|   Showed | Init Menu Side
    override func showed(){
        print("-----------| showed() |----------\n showed() --> \(self.appController?.v_side_container.tag) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        //---|   Open Menu Each Change
        self.appController?.v_side_container.tag = 0
        self.appController?.menu_toggle(false)
    }
    public func dismissed(){}
    public func interface_updated(notification: Notification?=nil){}
}
//---|   AppController
class AppController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //---|   Vars
    var delegate: AppBehavior?
    var navigation = [
        (name : "Vente", controller:"Sale", count : 0, icon : "m_sale.png"),
        (name : "Réservation", controller:"Agenda", count : 0, icon : "m_agenda.png"),
        (name : "Facturation", controller:"Bill", count : 0, icon : "m_invoice.png"),
        (name : "Statistique", controller:"Statistical", count : 0, icon : "m_statics.png"),
        (name : "Assistance", controller:"Support", count : 0, icon : "m_support.png"),
        (name : "Communication", controller:"Communication", count : 0, icon : "m_communication.png"),
        (name : "Configuration", controller:"ViewConfig", count : 0, icon : "m_setting.png"),
        (name : "Déconnection", controller:"logout", count : 0, icon : "m_logout.png")
        // (name : "Contact", controller:"Contact", count : 0, icon : "m_contact.png"),
    ]
    var currentController:Int = -1
    var controllers:[String:UIViewController] = [String:UIViewController]()
    let controllersForceReLoad:[String] = []
    //--| Outlet
    @IBOutlet weak var i_menu: SpringImageView!
        //--| Side
    @IBOutlet weak var v_side: UIView!
    @IBOutlet weak var v_side_const_width: NSLayoutConstraint!
    @IBOutlet weak var v_side_container: SpringView!
    @IBOutlet weak var v_logo_side: UIView!
        //--| Menu Active
    @IBOutlet weak var v_menu_container: UIView!
    @IBOutlet weak var menu_open_btn: UIButton!
    @IBOutlet weak var menu_const_height_custom: NSLayoutConstraint!
    @IBOutlet weak var menu_const_height_auto: NSLayoutConstraint!
    @IBOutlet weak var tv_navigation: UITableView!
    @IBOutlet weak var container: SpringView!
    //--| Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.side_toggle()
        self.tv_navigation.binding(self)
        self.loading()
        self.connection(){ (autoconnect) in
            print("-----------| autoconnect |----------\n autoconnect --> \(autoconnect) | \n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
            if autoconnect {
                self.tv_navigation.selectRow(self, 0) //---|   Binding && Select First Row
                //---| Session Start
                SessionManager().session_init()
                //---|   Setting
                SettingManager.ini()
                //---|   Notif All View is Connected
                NotificationCenter.default.post(name: BROADCAST.CONNECTED, object: nil)
                // if app.profile["reservation"] == nil { self.navigation.remove(at: 1) }
                // if app.profile["caisse"] == nil{ self.navigation.remove(at: 0) }
                // //self.loadoff()
                self.delegate?.connected()
                return
            }
            NotificationCenter.default.post(name: BROADCAST.PROVIDER_UPDATED, object: nil)
        }
    }
    //---|   Include View Controller
    func controllerInclude(_ index:Int, _ refresh:Bool=false) -> UIViewController{
        let current_name_controller = self.navigation[index].controller
        if (self.currentController != index) || (refresh) {
            //---|   dismissed
            if self.currentController > 0, self.currentController != index { self.delegate?.dismissed() }
            self.currentController = index
            self.side_toggle(true) //---|   Close Side
            if (self.controllers[current_name_controller] == nil) || (self.controllersForceReLoad.contains(current_name_controller)){
                self.controllers[current_name_controller] = self.include("\(current_name_controller)Controller", container)
                //---|
                if app.uid != nil {
                    self.delegate?.connected()
                    NotificationCenter.default.post(name: BROADCAST.CONNECTED, object: nil)
                }
            }else{ self.include(self.controllers[current_name_controller]!, container) }
            // TODO
            //(self.controllers[current_name_controller] as? AppViewController)?.showed()
        }
        return self.controllers[current_name_controller]!
    }

    //---|  Nav Selected
    func navSelected(_ index:Int=0) -> UITableViewCell{
        let cell = self.tv_navigation.cellForRow(at: IndexPath(row: index, section: 0)) as! navigationCell
        // UIView.animate(withDuration: 0.5) {
            cell.name.font = UIFont.boldSystemFont(ofSize: 16.0)
            cell.name.textColor = UIColor("#3D8EB9")
            cell.icon.alpha = 1
            cell.count.textColor = UIColor("#FFFFF7")
            cell.v_count.backgroundColor = UIColor("#EE543A", alpha:0.6)
        // }
        self.controllerInclude(index) //---|   Include View Controller
        return cell.deSelectRow()
    }

    //---|   Nav DeSelected
    func navDeSelected(_ cell:navigationCell) -> UITableViewCell{
        // UIView.animate(withDuration: 0.2) {
            cell.name.font = UIFont.systemFont(ofSize: 14.0, weight: UIFontWeightThin)
            cell.name.textColor = UIColor("#282830")
            cell.icon.alpha = 0.2
            cell.count.textColor = UIColor("#EE543A")
            cell.v_count.backgroundColor =  UIColor("#FFFFF7")
        // }
        return cell.deSelectRow()
    }
    //---|   toggleMenu
    func side_toggle(_ close:Bool?=nil){// Tag 1 Hide
        NotificationCenter.default.post(name: BROADCAST.INTERFACE.UPDATED, object: "start")
        // UIView.animate(withDuration: 0.1) {
            if (close == nil && self.v_side.tag == 0) || (close != nil && close!){
                self.v_side_const_width.constant = 40
                self.v_logo_side.isHidden = true
                self.v_side.tag = 1
                self.i_menu.duration = 0.5
                self.i_menu.rotate = 55
                self.i_menu.animateTo()
            }else if (close == nil) || (close != nil && !close!) {
                self.v_side_const_width.constant = 250
                self.v_logo_side.isHidden = false
                self.v_side.tag = 0
                self.i_menu.rotate = 0
                self.i_menu.animateTo()
            }
            self.delegate?.side_updated((self.v_side.tag == 0))
            // TODO
            NotificationCenter.default.post(name: BROADCAST.INTERFACE.UPDATED, object: "end")
            self.timeout({
                NotificationCenter.default.post(name: BROADCAST.INTERFACE.UPDATED, object: ["side" : (self.v_side.tag == 0)])
            }, 1000)
        // }
        self.tv_navigation.reloadData()
    }
    @IBAction func menuToggle(_ sender: AnyObject) { self.side_toggle() }
    //---|
    func menu_toggle(_ close_menu:Bool?=nil){
        // UIView.animate(withDuration: 0.1) {
            if (close_menu == nil && !self.menu_open_btn.isHidden) || (close_menu != nil && close_menu!) {
                self.menu_open_btn.isHidden = !(self.v_side_container.tag > 0)
                // print("-----\n#-----\nClosing Menu\n-----\n\(close_menu)\n------\n#-----\n")
                self.self.menu_const_height_auto.constant = -(self.v_menu_container.frame.size.height-60.0)
                self.tv_navigation.backgroundColor = UIColor("#eeeeee")
                //---|   Center Menu
                //self.tv_navigation.selectRow(self, self.currentController, false)
            }else if (close_menu == nil) || (!close_menu!) {
                self.menu_open_btn.isHidden = true
                // print("-----\n#-----\nClosing Container\n-----\n\(close_menu)\n------\n#-----\n")
                self.menu_const_height_auto.constant = CGFloat(-(self.v_side_container.tag))
                self.tv_navigation.backgroundColor = UIColor.clear
            }
        // }
    }
    //---|   Change Direction
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (_) in
            self.menu_toggle(!self.menu_open_btn.isHidden)
        }) { (_) in
            self.menu_toggle(!self.menu_open_btn.isHidden)
        }
    }
    @IBAction func sideClose(_ sender: AnyObject) { self.menu_toggle(false) }
    /*-----------------------------------------------*\
        #--| TableView
    \*-----------------------------------------------*/
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        return self.navigation.count
    }
    //---|   Fill
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "navigationCell", for: indexPath) as! navigationCell
        // UIView.animate(withDuration: 2) {
            let nav = self.navigation[indexPath.row]
            cell.name.text = nav.name
            cell.count.text = String(nav.count)
            cell.icon.image = UIImage(named: nav.icon)
            cell.v_count.isHidden = (nav.count <= 0)
            //---|   isOpened
            if self.v_side.tag == 0{
                cell.name.isHidden = false
                cell.count_const_y.constant = 0
                cell.count_const_right.constant = 8
            }else{ //---|   isClosed
                cell.name.isHidden = true
                cell.count_const_y.constant = -14
                cell.count_const_right.constant = 19
            }
            //---|   Menu : Selected Item
            if self.currentController == indexPath.row {
                // self.navDeSelected(cell)
            }else{
                self.navDeSelected(cell)
            }
        // }
        return cell
        //return tableView.cellForRow(at: indexPath)!
    }
    //---|   Select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.navigation.count > indexPath.row {
            if self.navigation[indexPath.row].controller == "logout" {
                self.confirm({ (str) in
                    self.logout()
                }, "Déconnection", "Voulez-vous vraiment quitter HavaPRO", ["Annuler", "Se Déconnecter"])
                return
            }
            self.navSelected(indexPath.row)
        }
    }
    //---|   DeSelect
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // UIView.animate(withDuration: 1) {
            self.navDeSelected(self.tv_navigation.cellForRow(at: indexPath as IndexPath) as! navigationCell)
        // }
    }

}

