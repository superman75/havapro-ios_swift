//
//  popupCommunicationController.swift
//  havapro
//
//  Created by HavaPRO on 18/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class popupCommunicationController: PopupViewController, UITableViewDataSource, UITableViewDelegate {
    //---|   VARS
    var itemCommunicationCtrl:CommunicationItemController = CommunicationItemController()
    //---|   OUTLETS
    // Outlet
    @IBOutlet weak var l_title: UILabel!
    @IBOutlet weak var v_proposal: UIView!
    @IBOutlet weak var tv_proposal: UITableView!
    @IBOutlet weak var v_refused: UIView!
    @IBOutlet weak var t_reason: UITextView!
    @IBOutlet var v_popup: PopupCommunicationView!
    //---|
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    //---|
    override func popShowing(){
        self.popResize(CGSize(width: 500,height: 500))
    }
    //---|
    override func popShowed(){
        super.popShowed()
        self.tv_proposal.binding(self)
        //---|   Refused
        self.t_reason.text = String(itemCommunicationCtrl.communication["reason"])
        //---|!String(itemCommunicationCtrl.communication["refused"]).bool
        self.proposal()
        //---|   Reload Data
        self.tv_proposal.reloadData()
    }
    func proposal(_ show:Bool=true){
        self.v_proposal.isHidden = !show
        self.v_refused.isHidden = show
        self.l_title.text = (show ? "PROPOSITION" : itemCommunicationCtrl.name).uppercased()
    }
    //--| Actions
    @IBAction func accepting(_ sender: AnyObject) {
        itemCommunicationCtrl.c_popdismissed(["accepted" : true])
        popup.dismiss()
    }
    @IBAction func refusing(_ sender: AnyObject) {
        self.proposal(false)
    }
    @IBAction func sending(_ sender: AnyObject) {
        itemCommunicationCtrl.c_popdismissed(["refused" : true, "reason" : self.t_reason.text])
        popup.dismiss()
    }
    @IBAction func ignoring(_ sender: AnyObject) {
        itemCommunicationCtrl.c_popdismissed(["refused" : true])
        popup.dismiss()
    }
    //-----------------------------------------------#
       //--|  TableView
    //-----------------------------------------------#
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // return 0
        return (itemCommunicationCtrl.communication["proposals"] != nil ? (itemCommunicationCtrl.communication["proposals"] as? Dictionary<String, AnyObject>)!.count+2 : 0)
    }
    // // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        // supportTView
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopupCommunicationCell", for: indexPath) as! PopupCommunicationCell
        if ((itemCommunicationCtrl.communication["proposals"]! as AnyObject).count)! > indexPath.row {
            let proposals = (itemCommunicationCtrl.communication["proposals"] as? Dictionary<String, Any>)!
            // let proposal = (proposals[proposals.key(indexPath.row)] as? Dictionary<String, AnyObject>)!
            if  proposals.count > indexPath.row{
                let proposal = proposals.dic(indexPath.row)  as Dictionary<String, Any>
                cell.key.text = String(proposal["key"])
                cell.value.text = String(proposal["value"])
            }
        }else if (itemCommunicationCtrl.communication["proposals"]! as AnyObject).count == indexPath.row{
            cell.key.text = "Date De Lancement"
            cell.value.text = (!String(itemCommunicationCtrl.communication["start"]).empty() ? Date(String(itemCommunicationCtrl.communication["start"]), "yyyy-MM-dd").string("dd/MM/yyyy") : " ... ")
        }else{
            cell.key.text = "Date Fin"
            cell.value.text = (!String(itemCommunicationCtrl.communication["end"]).empty() ? Date(String(itemCommunicationCtrl.communication["end"]), "yyyy-MM-dd").string("dd/MM/yyyy") : " ... ")
        }
        return cell
    }

}
