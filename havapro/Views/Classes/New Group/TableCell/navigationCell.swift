//
//  navigationCell.swift
//  HavaPRO
//
//  Created by abdelghani on 24/12/2017.
//  Copyright © 2017 Havatar. All rights reserved.
//

import UIKit

class navigationCell: UITableViewCell {
    // Init Vars
    @IBOutlet var name: UILabel!
    @IBOutlet var v_count: UIView!
    @IBOutlet var count: UILabel!
    @IBOutlet var icon: UIImageView!
    @IBOutlet weak var count_const_y: NSLayoutConstraint!
    @IBOutlet weak var count_const_right: NSLayoutConstraint!
}
