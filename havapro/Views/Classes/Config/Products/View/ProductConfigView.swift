//
//  ProductConfigView.swift
//  havapro
//
//  Created by HavaPRO on 04/02/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class ProductConfigView: UIView {

    //---|   OUTLETS
    @IBOutlet weak var v_head: HeadProductConfigView!
    @IBOutlet weak var v_lists: UIView!
    @IBOutlet weak var v_eCategory: UIView!
    @IBOutlet weak var v_eProduct: UIView!
    @IBOutlet weak var v_eComponent: UIView!
    //---|
    func ini(){

    }
}
