//
//  HeadProductConfigView.swift
//  havapro
//
//  Created by HavaPRO on 04/02/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class HeadProductConfigView: UIView {

    //---|   OUTLETS
    @IBOutlet weak var v_head: HeadCustomerView!
    @IBOutlet weak var v_edit: EditCustomerView!
    @IBOutlet weak var tv_list: UITableView!

}
