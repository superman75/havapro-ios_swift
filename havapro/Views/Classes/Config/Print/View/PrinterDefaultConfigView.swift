//
//  PrinterDefaultConfigView.swift
//  havapro
//
//  Created by HavaPRO on 05/02/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class PrinterDefaultConfigView: UIView {
    //---|
    var names = [0: "customer", 1: "ketchen", 2:"fenced"]
    //---|   OUTLET
    @IBOutlet weak var i_sCustomer: UIImageView!
    @IBOutlet weak var l_nCustomer: UILabel!
    @IBOutlet weak var i_sKetchen: UIImageView!
    @IBOutlet weak var l_nKetchen: UILabel!
    @IBOutlet weak var i_sFenced: UIImageView!
    @IBOutlet weak var l_nFenced: UILabel!

    //---|
    func ini(){
        for img in [i_sCustomer, i_sKetchen, i_sFenced]{ img!.image = UIImage(named: "cancel.png") }
        for label in [l_nCustomer, l_nKetchen, l_nFenced]{ label!.text = "..." }
    }
    func print(_ tag:Int, _ info:[String:Any]) {
       
        let imgs:[Int: UIImageView] = [0: i_sCustomer, 1: i_sKetchen, 2: i_sFenced]
        let labels:[Int: UILabel] = [0: l_nCustomer, 1: l_nKetchen, 2: l_nFenced]
        let status = [ 0 : "cancel", 1: "offline", 2: "online"]
        imgs[tag]!.image = UIImage(named: "\(status[String(info["status"], "0").toInt()]!).png")
        labels[tag]!.text = String(info["name"], "...")
    }
}
