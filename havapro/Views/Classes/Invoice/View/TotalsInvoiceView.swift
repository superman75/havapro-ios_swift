//
//  TotalsInvoiceView.swift
//  havapro
//
//  Created by abdelghani on 15/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class TotalsInvoiceView: UIView {

    @IBOutlet weak var l_ht: UILabel!
    @IBOutlet weak var l_tva: UILabel!
    @IBOutlet weak var l_ttc: UILabel!

    func ini(_ commande:[String:Any]?=nil){
        // self.l_ht.text = "00,00 €"
        // self.l_tva.text = "00,00 €"
        self.l_ttc.text = "TOTAL TTC 00,00 €"
        if let commande = commande as? [String:Any]{
            self.l_ttc.text  = "TOTAL = \(String(commande["total"], "0").format()) €"
        }
    }

}
