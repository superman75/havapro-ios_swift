//
//  EditInvoiceView.swift
//  havapro
//
//  Created by abdelghani on 15/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class EditInvoiceView: UIView {
    var v_parent:UIView = UIView()
    var v_list:UIView = UIView()
    //---|
    func ini(_ v_parent:UIView, _ v_list:UIView) -> UIView{
        self.v_parent = v_parent
        self.v_list = v_list
        self.show(false)
        return self
    }
    //---|
    func show(_ show:Bool=true){
        self.v_parent.isHidden = !show
        self.v_list.isHidden = show
    }

}
