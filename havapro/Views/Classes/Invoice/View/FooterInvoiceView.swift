//
//  FooterInvoiceView.swift
//  havapro
//
//  Created by abdelghani on 14/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class FooterInvoiceView: UIView {
    //
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var delete: UIButton!
    @IBOutlet weak var archive: UIButton!
    @IBOutlet weak var print: UIButton!
    func ini(){
        self.isHidden = true
    }
    func new(_ show:Bool=true){
        self.isHidden = false
        // self.save.isHidden = !show
        // self.delete.isHidden = !show
        self.archive.isHidden = show
        self.print.isHidden = show
        if !show { self.endEditing(true) }
    }
}
