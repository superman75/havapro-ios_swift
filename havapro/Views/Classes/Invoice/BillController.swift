//
//  BillController.swift
//  havapro
//
//  Created by Exo on 29/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import DropDown
import SearchTextField

class BillController: Transaction, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate  {
    //---|   VARS
    var cCustomer:Customer = Customer()
    var bills:Dictionary<String, Dictionary<String, Any>> = Dictionary<String, Dictionary<String, Any>>()
    let iBill:Dictionary<String, Any> = ["products": Dictionary<String, Dictionary<String, AnyObject>>(), "total" : 0, "tvas" : Dictionary<String, AnyObject>()]
    var bill:Dictionary<String, Any> = Dictionary<String, Any>()
    var totals:Array<Dictionary<String, Any>> = Array<Dictionary<String, Any>>()
    var iSelected:Dictionary<String, String?> = ["bill" : nil]

    //---|   Outlet
    @IBOutlet weak var v_main: UIView!
    @IBOutlet var v_edit: EditInvoiceCell!
    @IBOutlet weak var v_footer: FooterInvoiceView!

    @IBOutlet weak var totalsCView: UICollectionView!
    @IBOutlet weak var productsTView: UITableView!
    @IBOutlet weak var billsTView: UITableView!
    @IBOutlet weak var productsView: UIView!
    @IBOutlet weak var logoProvider: avatar!
    @IBOutlet weak var nameProvider: UILabel!
    @IBOutlet weak var addressProvider: UILabel!
    @IBOutlet weak var tvaProvider: UILabel!
    @IBOutlet weak var siretProvider: UILabel!
    @IBOutlet weak var menuCView: UIView!
        //---|   BUTTON
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var archiveBtn: UIButton!
    @IBOutlet weak var printBtn: UIButton!
    //---|   Client
    @IBOutlet weak var cLoader: UIButton!
    @IBOutlet weak var cName: UIButton!
    @IBOutlet weak var cContainer: UIView!
    @IBOutlet weak var bName: textFieldBorderBottom!
    @IBOutlet weak var bRef: textFieldBorderBottom!
    @IBOutlet weak var cFirstName: SearchTextField!
    @IBOutlet weak var cLastName: SearchTextField!
    @IBOutlet weak var cMobile: SearchTextField!
    @IBOutlet weak var cPhone: SearchTextField!
    @IBOutlet weak var cEmail: SearchTextField!
    @IBOutlet weak var cWebsite: textFieldBorderBottom!
    @IBOutlet weak var cAddress: textFieldBorderBottom!
    @IBOutlet weak var cHeightConst: NSLayoutConstraint!
    //---|   Dropdown
    var tavDropDown:Dictionary<Int, DropDown> = Dictionary<Int, DropDown>()
    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        //---|   Add Edit View
        self.v_main.include(self.v_edit)
        //---|   NOTIFICATION
            //---|   Customer
        NotificationCenter.default.addObserver(forName: BROADCAST.CUSTOMER_NAME_CHANGED, object: nil, queue: nil, using: self.notifCustomerNameChanged)
        NotificationCenter.default.addObserver(forName: BROADCAST.CUSTOMERS_LOADED, object: nil, queue: nil, using: self.customersLoaded)
        // Products table View
        self.productsTView.binding(self)
        // Totals
        self.totalsCView.binding(self)
        // Bills
        self.billsTView.binding(self)
        // Design
        self.productsView.dropShadow()
        // Bill Init
        self.billInit()
        // Design
        self.datePicker.backgroundColor = UIColor("#ffffff", alpha: 0.8)
        //---|   Hide Customer Edit
        self.customerToggle(true)
        //---|   SetUp
        self.cCustomer.setup([
            "first_name" : cFirstName,
            "last_name" : cLastName,
            "mobile" : cMobile,
            "phone" : cPhone,
            "email" : cEmail,
            "website" : cWebsite,
            "address" : cAddress,
        ])
    }
    //---|   connected
    override func connected(notification: Notification?=nil){
        super.connected()
        //---|   Load Customer
        self.cCustomer.loaded()
        //---|
        //self.nameProvider.text = String(app.profile["company"])
        //self.addressProvider.text = String(app.profile["address"])
        //self.tvaProvider.text = String(app.profile["tva"])
        //self.siretProvider.text = (app.profile["siret"] as? String)!
        //--| Avatar
        //self.logoProvider.assign(app.profile["logo"])
        //---|   Bills Get
        DB.BILLS.observe(.value, with: { (snap) in
            if snap.exists() {
                self.bills = (snap.value as? Dictionary<String, Dictionary<String, Any>>)!
            }
            self.billsTView.reloadData()
        })
    }
    //---|   Customer Loaded
    func customersLoaded(notification: Notification){
        self.billsTView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //---|
    func can() -> Bool{
        if self.bill["id"] != nil && self.bills[String(self.bill["id"])] != nil{ return false }
        return true
    }
    func canEdit(){
        let can = self.can()
        // UIView.animate(withDuration: 1.0, animations: {
            self.datePickerBtn.isEnabled = can
            self.bName.isEnabled = can
            self.bRef.isEnabled = can
            self.cFirstName.isEnabled = can
            self.cLastName.isEnabled = can
            self.cMobile.isEnabled = can
            self.cPhone.isEnabled = can
            self.cEmail.isEnabled = can
            self.cWebsite.isEnabled = can
            self.cAddress.isEnabled = can
            self.saveBtn.isHidden = !can
            self.deleteBtn.isHidden = !can
            self.archiveBtn.isHidden = can
            self.printBtn.isHidden = can
        // })
    }
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        if tableView == self.productsTView { return self.productsAction().count }
        if tableView == self.billsTView { return self.bills.count }
        return 0
    }
    // Item Set
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // productCell
        if tableView == self.productsTView {
            let productCell = tableView.dequeueReusableCell(withIdentifier: "cellBillProduct", for: indexPath) as! cellBillProduct
            productCell.isHidden = true
            //---|   Init Product
            let product = self.productsAction().dic(indexPath.row) as Dictionary<String, AnyObject>
            productCell.isHidden = false
            productCell.name.tag = indexPath.row
            productCell.name.text = String(product["name"], "...")
            productCell.qty.tag = indexPath.row
            productCell.qty.text = String(product["qty"])
            productCell.price.tag = indexPath.row
            productCell.price.text = String(product["price"])
            productCell.tva.setTitle(String(product["tva"]), for: UIControlState())
            // Action
            productCell.tva.tag = indexPath.row
            let can = self.can()
            if can {
                productCell.name.addTarget(self, action: #selector(self.productNameChanging(_:)), for: .editingDidEnd)
                productCell.qty.addTarget(self, action: #selector(self.productQtyChanging(_:)), for: .editingChanged)
                productCell.price.addTarget(self, action: #selector(self.productPriceChanging(_:)), for: .editingChanged)
                productCell.tva.addTarget(self, action: #selector(self.tvaDropdown(_:)), for: .touchUpInside)
                self.tavDropDown[indexPath.row] = DropDown();
                // Init Dropdown TVA
                self.dropdownInit(self.tavDropDown[indexPath.row]!, view: productCell.tvaView, datasource: app.tvas as NSArray, btnShowing: productCell.tva){ (index, tva) in
                    productCell.tva.setTitle(tva, for: .normal)
                    self.productsAction(tva as AnyObject, productCell.tva.tag, "tva")
                    self.calculationTvas(&self.bill)
                }
            }
            productCell.name.isEnabled = can
            productCell.qty.isEnabled = can
            productCell.price.isEnabled = can
            productCell.tva.isEnabled = can
            return productCell.deSelectRow()
        }
        if tableView == self.billsTView {
            let billCell = tableView.dequeueReusableCell(withIdentifier: "cellBills", for: indexPath) as! cellBills
            billCell.isHidden = true
            if  let bill = self.bills[self.bills.key(indexPath.row)]{
                billCell.name.text = String(bill["name"], "Facture - \(indexPath.row)")
                billCell.customer.text = String(self.g(cCustomer.customers, "\(String(bill["customer"])).name", "..." as AnyObject))
                billCell.total.text = "\(String(bill["total"], "0")) €"
                billCell.count.text = String(((bill["products"] as? NSDictionary)! ?? NSDictionary()).count)
                billCell.isHidden = false
            }
            return billCell.deSelectRow()
        }
        return tableView.cellForRow(at: indexPath)!
    }
    // Active Deleting
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return (tableView == self.productsTView && self.can())
    }
    // Deleting
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == self.productsTView, editingStyle == .delete {
            self.productsAction(indexPath.row as AnyObject)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.productsTView.reloadData()
        }
    }
    // Cell Selected Of Menu Side
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.deSelectRow(indexPath)
        if tableView == self.billsTView {
            selectedCell.backgroundColor = UIColor("#242b36")
            self.clean()
            self.productsTView.reloadData()
            //---|
            self.bill = self.bills[self.bills.key(indexPath.row)]!
            cCustomer.selected(String(self.bill["customer"]))
            self.bName.text = String(self.bill["name"])
            self.bRef.text = String(self.bill["ref"])
            self.datePicker.date = Date(String(self.bill["date"]), "timestamp")
            datePickerBtn.setTitle(self.datePicker.date.string("dd/MM/yyyy"), for: .normal)
            self.customerNameChanged()
            self.calculationTvas(&self.bill)
            self.canEdit()
            self.productsTView.reloadData()
        }
    }
    //---|   DidDeselect
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) { tableView.deSelectRow(indexPath) }
    //---|   Total
    // Number Of section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return String(self.bill["total"], "0").toFloat(0) > 0 ? (self.bill["tvas"] as! NSArray).count+2 : 0
    }
    // Set Data
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //---|   Total
        let cellTotal = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTotals", for: indexPath) as! cellTotals
        if indexPath.row == 0 {
            cellTotal.name.text = "Total HT"
            cellTotal.value.text = String(self.bill["total_ht"]).format()
        }else{
            let tvas = (self.bill["tvas"] as? Array<Dictionary<String, AnyObject>>)!
            if tvas.count >= indexPath.row{
                let tva = tvas[indexPath.row-1]
                cellTotal.name.text = "Dont TVA \(String(tva["key"]))%"
                cellTotal.value.text = String(tva["value"]).format()
            }else{
                cellTotal.name.text = "Total TTC"
                cellTotal.value.text = String(self.bill["total"]).format()
            }
        }
        return cellTotal
    }
    func productAdding(){
        //---|   Can't Edit
        if !self.can() { self.billInit() }
        else{
            self.customerToggle(true)
            self.productsAction(["tva":app.tvas[0] as AnyObject,"qty":"1" as AnyObject,"price":"" as AnyObject,"name":"" as AnyObject] as AnyObject)
            self.productsTView.reloadData()
            // Focus
            if let productCell = self.productsTView!.cellForRow(at: IndexPath(row: self.productsAction().count-1, section: 0)) as? cellBillProduct{
                productCell.name.becomeFirstResponder()
            }
        }
    }
    // Add Product
    @IBAction func productAdd(_ sender: Any) { self.productAdding() }
    //---|
    func priceInt(_ price:Any? = nil, _ back:Double=0)->Int{
        var back = back
        if price is String{
            back = Double((price as? String)!)!
        }else if price is Int{
            back = Double((price as? Int)!)
        }else if (price is Double) || (price is Float){
            back = Double((price as? Double)!)
        }
        return Int(back*100)
    }
    func priceStr(_ price:Int)->String{ return String(describing: (price/100)) }
    // Calucation
    override func calculationTvas(_ obj: inout Dictionary<String, Any>, _ sProducts:NSDictionary? = nil, _ callback : (() -> Void)? = nil){

    //override func calculationTvas(_ obj: inout Dictionary<String, Any>, _ sProducts: Dictionary<String, Dictionary<String, Any>>?, _ callback: (() -> Void)?) {
        super.calculationTvas(&obj, sProducts, callback)
        self.totalsCView.reloadData()
    }
    // func calculation(){
    //     var t_ttva:Float = 0
    //     var t_mtva:Float = 0
    //     var t_ht:Float = 0
    //     var t_ttc:Float = 0
    //     var tav_group = Dictionary<String, Float>()
    //     for product in (self.bill["products"] as? Array<Dictionary<String, AnyObject>>)!{
    //         // TVA
    //         if !String(product["price"]).empty(){
    //             let price = String(product["price"], "0").toFloat()
    //             if  price > 0{
    //                 // Convert To Int
    //                 let qty:Float = String(product["qty"], "1").toFloat(1)
    //                 let tva:Float =  String(product["tva"]).toFloat()
    //                 let ttc:Float = price*qty
    //                 let mtva:Float = ttc*(tva/100)
    //                 let ht:Float = ttc-mtva
    //                 // Totals
    //                 let tva_str = String(product["tva"]!)
    //                 if tav_group[tva_str] == nil{ tav_group[tva_str] = 0}
    //                 tav_group[tva_str] = tav_group[tva_str]!+mtva
    //                 t_ttc+=ttc
    //                 t_ht+=ht
    //             }
    //         }
    //     }
    //     self.bill["total_ht"] = t_ht as AnyObject
    //     var tvas = Array<Dictionary<String, AnyObject>>()
    //     for (ttva, mtva) in tav_group{
    //         tvas.append(["key" : ttva as AnyObject, "value" : mtva as AnyObject])
    //     }
    //     self.bill["tvas"] = tvas as AnyObject
    //     self.bill["total"] = t_ttc as AnyObject
    //     self.totalsCView.reloadData()
    // }
    // TVA Dropdown
    func tvaDropdown(_ sender: AnyObject) {
        self.tavDropDown[Int(sender.tag!)]!.show()
    }
    //---| IF Index IsSet => Update || Value is Int => Delete || Value is Dictionary => Add
    func productsAction(_ value:AnyObject?=nil, _ index:Int?=nil, _ key:String?="") -> Dictionary<String, Dictionary<String, Any>>{
        var products = (self.bill["products"] as? Dictionary<String, Dictionary<String, Any>>)!
        if value != nil{
            if index != nil{ products[products.key(index!)]?[key!] = value }
            else if value is Int{ products.removeValue(forKey: products.key(value as! Int)) }
            else{ products[app.id()] = value as! Dictionary<String, AnyObject> }
            self.bill["products"] = products
        }
        return products
    }
    func productNameChanging(_ sender:AnyObject){
        self.productsAction((sender as! UITextField).text as AnyObject, sender.tag, "name")
    }
    func productQtyChanging(_ sender:AnyObject){
        self.productsAction((sender as! UITextField).text as AnyObject, sender.tag, "qty")
        self.calculationTvas(&self.bill)
    }
    func productPriceChanging(_ sender:AnyObject){
        self.productsAction((sender as! UITextField).text as AnyObject, sender.tag, "price")
        self.calculationTvas(&self.bill)
    }
    func clean(){
        self.cName.setTitle("Nom de la facture & Nom du client", for: .normal)
        self.bName.text = ""
        self.bRef.text = ""
        self.cCustomer.clean()
    }
    //---|   init
    func billInit(){
        self.datePicker.date = Date()
        datePickerBtn.setTitle(self.datePicker.date.string("dd/MM/yyyy"), for: .normal)
        self.clean()
        self.bill = self.iBill
        self.canEdit()
        self.productAdding()
        self.calculationTvas(&self.bill)
        self.billsTView.reloadData()
    }
    //---|   Click
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerBtn: UIButton!
    @IBAction func datePickerClick(_ sender: Any) {
        self.customerToggle(true)
        if datePickerBtn.currentTitle == "Done"{
            datePickerBtn.setTitle(self.datePicker.date.string("dd/MM/yyyy"), for: .normal)
            self.datePicker.isHidden = true
        }else{
            self.datePicker.isHidden = false
            datePickerBtn.setTitle("Done", for: .normal)
        }
    }
        //---|   Delete
    @IBAction func deleteBill(_ sender: Any) { self.billInit() }
        //---|   Save
    @IBAction func save(_ sender: Any) {
        if self.bill["id"] == nil { self.bill["id"] = app.id() as AnyObject }
        self.transactionModal(String(self.bill["total"]))
    }

    override func transactionSave(_ transaction: inout Dictionary<String, Any>, _ obj:inout Dictionary<String, Any>){
        super.transactionSave(&transaction, &self.bill)
        cCustomer.save { (customer) in
            let id = String(self.bill["id"])
            self.bill["customer"] = customer //as AnyObject
            self.bill["ref"] = String(self.bRef.text) //as AnyObject
            self.bill["name"] = String(self.bName.text, "\(String(self.cFirstName.text)) \(String(self.cLastName.text))") //as AnyObject
            self.bill["date"] = self.datePicker.date.timestamp //as AnyObject
            self.bills[id] = self.bill as [String : AnyObject]
            DB.BILLS.child(id).setValue(self.bill)
            self.billsTView.reloadData()
            //---|   Select Last ROW
            let indexPath = IndexPath(row: self.bills.count-1, section: 0)
            print("indexPath", indexPath)
            self.billsTView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
            self.billsTView.delegate?.tableView!(self.billsTView, didSelectRowAt: indexPath)
        }
    }
    //---|
    func customerToggle(_ close:Bool=false){
        // UIView.animate(withDuration: 0.2, animations: {
            // if !close && self.cHeightConst.constant == 0 {
            //     self.cHeightConst.constant = 420
            //     self.cLoader.isHidden = false
            // }else{
            //     self.cHeightConst.constant = 0
            //     self.cLoader.isHidden = true
            // }
        // })
    }
    @IBAction func customerToggleClick(_ sender: AnyObject) { self.customerToggle() }
    //---|   Name Change

    func customerNameChanged(){
        self.cName.setTitle(!String(self.bName.text).empty() ? String(self.bName.text) : "\(String(self.cFirstName.text)) \(String(self.cLastName.text))", for: .normal)
    }
    func notifCustomerNameChanged(notification: Notification){ self.customerNameChanged() }
    @IBAction func cNameChanging(_ sender: AnyObject) { self.customerNameChanged() }
    //---|   Archived
    func archived(_ pId:String?=nil){

        let id = pId ?? String(self.bill["id"])
        //---|   Archived
        DB.ABILLS.child(id).setValue(self.bill)
        DB.BILLS.child(id).removeValue()
        //---|   Remove From
        self.bills.removeValue(forKey: id)
        billInit()
    }
    @IBAction func archivedClick(_ sender: AnyObject) { self.archived() }
    //---|   Print
    @IBAction func printClick(_ sender: AnyObject) { self.alert("Printing", "PRINT") }
}
