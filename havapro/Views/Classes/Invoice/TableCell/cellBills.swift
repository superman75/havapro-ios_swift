//
//  cellBills.swift
//  havapro
//
//  Created by Exo on 29/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class cellBills: UITableViewCell {
    // Init Vars
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var customer: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var row: UIView!
    //---|   Selected
    func selected(){
        self.color(UIColor.white, Color.active)
    }
    //---|   Init
    func ini() -> UITableViewCell {
        self.color(Color.active, UIColor.clear)
        return self.deSelectRow()
    }
    //---|   Color
    func color(_ label_color: UIColor, _ row_color:UIColor){
        self.row.backgroundColor = row_color
        for label in [self.name] as! [UILabel]{
            label.textColor = label_color
        }
    }
}
