//
//  cellBillProduct.swift
//  havapro
//
//  Created by Exo on 29/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class cellBillProduct: UITableViewCell {
    // Init Vars
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var qty: UITextField!
    @IBOutlet weak var price: UITextField!
    @IBOutlet weak var tva: btnDropdown!
    @IBOutlet weak var tvaView: UIView!
    @IBOutlet weak var v_edit: UIView!
    @IBOutlet weak var c_add: UIView!
    @IBOutlet weak var b_minus: UIButton!
    @IBOutlet weak var b_plus: UIButton!
    // Show
    func new(_ show:Bool=true){
        self.v_edit.isHidden = show
        self.c_add.isHidden = !show
    }


}
