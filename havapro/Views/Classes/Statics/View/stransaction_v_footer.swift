//
//  stransaction_v_footer.swift
//  havapro
//
//  Created by abdelghani on 07/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class stransaction_v_footer: UIView {
    @IBOutlet weak var close: UIButton!
    @IBOutlet weak var print: UIButton!
    @IBOutlet weak var send: UIButton!
    @IBOutlet weak var refund: UIButton!
}
