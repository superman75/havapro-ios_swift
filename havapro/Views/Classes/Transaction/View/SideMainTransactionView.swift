//
//  SideMainTransactionView.swift
//  havapro
//
//  Created by abdelghani on 13/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class SideMainTransactionView: UIView {
    //---|   Outlet
    @IBOutlet weak var vs_sale: UIView!
    @IBOutlet weak var b_sale: UIButton!
    @IBOutlet weak var vs_journal: UIView!
    @IBOutlet weak var b_journal: UIButton!
    @IBOutlet weak var v_search: UIView!
    @IBOutlet weak var t_search: UITextField!
    @IBOutlet weak var v_date: UIView!
    @IBOutlet weak var l_date: UILabel!
    @IBOutlet weak var tv_transaction: UITableView!


    //---|
    init() {
        print("-----\n#-----\nSideMainTransactionView init()\n#-----\n")
        super.init(frame: UIScreen.main.bounds)
        self.ini()
        return
    }

    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    func ini(){
        // UIView.animate(withDuration: 0.2, animations: {
            self.saleEnable()
            self.v_date.isHidden = false
            self.v_search.isHidden = true
            //---|
            self.vs_sale.isHidden = true
            self.vs_journal.isHidden = true
            //---|
            self.vs_sale.backgroundColor = Color.active
            self.vs_journal.backgroundColor = Color.active
            //---|
            self.btn(self.b_sale, Color.gray)
            self.btn(self.b_journal, Color.gray)
        // })
        // print("-----\n#-----\nSideMainTransactionView Ini\n#-----\n")


    }
    func deSale(_ hasData:Bool=true, _ journal:Bool=true){
        // UIView.animate(withDuration: 0.2, animations: {
            // self.v_date.isHidden = false//!hasData//!(!journal && hasData)
            // self.v_search.isHidden = !(journal && hasData)
            //---| Active Bar Btn
            self.vs_sale.isHidden = journal
            self.vs_journal.isHidden = !journal
            //---|
            let colors:[Bool:UIColor] = [true : Color.gray, false : Color.active]
            self.btn(self.b_sale, colors[journal]!)
            self.btn(self.b_journal, colors[!journal]!)
            self.saleEnable()
        // })
    }
    func btn(_ btn:UIButton, _ color:UIColor){
        btn.setTitleColor(color, for: .normal)
    }
    //---|   disa
    func saleEnable() -> Bool{
        let enable = !String(app.profile["session_starting"]).empty()
        self.b_sale.isEnabled = enable
        self.b_sale.alpha = (enable ? 1.0 : 0.3)
        return enable
    }

}
