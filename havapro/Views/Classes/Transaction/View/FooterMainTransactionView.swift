//
//  FooterMainTransactionView.swift
//  havapro
//
//  Created by abdelghani on 13/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class FooterMainTransactionView: UIView {
    //---|   Vars
    //---|   Outlet
    @IBOutlet weak var b_close: UIButton!
    @IBOutlet weak var b_print: UIButton!
    @IBOutlet weak var b_refund: UIButton!
    //---|   Init
    func ini(_ hasOrder:Bool=false){
        // self.isHidden = true
        self.b_close.isHidden = String(app.profile["session_starting"]).empty()
        self.b_refund.isHidden = true
        self.b_print.isHidden = !hasOrder
    }
}
