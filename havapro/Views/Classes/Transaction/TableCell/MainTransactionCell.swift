//
//  MainTransactionCell.swift
//  havapro
//
//  Created by abdelghani on 13/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class MainTransactionCell: UITableViewCell {
    //---|   Outlets
    @IBOutlet weak var index: UILabel!
    @IBOutlet weak var c_index: UIView!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var c_icon: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var row: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //---|   Selected
    func selected(){
        self.color(UIColor.white, Color.active)
    }
    //---|   Init
    func ini() -> UITableViewCell {
        self.color(Color.active, Color.clean)
        return self.deSelectRow()
    }
    //---|   Color
    func color(_ label_color: UIColor, _ row_color:UIColor){
        self.row.backgroundColor = row_color
        for label in [self.total, self.time] as! [UILabel]{
            label.textColor = label_color
        }
    }

}
