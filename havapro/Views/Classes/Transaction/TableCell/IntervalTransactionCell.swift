//
//  IntervalTransactionCell.swift
//  havapro
//
//  Created by abdelghani on 11/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class IntervalTransactionCell: UITableViewCell {

    // Outlet
    @IBOutlet weak var row: UIView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var tva: UILabel!
    @IBOutlet weak var ticket: UILabel!
    @IBOutlet weak var cash: UILabel!
    @IBOutlet weak var cb: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    //---|   Selected
    func selected(){
        self.color(UIColor.white, Color.active)
    }
    //---|   Init
    func ini() -> UITableViewCell {
        self.color(Color.dark, Color.clean)
        return self.deSelectRow()
    }
    //---|   Color
    func color(_ label_color: UIColor, _ row_color:UIColor){
        self.row.backgroundColor = row_color
        for label in [self.date, self.total, self.tva, self.ticket, self.cash, self.cb] as! [UILabel]{
            label.textColor = label_color
        }
    }

}
