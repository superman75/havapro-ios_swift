//
//  FooterSaleView.swift
//  havapro
//
//  Created by HavaPRO on 20/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class FooterSaleView: UIView {
    //---|   OUTLETS
    @IBOutlet weak var pay: UIButton!
    @IBOutlet weak var validate: UIButton!
    @IBOutlet weak var delete: UIButton!
    @IBOutlet weak var archive: UIButton!
    @IBOutlet weak var ticket: UIButton!
    @IBOutlet weak var production: UIButton!
    //---|
    func ini(_ commande:[String:Any]?=nil){
        self.isHidden = true
        if let commande = commande as? [String:Any]{
            self.isHidden = false
            let paid = String(commande["paid"]).bool
            let validate = String(commande["validate"]).bool
            let total = (String(commande["total"], "0").toDouble() > 0)
            let session_opend = String(app.profile["session_starting"]).empty()
            //---|
            self.pay.isHidden = (paid || !total || session_opend)
            self.validate.isHidden = (validate || !total)
            self.production.isHidden = !validate
            self.delete.isHidden = paid
            self.archive.isHidden = !paid
            self.ticket.isHidden = !paid
        }
    }
}
