//
//  TotalsSaleView.swift
//  havapro
//
//  Created by HavaPRO on 23/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class TotalsSaleView: UIView {

    @IBOutlet weak var l_ht: UILabel!
    @IBOutlet weak var l_tva: UILabel!
    @IBOutlet weak var l_ttc: UILabel!
    
    func ini(){
        self.l_ht.text = "00,00 €"
        self.l_tva.text = "00,00 €"
        self.l_ttc.text = "00,00 €"
    }

}
