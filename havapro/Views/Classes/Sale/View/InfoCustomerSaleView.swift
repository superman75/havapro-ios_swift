//
//  InfoCustomerSaleView.swift
//  havapro
//
//  Created by HavaPRO on 20/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class InfoCustomerSaleView: UIView {
    //---|
    let PCONST = (mobile : "+00 00 00 00", name : "Prénom NOM", address: "Adresse")
    //---|   OutLet
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var b_popup: UIButton!
    //---|   Clear
    func ini(){
        self.isHidden = true
        self.address.isHidden = true
        self.b_popup.isEnabled = false
        self.assign(self.mobile, "", PCONST.mobile)
        self.assign(self.name, "", PCONST.name)
        self.assign(self.address, "", PCONST.address)
    }
    //---|   Set Customer
    func define(_ customer:Any?=nil) -> String{
        self.ini()
        self.isHidden = false
        if let customer = customer as? [String:Any]{
            self.assign(self.mobile, String(customer["mobile"]), PCONST.mobile)
            self.assign(self.name, "\(String(customer["first_name"])) \(String(customer["last_name"]))", PCONST.name)
            self.assign(self.address, String(customer["delivery_address"]), PCONST.address)
            self.address.isHidden = (self.address.text == PCONST.address)
            self.b_popup.isEnabled = true
        }
        return self.address.text! //---|   Address Delivery See Sale Controller
    }
    func assign(_ label:UILabel, _ str:String, _ def:String){
        label.textColor = Color.def
        label.text = def
        label.alpha = 0.8
        if !str.trim().empty() {
            label.textColor = Color.active
            label.text = str
            label.alpha = 1
        }
    }
}
