//
//  HeadSaleView.swift
//  havapro
//
//  Created by HavaPRO on 25/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class HeadSaleView: UIView {
    //---|   OUTLETS
    @IBOutlet var b_about: Btn!
    @IBOutlet var b_drawer: Btn!
    @IBOutlet var b_order: Btn!
    @IBOutlet var b_session: Btn!
    //---|   Ini
    func ini(){
        let session_opend = String(app.profile["session_starting"]).empty()
        let printer = (UIViewController().g(app.settings, "peripheral.printer.customer", [String:Any]()) as? [String:Any])!.count.bool
        self.b_order.isHidden = session_opend
        self.b_session.isHidden = !session_opend
        self.b_about.isHidden = !printer
        self.b_drawer.isHidden = !printer
    }
}
