//
//  DatePicker.swift
//  havapro
//
//  Created by abdelghani on 11/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit
// // class DatePickerToggleOptions : NSObject{
// struct DatePickerToggleOptions{
//     var btn:BtnDate
//     var start:BtnDate
//     var end:BtnDate
//     var container:UIView?
//     var loader:UIView?
//     //var changed:(() -> ())? = nil
//     // class func Init(_ date:Date, _ i:Int=1)->DatePickerToggleOptions{
//     //     var ops = DatePickerToggleOptions()
//     //     return ops
//     // }
// }
class DatePicker: UIDatePicker {

    func ini(_ btn_other:BtnDate){
        if String(btn_other.params["is"]!) == "start"{
            self.minimumDate = btn_other.params["date"] as! Date
            self.maximumDate = nil
        }else{
            self.minimumDate = nil
            self.maximumDate = btn_other.params["date"] as! Date
        }

    }

    func toggle(_ btn: BtnDate, _ start: BtnDate, _ end: BtnDate, _ container:UIView?, _ changed:(()->())? = nil){
        let container = container ?? (self as? UIView)!
        let btn_other : BtnDate = ["end" : start, "start" : end][String(btn.params["is"]!)]!
        // UIView.animate(withDuration: 0.2, animations: {
            //---|   Calendar is closed
            if container.isHidden {
                btn.open(self)
                container.isHidden = false
            }else{
                //---|   Close Calendar
                if String(btn.params["changing"]).bool {
                    btn.title(self.date)
                    container.isHidden = true
                }else{
                    btn_other.title(self.date)
                    btn.open(self)
                }
            }
            self.ini(btn_other) //---|   Init
            //---|   is Closed
            if container.isHidden && (String(start.params["changed"]).bool || String(end.params["changed"]).bool){
                if changed != nil{ changed!() }
                start.params.removeValue(forKey: "changed")
                end.params.removeValue(forKey: "changed")
            }
        // })

    }

}
