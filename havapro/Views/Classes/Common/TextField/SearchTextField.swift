//
//  SearchTextField.swift
//  havapro
//
//  Created by abdelghani on 27/08/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import SearchTextField
// Btn
class SearchText: SearchTextField {
    public func darkTheme(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x:0.0, y:self.frame.height - 1, width:self.frame.width, height:1.0)
        bottomLine.backgroundColor = UIColor("#eeeeee").cgColor
        self.borderStyle = UITextBorderStyle.none
        self.layer.addSublayer(bottomLine)
        self.theme.bgColor = UIColor("#2c3e50", alpha: 0.8)
        self.theme.borderColor = UIColor("#000000", alpha: 0.8)
        self.theme.separatorColor =  UIColor("#000000", alpha: 0.8)
        self.maxResultsListHeight = 400
        self.startVisible = true
        //self.theme = SearchTextFieldTheme.darkTheme()
        self.highlightAttributes = [NSBackgroundColorAttributeName: UIColor("#27ae60", alpha: 0.2), NSFontAttributeName:UIFont.boldSystemFont(ofSize: 12)]
    }

    override func layoutSubviews() {
        super.layoutSubviews()

    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.darkTheme()
    }
}
