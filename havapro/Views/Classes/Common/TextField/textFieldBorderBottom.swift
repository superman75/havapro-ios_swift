//
//  textFieldBorderBottom.swift
//  havapro
//
//  Created by Exo on 20/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
// class BorderBottomView: UIView{
//     //---|
//     public func border_bottom(_ color:UIColor=Color.def, _ height:CGFloat=1.0){
//         var bottomLine = CALayer()
//         bottomLine.frame = CGRect(x:0.0, y:self.height - height, width:self.width, height:height)
//         bottomLine.backgroundColor = color.cgColor
//         self.layer.addSublayer(bottomLine)
//     }
// }
// Btn
class textFieldBorderBottom: UITextField {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.border_bottom()
    }
    public func border_bottom(_ color:UIColor=Color.def, _ height:CGFloat=1.0){
        var bottomLine = CALayer()
        bottomLine.frame = CGRect(x:0.0, y:self.height - height, width:self.width, height:height)
        bottomLine.backgroundColor = color.cgColor
        self.layer.addSublayer(bottomLine)
        self.borderStyle = UITextBorderStyle.none
        self.layer.masksToBounds = true
    }
}
// Btn
class textFieldBorderBottomWhite: textFieldBorderBottom {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.border_bottom(UIColor.white, 2.0)
    }
}
class buttonBorderBottom: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.border_bottom()
    }
    //---|
    public func border_bottom(_ color:UIColor=Color.def, _ height:CGFloat=1.0){
        var bottomLine = CALayer()
        bottomLine.frame = CGRect(x:0.0, y:self.height - height, width:self.width, height:height)
        bottomLine.backgroundColor = color.cgColor
        self.layer.addSublayer(bottomLine)
    }

}
