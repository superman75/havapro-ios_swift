//
//  Btn.swift
//  havapro
//
//  Created by abdelghani on 10/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    //---|   BTN
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
class Btn: UIButton {
    var params:[String:Any] = [String:Any]()
    override func layoutSubviews() {
        super.layoutSubviews()
        self.style()
    }
    func style(_ bg:UIColor=Color.active){
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 8
        self.bg(bg)
        self.setTitleColor(UIColor.white, for: UIControlState.normal)
    }
    func bg(_ bg:UIColor=Color.active){
        self.layer.borderColor = bg.adjust(3).cgColor
        self.backgroundColor = bg
    }
}
//---|   RED
class BtnRed: Btn {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.bg(Color.red)
    }
}
//---|   Gray
class BtnGray: Btn {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.bg(Color.gray)
    }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    //---|   BTN ICONS
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
class BtnIcon: Btn {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
}
//---|   Def
class BtnIconDefault: BtnIcon {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.bg(Color.def.adjust(12))
    }
}
//---|   Gray
class BtnIconGray: BtnIcon {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.bg(Color.gray)
    }
}
