//
//  BtnClear.swift
//  havapro
//
//  Created by HavaPRO on 20/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class BtnClear: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func layoutSubviews() {
        super.layoutSubviews()
        self.style()
    }
    func style(_ color:UIColor=UIColor.white){
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = 4
        self.backgroundColor = UIColor.clear
        self.setTitleColor(color, for: UIControlState.normal)
    }
}
