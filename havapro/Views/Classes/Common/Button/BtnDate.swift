//
//  BtnDate.swift
//  havapro
//
//  Created by abdelghani on 11/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class BtnDate: UIButton {
    // Vars
    var params:[String: Any] = [:]
    //---|   Need To Set Is For Start Or End
    func ini(_ setting:[String: Any])-> BtnDate{
        self.params.merge(setting)
        return self
    }
    func title(_ date:Date, _ format:String="dd/MM/yyyy"){
        self.setTitle(date.string(format), for: .normal)
        self.setTitleColor(UIColor("#2C82C9"), for: UIControlState.normal)
        //---| Date is Changed
        if let date_old = self.params["date"] as? Date, date_old.timestamp != date.timestamp {
            self.params["changed"] = true
        }
        self.params["date"] = date
        //---|
        self.params.removeValue(forKey: "changing")
    }
    //---|   open
    func open(_ DatePicker:UIDatePicker){
        self.setTitle("Términer", for: .normal)
        DatePicker.date = (self.params["date"] as? Date)!
        self.setTitleColor(UIColor("#2CC990"), for: UIControlState.normal)
        self.params["changing"] = true
    }

}
