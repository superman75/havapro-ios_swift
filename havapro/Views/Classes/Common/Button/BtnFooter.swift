//
//  BtnFooter.swift
//  havapro
//
//  Created by HavaPRO on 28/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    //---|   BTN SIMPLE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
class BtnFooter: UIButton {

    var params:[String:Any] = [String:Any]()
    func style(_ color:UIColor=Color.def, _ txt:UIColor=UIColor.white){
        self.setTitleColor(txt, for: UIControlState.normal)
        self.backgroundColor = color
    }
}

//---|   RED
class BtnFooterRed: BtnFooter {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.style(Color.red)
    }
}
//---|   GREEN
class BtnFooterGreen: BtnFooter {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.style(Color.green)
    }
}
//---|
class BtnFooterDef: BtnFooter {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.style(Color.def)
    }
}
//---| DEF
class BtnFooterBlue: BtnFooter {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.style(Color.blue)
    }
}
//---| YELLOW
class BtnFooterYellow: BtnFooter {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.style(Color.yellow)
    }
}
