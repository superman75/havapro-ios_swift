//
//  SideAppView.swift
//  havapro
//
//  Created by HavaPRO on 04/02/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit
import Spring

class SideAppView: UIView {
    var navigation = [
        (name : "Vente", controller:"Sale", count : 0, icon : "m_sale.png"),
        (name : "Réservation", controller:"Agenda", count : 0, icon : "m_agenda.png"),
        (name : "Facturation", controller:"Bill", count : 0, icon : "m_invoice.png"),
        (name : "Statistique", controller:"Statistical", count : 0, icon : "m_statics.png"),
        (name : "Assistance", controller:"Support", count : 0, icon : "m_support.png"),
        (name : "Communication", controller:"Communication", count : 0, icon : "m_communication.png"),
        (name : "Configuration", controller:"ViewConfig", count : 0, icon : "m_setting.png"),
        (name : "Déconnection", controller:"logout", count : 0, icon : "m_logout.png")
        // (name : "Contact", controller:"Contact", count : 0, icon : "m_contact.png"),
    ]
    //--| Outlet
    @IBOutlet weak var i_menu: SpringImageView!
        //--| Side
    @IBOutlet weak var v_side: UIView!
    @IBOutlet weak var v_side_const_width: NSLayoutConstraint!
    @IBOutlet weak var v_side_container: SpringView!
    @IBOutlet weak var v_logo_side: UIView!
        //--| Menu Active
    @IBOutlet weak var v_menu_container: UIView!
    @IBOutlet weak var menu_open_btn: UIButton!
    @IBOutlet weak var menu_const_height_custom: NSLayoutConstraint!
    @IBOutlet weak var menu_const_height_auto: NSLayoutConstraint!
    @IBOutlet weak var tv_navigation: UITableView!

    //---|   toggleMenu
    func side_toggle(){// Tag 1 Hide
        UIView.animate(withDuration: 0.2) {
            if self.v_side.tag == 0{
                self.v_side_const_width.constant = 40
                self.v_logo_side.isHidden = true
                self.v_side.tag = 1
                self.i_menu.duration = 1.5
                self.i_menu.rotate = 55
                self.i_menu.animateTo()
            }else{
                self.v_side_const_width.constant = 250
                self.v_logo_side.isHidden = false
                self.v_side.tag = 0
                self.i_menu.rotate = 0
                self.i_menu.animateTo()
            }

        }
        self.tv_navigation.reloadData()
    }

    //---|
    func menu_toggle(_ close_menu:Bool?=nil){
        UIView.animate(withDuration: 0.5) {
            if (close_menu == nil && !self.menu_open_btn.isHidden) || (close_menu != nil && close_menu!) {
                self.menu_open_btn.isHidden = !(self.v_side_container.tag > 0)
                // print("-----\n#-----\nClosing Menu\n-----\n\(close_menu)\n------\n#-----\n")
                self.self.menu_const_height_auto.constant = -(self.v_menu_container.frame.size.height-60.0)
                self.tv_navigation.backgroundColor = UIColor("#eeeeee")
                //---|   Center Menu
                //self.tv_navigation.selectRow(self, self.currentController, false)
            }else if (close_menu == nil) || (!close_menu!) {
                self.menu_open_btn.isHidden = true
                // print("-----\n#-----\nClosing Container\n-----\n\(close_menu)\n------\n#-----\n")
                self.menu_const_height_auto.constant = CGFloat(-(self.v_side_container.tag))
                self.tv_navigation.backgroundColor = UIColor.clear
            }
        }
    }

}
