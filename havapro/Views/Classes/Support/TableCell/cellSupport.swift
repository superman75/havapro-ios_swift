//
//  cellSupport.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class cellSupport: UITableViewCell {
    // Init Vars
    @IBOutlet weak var avatar: avatar!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!

}
