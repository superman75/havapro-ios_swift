//
//  StartCommunicationView.swift
//  havapro
//
//  Created by HavaPRO on 17/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class StartCommunicationView: UIView {
    //
    var name:String = ""
    var networks = [
        "facebook" : (color : "#3b5999", logo: "facebook_w.png", favicon : nil),
        "google" : (color : "#dd4b39", logo: "google_w.png", favicon : "i_adword.png"),
        // "havatar" : (color : "#34465d", logo: "logo.png", favicon : nil),
        "instagram" : (color : "#e4405f", logo: "instagram_w.png", favicon : "i_instagram_w.png"),
        "youtube" : (color : "#cd201f", logo: "youtube_w.png", favicon : "i_youtube_w.png"),
    ]
    // OUTLETS
    @IBOutlet weak var s_start: UIStackView!
    @IBOutlet weak var v_form: UIView!
    @IBOutlet weak var i_favicon: UIImageView!
    @IBOutlet weak var l_logo: UIImageView!
    @IBOutlet weak var a_loader: UIActivityIndicatorView!
    @IBOutlet weak var l_msg: UILabel!
    @IBOutlet weak var b_action: UIButton!
    @IBOutlet weak var b_toggle: UIButton!
    @IBOutlet weak var b_connect: UIButton!
        //
    @IBOutlet weak var v_networks: UIView!
    @IBOutlet weak var v_connect: UIView!
    //---|   Load
    func load(_ name:String){
        self.backgroundColor = UIColor(self.networks[name]!.color)
        self.l_logo.image = UIImage(named: self.networks[name]!.logo)
        //---|   Has Favicon
        if self.networks[name]!.favicon != nil { self.i_favicon.image = UIImage(named: self.networks[name]!.favicon!) }
        self.name = name
        self.ini(true)
    }
    // Init
    func ini(_ loader:Bool=false){
        // UIView.animate(withDuration: 0.5) {
            // self.i_favicon.isHidden = (self.networks[self.name]!.favicon == nil)
            self.i_favicon.isHidden = false
            self.l_msg.isHidden = true
            self.b_action.isHidden = true
            self.b_toggle.isHidden = loader
            self.v_form.isHidden = true
            self.v_networks.isHidden = true
            self.b_connect.isHidden = true
            self.a_loader.isHidden = !loader
        // }
    }
    func header(){
        // UIView.animate(withDuration: 0.5) {
            self.i_favicon.isHidden = true
            self.b_action.isHidden = true
            self.l_msg.isHidden = true
            self.a_loader.isHidden = true
            self.v_form.isHidden = false
        // }
    }
    func action(_ msg:String="", _ action:Bool=false, _ b_consulting:Bool=true){
        self.ini()
        self.b_toggle.isHidden = true
        self.l_msg.isHidden = false
        self.l_msg.text = msg
        self.b_action.isHidden = !(action && b_consulting)
        self.b_connect.isHidden = !(action && !b_consulting)
    }
    //---|
    func loading(_ show:Bool=true){
        self.ini()
        self.b_toggle.isHidden = !show
        self.a_loader.isHidden = !show
    }
    //---|   network
    func network(){
        self.ini()
        self.b_toggle.isHidden = true
        self.v_networks.isHidden = false
    }
    //
    func toggle(_ close:Bool?=nil, _ action:Bool=false){
        // UIView.animate(withDuration: 1) {
            //---|   Hide Form
            if (close == nil && !self.v_form.isHidden) || (close != nil && close!) { self.ini() }
            else if (close == nil) || (!close!){ self.header() }
        // }
    }
}
