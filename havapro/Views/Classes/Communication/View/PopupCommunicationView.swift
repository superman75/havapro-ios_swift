//
//  PopupCommunicationView.swift
//  havapro
//
//  Created by HavaPRO on 18/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class PopupCommunicationView: UIView {
    //---|   VARS
    var name:String = ""
    // Outlet
    @IBOutlet weak var l_title: UILabel!
    @IBOutlet weak var v_proposal: UIView!
    @IBOutlet weak var tv_proposal: UITableView!
    @IBOutlet weak var v_refused: UIView!
    @IBOutlet weak var t_reason: UITextView!

    //---|   Init
    func ini(_ name:String){
        self.name = name.uppercased()
    }

    func proposal(_ show:Bool=true){
        self.v_proposal.isHidden = !show
        self.v_refused.isHidden = show
        self.l_title.text = (show ? "PROPOSITION" : self.name)
    }
}
