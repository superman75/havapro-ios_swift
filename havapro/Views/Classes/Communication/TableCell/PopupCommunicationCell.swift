//
//  PopupCommunicationCell.swift
//  havapro
//
//  Created by HavaPRO on 18/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class PopupCommunicationCell: UITableViewCell {
    // Outlet
    @IBOutlet weak var key: UILabel!
    @IBOutlet weak var value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
