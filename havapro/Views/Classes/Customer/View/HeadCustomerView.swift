//
//  HeadCustomerView.swift
//  havapro
//
//  Created by HavaPRO on 01/02/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class HeadCustomerView: UIView {

    //---|   OUTLETS
    @IBOutlet weak var v_prev: UIView!
    @IBOutlet weak var v_save: UIView!
    @IBOutlet weak var v_delete: UIView!
    @IBOutlet weak var v_title: UIView!
    @IBOutlet weak var v_add: UIView!
    //---|
    func ini(_ list:Bool=true){
        self.v_prev.isHidden = list
        self.v_save.isHidden = list
        self.v_delete.isHidden = list
        self.v_title.isHidden = !list
        self.v_add.isHidden = !list
    }
}
