//
//  CustomerView.swift
//  havapro
//
//  Created by HavaPRO on 01/02/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class CustomerView: UIView {
    //---|   OUTLETS
    @IBOutlet weak var v_head: HeadCustomerView!
    @IBOutlet weak var v_edit: EditCustomerView!
    @IBOutlet weak var tv_list: UITableView!
    //---|
    func ini(_ list:Bool=true){
        self.v_head.ini(list)
        self.tv_list.isHidden = !list
        self.v_edit.isHidden = list
    }

}
