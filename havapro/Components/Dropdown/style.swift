//
//  style.swift
//  havapro
//
//  Created by Exo on 15/10/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
class btnDropdown: UIButton{
    var params = [String:Any]()
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 0
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor("#eeeeee").cgColor
    }
    func ini(_ data:[String]) -> NSArray{
        params["data"] = data
        self.tag = -1
        return data as NSArray
    }
    func title(_ index:Int, _ data:[String]? = nil){
        var data = data ?? (params["data"] as? [String])!
        self.setTitle(String(data[index]), for: .normal)
        self.tag = index
    }
}
