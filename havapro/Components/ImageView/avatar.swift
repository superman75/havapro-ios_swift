//
//  avatar.swift
//  havapro
//
//  Created by Exo on 23/09/2016.
//  Copyright © 2016 Exo. All rights reserved.
//
import UIKit
// Btn
class avatar: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        // self.layer.borderWidth = 5
        // self.layer.cornerRadius = self.frame.size.width / 2
        // self.layer.borderColor = UIColor("#000000").CGColor
        self.layer.borderWidth = 2
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor("#eeeeee").cgColor
        self.layer.cornerRadius = max(self.frame.size.width, self.frame.size.height) / 2
        self.clipsToBounds = true

    }

}
