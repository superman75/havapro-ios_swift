//
//  TransactionController.swift
//  havapro
//
//  Created by Exo on 25/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import DropDown


protocol TransactionChangedDelegate {
    func transactionDismissed(_ sendor:TransactionController, onPaymentChanged transaction: inout Dictionary<String, Any>,_ action:String)
}

class TransactionController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // Var
    var delegate: TransactionChangedDelegate?
    var transaction:Dictionary<String, Any> = ["total": 0, "actions" : [] , "rendered" : 0]
    var types = (values : ["Éspace", "Carte Bleu", "RESTAURANT TICKET"], keys : ["cash", "cb", "ticket"])
    var dissmised = false
    let paymentDDown = DropDown()
    var paymentList:[[String:Any]] = [[String:Any]]()

    // Outlit
    @IBOutlet weak var paymentTView: UITableView!
    @IBOutlet weak var priceTF: UITextField!
    @IBOutlet var paymentDropDownView: UIView!
    @IBOutlet var paymentBtnDropDown: btnDropdown!
    @IBOutlet var payBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAnimate()
        // Table View
        paymentTView.dataSource = self
        paymentTView.delegate = self
        // Init Dropdonw Aavatar Physically Payment
        self.dropdownInit(self.paymentDDown, view: self.paymentDropDownView, datasource: paymentBtnDropDown.ini(types.values), btnShowing: self.paymentBtnDropDown, self.dropPayment)
        paymentBtnDropDown.title(0)
        self.calculation()
        priceTF.becomeFirstResponder()
        //---|   Init Total
        self.transaction["itotal"] = String(self.transaction["total"]) as AnyObject
    }
    @IBAction func payClick(_ sender: Any) {
        let price = String(self.priceTF.text, "0").toDouble()
        if price > 0 {
            var actions:Array<Dictionary<String, Any>> = (transaction["actions"] as? Array<Dictionary<String, Any>>)!
            let rendered = String(transaction["rendered"], "0").toDouble()
            var action:[String : Any] = ["type" : String(types.keys[paymentBtnDropDown.tag]), "price" : price]
            if rendered > 0{
                action["paid"] = price
                action["price"] = price-rendered
            }
            actions.append(action)
            transaction["actions"] = actions as AnyObject
            if rendered >= 0{
                if delegate != nil{ delegate!.transactionDismissed(self, onPaymentChanged: &self.transaction, "save") }
                self.close()
            }
            else{ paymentDDown.show() }
        }
        self.priceTF.text = ""
        self.calculation()
    }
    @IBAction func priceEditing(_ sender: AnyObject) {
        self.calculation()
    }

    @IBAction func cancelClick(_ sender: AnyObject) {
        if delegate != nil{ delegate!.transactionDismissed(self, onPaymentChanged: &self.transaction, "cancel") }
        self.close()
    }
    override func viewWillDisappear(_ animated: Bool){
        paymentDDown.hide()
        if delegate != nil, !self.dissmised{ delegate!.transactionDismissed(self, onPaymentChanged: &self.transaction, "dissmis") }
    }

    //---|   Private
    func calculation(){

        paymentList = [[String:Any]]()

        let ntotal = Double(String(transaction["total"]!))!
        paymentList.append([
            "_key" : "total",
            "key" : "Montant à payer ",
            "value" : ntotal,
        ])

        var paid:Double = 0
        for action in (transaction["actions"] as? Array<Dictionary<String, AnyObject>>)!{
            paymentList.append([
                "_key" : "action",
                "key" : " - \(app.paymenttype[String(action["type"])]!)",
                "value" : String(action["price"]),
            ])
            paid += String(action["price"], "0").toDouble()
        }
        if self.priceTF.text != nil, !(self.priceTF.text!.isEmpty){
            paid += String(self.priceTF.text, "0").toDouble()
            self.payBtn.isEnabled = true
        }else{
            self.payBtn.isEnabled = false
        }


        let rendered:Double = paid-ntotal
        transaction["rendered"] = rendered
        if -1*ntotal != rendered{
            transaction["rendered"] = rendered
            let lacking = (rendered < 0)
            paymentList.append([
                "lacking" : lacking,
                "_key" : "rendered",
                "key" : (lacking ? "A payer" : "Rendu"),
                "value" : rendered,
            ])
        }
        self.paymentTView.reloadData()
    }
    //---|   dismiss
    func close(){
        removeAnimate()
        self.dissmised = true
        self.dismiss(animated: true)
    }

    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }

    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }

    func dropPayment(index: Int, item: String){
        paymentBtnDropDown.title(index)
        priceTF.becomeFirstResponder()
    }
    // Toggle Modal Order Now
    @IBAction func paymentDropDownView(_ sender: AnyObject) { paymentDDown.show() }

    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        // tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
        return paymentList.count
        // return 0
    }
    // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // paymentTView
        if tableView == self.paymentTView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellPayment", for: indexPath) as! cellPayment
            let item = self.paymentList[indexPath.row]
            cell.key.text = String(item["key"])
            cell.value.text = String(item["value"]).format()
            cell.value.textColor = String(item["lacking"]).bool ? Color.red : Color.dark
            if String(item["_key"]) == "action"{
                cell.leftConst.constant = 30
                cell.value.font = cell.value.font.withSize(14)
                cell.key.font = cell.value.font
                cell.value.textColor = Color.gray
            }
            return cell.deSelectRow()
        }
        return tableView.cellForRow(at: indexPath)!
    }
    // Active Editing
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if [0, self.paymentList.count-1].contains(indexPath.row)  { return false }
        return true
    }
    // Deleting
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.paymentList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.paymentTView.reloadData()
            self.calculation()
        }
    }
}
