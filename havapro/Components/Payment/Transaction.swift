//
//  Transaction.swift
//  havapro
//
//  Created by Exo on 25/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//
import Firebase

//open
class Transaction : AppViewController, UIPopoverPresentationControllerDelegate, TransactionChangedDelegate{
    //---|   VARS
    var tModel:FIRDatabaseReference? =  nil
    //---|   Calculate TVAs
    public func calculationTvas(_ obj: inout Dictionary<String, Any>, _ sProducts:NSDictionary? = nil, _ callback : (() -> Void)? = nil){
        //var t_ttva:Float = 0
        //var t_mtva:Float = 0
        var t_ht:Float = 0
        var t_ttc:Float = 0
        var tav_group = Dictionary<String, Float>()
        if let products = obj["products"] as? Dictionary<String, Dictionary<String, Any>>{
            var sProducts = (sProducts != nil ? (sProducts as? Dictionary<String, Dictionary<String, Any>>)! : products)
            for (idProduct, product) in products{
                // TVA
                if !String(sProducts[idProduct]?["price"]).empty(){
                    let price = String(sProducts[idProduct]!["price"], "0").toFloat()
                    if  price > 0{
                        // Convert To Int
                        let tva_str = String(sProducts[idProduct]!["tva"])
                        let qty:Float = String(product["qty"], "1").toFloat(1)
                        let tva:Float =  tva_str.toFloat()
                        let ttc:Float = price*qty
                        let mtva:Float = ttc*(tva/100)
                        let ht:Float = ttc-mtva
                        // Totals
                        if tav_group[tva_str] == nil{ tav_group[tva_str] = 0}
                        tav_group[tva_str] = tav_group[tva_str]!+mtva
                        t_ttc+=ttc
                        t_ht+=ht
                    }
                }
            }
        }

        obj["total_ht"] = t_ht
        var tvas = Array<Dictionary<String, Any>>()
        for (ttva, mtva) in tav_group{
            tvas.append(["key" : ttva, "value" : mtva])
        }
        obj["tvas"] = tvas
        obj["total"] = t_ttc
        if callback != nil { callback!() }
    }
    //---|
    public func transactionModal(_ total:String="0") {
        let transactionModal = UIStoryboard.init(name: "Transaction", bundle: nil).instantiateViewController(withIdentifier: "TransactionController") as! TransactionController
        transactionModal.transaction["total"] = total as AnyObject
        transactionModal.delegate = self
        transactionModal.didMove(toParentViewController: self)

        var nc: UINavigationController? = nil
        nc = UINavigationController(rootViewController: transactionModal)
        nc!.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = nc!.popoverPresentationController
        popover!.delegate = self

        popover!.sourceView = self.view
        popover!.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)

        popover!.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)

        self.present(nc!, animated: true)
    }
    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
    func transactionDismissed(_ sendor:TransactionController, onPaymentChanged transaction: inout Dictionary<String, Any>, _ action:String) {
        if action == "save" {
            //---| Transaction
            var transaction = transaction
            //---|   Rendered
            let rendered = String(transaction["rendered"], "0").toDouble()
            if rendered > 0 {
                self.timeout({ () -> Void in
                    self.alert("Payment", "Rendu : \(String(rendered).format())")
                })
            }
            var tochange = Dictionary<String, Any>()
            self.transactionSave(&transaction, &tochange)
        }
    }
    public func transactionSave(_ transaction: inout Dictionary<String, Any>, _ obj:inout Dictionary<String, Any>){
        //---|   VARS
        let transaction_id = String(obj["id"])
        //---|   TODO : ReCalcule TVA
        self.calculationTvas(&obj)
        //---|   More Detail Set
        transaction["created"] = [".sv":"timestamp"]
        transaction["id"] = transaction_id
        transaction["view"] = String(self.classForCoder).lowercased().replacingOccurrences(of: "controller", with: "")
        transaction["total_ht"] = String(obj["total_ht"])
        //---|   TVAs
        if let tvas = obj["tvas"] as? Array<Dictionary<String, AnyObject>>{
            transaction["tvas"] = tvas
            var ctva:Float = 0
            for tva in tvas{
                ctva += String(tva["value"]).toFloat()
            }
            transaction["tva"] = ctva
        }
        //---|   Save Statistique
        DB.TRANSACTIONS.child(transaction_id).setValue(transaction)
        //---|   Paid & Transaction
        obj["paid"] = true
        obj["transaction"] = transaction
        //---|   Save Database
        if self.tModel != nil {
            self.tModel!.child("\(transaction_id)/paid").setValue(true)
            self.tModel!.child("\(transaction_id)/transaction").setValue(transaction)
            //---|   Save TVA
            if let tvas = obj["tvas"] as? Array<Dictionary<String, AnyObject>>{
                self.tModel!.child("\(transaction_id)/tvas").setValue(tvas)
                self.tModel!.child("\(transaction_id)/total_ht").setValue(String(obj["total_ht"]))
            }
        }
    }
}
