//
//  popupContentViewController.swift
//  HavaPRO
//
//  Created by abdelghani on 01/01/2018.
//  Copyright © 2018 Havatar. All rights reserved.
//

import UIKit
import PopupController
import Spring

class popupContentViewController: UIViewController, PopupContentViewController {
    //
    var popSize:CGSize = CGSize(width: 300,height: 300)
    var popup = PopupController()
    var popData:[String: Any] = [String: Any]()

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // Change orintation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        UIView.animate(withDuration: 1, animations: {
            self.view.alpha = 0
        })
        coordinator.animate(alongsideTransition: { (_) in
            self.resize()
        }) { (_) in
            self.resize()
            UIView.animate(withDuration: 2, animations: {
                self.view.alpha = 1
            })
        }
    }
    //---|
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize { return self.popSize }
    //---| Resize PopUp
    func resize(_ sSize: CGSize?=nil){
        if sSize != nil{ self.popSize = sSize! }
        self.popup.viewDidLayoutSubviews()
    }

}
