//
//  modal.swift
//  havapro
//
//  Created by Exo on 03/10/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
class modal: UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor("#eeeeee").cgColor
        self.layer.shadowColor = UIColor("#000000", alpha :0.4).cgColor
        self.layer.shadowOpacity = 1
        //self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 4
        // self.isHidden = true
    }
}
