//
//  modalComponent.swift
//  havapro
//
//  Created by Exo on 13/01/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
class modalComponent: UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 2
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor("#000000", alpha :0.8).cgColor
        self.backgroundColor = UIColor("#000000", alpha :0.8)
    }
}
