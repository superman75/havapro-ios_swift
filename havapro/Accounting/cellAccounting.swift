//
//  cellAccounting.swift
//  havapro
//
//  Created by abdelghani on 31/08/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import Foundation
class cellAccounting : UITableViewCell{
    //---|   Outlet
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var tva: UILabel!
    @IBOutlet weak var total: UILabel!
}
