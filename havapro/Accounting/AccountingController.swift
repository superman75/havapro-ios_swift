//
//  AccountingController.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class AccountingController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //---|   VARS
    var accountings:Dictionary<String, Dictionary<String, AnyObject>> =  Dictionary<String, Dictionary<String, AnyObject>>()
    // Outlet
    @IBOutlet weak var accountingsTView: UITableView!
    //---|
    var staticsCtrl:StatisticalController?=nil
    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        //---| StatisticalController
        self.staticsCtrl = (self.parent as? StatisticalController)!
        //---|   NOTIFICATION
        NotificationCenter.default.addObserver(forName: BROADCAST.CONNECTED, object: nil, queue: nil, using: self.connected)
        // sourcing table View
        self.accountingsTView.dataSource = self
        self.accountingsTView.delegate = self
    }
    //---|   Loaded
    func connected(notification: Notification?=nil){
        //---|   Accountings Get
        DB.EACCOUNTINGS.queryOrdered(byChild: "uid").queryEqual(toValue: app.uid).observe(.value, with: { (snap) in
            self.accountings = Dictionary<String, Dictionary<String, AnyObject>>()
            if snap.exists() {
                self.accountings = (snap.value as? Dictionary<String, Dictionary<String, AnyObject>>)!
            }
            self.accountingsTView.reloadData()
        })
    }
    //---|   TableView
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        return self.accountings.count
    }
    // Item Set
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{

        let accountingCell = tableView.dequeueReusableCell(withIdentifier: "cellAccounting", for: indexPath) as! cellAccounting
        let accounting = self.accountings[self.accountings.key(indexPath.row)]!
        accountingCell.name.text = String(accounting["name"])
        accountingCell.date.text = String(accounting["date"])
        accountingCell.tva.text = String(accounting["tva"])
        accountingCell.total.text = String(accounting["total"])
        return accountingCell.deSelectRow()//---|   DeSelect
    }
}
