//
//  CommunicationItemController.swift
//  havapro
//
//  Created by Exo on 25/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import FBSDKLoginKit
import DropDown

class CommunicationItemController: NetworksController {

    //---|   VARS
    var communication:[String : Any] = [String : Any]()
    var name:String = ""
    var networks = [String: [String:Any]]()

    //---|   Outlet
    @IBOutlet var v_start: StartCommunicationView!
    @IBOutlet weak var price: textFieldBorderBottom!
    @IBOutlet weak var content: KMPlaceholderTextView!
    @IBOutlet weak var btn: UIButton!
        //---|   Choose Page
    @IBOutlet weak var VChoose: UIView!
    @IBOutlet weak var BChoose: UIButton!
    @IBOutlet weak var BRassociated: UIButton!
    @IBOutlet weak var vCPages: UIView!
    @IBOutlet weak var bCPages: btnDropdown!
    let dropDownPages = DropDown()

    weak var delegate: CommunicationController!
    var CommunicationCtrl:CommunicationController?
    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        CommunicationCtrl = (parent as? CommunicationController)!
    }
    override func g_login_callback(_ re_google_youtube:[String:[String:[String:Any]]]) {
        super.g_login_callback(re_google_youtube)
        self.network_logged(re_google_youtube[name]!)
    }
    //---|   Inited
    func inited(){
        //---|   Load Start View
        self.v_start.load(name)
    }
    //---|   Connected
    func connected(notification: Notification? = nil){
        //---|   Set Networks
        DB.NETWORKS.child(name).observe(.value, with: { (snap) in
            self.networks = [String: [String:Any]]()
            if snap.exists() { self.networks = (snap.value as? [String: [String:Any]])! }
            self.changing(self.communication)
        })
    }
    public func changing(_ communication:[String : Any]?=nil){
        // self.loaderShow()
        self.v_start.ini()
        if communication != nil, !String(communication!["name"]).empty() {
            self.communication = communication!
            if self.communication["proposals"] != nil {
                if String(self.g(self.communication, "accepted")).bool {
                    self.v_start.loading()
                    //---|   Has Account
                    if !String(self.communication["account"]).empty() {
                        self.c_finished()
                        return
                    }
                    self.page_ini()
                    //---|   Has Page
                    if self.networks.count > 0 {
                        self.dropdownInit(self.dropDownPages, view: self.vCPages, datasource: self.networks.olist() as NSArray, btnShowing: self.bCPages, self.pageChoose)
                        self.v_start.network()
                        return
                    }
                    self.v_start.action("VEUILLEZ CONNECTER AVEC VOTRE COMPTE", true, false)
                    return
                }
                //---|   REFUSED
                if String(self.g(self.communication, "refused")).bool {
                    self.v_start.action("VOUS AVEZ REFUSÉ LA DEMANDE", true)
                } //---|   Has Proposition
                else{ self.v_start.action("VOUS AVEZ RECU UNE PROPOSITION", true) }
            }else{
                self.v_start.action("DEMANDE EN COUR DE TRAITEMENT")
            }
        }
    }
    //---|   Pop Dissmissed
    func c_popdismissed(_ changed:[String:Any]){
        DB.COMMUNICATIONS.child("\(String(self.communication["id"]))/updated").setValue([".sv" : "timestamp"])
        for (k, v) in changed{
            self.communication[k] = v
            DB.COMMUNICATIONS.child("\(String(self.communication["id"]))/\(k)").setValue(v)
        }
        self.changing(self.communication)
    }
    func c_finished( _ account:String?=nil){
        let communication_id = String(self.communication["id"])
        //---|   Set Only Id
        if !String(account).empty() { self.communication["account"] = account! }
        //---|   Move to Accounting
        DB.PACCOUNTINGS.child(communication_id).setValue(self.communication)
        //---|   Remove From Communication
        DB.COMMUNICATIONS.child(communication_id).removeValue()
        //---|   Init
        self.communication = [String : Any]()
        self.v_start.ini()
        parent?.alert(name.uppercased(), "Votre demande est prise en compte")
    }
    //--| Page Init
    func page_ini() {
        self.BChoose.isEnabled = false
        self.bCPages.setTitle("Selectionner Profile/Page", for: .normal)
        self.bCPages.tag = -1
    }
    // Commande
    func pageChoose(index: Int, item: String){
        self.bCPages.setTitle(item, for: .normal)
        self.bCPages.tag = index
        self.BChoose.isEnabled = true
    }
    //---|   After Network Login
    func network_logged(_ re:[String:[String:Any]]){
        self.networks.merge(re)
        self.changing(self.communication)
        self.dropDownPages.show()
    }
    //-----------------------------------------------#
       //--| BUTTON Actions
    //-----------------------------------------------#
    @IBAction func typing(_ sender: AnyObject) {
        self.btn.isEnabled = false
        if !String(price.text).empty() {
            self.btn.isEnabled = true
        }
        // print("Typing", self.btn.isEnabled)
    }
    //---|   save
    @IBAction func save(_ sender: AnyObject) {
        let id = app.id()
        self.communication = ["id" : id ,"uid": app.uid, "name": name, "price": price.text ,"msg":content.text, "created" :[".sv" : "timestamp"], "updated" :[".sv" : "timestamp"] ]
        DB.COMMUNICATIONS.child(id).setValue(self.communication)
        price.text = ""
        content.text = ""
        self.changing(self.communication)
        parent?.alert(name.uppercased(), "Votre demande d'etude à été enregistrer avec success \n Le service marketing reviendra vers vous prochainement")
    }
    //---|   associatedWith
    @IBAction func associatedWith(_ sender: AnyObject) {
        if name == "facebook" { self.fb_login(self.network_logged) }
        if name == "instagram" { self.i_login(self.network_logged) }
        if (name == "google") || (name == "youtube") { self.g_login() }
    }
    //---|   Open proposal
    @IBAction func proposalOpen(_ sender: AnyObject) {
        (CommunicationCtrl?.popupCommunicationCtrl)!.itemCommunicationCtrl = self
        CommunicationCtrl?.popup((CommunicationCtrl?.popupCommunicationCtrl)!)
    }
    //---|   Open Form
    @IBAction func formOpen(_ sender: AnyObject) { self.v_start.toggle() }
        //---| Google
    //---|   choosePageShow
    @IBAction func choosePageShow(_ sender: AnyObject) { dropDownPages.show() }
    //---|   finish
    @IBAction func finish(_ sender: AnyObject) {
        self.c_finished(String(self.networks.olist("id")[self.bCPages.tag]))
    }
}
