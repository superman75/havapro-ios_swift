//
//  cellCommunicationItem.swift
//  havapro
//
//  Created by Exo on 25/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class cellCommunicationItem: UITableViewCell {
    //---|   Outlet
    @IBOutlet weak var key: UILabel!
    @IBOutlet weak var value: UILabel!
}
