//
//  CommunicationController.swift
//  havapro
//
//  Created by Exo on 25/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import Spring

class CommunicationController: AppViewController {

    //---|   VARS
    var communications:[String:[String:Any]] = [String:[String:Any]]()
    var items:Dictionary<String, CommunicationItemController> = Dictionary<String, CommunicationItemController>()
    var popupCommunicationCtrl:popupCommunicationController?=nil

    @IBOutlet weak var googleView: UIView!
    @IBOutlet weak var instagramView: UIView!
    @IBOutlet weak var havatarView: UIView!
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var youtubeView: UIView!
    @IBOutlet weak var menuCView: UIView!

    //---|   OUTLETS
    @IBOutlet weak var google: UIView!
    @IBOutlet weak var facebook: UIView!
    @IBOutlet weak var havatar: UIView!
    @IBOutlet weak var instagram: UIView!
    @IBOutlet weak var youtube: UIView!

    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        popupCommunicationCtrl = UIStoryboard(name: "Communication", bundle: nil).instantiateViewController(withIdentifier: "popupCommunicationController") as! popupCommunicationController
        self.including("google", google)
        self.including("facebook", facebook)
        //self.including("havatar", havatar)
        self.including("instagram", instagram)
        self.including("youtube", youtube)
    }
    //---|
    override func connected(notification: Notification?=nil){
        DB.COMMUNICATIONS.queryOrdered(byChild: "uid").queryEqual(toValue: app.uid).observe(.value, with: { (snap) in
            self.communications =  [String:[String:Any]]()
            if snap.exists() { self.communications = (snap.value as? [String:[String:Any]])! }
            //---|   Changing
            for name in ["google", "facebook", "instagram", "youtube"]{
                self.items[name]?.changing(self.communications.search(name, "name"))
            }
        })
        for name in ["facebook", "google", "youtube", "instagram"]{ self.items[name]?.connected() }
    }
    public func including(_ name:String, _ container:UIView){
        // items[name] = self.include("CommunicationItemController", container, "Communication") as! CommunicationItemController
        items[name] = storyboard?.instantiateViewController(withIdentifier: "CommunicationItemController") as! CommunicationItemController
        self.addChildViewController(items[name]!)
        items[name]?.view.frame = CGRect(x:0, y:0, width:container.frame.size.width, height:container.frame.size.height);
        container.addSubview((items[name]?.view)!)
        items[name]?.didMove(toParentViewController: self)
        items[name]?.name = name
        items[name]?.inited()
    }
    // Save
    func save(_ name:String,_ price:UITextField, _ msg:UITextView){
        let data:Dictionary<String, Any> = ["uid": app.uid, "name": name, "price":price.text ,"msg":msg.text]
        DB.COMMUNICATIONS.childByAutoId().setValue(data)
        price.text = ""
        msg.text = ""
        self.alert(name, "Votre demande a été enregistrée avec succès")
    }
    // Google
    @IBOutlet weak var priceGoogle: UITextField!
    @IBOutlet weak var msgGoogle: UITextView!
    @IBAction func saveGoogle(_ sender: Any) { self.save("google", priceGoogle, msgGoogle) }
    // Facebook
    @IBOutlet weak var priceFacebook: UITextField!
    @IBOutlet weak var msgFacebook: UITextView!
    @IBAction func saveFacebook(_ sender: Any) { self.save("facebook", priceFacebook, msgFacebook) }
    // Havatar
    @IBOutlet weak var msgHavatar: UITextView!
    @IBOutlet weak var priceHavatar: UITextField!
    @IBAction func saveHavatar(_ sender: Any) { self.save("havatar", priceHavatar, msgHavatar) }
    // Instagram
    @IBOutlet weak var priceInstagram: UITextField!
    @IBOutlet weak var msgInstagram: UITextView!
    @IBAction func saveInstagram(_ sender: Any) { self.save("instagram", priceInstagram, msgInstagram) }
    // Youtube
    @IBOutlet weak var msgYoutube: UITextView!
    @IBOutlet weak var priceYoutube: UITextField!
    @IBAction func saveYoutube(_ sender: Any) { self.save("youtube", priceYoutube, msgYoutube) }

}
