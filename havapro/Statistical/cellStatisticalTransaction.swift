//
//  cellStatisticalTransaction.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class cellStatisticalTransaction: UITableViewCell {
    // Init Vars
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var by_visa: UILabel!
    @IBOutlet weak var cash: UILabel!
    @IBOutlet weak var r_ticket: UILabel!
    @IBOutlet weak var total: UILabel!
}
