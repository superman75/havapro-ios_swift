//
//  StatisticalTransactionController.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import Graphs



class StatisticalTransactionController: AppViewController, UITableViewDataSource, UITableViewDelegate {

    // Var
    var sDateOpenFor:String = "start"
    let sDateFormat:String = "dd/MM/yyyy HH:mm"
    var sQuery:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    var sQueryTView:Dictionary<String, Dictionary<String, Float>> = Dictionary<String, Dictionary<String, Float>>()
    let sActions:Dictionary<Int, String> = [0:"table.png", 1:"pie-chart.png", 2:"bars-chart.png"]
    let sPayment:Dictionary<String, String> = ["BY VISA":"by_visa", "CASH":"cash", "RESTAURANT TICKET":"r_ticket"]
    let sPaymentInit:Dictionary<String, Float> = ["by_visa" : 0, "cash" : 0, "r_ticket" : 0, "total" : 0]
    var sGraph:Array<Float> = Array<Float>()

    // Outlet
    @IBOutlet weak var sDatePicker: UIDatePicker!
    @IBOutlet weak var sDatePickerView: UIView!
    @IBOutlet weak var sStartDate: UIButton!
    @IBOutlet weak var sEndDate: UIButton!
    @IBOutlet weak var sActionSwitch: UIButton!
    @IBOutlet weak var sSwitcherView: UIView!
    @IBOutlet weak var sSwitcherSView: UIStackView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var sView0: UIView!
    @IBOutlet weak var sView1: UIView!
    @IBOutlet weak var sView2: UIView!
    @IBOutlet weak var sTableView: UITableView!
    @IBOutlet weak var v_turnover: v_turnover!

    var statisticalCtrl:StatisticalController?=nil
    var staticsCtrl:StaticsTransactionController?=nil
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        print("-----\n#-----\n StatisticalTransactionController viewDidLoad \n-----\n\(app.uid)\n-----\n#-----\n")
        //---| StatisticalController
        statisticalCtrl = (self.parent as? StatisticalController)!
        timeout({
            self.staticsCtrl = self.appController!.include("StaticsTransactionController", self.appController!.container, "Statistical", nil) as! StaticsTransactionController
        })
        // Init
        sTableView.dataSource = self
        sTableView.delegate = self
        sDatePicker.date = Date().set(00, .hour).set(00, .minute).set(00, .second)
        sStartDate.setTitle(sDatePicker.date.string(), for: .normal)
        sEndDate.setTitle(Date().string(), for: .normal)
        sSwitcherView.translatesAutoresizingMaskIntoConstraints = false
        sSwitcherSView.translatesAutoresizingMaskIntoConstraints = false
        self.sSwitched(0)
        self.pieChart()
        self.barChart()
        // //---|   Connection | Get Provider Local Information
        // self.connection(){ Void in
        //     self.sGetData()
        // }
    }
    override func showed(){
        super.showed()
        print("-----\n#-----\nStatisticalTransactionController Showed \n-----\n\(app.uid)\n-----\n#-----\n")
    }
    //---|   Connection | Get Provider Local Informati
    override func connected(notification: Notification?=nil){
        self.sGetData()
        print("-----\n#-----\n StatisticalTransactionController connected \n-----\n\(app.uid)\n-----\n#-----\n")
    }
    //---|   Statistical
    private func openDatePicker(_ openBtn : UIButton, _ closeBtn : UIButton, _ opened:String ){
        if self.sDatePickerView.isHidden || sDateOpenFor == opened{
            self.sDatePicker.date = Date((openBtn.titleLabel?.text)!, self.sDateFormat)
            sDateOpenFor = ["end" : "start", "start" : "end"][opened]!
            closeBtn.titleLabel?.textColor = UIColor.white
            openBtn.titleLabel?.textColor = UIColor.red
            self.view.layoutIfNeeded()
            self.sDatePickerView.isHidden = false
        }else{
            self.sGetData()
        }
    }
    @IBAction func sStartDateToggle(_ sender: AnyObject) { self.openDatePicker(sStartDate, sEndDate, "end") }
    @IBAction func sEndDateToggle(_ sender: AnyObject) { self.openDatePicker(sEndDate, sStartDate, "start") }
    @IBAction func sActionSwitchToggle(_ sender: AnyObject) {
        if !self.sDatePickerView.isHidden { self.sGetData() }
        self.sSwitcherView.isHidden = !self.sSwitcherView.isHidden
    }
    private func sSwitched(_ index:Int){
        if let image = UIImage(named: self.sActions[index]!) { self.sActionSwitch.setImage(image, for: .normal) }
        print("#----\n#--| sGraph --: \(sGraph) |--#\n")
        UIView.animate(withDuration: 0.2, animations: {
            for (i, _) in self.sActions{
                if index == i{
                    self.sSwitcherSView.arrangedSubviews[index].isHidden = true
                }else{
                    self.sSwitcherSView.arrangedSubviews[i].isHidden = false
                }
            }
            if index == 0{
                self.sView1.isHidden = true
                self.sView2.isHidden = true
                self.sView0.isHidden = false
            }else if index == 1{
                self.sView0.isHidden = true
                self.sView2.isHidden = true
                self.sView1.isHidden = false
            }else if index == 2{
                self.sView0.isHidden = true
                self.sView1.isHidden = true
                self.sView2.isHidden = false
            }
            self.view.layoutIfNeeded()
        })
        self.sSwitcherView.isHidden = true

    }
    @IBAction func switch0Click(_ sender: AnyObject) { self.sSwitched(0) }
    @IBAction func siwtch1Click(_ sender: AnyObject) { self.sSwitched(1) }
    @IBAction func switch2Click(_ sender: AnyObject) { self.sSwitched(2) }
    @IBAction func sDateChanging(_ sender: AnyObject) {
        if sDateOpenFor == "start"{
            self.sStartDate.setTitle(self.sDatePicker.date.string(), for: .normal)
            self.sDatePicker.minimumDate = self.sDatePicker.date.add(-1, .year)
            self.sDatePicker.maximumDate = Date((sEndDate.titleLabel?.text)!, self.sDateFormat)
        }else{
            self.sEndDate.setTitle(self.sDatePicker.date.string(), for: .normal)
            self.sDatePicker.minimumDate = Date((sStartDate.titleLabel?.text)!, self.sDateFormat)
            let yearPlus = self.sDatePicker.date.add(1, .year)
            self.sDatePicker.maximumDate = (Date().diff(yearPlus, .minute)>1 ? yearPlus : Date())
        }
    }
    func sGetData(_ callback : ((_ data:Dictionary<String, AnyObject>) -> Void)? = nil){
        self.sDatePickerView.isHidden = true
        sEndDate.titleLabel?.textColor = UIColor.white
        sStartDate.titleLabel?.textColor = UIColor.white
        let start = Date((sStartDate.titleLabel?.text)!, self.sDateFormat).timestamp.toInt64()
        let end = Date((sEndDate.titleLabel?.text)!, self.sDateFormat).timestamp.toInt64()
        print("Start", start, end)
        self.sloading()
        DB.TRANSACTIONS.queryOrdered(byChild: "created").queryStarting(atValue: start).queryEnding(atValue: end).observe(.value, with: {
            (snap) in
            self.sQuery = Dictionary<String, AnyObject>()
            self.timeout(self.sloadoff, 1000)
            if snap.exists() {
                self.sQuery = (snap.value as? Dictionary<String, AnyObject>)!
            }
            print("#----\n#--| snap --: \(snap.value) |--#\n")
            if callback != nil { callback!(self.sQuery) }
            self.tableViewQuery()
            // self.sloadoff()
        })
    }
    // func TRANSACTIONS(_ start:Int64, _ end:Int64, _ callback : ((_ data:[String:Any]?) -> Void)? = nil){
    //     DB.TRANSACTIONS.queryOrdered(byChild: "created").queryStarting(atValue: start).queryEnding(atValue: end).observe(.value, with: {
    //         (snap) in
    //         if callback {
    //             callback!(snap.value)
    //         }
    //         self.timeout(self.sloadoff, 1000)
    //         if snap.exists() {
    //             self.sQuery = (snap.value as? Dictionary<String, AnyObject>)!
    //         }
    //         print("#----\n#--| snap --: \(snap.value) |--#\n")
    //         if callback != nil { callback!(self.sQuery) }
    //         self.tableViewQuery()
    //         // self.sloadoff()
    //     })
    // }
    private func sloading(){
        self.loaderView.isHidden = false
        self.sStartDate.isEnabled = false
        self.sStartDate.alpha = 0.5
        self.sEndDate.isEnabled = false
        self.sEndDate.alpha = 0.5
    }
    private func sloadoff(){
        self.loaderView.isHidden = true
        self.sStartDate.isEnabled = true
        self.sStartDate.alpha = 1
        self.sEndDate.isEnabled = true
        self.sEndDate.alpha = 1
    }
    private func tableViewQuery(){
        sQueryTView = Dictionary<String, Dictionary<String, Float>>()
        var total = self.sPaymentInit
        for(id, transaction) in (self.sQuery as? Dictionary<String, AnyObject>)!{
            let date = Date(String(transaction["created"]!), "timestamp").string("dd/MM/yyyy")
            if sQueryTView[date] == nil{ sQueryTView[date] = self.sPaymentInit }
            var atransaction:Array<Dictionary<String, AnyObject>> = (transaction["actions"] as? Array<Dictionary<String, AnyObject>>)!
            //---|   Minus rendered
            if transaction["rendered"] != nil {
                let rendered = Float(String(transaction["rendered"]!, "0"))!
                if rendered>0 {
                    atransaction[atransaction.count-1]["price"] = ((Float(String(atransaction[atransaction.count - 1]["price"], "0"))! - rendered) as? AnyObject)!
                }
            }

            for action in atransaction{
                let type = self.sPayment[String(action["type"])]!
                sQueryTView[date]![type]! += Float(String(action["price"], "0"))!
                sQueryTView[date]!["total"]! += Float(String(action["price"], "0"))!
                total[type]! += Float(String(action["price"], "0"))!
                total["total"]! += Float(String(action["price"], "0"))!
                //
            }
        }
        //---|   sGraph
        sGraph = Array<Float>()
        for (id, query) in sQueryTView{
            sGraph.append(Float(String(query["total"], "0"))!)
        }
        if sQueryTView.count>1 { sQueryTView["TOTAL"] = total }
        self.pieChart()
        self.barChart()
        print("#----\n#--| sGraph --: \(sGraph) |--#\n")
        sTableView.reloadData()
    }
    //---|   TableView
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        return self.sQueryTView.count
    }
    // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let sTableView = tableView.dequeueReusableCell(withIdentifier: "cellStatisticalTransaction", for: indexPath) as! cellStatisticalTransaction
        let date = sQueryTView.key(indexPath.row)
        let item:Dictionary<String, Float> = sQueryTView[date]!
        sTableView.date.text = date
        sTableView.by_visa.text = String(item["by_visa"]).format()
        sTableView.cash.text = String(item["cash"]).format()
        sTableView.r_ticket.text = String(item["r_ticket"]).format()
        sTableView.total.text = String(item["total"]).format()
        return sTableView
    }
    private func barChart(){
        let frame = CGRect(x:0, y:0, width:sView2.frame.size.width, height:sView2.frame.size.height)
        let barChart = sGraph.barGraph(GraphRange(min: 0, max: (sGraph.count > 0 ? sGraph.max() : 10)!)).view(CGRect(x:0, y:0, width:sView2.frame.size.width, height:sView2.frame.size.height))
        barChart.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.sView2.addSubview(barChart)
    }
    private func pieChart(){
        print("#----\n#--| sGraph --: \(sGraph) |--#\n")
        let frame = CGRect(x:0, y:0, width:sView1.frame.size.width, height:sView1.frame.size.height)
        let pieChart = sGraph.pieGraph(){ (u, t) -> String? in String(format: "%.0f%%", (Float(u.value) / Float(t)))}.view(frame).pieGraphConfiguration({ PieGraphViewConfig(textFont: UIFont(name: "DINCondensed-Bold", size: 14.0), isDounut: true, contentInsets: UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)) })
        pieChart.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.sView1.addSubview(pieChart)
    }
    @IBAction func staticsOpen(_ sender: AnyObject) {
        print("-----\n#-----\nStaticsOpen\n-----\n#-----\n")
        // self.appController!.include(StaticsTransactionController(), (self.appController?.container)!)

        //self.appController!.include("StaticsTransactionController", (self.appController?.container)!, "Statistical", [String:Any](), self.staticsCtrl) as! StaticsTransactionController
        // self.staticsCtrl!.StaticsCtrl = self
    }
}
