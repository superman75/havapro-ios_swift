//
//  StatisticalController.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit
import Spring

class StatisticalController: AppViewController {

    // Outlet
    @IBOutlet weak var transactionCView: SpringView!
    @IBOutlet weak var customerCView: SpringView!
    @IBOutlet weak var investmentCView: SpringView!
    @IBOutlet weak var accountingCView: SpringView!

    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
        print("-----\n#-----\n StatisticalController viewDidLoad \n-----\n\(app.uid)\n-----\n#-----\n")
        self.include("TrunoverTransactionController", self.transactionCView, "Statistical")
        self.include("CustomerController", self.customerCView, "Statistical")
        self.include("InvestmentController", self.investmentCView, "Statistical")
        self.include("AccountingController", self.accountingCView, "Statistical")
    }
    override func showed(){
        super.showed()
        print("-----\n#-----\nStatisticalController Showed \n-----\n\(app.uid)\n-----\n#-----\n")
    }
}
