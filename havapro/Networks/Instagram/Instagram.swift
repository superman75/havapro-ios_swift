//
//  Instagram.swift
//  havapro
//
//  Created by abdelghani on 14/11/2017.
//  Copyright © 2017 Exo. All rights reserved.
// https://github.com/AnderGoig/SwiftInstagram



// You can get the list of users a user follows using this call:

// https://api.instagram.com/v1/users/{user-id}/follows?access_token=ACCESS-TOKEN

// You can get the list of users that are following a user with this call:

// https://api.instagram.com/v1/users/{user-id}/followed-by?access_token=ACCESS-TOKEN

// Where {user-id} is the ID of the user you want to check.

import Alamofire
class Instagram: UIViewController{
    //---|   Login
    func Instagram_login(_ callback:@escaping ((_ result:Dictionary<String, Any>) -> Void), _ scopes:Array<String>=["manage_pages", "publish_actions"], _ needed:String="manage_pages"){
        // app url that will handle callback, registered under URLTypes in Info.plist
        let callbackUrl  = "InstagramOauth://"
        // server url for Instagram callback
        let redirectUrl = "http://localhost:5000/callback"
        // Instagram client id - this must be created in dev portal and should be kept secret for real app
        let instragmClientId = "50fb632484524f6eb662364bf006355f"
        // Instagram auth URL, including client id (should be kept secret in real app) and redirect uri to call
        let authURL = "https://api.instagram.com/oauth/authorize/?client_id=" + instragmClientId + "&redirect_uri=" + redirectUrl + "&response_type=code"
        Alamofire.request("https://api.instagram.com/oauth/authorize/?client_id=" + instragmClientId + "&redirect_uri=" + redirectUrl + "&response_type=code")
        // //Initialize auth session
        // self.authSession = SFAuthenticationSession(url: URL(string: authURL)!, callbackURLScheme: callbackUrl, completionHandler: { (callBack:URL?, error:Error? ) in
        //     guard error == nil, let successURL = callBack else {
        //         print(error!)
        //         self.loginStatus.text = "Error logging in with Instagram"
        //         return
        //     }
        //     // get token from query string
        //     let token = self.getQueryStringParameter(url: (successURL.absoluteString), param: "token")
        //     // use token fetch username from Instagram and set in UI
        //     self.setInstagramUsername(token: token!)
        // })
        self.loginStatus.text = "Starting SFAuthenticationSession..."
        self.authSession?.start()
    }
}
