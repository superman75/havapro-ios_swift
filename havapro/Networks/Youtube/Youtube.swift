//
//  Youtube.swift
//  havapro
//
//  Created by abdelghani on 14/11/2017.
//  Copyright © 2017 Exo. All rights reserved.
//



// Use it like this:

// https://www.googleapis.com/youtube/v3/channels?part=statistics&id=channel_id&key=your_key

// You can try your API request here: https://developers.google.com/youtube/v3/docs/channels/list#try-it

import FBSDKLoginKit
class Youtube: UIViewController{
    //---|   Login
    func fb_login(_ callback:@escaping ((_ result:Dictionary<String, Any>) -> Void), _ scopes:Array<String>=["manage_pages", "publish_actions"], _ needed:String="manage_pages"){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withPublishPermissions: scopes, from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains(needed))
                    {
                        self.fb_request(callback, "me", ["fields": "id, name, first_name, last_name, picture.type(large), email, accounts, groups"])
                        // self.getFBUserData(callback)
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    // //---|  getFBUserData
    // func getFBUserData(_ callback:@escaping ((_ result:Dictionary<String, Any>?, _ token:String?) -> Void)){
    //     if((FBSDKAccessToken.current()) != nil){
    //         let token = FBSDKAccessToken.current().tokenString!
    //         FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, accounts, groups"]).start(completionHandler: { (connection, result, error) -> Void in
    //             if (error == nil){
    //                 callback(["result" : result, "connection" : connection], token)
    //                 print("#----\n#--| token --: \(token) |--#\n")
    //                 // //self.dict = result as! [String : AnyObject]
    //                 // print("#----\n#--| result --: \(result) |--#\n")
    //                 // print("#----\n#--| connection --: \(connection) |--#\n")
    //                 // //print(self.dict)
    //             }
    //         })
    //     }
    // }
    //---|   REQUEST
    func fb_request(_ success:@escaping ((_ result:Dictionary<String, Any>) -> Void), _ path:String="me", _ params:Dictionary<AnyHashable, Any>, _ token:String?=nil, _ errorback: ((_ result:Dictionary<String, Any>?) -> Void)?=nil){
        if token == nil, FBSDKAccessToken.current() != nil{ var token = FBSDKAccessToken.current().tokenString! }
        FBSDKGraphRequest(graphPath: path, parameters: params, tokenString: token, version: nil, httpMethod: nil).start { (connection, result, error) in
            if (error == nil){
                success(["result" : result, "connection" : connection, "token" : token])
            }else if errorback != nil{
                errorback!(["error" : error, "result" : result, "connection" : connection, "token" : token])
            }
        }
    }
    //---|   Count Like : https://developers.Youtube.com/docs/graph-api/reference/page/
    //fields=fan_count,likes,country_page_likes,new_like_count,engagement
    // fb_request((), "681909428544338", ["fields" : "fan_count"], String(self.networks[page_id]["access_token"]))
}
