//
//  cellNavigation.swift
//  havapro
//
//  Created by Exo on 02/10/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit

class cellNavigation: UITableViewCell {
    // Init Vars
    @IBOutlet var name: UILabel!
    @IBOutlet var icon: UIImageView!
}
