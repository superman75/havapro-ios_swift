//
//  NavigationController.swift
//
//  Created by Karbal Mehdi on 12/06/17.
//  Copyright © 2017 Karbal Mehdi. All rights reserved.
//

import UIKit
class NavigationController: UITableViewController {
    //---|   Vars
    var navigation_old = [
        (name : "Vente", controller:"Sale"),
        (name : "Réservation", controller:"Agenda"),
        (name : "Facturation", controller:"Bill"),
        (name : "Statistique", controller:"Statistical"),
        // (name : "Contact", controller:"Contact"),
        (name : "Assistance", controller:"Support"),
        (name : "Communication", controller:"Communication"),
        (name : "Configuration", controller:"ViewConfig"),
        (name : "Déconnection", controller:"logout")
    ]
    //Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.connection(){ Void in
        //     NotificationCenter.default.post(name: BROADCAST.CONNECTED, object: nil)
        //     if app.profile["reservation"] == nil {
        //         self.navigation_old.remove(at: 2)
        //     }
        //     if app.profile["caisse"] == nil{
        //         self.navigation_old.remove(at: 0)
        //     }
        // }
        //print("super.self-----")
        //dump(super.self)
        // Get the Navigation Controller
        // let navControl = self.navigationController
        // print("parent----\(self.parent)")

        // dump(self.parent)
        // print("navControl-----\(navControl)")
        // dump(navControl)

        // print("navControl!.viewControllers-----\(navControl!.viewControllers)")
        // dump(navControl!.viewControllers)
        // print("navigationController-----\(self.navigationController)")
        // dump(self.navigationController)
        for (index, nav) in navigation_old.enumerated(){
        }
    }
    //---|   TableView
        //---|   Count
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        return navigation_old.count
    }
        //---|
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNavigation", for: indexPath) as! cellNavigation
        cell.name.text = self.navigation_old[indexPath.row].name
        cell.name.textColor = UIColor("#ecf0f1")
        if String(self.parent!.classForCoder) == "\(self.navigation_old[indexPath.row].controller)Controller" {
            cell.name.textColor = UIColor("#18FFFF")
        }
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        return cell
    }
    //---|   Select
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNavigation", for: indexPath) as! cellNavigation
        cell.name.textColor = UIColor("#18FFFF")
        cell.name.text = self.navigation_old[indexPath.row].name
        if self.navigation_old[indexPath.row].controller == "logout" {
            self.logout()
        }else if String(self.parent!.classForCoder) != "\(self.navigation_old[indexPath.row].controller)Controller" {
            cell.name.textColor = UIColor("#f44336")
            self.performSegue(withIdentifier: self.navigation_old[indexPath.row].controller, sender:self)
        }
        cell.backgroundColor = UIColor.clear
    }
}
