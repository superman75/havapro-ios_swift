//
//  CustomerController.swift
//  havapro
//
//  Created by Exo on 23/04/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit



class CustomerController: AppViewController, UITableViewDataSource, UITableViewDelegate {

    // Var
    var customers:Dictionary<String, Dictionary<String, AnyObject>> = Dictionary<String, Dictionary<String, AnyObject>>()
    var customer:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    // Outlet
    @IBOutlet weak var listView: UIView!
        //---|  Edit
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var editAvatar: UIImageView!
    @IBOutlet weak var editName: UIButton!
    @IBOutlet weak var editNameSView: UIStackView!
    @IBOutlet weak var editFirstName: UITextField!
    @IBOutlet weak var editLastName: UITextField!
    @IBOutlet weak var editMobileSView: UIStackView!
    @IBOutlet weak var editMobile: UIButton!
    @IBOutlet weak var editMobileTF: UITextField!
    @IBOutlet weak var editPhoneSView: UIStackView!
    @IBOutlet weak var editPhoneTF: UITextField!
    @IBOutlet weak var editPhone: UIButton!
    @IBOutlet weak var editEmailSView: UIStackView!
    @IBOutlet weak var editEmailTF: UITextField!
    @IBOutlet weak var editEmail: UIButton!
    @IBOutlet weak var editAddressSView: UIStackView!
    @IBOutlet weak var editAddressTF: UITextField!
    @IBOutlet weak var editAddress: UIButton!

    var staticsCtrl:StatisticalController?=nil
    @IBOutlet weak var v_customer: CustomerView!

    //---|   Load
    override func viewDidLoad() {
        super.viewDidLoad()
        //---| StatisticalController
        self.staticsCtrl = (self.parent as? StatisticalController)!
        // Init
        self.v_customer.ini()
        self.v_customer.tv_list.binding(self)
        // //---|   NOTIFICATION
        // NotificationCenter.default.addObserver(forName: BROADCAST.CONNECTED, object: nil, queue: nil, using: self.connected)
        // self.connection(){ Void in
        //     self.loaded()
        // }
    }
    //---|   Connection | Get Provider Local Informati
    override func connected(notification: Notification?=nil){
        self.loaded()
    }
    func loaded(_ callback : (() -> Void)? = nil){
        //---|   Customer get
        DB.CUSTOMERS.observe(.value, with: { (snap) in
            self.customers = Dictionary<String, Dictionary<String, AnyObject>>()
            if snap.exists() {
                for (id, customer) in (snap.value as? Dictionary<String, Dictionary<String, AnyObject>>)!{
                    if String(customer["uid"]!) == app.uid || String(customer["public"]).bool {
                        self.customers[id] = customer
                    }
                }
            }
            self.self.v_customer.tv_list.reloadData()
        })
    }

    func initSection(_ sView:UIStackView, _ text:UITextField, _ btn:UIButton, _ field:String){
        sView.isHidden = true
        text.isHidden = true
        btn.isHidden = false
        btn.isEnabled = false
        if !String(customer[field]).empty(){
            sView.isHidden = false
        }
        if hasPermission(){
            sView.isHidden = false
            btn.isEnabled = true
            if String(customer[field]).empty() {
                btn.isHidden = true
                text.isHidden = false
            }
        }
        text.text = String(customer[field])
        btn.setTitle(String(customer[field]), for: .normal)
    }
    func hasPermission() -> Bool{ return (String(customer["uid"]!) == app.uid) }
    func editing(){
        //---|   Init
        self.v_customer.ini(false)
        self.initSection(editNameSView, editLastName, editName, "name")
        self.initSection(editMobileSView, editMobileTF, editMobile, "mobile")
        self.initSection(editPhoneSView, editPhoneTF, editPhone, "phone")
        self.initSection(editEmailSView, editEmailTF, editEmail, "email")
        self.initSection(editAddressSView, editAddressTF, editAddress, "address")
        // saveBtn.isEnabled = false
        // deleteBtn.isHidden = true
        editNameSView.isHidden = true
        editFirstName.isHidden = true
        editFirstName.text = String(self.customer["first_name"])
        editLastName.isHidden = true
        editLastName.text = String(self.customer["last_name"])
        editName.isHidden = false
        if self.hasPermission() {
            deleteBtn.isHidden = false
            saveBtn.isEnabled = true
            //---|   Show Edit
            if String(customer["first_name"]).empty() || String(customer["last_name"]).empty() {
                editNameSView.isHidden = false
                editFirstName.isHidden = false
                editLastName.isHidden = false
                editName.isHidden = true
            }
        }
        //---|
        // UIView.animate(withDuration: 0.5, animations: {
        //     self.listView.isHidden = true
        //     self.editView.isHidden = false
        //     self.view.layoutIfNeeded()
        // })
    }
    func back_a(){
        UIView.animate(withDuration: 1.0, animations: {
            self.editView.isHidden = true
            self.listView.isHidden = false
            self.view.layoutIfNeeded()
        })
    }
    //---|   open
    func openField(_ btn:UIButton, _ field:UITextField, _ sView:UIStackView?=nil, _ focus:Bool=true){
        // if String(self.customer["uid"]) == app.uid {
            btn.isHidden = true
            field.isHidden = false
            if sView != nil { sView!.isHidden = false }
            if focus { field.becomeFirstResponder() }
        // }
    }
    func blurField(_ btn:UIButton, _ text:UITextField, _ field:String){
        if !String(self.customer[field]).empty() {
            btn.isHidden = false
            text.isHidden = true
        }
        btn.setTitle(String(text.text, "..."), for: .normal)
        self.customer[field] = String(text.text) as AnyObject
    }
    //---|   Editing
    @IBAction func addClick(_ sender: AnyObject) {

    }
    //---|   Back
    @IBAction func backClick(_ sender: AnyObject) {
        self.v_customer.ini()
    }
    //---|   Delete
    @IBAction func deleteClick(_ sender: AnyObject) {

    }
    //---|   OPENING
    @IBAction func nameOpen(_ sender: AnyObject) {
        self.openField(editName, editFirstName, editNameSView)
    }
    @IBAction func mobileOpen(_ sender: AnyObject) {
        self.openField(editMobile, editMobileTF, editMobileSView)
    }
    @IBAction func phoneOpen(_ sender: AnyObject) {
        self.openField(editPhone, editPhoneTF, editPhoneSView)
    }
    @IBAction func emailOpen(_ sender: AnyObject) {
        self.openField(editEmail, editEmailTF, editEmailSView)
    }
    @IBAction func addressOpen(_ sender: AnyObject) {
        self.openField(editAddress, editAddressTF, editAddressSView)
    }


    //---|   TYPING
    @IBAction func firstNameLeave(_ sender: AnyObject) {
        let strFirst:String = String(editFirstName.text)
        self.customer["name"] = "\(strFirst) \(String(editLastName.text))" as AnyObject
        self.customer["first_name"] = strFirst as AnyObject
        self.editName.setTitle(String(self.customer["name"]), for: .normal)
    }
    @IBAction func lastNameLeave(_ sender: AnyObject) {
        self.blurField(editName, editLastName, "name")
        let strLast:String = String(editLastName.text)
        self.customer["name"] = "\(String(editFirstName.text)) \(strLast)" as AnyObject
        self.customer["last_name"] = strLast as AnyObject
        editLastName.isHidden = false
        editName.isHidden = true
        self.editName.setTitle(String(self.customer["name"]), for: .normal)
        if !String(editFirstName.text).empty() && !String(editLastName.text).empty() {
            editName.isHidden = false
            editNameSView.isHidden = true
        }
    }
    @IBAction func mobileLeave(_ sender: AnyObject) {
        self.blurField(editMobile, editMobileTF, "mobile")
    }
    @IBAction func phoneLeave(_ sender: AnyObject) {
        self.blurField(editPhone, editPhoneTF, "phone")
    }
    @IBAction func emailLeave(_ sender: AnyObject) {
        self.blurField(editEmail, editEmailTF, "email")
    }
    @IBAction func addressLeave(_ sender: AnyObject) {
        self.blurField(editAddress, editAddressTF, "address")
    }
    @IBAction func save(_ sender: AnyObject) {

    }
    //---|   ADD
    @IBAction func add_click(_ sender: AnyObject) {
        if !String(self.customer["draft"]).bool { self.customer = ["uid" : app.uid as AnyObject, "draft" : true as AnyObject] }
        self.editing()
    }
    //---|   BACK
    @IBAction func back_click(_ sender: AnyObject) {
        self.v_customer.ini()
    }
    //---|   DELETE
    @IBAction func delete_click(_ sender: AnyObject) {
        if let id = self.customer["id"] as? String {
            DB.CUSTOMERS.child(id).removeValue()
            self.self.v_customer.tv_list.reloadData()
        }
        self.customer = Dictionary<String, AnyObject>()
        self.v_customer.ini()
    }
    //---|   SAVE
    @IBAction func save_click(_ sender: AnyObject) {
        if self.customer["id"] == nil { self.customer["id"] = app.id() as AnyObject }
        self.customer.removeValue(forKey: "draft")
        let id:String = String(self.customer["id"])
        self.customers[id] = self.customer
        DB.CUSTOMERS.child(id).setValue(self.customer)
        self.self.v_customer.tv_list.reloadData()
        self.v_customer.ini()
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
        //---|   TableView
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        return self.customers.count
    }
    // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellCustomer", for: indexPath) as? cellCustomer{
            let customerItem = self.customers[self.customers.key(indexPath.row)]!
            cell.name.text = String(customerItem["name"], "---")
            cell.phone.text = String(customerItem["mobile"], String(customerItem["phone"], "..."))
            cell.option.isHidden = true
            // if String(customerItem["uid"]) == app.uid { cell.option.isHidden = false }
            //---|   Avatar
            cell.avatar.assign(customerItem["avatar"])
            return cell.deSelectRow()
        }
        return tableView.cellForRow(at: indexPath)!
    }
    // Cell Selected Of Menu Side
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        let customerItem = self.customers[self.customers.key(indexPath.row)]!
        self.customer = customerItem
        self.editing()
    }
    // Active Editing
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return (String(self.customers[self.customers.key(indexPath.row)]!["uid"]) == app.uid ? true : false)
    }
    // Deleting
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.customers.removeValue(forKey: self.customers.key(indexPath.row))
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.self.v_customer.tv_list.reloadData()
        }
    }
}
