//
//  btnProductList.swift
//  havapro
//
//  Created by Exo on 18/09/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
// Btn
class btnProductList: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 4
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor("#d28cb4").cgColor
        self.backgroundColor = UIColor("#ffb0dd")
    }

}
