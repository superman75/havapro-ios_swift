//
//  cellProductList.swift
//  havapro
//
//  Created by Exo on 18/09/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit

class cellProductList: UICollectionViewCell {
    // Init Vars
    @IBOutlet var product: UIImageView!
    @IBOutlet var content: UILabel!
    @IBOutlet var checked: UIImageView!
    @IBOutlet var price: UILabel!
    @IBOutlet var btnComponent: btnProductList!
    @IBOutlet var components: UITableView!
    @IBOutlet var modal: modal!
    @IBOutlet var online: UIButton!

}
