//
//  cellComponent.swift
//  havapro
//
//  Created by Exo on 14/12/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit

class cellComponent: UITableViewCell {
    // Init Vars
    @IBOutlet var component: UILabel!
    //@IBOutlet var price: UILabel!
    @IBOutlet var checked: UIImageView!
    @IBOutlet weak var online: PButton!
    @IBOutlet weak var raw: UIView!
    //---|
    func ini() -> UITableViewCell{
        self.raw.backgroundColor = UIColor("#000000", alpha : 0.4)
        return self.deSelectRow()
    }
    func selected(){
        self.raw.backgroundColor = UIColor.black
    }
}
