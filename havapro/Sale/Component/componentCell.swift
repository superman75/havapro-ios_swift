//
//  componentCell.swift
//  havapro
//
//  Created by Exo on 14/12/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit

class componentCell: UITableViewCell {
    // Init Vars
    @IBOutlet var name: UILabel!
    @IBOutlet var qty: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var plus: UIButton!
    @IBOutlet var minus: UIButton!
    //@IBOutlet var price: UILabel!
    @IBOutlet var checked: UIImageView!
    @IBOutlet weak var online: PButton!
    func canEdit(_ canEdit:Bool)->Bool{
        return canEdit
    }
}
