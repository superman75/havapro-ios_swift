//
//  CommandeController.swift
//  HavaPRO
//
//  Created by abdelghani on 02/01/2018.
//  Copyright © 2018 Havatar. All rights reserved.
//

import UIKit

class CommandeController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //---|   Vars
    var saleCtrl:SaleController?
    // Outlet
    @IBOutlet weak var tv_commande: UITableView!
    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tv_commande.binding(self, false)
    }
    func sale_ctrl() -> SaleController{ return (saleCtrl ?? SaleController()) }
    //---|   CloseMenu
    @IBAction func menuClose(_ sender: AnyObject) { self.saleCtrl?.appController?.menu_toggle(true) }
    // Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Remove Space in Header And Footer
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        // Menu Side Count Row
        if self.saleCtrl != nil, self.saleCtrl?.commandes != nil { return  (self.saleCtrl?.commandes.count)! }
        return 0
    }
    // Cells Of Menu Side
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // commandeTView
        let cell = tableView.dequeueReusableCell(withIdentifier: "commandeTView", for: indexPath) as! cellCommande
        cell.ini()
        if let commande = self.saleCtrl?.commandes.dic(indexPath.row) as? [String : Any], commande["id"] != nil {
            //---|
            cell.define(commande, String(indexPath.row+1))
            //---|   Selected
            if !String(self.saleCtrl?.commandeCurrent).empty(), String(self.saleCtrl?.commandeCurrent) == String(commande["id"]){
                cell.selected(commande)
            }
        }else{
            self.alert("Erreur ", "self.saleCtrl?.commandes.dic(indexPath.row) \n \(indexPath.row)")
            print("----\n---\n--\nDosen't have ID : self.saleCtrl?.commandes[indexPath.row]\n-----");
            print("-----------| self.saleCtrl?.commandes.dic(indexPath.row) |----------\n \(indexPath.row) --> ");debugPrint(self.saleCtrl?.commandes.dic(indexPath.row));print(" |\n\n≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈\n")
        }
        return cell
    }

    // Cell Selected Of Menu Side
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.saleCtrl?.commandeSelected(indexPath.row)
    }
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.reloadData()
    }
    // Active Editing
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    // Deleting
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    }
}
