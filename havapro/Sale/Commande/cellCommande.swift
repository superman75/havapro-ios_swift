//
//  cellCommande.swift
//  havapro
//
//  Created by Exo on 13/09/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit

class cellCommande: UITableViewCell {

    @IBOutlet var total: UILabel!

    @IBOutlet var v_index: UIView!
    @IBOutlet var l_index: UILabel!

    @IBOutlet var type: UILabel!
    @IBOutlet var paid: UILabel!
    @IBOutlet weak var row: UIView!
    let indexTop:Int = 36
    //Load
    //
    required init(coder decoder: NSCoder) {
        super.init(coder: decoder)!
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //---|   Index
        self.v_index.backgroundColor = UIColor.clear
        self.v_index.layer.borderWidth = 2
        self.v_index.layer.cornerRadius = 4

    }
    //---|   Selected
    func selected(_ commande:[String:Any]){
        self.define(commande)
        // index_style(UIColor.white)
        self.color(UIColor.white, Color.active)
        self.l_index.textColor = UIColor.white
    }
    //---|
    func define(_ commande:[String:Any], _ index:String="…"){
        self.ini(commande)
        self.total.text = "\(String(commande["total"], "0").format()) €"
        self.l_index.text = String(commande["index"], index)
        self.type.text = app.ordertype[String(commande["type"], "on_shop")] ?? app.ordertype["on_shop"]
    }
    //---|   Init
    func ini(_ commande:[String:Any]?=nil) -> UITableViewCell {
        self.color(Color.active, Color.clean)
        if let commande = commande as? [String:Any]{
            // is PAID
            let paid = String(commande["paid"]).bool
            //---|
            if paid {
                self.index_style(Color.green)
            } //---|   VALIDATE
            else if String(commande["validate"]).bool{
                self.index_style(Color.active)
            }else{
                self.index_style()
            }
            self.paid.isHidden = !paid
        }else{
            self.index_style()
        }
        return self.deSelectRow()
    }
    //---|   Color
    func color(_ label_color: UIColor, _ row_color:UIColor){
        self.row.backgroundColor = row_color
        for label in [self.total, self.type, self.paid] as! [UILabel]{
            label.textColor = label_color
        }

    }
    func index_style(_ color:UIColor=Color.red){
        self.v_index.layer.borderColor = color.cgColor
        self.l_index.textColor = color
    }
}
