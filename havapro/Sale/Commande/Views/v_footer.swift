//
//  v_footer.swift
//  havapro
//
//  Created by abdelghani on 07/01/2018.
//  Copyright © 2018 Exo. All rights reserved.
//

import UIKit

class v_footer: UIView {
    @IBOutlet weak var pay: UIButton!
    @IBOutlet weak var validate: UIButton!
    @IBOutlet weak var delete: UIButton!
    //---|
    func clean(){
        self.init_btn(self.pay, "Payer")
        self.init_btn(self.validate, "Valider")
        self.init_btn(self.delete, "Supprimer")
    }
    //---|   Init
    func init_btn(_ btn:UIButton, _ name:String){
        btn.text = name
        btn.isEnabled = false
        btn.alpha = 0.5
        btn.tag = 0
    }
    //---|   Enable
    func enable(_ btn:UIButton, _ tag:Int?=nil, _ text:String?=nil) -> UIButton{
        btn.isEnabled = true
        btn.alpha = 1
        btn.tag = tag ?? btn.tag
        btn.text = text ?? btn.text
        return btn
    }
}
