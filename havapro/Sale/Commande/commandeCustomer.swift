import UIKit

class commandeCustomer: UIView {
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var first_name: UITextField!
    @IBOutlet weak var last_name: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var open_popup: UIButton!
    //---|   Clear
    func clear(){
        self.mobile.text = ""
        self.first_name.text = ""
        self.last_name.text = ""
        self.address.text = ""
    }
}
