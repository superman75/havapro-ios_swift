import UIKit

class commandeType: UIView {
    @IBOutlet weak var popover: UIView!
    @IBOutlet weak var popover_width: NSLayoutConstraint!
    @IBOutlet weak var popover_height: NSLayoutConstraint!
    @IBOutlet weak var popover_middle: NSLayoutConstraint!
    @IBOutlet weak var v_btns: UIView!
    @IBOutlet weak var v_form: UIView!
    @IBOutlet weak var v_form_take_away: UIView!
    @IBOutlet weak var v_form_take_deliver: UIView!
    var keyboard=( close : false, height : 0.0 as CGFloat )
    func toggle(){
        UIView.animate(withDuration: 1.0, animations: {
            self.v_form.isHidden = true
            self.v_btns.isHidden = false
            self.isHidden = !self.isHidden
            self.popover_width.constant = 300
            self.popover_height.constant = 200

        })
    }
    func fromShow( _ delivre:Bool=false){
        self.v_form_take_away.isHidden = true
        self.v_form_take_deliver.isHidden = true
        UIView.animate(withDuration: 0.2, animations: {
            self.isHidden = false
            self.v_btns.isHidden = !self.v_btns.isHidden
            self.v_form.isHidden = !self.v_form.isHidden
            self.popover_width.constant = 500
            self.popover_height.constant = (delivre ? 330 : 180)
            self.v_form_take_away.isHidden = delivre
            self.v_form_take_deliver.isHidden = !delivre
        })
    }
    func keyboard_toogle(_ closing:Bool?=nil, _ height:CGFloat?=nil){
        if !self.isHidden {
            if closing != nil { keyboard.close = closing! }
            if height != nil { keyboard.height = height!     }
            if keyboard.close {
                popover_middle.constant = 0
            }else{
                let bHeight = (UIScreen.main.bounds.size.height/2) - (self.popover_height.constant/2)
                var nHeight = bHeight - keyboard.height
                if (nHeight > bHeight) || (nHeight < 0) { nHeight = bHeight }
                popover_middle.constant = -nHeight
            }
        }
    }
    @IBAction func close(_ sender: AnyObject) {
        print("Clicked-----------Close")
        self.toggle()
    }
}
