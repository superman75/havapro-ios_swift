//
//  cellCategories.swift
//  havapro
//
//  Created by abdelghani on 02/09/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class cellCategories: UICollectionViewCell {
    // Init Vars
    @IBOutlet var checked: UIImageView!
    @IBOutlet var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var online: UIButton!
}
