//
//  cellProductSlider.swift
//  havapro
//
//  Created by Exo on 02/10/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit

class cellCategoryProduct: UICollectionViewCell {
    // Init Vars
    @IBOutlet var product: UIImageView!
    @IBOutlet var content: UILabel!
    @IBOutlet weak var checked: UIImageView!
    @IBOutlet weak var online: UIButton!
    @IBOutlet weak var edit: UIButton!
}
