//
//  cellOrderCV.swift
//  havapro
//
//  Created by Exo on 05/02/2017.
//  Copyright © 2017 Exo. All rights reserved.
//

import UIKit

class cellOrderCV: UICollectionViewCell {
    // Init Vars
    @IBOutlet weak var type: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var total: UILabel!
    //
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = UIColor("#f7f7f7")
    }
}
