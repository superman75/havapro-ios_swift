//
//  btnProductConsole.swift
//  havapro
//
//  Created by Exo on 17/09/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit
// Btn
class btnProductConsole: UIButton {
    public func style(type: String?=nil) {
        switch(String(type)) {
            case "red":
                self.layer.borderColor = UIColor("#e74c3c").cgColor
                self.setTitleColor(UIColor("#e74c3c"), for: UIControlState.normal)
                self.layoutIfNeeded()
                break;

            default:
                self.layer.borderColor = UIColor("#ffb0dd").cgColor
                self.setTitleColor(UIColor("#ffb0dd"), for: UIControlState.normal)
                self.layoutIfNeeded()
                break;
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderWidth = 1
        self.style()
    }
}
