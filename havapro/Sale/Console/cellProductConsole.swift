
//
//  cellProductConsole.swift
//  havapro
//
//  Created by Exo on 17/09/2016.
//  Copyright © 2016 Exo. All rights reserved.
//

import UIKit

class cellProductConsole: UITableViewCell {
    // Init Vars
    @IBOutlet var title: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet weak var qty: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var componentBtn: Btn!

    func canEdit(_ canEdit:Bool)->Bool{
        return canEdit
    }

}
